﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEnemyActiveOffU3Ed__16_t4AC26D9F732F008DD8073C8CE52FE9581ED94A82_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEnemyActiveOffU3Ed__17_t090E1E848E32CB9838DEE9486F4AD27607A95648_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEnemyActiveOffU3Ed__17_t1104DECC029E3AAF5304A9D29AF5A680FF9AE010_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEnemyActiveOffU3Ed__17_t43D619270E85C4030AD2A6F1C1D98AC33720EB34_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEnemyActiveOffU3Ed__17_t7AB5E724E27EC4168E7270B7CDBFD943849D21CB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEnemyActiveOffU3Ed__17_t95404BB5A4134F5B62C67289E8DD94C5C9E69FD3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEnemyActiveOffU3Ed__17_t989DFCAF68BD051B69CBA1C02D74A7C43368C8D7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CKnockCoU3Ed__3_t2484562B1B4BC1C411DE1DC6A9A737CAAB9754F3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadAsynchronouslyU3Ed__10_t96EF2F6BC38FD48D0853B90F403D5486EFFB3743_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadAsynchronouslyU3Ed__18_tCBD107D92FB476B8A2BFB64E705C198FE6332D8A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPlayerActiveOffU3Ed__17_tB4A573BE9F9DDBD451F4A9F4E97C1AF3413958AA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CReloadU3Ed__38_t4DF7DFFCF5D53852FEB6334B1D83CDCB9136AA1E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSavingTimeU3Ed__15_tB688C282B8B0A9C6363A6F10C310BAF1BC084883_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTextScrollU3Ed__14_t08B7AB540E24FF97A9F2DC6C8EBF5D09F1CD2832_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTextScrollU3Ed__14_t218CCB237B5D22D65D963056F8D59F9A65CC157D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTextScrollU3Ed__14_t45ECEB7344B984B488ED5003C4C7C4E07F13650A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTextScrollU3Ed__14_t5F82A72CC11220BD411C30F21EC1CEF4CA9EAA96_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTextScrollU3Ed__14_tA8FFAA4799C58B02C4E24F5B9ED15F20312E32C3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTextScrollU3Ed__14_tAD044D8A38DC6A68C21C650C35EA1B97827C6F51_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTextScrollU3Ed__14_tB27BDF2A99BD3A5DFCFAEE9F4F70FE5B4BAF1F07_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTextScrollU3Ed__14_tD25DEB4F943540119F24078DA82B738A977553A6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTextScrollU3Ed__14_tE6751550689136A9E9A020C0C5749385DCE0E76B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTextScrollU3Ed__15_t58A1A5158A72226876545889B4F26A608D8006A4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTextScrollU3Ed__15_t9E52613D815AE881174B02ED341EE752693BE4DD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTimeBuffU3Ed__45_t0BE322FFFF97614A730EBA98616AD9AD1615617D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTimeIEU3Ed__10_t28D64D34D87FF08928C4AC197A7A8B5840E2D870_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTimeIEU3Ed__10_t79F8D5D08CA76FE8A3892AC9C2EE6A4313A3CEFF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTimeIEU3Ed__10_tA7DB19C142169A8DCDE9BF17DE95304A5093961C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTimeIEU3Ed__10_tD09479C2D9173EFB8015F2C800021947CB904643_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTimeIEU3Ed__12_t58B0454AD5F54B56627D9C4271098ECBA023F776_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTimeIEU3Ed__8_t2248B562C3607F1D7051857BA7F8109E6F353FA0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTypeLineU3Ed__13_t0195F7FADE21AC251086C7AFE1386AD855C19A95_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTypeLineU3Ed__13_t23C6FD7AAE846FE48337853DE0EF332B5E9F53D0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTypeLineU3Ed__13_t24E48C8240490AAE9B028BBA260B00934F9F101A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTypeLineU3Ed__13_t5F92CF8B8EBB2BD80BDBA9A1A59408257269824B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTypeLineU3Ed__15_t5C0B45D84D61265E157C216B6535D3DE51337A23_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitforAttackCDU3Ed__40_tF43E5B4B83DEE1A35B02B69DEBA359A94D551D49_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitforBurningManyTimesU3Ed__24_t95FAAD6C2B8E8CDC9951F60AC1E8325FB3F04ABE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitforBurningTwiceTimesU3Ed__23_t6BEEC8618296DA43B378F2842EA8448939DB4F74_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitforControlU3Ed__42_t4BC1189F93B40CC735F2DF62FD76971BF7CAC346_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitforDashCDU3Ed__39_t14E13865EE683795695090D3455320127C937F80_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitforSlowU3Ed__41_t601851BD5C43BACFE807E44C1FB1DA9CBC028E6F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void IntroManager_t0830037B4178478802A163C0B16656A3D4EE7B54_CustomAttributesCacheGenerator_IntroManager_TimeIE_m8671693B4F354CA40F239A02EB160E5EE906CB40(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTimeIEU3Ed__10_tA7DB19C142169A8DCDE9BF17DE95304A5093961C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTimeIEU3Ed__10_tA7DB19C142169A8DCDE9BF17DE95304A5093961C_0_0_0_var), NULL);
	}
}
static void IntroManager_t0830037B4178478802A163C0B16656A3D4EE7B54_CustomAttributesCacheGenerator_IntroManager_TypeLine_m2D1AB9015F67B6094D56BCD80BB4198A1615C4A6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTypeLineU3Ed__13_t24E48C8240490AAE9B028BBA260B00934F9F101A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTypeLineU3Ed__13_t24E48C8240490AAE9B028BBA260B00934F9F101A_0_0_0_var), NULL);
	}
}
static void U3CTimeIEU3Ed__10_tA7DB19C142169A8DCDE9BF17DE95304A5093961C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_tA7DB19C142169A8DCDE9BF17DE95304A5093961C_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10__ctor_m11C8A6394695414B03FF139147D221BE9CC7F656(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_tA7DB19C142169A8DCDE9BF17DE95304A5093961C_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_IDisposable_Dispose_mC05D66A28083727BFEE3EF10506098A204A03409(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_tA7DB19C142169A8DCDE9BF17DE95304A5093961C_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m25917A42988161EEBA1271CF3B23AEB337543CA9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_tA7DB19C142169A8DCDE9BF17DE95304A5093961C_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_IEnumerator_Reset_m65DAD9114DAB93BE8D5BB1C625F1FB2CF0F0D472(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_tA7DB19C142169A8DCDE9BF17DE95304A5093961C_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_IEnumerator_get_Current_m2A25C3B0223067B36A377B4C3FDAE686F28D539E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t24E48C8240490AAE9B028BBA260B00934F9F101A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t24E48C8240490AAE9B028BBA260B00934F9F101A_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13__ctor_mB4362558C8CDE1FCF2DB18EE13D09F9A77800DA3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t24E48C8240490AAE9B028BBA260B00934F9F101A_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_IDisposable_Dispose_mBF2CA07B9929F52BB9D6757B4ABBEBB156D51C92(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t24E48C8240490AAE9B028BBA260B00934F9F101A_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6016236D9EEC958D1B15C210D6D80897805FEE75(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t24E48C8240490AAE9B028BBA260B00934F9F101A_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_IEnumerator_Reset_m20BF6AA37542A6DE14CE8606807FBA0E4EFD2815(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t24E48C8240490AAE9B028BBA260B00934F9F101A_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_IEnumerator_get_Current_m623B9FE74115FEF5D16925CEE1E5397C12B38BCB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Story1Manager_t5159C7F9EABC50A48ADF896CA08F3FE3B110DEFE_CustomAttributesCacheGenerator_Story1Manager_TimeIE_m9F889DA26E3CF321B6DDD407B42D4EEED1CAEDCD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTimeIEU3Ed__10_t79F8D5D08CA76FE8A3892AC9C2EE6A4313A3CEFF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTimeIEU3Ed__10_t79F8D5D08CA76FE8A3892AC9C2EE6A4313A3CEFF_0_0_0_var), NULL);
	}
}
static void Story1Manager_t5159C7F9EABC50A48ADF896CA08F3FE3B110DEFE_CustomAttributesCacheGenerator_Story1Manager_TypeLine_m597776B3A41CAA93A0671A6DD7F50DEA37F5E4FC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTypeLineU3Ed__13_t0195F7FADE21AC251086C7AFE1386AD855C19A95_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTypeLineU3Ed__13_t0195F7FADE21AC251086C7AFE1386AD855C19A95_0_0_0_var), NULL);
	}
}
static void U3CTimeIEU3Ed__10_t79F8D5D08CA76FE8A3892AC9C2EE6A4313A3CEFF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_t79F8D5D08CA76FE8A3892AC9C2EE6A4313A3CEFF_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10__ctor_m2458A1336253AF66749EA49CDCF74B1810C923B6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_t79F8D5D08CA76FE8A3892AC9C2EE6A4313A3CEFF_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_IDisposable_Dispose_m126FFCFD9B1CD0A1C81AD3798874A380A891518F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_t79F8D5D08CA76FE8A3892AC9C2EE6A4313A3CEFF_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA6B0872963BDC898023AD817C434FE51C7854FF9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_t79F8D5D08CA76FE8A3892AC9C2EE6A4313A3CEFF_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_IEnumerator_Reset_mA408B839522C0E46A97723C45EF4DCF246215799(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_t79F8D5D08CA76FE8A3892AC9C2EE6A4313A3CEFF_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_IEnumerator_get_Current_mB2BCCF780BDA64276290CB3080C262B0AF33773A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t0195F7FADE21AC251086C7AFE1386AD855C19A95_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t0195F7FADE21AC251086C7AFE1386AD855C19A95_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13__ctor_mB8227D04CFD9803A9E3D112D82A1C2CA0320C3AF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t0195F7FADE21AC251086C7AFE1386AD855C19A95_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_IDisposable_Dispose_m5DE8B5998D1316BFCFE3D69C8D65AA90C8A4A614(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t0195F7FADE21AC251086C7AFE1386AD855C19A95_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4785A5F20FCC7D15936A974801F1ABF86969FBA4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t0195F7FADE21AC251086C7AFE1386AD855C19A95_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_IEnumerator_Reset_m26715E5D4DA17CBB171BE6A64BBB6AFB56E647A4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t0195F7FADE21AC251086C7AFE1386AD855C19A95_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_IEnumerator_get_Current_mFD03E403D694A40D19E16EDC51A696B7E6DB7FFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Story2Manager_t754E10D4B87F080C1F6D63E55620DB9900A7ACCD_CustomAttributesCacheGenerator_Story2Manager_TimeIE_m6A9B40A187E64B7BEBE3F30EF100815FBE55FCC2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTimeIEU3Ed__10_t28D64D34D87FF08928C4AC197A7A8B5840E2D870_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTimeIEU3Ed__10_t28D64D34D87FF08928C4AC197A7A8B5840E2D870_0_0_0_var), NULL);
	}
}
static void Story2Manager_t754E10D4B87F080C1F6D63E55620DB9900A7ACCD_CustomAttributesCacheGenerator_Story2Manager_TypeLine_m9F81497C07EB9F4A08C1A6761B747F414B723215(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTypeLineU3Ed__13_t5F92CF8B8EBB2BD80BDBA9A1A59408257269824B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTypeLineU3Ed__13_t5F92CF8B8EBB2BD80BDBA9A1A59408257269824B_0_0_0_var), NULL);
	}
}
static void U3CTimeIEU3Ed__10_t28D64D34D87FF08928C4AC197A7A8B5840E2D870_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_t28D64D34D87FF08928C4AC197A7A8B5840E2D870_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10__ctor_mC61D5359D18BC71478D2B26D364DA9E5215CA853(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_t28D64D34D87FF08928C4AC197A7A8B5840E2D870_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_IDisposable_Dispose_m28569B7D30B0704ACF7F0C2FD6E9A83606F8AA55(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_t28D64D34D87FF08928C4AC197A7A8B5840E2D870_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0B75C09C6CCDEBC775E1A4FE0151E6BFE4F41DC7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_t28D64D34D87FF08928C4AC197A7A8B5840E2D870_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_IEnumerator_Reset_m289A9D9686AC91F03213A3EF84EB9C62091B7B1C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_t28D64D34D87FF08928C4AC197A7A8B5840E2D870_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_IEnumerator_get_Current_m938DBF30F82FDDE5FA86D026FC89B0FAE95EB447(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t5F92CF8B8EBB2BD80BDBA9A1A59408257269824B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t5F92CF8B8EBB2BD80BDBA9A1A59408257269824B_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13__ctor_m3381142803F5490CA2C6EA44676B001A924C87B0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t5F92CF8B8EBB2BD80BDBA9A1A59408257269824B_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_IDisposable_Dispose_mA2E73E049CF5E008F407436C5885C01CD1BE1CFD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t5F92CF8B8EBB2BD80BDBA9A1A59408257269824B_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB3E18BB92F474F438C262387EF09784344930706(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t5F92CF8B8EBB2BD80BDBA9A1A59408257269824B_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_IEnumerator_Reset_m18F6D13CF39437E0C7D6FCDFD2A9F7431F5096FE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t5F92CF8B8EBB2BD80BDBA9A1A59408257269824B_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_IEnumerator_get_Current_m32405CE40FDE12E1870029E0EC5B92E58F8582E4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Story3Manager_t907700194AF7612DCD53CC0D381D7DA8354B3780_CustomAttributesCacheGenerator_Story3Manager_TimeIE_m3BFB48C73E1A83E8E21720324DE7CC270D8DE122(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTimeIEU3Ed__10_tD09479C2D9173EFB8015F2C800021947CB904643_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTimeIEU3Ed__10_tD09479C2D9173EFB8015F2C800021947CB904643_0_0_0_var), NULL);
	}
}
static void Story3Manager_t907700194AF7612DCD53CC0D381D7DA8354B3780_CustomAttributesCacheGenerator_Story3Manager_TypeLine_m5EC34C6FF1711EC998F1D8D6EE88EAFC52A61675(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTypeLineU3Ed__13_t23C6FD7AAE846FE48337853DE0EF332B5E9F53D0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTypeLineU3Ed__13_t23C6FD7AAE846FE48337853DE0EF332B5E9F53D0_0_0_0_var), NULL);
	}
}
static void U3CTimeIEU3Ed__10_tD09479C2D9173EFB8015F2C800021947CB904643_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_tD09479C2D9173EFB8015F2C800021947CB904643_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10__ctor_m13DC5856052DFE59F5E14120CEA19C0193D0BEAA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_tD09479C2D9173EFB8015F2C800021947CB904643_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_IDisposable_Dispose_mBFA4A57358A033314A1AF41AAB8F2AF92F24C646(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_tD09479C2D9173EFB8015F2C800021947CB904643_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52B342497384166C38981CA667740BD580076450(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_tD09479C2D9173EFB8015F2C800021947CB904643_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_IEnumerator_Reset_mBC68911DBF658B3477EA295D7BF2FFD8B0FC484B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__10_tD09479C2D9173EFB8015F2C800021947CB904643_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_IEnumerator_get_Current_m75BB82C5CAE4C881AC8B70EECF6E4989EC8470DC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t23C6FD7AAE846FE48337853DE0EF332B5E9F53D0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t23C6FD7AAE846FE48337853DE0EF332B5E9F53D0_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13__ctor_m8268E1A248C3515BFE52D666819313D5E77E1197(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t23C6FD7AAE846FE48337853DE0EF332B5E9F53D0_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_IDisposable_Dispose_m34607E4719E82B3C90FCAA602BA9158A8AEA2B5B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t23C6FD7AAE846FE48337853DE0EF332B5E9F53D0_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2A5EA8BF16919F3AEC394B997A12533E9AE0DAC0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t23C6FD7AAE846FE48337853DE0EF332B5E9F53D0_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_IEnumerator_Reset_m18E7B4304AC09386ECD997307FA234B521E21439(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__13_t23C6FD7AAE846FE48337853DE0EF332B5E9F53D0_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_IEnumerator_get_Current_mD86346C7738D5B2E93D38B4CEFFA74C98305D845(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Story4Manager_t88DB1EB3693D1302B55DCBD3A8DF19F6F713C5C4_CustomAttributesCacheGenerator_Story4Manager_TimeIE_mF20ABF95DD1A9D0D8BDBBA7D7C3E0529133F2EE2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTimeIEU3Ed__12_t58B0454AD5F54B56627D9C4271098ECBA023F776_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTimeIEU3Ed__12_t58B0454AD5F54B56627D9C4271098ECBA023F776_0_0_0_var), NULL);
	}
}
static void Story4Manager_t88DB1EB3693D1302B55DCBD3A8DF19F6F713C5C4_CustomAttributesCacheGenerator_Story4Manager_TypeLine_mB11C4356720EACBB75ED9E29CA3370751FA43C33(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTypeLineU3Ed__15_t5C0B45D84D61265E157C216B6535D3DE51337A23_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTypeLineU3Ed__15_t5C0B45D84D61265E157C216B6535D3DE51337A23_0_0_0_var), NULL);
	}
}
static void Story4Manager_t88DB1EB3693D1302B55DCBD3A8DF19F6F713C5C4_CustomAttributesCacheGenerator_Story4Manager_LoadAsynchronously_mD438C42C9179A37EB643566B09675E3FA842CFE6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadAsynchronouslyU3Ed__18_tCBD107D92FB476B8A2BFB64E705C198FE6332D8A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadAsynchronouslyU3Ed__18_tCBD107D92FB476B8A2BFB64E705C198FE6332D8A_0_0_0_var), NULL);
	}
}
static void U3CTimeIEU3Ed__12_t58B0454AD5F54B56627D9C4271098ECBA023F776_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__12_t58B0454AD5F54B56627D9C4271098ECBA023F776_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__12__ctor_mBB9F8F85D5DAE9BD94AE09041320E26BA12DADA0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__12_t58B0454AD5F54B56627D9C4271098ECBA023F776_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__12_System_IDisposable_Dispose_m4B0734E90CB756B4B8E4B4D73A4C53D967759F1D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__12_t58B0454AD5F54B56627D9C4271098ECBA023F776_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C73D565CC03D11BC1C842373611D2810BB5B3A4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__12_t58B0454AD5F54B56627D9C4271098ECBA023F776_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__12_System_Collections_IEnumerator_Reset_m7EFD622AE07588EFBC6530F070A4DA48F45B5B6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__12_t58B0454AD5F54B56627D9C4271098ECBA023F776_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__12_System_Collections_IEnumerator_get_Current_m79A22DBAF3530D9FF60B2AA9B5A268FACD7E0C1B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__15_t5C0B45D84D61265E157C216B6535D3DE51337A23_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__15_t5C0B45D84D61265E157C216B6535D3DE51337A23_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__15__ctor_m5E924108C2849A14B99387420583E4FF4191ACDC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__15_t5C0B45D84D61265E157C216B6535D3DE51337A23_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__15_System_IDisposable_Dispose_m1751D8F1A560C0BCD88C8384796F0FE92D9EC701(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__15_t5C0B45D84D61265E157C216B6535D3DE51337A23_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m76FC58D00BCEA3EE34B60EEA166E53E8D68D2ACD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__15_t5C0B45D84D61265E157C216B6535D3DE51337A23_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__15_System_Collections_IEnumerator_Reset_mBDC2E72B1994109877CBA9AF3F9F2906A14AF27C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeLineU3Ed__15_t5C0B45D84D61265E157C216B6535D3DE51337A23_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__15_System_Collections_IEnumerator_get_Current_m9670A64D4B8B6142E389CD03268C21FF7123EF88(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAsynchronouslyU3Ed__18_tCBD107D92FB476B8A2BFB64E705C198FE6332D8A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadAsynchronouslyU3Ed__18_tCBD107D92FB476B8A2BFB64E705C198FE6332D8A_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__18__ctor_m9193454C25CF53C50BE2952AD275D59D148125B8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAsynchronouslyU3Ed__18_tCBD107D92FB476B8A2BFB64E705C198FE6332D8A_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__18_System_IDisposable_Dispose_m81EAE1BAA920EEAAB99C7292C8063FE9A18003D6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAsynchronouslyU3Ed__18_tCBD107D92FB476B8A2BFB64E705C198FE6332D8A_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1CD8EE6EC9E99257A29EBE8EE8ACDE0A21FBE909(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAsynchronouslyU3Ed__18_tCBD107D92FB476B8A2BFB64E705C198FE6332D8A_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__18_System_Collections_IEnumerator_Reset_m533E97DA9C85B050583E198153B03F0C6442557B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAsynchronouslyU3Ed__18_tCBD107D92FB476B8A2BFB64E705C198FE6332D8A_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__18_System_Collections_IEnumerator_get_Current_m13A26DE6C2E4018242E9DC5FB764FC8685006B48(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DialogueManager_t609C696D1E27CEA7A0895EDADECF76865D33931D_CustomAttributesCacheGenerator_DialogueManager_TextScroll_m5D3194B542493389CE2040A3ABF204E5CDA2A029(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTextScrollU3Ed__14_t08B7AB540E24FF97A9F2DC6C8EBF5D09F1CD2832_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTextScrollU3Ed__14_t08B7AB540E24FF97A9F2DC6C8EBF5D09F1CD2832_0_0_0_var), NULL);
	}
}
static void U3CTextScrollU3Ed__14_t08B7AB540E24FF97A9F2DC6C8EBF5D09F1CD2832_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t08B7AB540E24FF97A9F2DC6C8EBF5D09F1CD2832_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14__ctor_m0FF343CAE7D09A873B318ACAD7760727CBBBBD44(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t08B7AB540E24FF97A9F2DC6C8EBF5D09F1CD2832_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m8DD31CCCB316C58800E444EE15133E4E8BC15ECD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t08B7AB540E24FF97A9F2DC6C8EBF5D09F1CD2832_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4FDF89D7CC6D5A16A2361C1113345C725CBCC522(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t08B7AB540E24FF97A9F2DC6C8EBF5D09F1CD2832_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m11694E97359876379F5AB7702B0E37F2A0E8C0F9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t08B7AB540E24FF97A9F2DC6C8EBF5D09F1CD2832_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mABE1658974027AC5CF135A1980F2E95028C2C7C4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DialogueManager111_tF83F90FA20C3590DAF69C6E5A1E3D5D38A2FE52B_CustomAttributesCacheGenerator_DialogueManager111_TextScroll_mBF4DF710DB2696EFEB78F5E9EB3B19A45EC6CD95(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTextScrollU3Ed__15_t58A1A5158A72226876545889B4F26A608D8006A4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTextScrollU3Ed__15_t58A1A5158A72226876545889B4F26A608D8006A4_0_0_0_var), NULL);
	}
}
static void U3CTextScrollU3Ed__15_t58A1A5158A72226876545889B4F26A608D8006A4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__15_t58A1A5158A72226876545889B4F26A608D8006A4_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15__ctor_mFB72C4F6C3A9277F985C226F0C5AE5C882610675(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__15_t58A1A5158A72226876545889B4F26A608D8006A4_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15_System_IDisposable_Dispose_m8682A2B86D921428757604CC5147E8BFB5DE8D80(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__15_t58A1A5158A72226876545889B4F26A608D8006A4_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF07A8CC8414E93E791847D1301E2CDF2629D324(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__15_t58A1A5158A72226876545889B4F26A608D8006A4_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15_System_Collections_IEnumerator_Reset_m18CBF4829F14DFF81C74F4E30461372D237C879F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__15_t58A1A5158A72226876545889B4F26A608D8006A4_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15_System_Collections_IEnumerator_get_Current_m2E43D012A1C14989FE33597AD52AA78013DCF877(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DialogueManager10_t3412F239D84E470F38ECCEF84946B8D5A550987B_CustomAttributesCacheGenerator_DialogueManager10_TextScroll_mEFC8587C6A8F527F06560AA0FF26CE9351CBA49C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTextScrollU3Ed__14_t5F82A72CC11220BD411C30F21EC1CEF4CA9EAA96_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTextScrollU3Ed__14_t5F82A72CC11220BD411C30F21EC1CEF4CA9EAA96_0_0_0_var), NULL);
	}
}
static void U3CTextScrollU3Ed__14_t5F82A72CC11220BD411C30F21EC1CEF4CA9EAA96_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t5F82A72CC11220BD411C30F21EC1CEF4CA9EAA96_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14__ctor_m25372485B50A52BDD8128CC237EE2E22C46EF534(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t5F82A72CC11220BD411C30F21EC1CEF4CA9EAA96_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_IDisposable_Dispose_mBD7F7A2F7E8A76C7B522049B83C0F99302195930(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t5F82A72CC11220BD411C30F21EC1CEF4CA9EAA96_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE424541DA8B50682073EF643295B76FA41785D78(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t5F82A72CC11220BD411C30F21EC1CEF4CA9EAA96_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m30934F0B954200D61D8A62774DF6D0D2885265F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t5F82A72CC11220BD411C30F21EC1CEF4CA9EAA96_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_m09ED21E076FD07A2D85934D5EDF75577CDA2EC18(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DialogueManager1_tDEC1CAE4C52F9AC218021B0C3F31C930A3F6C189_CustomAttributesCacheGenerator_DialogueManager1_TextScroll_m449B179ECE4CEC9C9F207D5AFAE30142BA1719C8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTextScrollU3Ed__14_tE6751550689136A9E9A020C0C5749385DCE0E76B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTextScrollU3Ed__14_tE6751550689136A9E9A020C0C5749385DCE0E76B_0_0_0_var), NULL);
	}
}
static void U3CTextScrollU3Ed__14_tE6751550689136A9E9A020C0C5749385DCE0E76B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tE6751550689136A9E9A020C0C5749385DCE0E76B_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14__ctor_m46153879164D1408755D717F0CFF3804B9D0DA21(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tE6751550689136A9E9A020C0C5749385DCE0E76B_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_IDisposable_Dispose_mD54DB86896E9D8FDCF8BC296B9FEF71554B67D6C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tE6751550689136A9E9A020C0C5749385DCE0E76B_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB172AE4343A8D82D557AF6D79E3A12EB643BB220(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tE6751550689136A9E9A020C0C5749385DCE0E76B_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m8C8F933102AF5469324D53568C59C3211A0721BD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tE6751550689136A9E9A020C0C5749385DCE0E76B_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_m08ECAF3014121CD6964575229A159602CFAD3E58(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DialogueManager3_t4AC1E2D7E7E9F4698B40D55081677F1E8F022244_CustomAttributesCacheGenerator_DialogueManager3_TextScroll_m1B5D37D563F9AAE195726DB2DE62F7877BEE2C4A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTextScrollU3Ed__15_t9E52613D815AE881174B02ED341EE752693BE4DD_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTextScrollU3Ed__15_t9E52613D815AE881174B02ED341EE752693BE4DD_0_0_0_var), NULL);
	}
}
static void U3CTextScrollU3Ed__15_t9E52613D815AE881174B02ED341EE752693BE4DD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__15_t9E52613D815AE881174B02ED341EE752693BE4DD_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15__ctor_m4D514A28DAFB725FCCC3F86305EF9047E11B835F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__15_t9E52613D815AE881174B02ED341EE752693BE4DD_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15_System_IDisposable_Dispose_m5B3A2BA0FDE76B3D910F9F543622E3B7E0104CA5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__15_t9E52613D815AE881174B02ED341EE752693BE4DD_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44B1FEC4291372B6B51003ABC8F76DF83193D7B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__15_t9E52613D815AE881174B02ED341EE752693BE4DD_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15_System_Collections_IEnumerator_Reset_m98B30F5DEA0D66BE62F371A7A6C3DCB9B95FF6C4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__15_t9E52613D815AE881174B02ED341EE752693BE4DD_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15_System_Collections_IEnumerator_get_Current_m0F1D7EA630D47E82DA6928695681B07E7F6F3785(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DialogueManager4_tD71D375A3EE0F27D3B35A57B54FC3D0E89C60B5F_CustomAttributesCacheGenerator_DialogueManager4_TextScroll_m0C54E3C56347658AD1502A8612C40A2804A86F1B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTextScrollU3Ed__14_t218CCB237B5D22D65D963056F8D59F9A65CC157D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTextScrollU3Ed__14_t218CCB237B5D22D65D963056F8D59F9A65CC157D_0_0_0_var), NULL);
	}
}
static void U3CTextScrollU3Ed__14_t218CCB237B5D22D65D963056F8D59F9A65CC157D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t218CCB237B5D22D65D963056F8D59F9A65CC157D_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14__ctor_m287E95932EF65B84A11568F314D1A15E320B6536(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t218CCB237B5D22D65D963056F8D59F9A65CC157D_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m66D5900B028A65713A5BC970753B95A980024A9E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t218CCB237B5D22D65D963056F8D59F9A65CC157D_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m67E6C4920DA41BB4F2897A59F1B82A46C76EDB90(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t218CCB237B5D22D65D963056F8D59F9A65CC157D_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m2BD1E12D338D61FAA0A2A9B8C4778CE45A264B89(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t218CCB237B5D22D65D963056F8D59F9A65CC157D_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_m9349BFBAB9DF9427D539E5EA95DA106B8B7D9158(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DialogueManager5_tEA5E19E2260F2CD379D85CC269891BE7B9E86D16_CustomAttributesCacheGenerator_DialogueManager5_TextScroll_mA868CFAC74505D370CE5C4BA8E8469957AD8F086(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTextScrollU3Ed__14_t45ECEB7344B984B488ED5003C4C7C4E07F13650A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTextScrollU3Ed__14_t45ECEB7344B984B488ED5003C4C7C4E07F13650A_0_0_0_var), NULL);
	}
}
static void U3CTextScrollU3Ed__14_t45ECEB7344B984B488ED5003C4C7C4E07F13650A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t45ECEB7344B984B488ED5003C4C7C4E07F13650A_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14__ctor_m5CD63C4796AA3EFFCDDB1B1695B299FA5F129190(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t45ECEB7344B984B488ED5003C4C7C4E07F13650A_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m1131A72D8DAA306883DC73A644E10DF939E3DDC8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t45ECEB7344B984B488ED5003C4C7C4E07F13650A_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m586FF4D51CD4F659D098B08A3EF8B37230C59BA6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t45ECEB7344B984B488ED5003C4C7C4E07F13650A_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_mB92B68E8BE2177A8B7EA040B8CC0328708947DB2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_t45ECEB7344B984B488ED5003C4C7C4E07F13650A_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_m83A16F0C427A38D04171F21D0FB92541B0319C76(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DialogueManager6_t0AFC57B6176046A403A50D84778DB5ECCE5BD029_CustomAttributesCacheGenerator_DialogueManager6_TextScroll_m5A10285269F39472F209FE26602A0D0D1D2AE92F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTextScrollU3Ed__14_tB27BDF2A99BD3A5DFCFAEE9F4F70FE5B4BAF1F07_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTextScrollU3Ed__14_tB27BDF2A99BD3A5DFCFAEE9F4F70FE5B4BAF1F07_0_0_0_var), NULL);
	}
}
static void U3CTextScrollU3Ed__14_tB27BDF2A99BD3A5DFCFAEE9F4F70FE5B4BAF1F07_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tB27BDF2A99BD3A5DFCFAEE9F4F70FE5B4BAF1F07_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14__ctor_m29BA7873E6EF4DFB87C961BB0BF78BF2976D3128(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tB27BDF2A99BD3A5DFCFAEE9F4F70FE5B4BAF1F07_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m82BB053A1C51D75CF09DDB5F7DD3CB29FC73CE9E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tB27BDF2A99BD3A5DFCFAEE9F4F70FE5B4BAF1F07_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB001D87EE04371590F576A76F96AC0CDA78BAFEE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tB27BDF2A99BD3A5DFCFAEE9F4F70FE5B4BAF1F07_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_mCD2873EF7E990CC57029E50B039B3B93F55CFF86(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tB27BDF2A99BD3A5DFCFAEE9F4F70FE5B4BAF1F07_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mE5FB965E79F06B9E364142856FF3C3CAA11040D7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DialogueManager7_tA43C1EF0EC6E1266C0249B81607A8FC1625C3032_CustomAttributesCacheGenerator_DialogueManager7_TextScroll_mBE32AC7FF53D80990FD20B69E4538EDAAA79D380(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTextScrollU3Ed__14_tAD044D8A38DC6A68C21C650C35EA1B97827C6F51_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTextScrollU3Ed__14_tAD044D8A38DC6A68C21C650C35EA1B97827C6F51_0_0_0_var), NULL);
	}
}
static void U3CTextScrollU3Ed__14_tAD044D8A38DC6A68C21C650C35EA1B97827C6F51_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tAD044D8A38DC6A68C21C650C35EA1B97827C6F51_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14__ctor_mCCF5C5A9B5C4FDC2A5361E59F0E6D5B63497E721(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tAD044D8A38DC6A68C21C650C35EA1B97827C6F51_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m86803CD35DE20355B45C10DFB1BBEFB11FACC611(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tAD044D8A38DC6A68C21C650C35EA1B97827C6F51_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF0D25459F989654F0F9931A907E0BE2D7C8C8957(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tAD044D8A38DC6A68C21C650C35EA1B97827C6F51_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m612A77A93459DCF470D8F30C2D062034F01B8E2E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tAD044D8A38DC6A68C21C650C35EA1B97827C6F51_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mBA75C9E3BB40C9F9CAD7791953A001FAE2B3A531(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DialogueManager8_t497B60BABDD18643C84EFD35DF218070380F3642_CustomAttributesCacheGenerator_DialogueManager8_TextScroll_m530F4F98A324FEE74AC4BCB4499A04AE0A4BA482(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTextScrollU3Ed__14_tA8FFAA4799C58B02C4E24F5B9ED15F20312E32C3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTextScrollU3Ed__14_tA8FFAA4799C58B02C4E24F5B9ED15F20312E32C3_0_0_0_var), NULL);
	}
}
static void U3CTextScrollU3Ed__14_tA8FFAA4799C58B02C4E24F5B9ED15F20312E32C3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tA8FFAA4799C58B02C4E24F5B9ED15F20312E32C3_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14__ctor_mD21839154365B3E4559D29CC0531B9C507BFBBBE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tA8FFAA4799C58B02C4E24F5B9ED15F20312E32C3_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_IDisposable_Dispose_mB081476BC8CD3EB1AB9DD3CE91393410457FA640(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tA8FFAA4799C58B02C4E24F5B9ED15F20312E32C3_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3FEFEE3C8052FA546CF052470E36DCFD51824CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tA8FFAA4799C58B02C4E24F5B9ED15F20312E32C3_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m10F6AFF907AA15D3F12357D6D000715B2FD102B9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tA8FFAA4799C58B02C4E24F5B9ED15F20312E32C3_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mF851251A598615B608406828EC8BB974DB7616B3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DialogueManager9_t267494FE0F8A0076B623B6C66903F8152735B734_CustomAttributesCacheGenerator_DialogueManager9_TextScroll_m6A595793A9FCDE637C136F78B85D75D9455ECF19(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTextScrollU3Ed__14_tD25DEB4F943540119F24078DA82B738A977553A6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTextScrollU3Ed__14_tD25DEB4F943540119F24078DA82B738A977553A6_0_0_0_var), NULL);
	}
}
static void U3CTextScrollU3Ed__14_tD25DEB4F943540119F24078DA82B738A977553A6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tD25DEB4F943540119F24078DA82B738A977553A6_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14__ctor_mF3E5A9448661287179E4EA31143CF930B9359943(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tD25DEB4F943540119F24078DA82B738A977553A6_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m3112D9AA996F620897BE1A44084071E371DCF98F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tD25DEB4F943540119F24078DA82B738A977553A6_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C55D1856B48DA623AC540EA0215DD4D997EC254(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tD25DEB4F943540119F24078DA82B738A977553A6_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_mC7D4E6F771B2947EB0A45186D5F769EEFD992FDC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTextScrollU3Ed__14_tD25DEB4F943540119F24078DA82B738A977553A6_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mF4C9073C4148A84F5564C1CD781597BE870B28A7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BossHealth_bat_t3A01B528FB0D0F8B5CA18A0CD45F479D02530194_CustomAttributesCacheGenerator_BossHealth_bat_EnemyActiveOff_m0E6061CF40413FD702604226B2C8CB6CCD6AC732(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEnemyActiveOffU3Ed__17_t7AB5E724E27EC4168E7270B7CDBFD943849D21CB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CEnemyActiveOffU3Ed__17_t7AB5E724E27EC4168E7270B7CDBFD943849D21CB_0_0_0_var), NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t7AB5E724E27EC4168E7270B7CDBFD943849D21CB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t7AB5E724E27EC4168E7270B7CDBFD943849D21CB_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17__ctor_mA577EC9EA8948E1D60DA759F2D296C5044F54096(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t7AB5E724E27EC4168E7270B7CDBFD943849D21CB_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_mE52F244C415FAAB8E042303C9A8BE828C153F6E4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t7AB5E724E27EC4168E7270B7CDBFD943849D21CB_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m794E31662324F3791521289BCBE1BB477C71F130(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t7AB5E724E27EC4168E7270B7CDBFD943849D21CB_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_mC3BF406E3FF14C5382C3C0C5642F7078C23CAFFF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t7AB5E724E27EC4168E7270B7CDBFD943849D21CB_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m3A95CFD2F8CA16B187FBEF68F07D730F36A098F7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BossHealth_bear_t1C8229963E4A243DA7ABFBD7F8582BBFBEAF2918_CustomAttributesCacheGenerator_BossHealth_bear_EnemyActiveOff_m2F45DF2FA4E4D971EA4042766EA11222FD30CC8A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEnemyActiveOffU3Ed__17_t95404BB5A4134F5B62C67289E8DD94C5C9E69FD3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CEnemyActiveOffU3Ed__17_t95404BB5A4134F5B62C67289E8DD94C5C9E69FD3_0_0_0_var), NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t95404BB5A4134F5B62C67289E8DD94C5C9E69FD3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t95404BB5A4134F5B62C67289E8DD94C5C9E69FD3_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17__ctor_m464AE6200A1E122F7A40E31654B6CCD9E7776279(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t95404BB5A4134F5B62C67289E8DD94C5C9E69FD3_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_m253B9F6D14FFB4368754C68A0B731BDE59F82626(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t95404BB5A4134F5B62C67289E8DD94C5C9E69FD3_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD776DC29AC55AD6852773C217E3FCC42429CA3B8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t95404BB5A4134F5B62C67289E8DD94C5C9E69FD3_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m5D62DE7F1C1F40D685BEC2EBFEA3BDA71D8B5E8D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t95404BB5A4134F5B62C67289E8DD94C5C9E69FD3_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m8CAD4C264644BCD9C4EAE254FB2D21F63C5FCF0F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BossHealth_bee_t5F68827FC9DDC575F069A010DF9AB0EE7123DD63_CustomAttributesCacheGenerator_BossHealth_bee_EnemyActiveOff_m2A0A8EC50551CC68290E2D4D4569AC31159ACA7D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEnemyActiveOffU3Ed__17_t989DFCAF68BD051B69CBA1C02D74A7C43368C8D7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CEnemyActiveOffU3Ed__17_t989DFCAF68BD051B69CBA1C02D74A7C43368C8D7_0_0_0_var), NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t989DFCAF68BD051B69CBA1C02D74A7C43368C8D7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t989DFCAF68BD051B69CBA1C02D74A7C43368C8D7_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17__ctor_mC83BEBA335A88147D5893E6528372BF833DC7D2C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t989DFCAF68BD051B69CBA1C02D74A7C43368C8D7_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_m0A8398FF581CE322BA9D7D845297507334D68646(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t989DFCAF68BD051B69CBA1C02D74A7C43368C8D7_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46706DBCDBF6B07645423CCC5A194AAE73928688(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t989DFCAF68BD051B69CBA1C02D74A7C43368C8D7_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m2AEA766F9011936F0D23BF9592ABFF38B737EAED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t989DFCAF68BD051B69CBA1C02D74A7C43368C8D7_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m98824C77418948E11B692A5274B598916C00113D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BossHealth_guide_t8A1226CD7EA2DE00D8FC3C3925FBF16FAC7413BD_CustomAttributesCacheGenerator_BossHealth_guide_EnemyActiveOff_m57EA0B9D50AC33B4E0ABDB50F9483C29D89ACF91(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEnemyActiveOffU3Ed__17_t090E1E848E32CB9838DEE9486F4AD27607A95648_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CEnemyActiveOffU3Ed__17_t090E1E848E32CB9838DEE9486F4AD27607A95648_0_0_0_var), NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t090E1E848E32CB9838DEE9486F4AD27607A95648_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t090E1E848E32CB9838DEE9486F4AD27607A95648_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17__ctor_m09EFDED0A4E6C509759C171FC612A7A4190C362A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t090E1E848E32CB9838DEE9486F4AD27607A95648_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_m1F58E811E2CDB854E5C6DB1FC351391AD8881B1D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t090E1E848E32CB9838DEE9486F4AD27607A95648_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4E97E597A05A689D1F935145EF371638BB8972C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t090E1E848E32CB9838DEE9486F4AD27607A95648_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m213302082DF8640A57F8F8D259FEDCB319F1564A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t090E1E848E32CB9838DEE9486F4AD27607A95648_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_mF3B08ECAA4BDC5A885CF6D75480B2D30BBBD2365(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BossHealth_octopus_t1BB7720A9CB7293BF024FCBB3DEA14970DEB669D_CustomAttributesCacheGenerator_BossHealth_octopus_EnemyActiveOff_mF20C14DC9DE90D938AE47F959C8C3181FE9F3DE6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEnemyActiveOffU3Ed__16_t4AC26D9F732F008DD8073C8CE52FE9581ED94A82_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CEnemyActiveOffU3Ed__16_t4AC26D9F732F008DD8073C8CE52FE9581ED94A82_0_0_0_var), NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__16_t4AC26D9F732F008DD8073C8CE52FE9581ED94A82_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__16_t4AC26D9F732F008DD8073C8CE52FE9581ED94A82_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__16__ctor_m656F1D29A891EF8964C433F09D84AFD32EF87E91(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__16_t4AC26D9F732F008DD8073C8CE52FE9581ED94A82_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__16_System_IDisposable_Dispose_m40B57F8B7DC59586C6D0BD37F92FFF21F0612FF5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__16_t4AC26D9F732F008DD8073C8CE52FE9581ED94A82_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m014CC50FCD57228D47D667F2A4D3D5B3CE9A13F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__16_t4AC26D9F732F008DD8073C8CE52FE9581ED94A82_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__16_System_Collections_IEnumerator_Reset_m08A275A350536FF546323FFCAB9EB2BE90D166A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__16_t4AC26D9F732F008DD8073C8CE52FE9581ED94A82_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__16_System_Collections_IEnumerator_get_Current_m69BAF2DE3C9991F838E0FAEB774C31E5F75958EB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BossHealth_tFA863FC454D56EBCFF129D076CA4AD68724B1112_CustomAttributesCacheGenerator_BossHealth_EnemyActiveOff_mF760A81E37497AF5E86198E6A3FAA7BB5391D9A4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEnemyActiveOffU3Ed__17_t1104DECC029E3AAF5304A9D29AF5A680FF9AE010_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CEnemyActiveOffU3Ed__17_t1104DECC029E3AAF5304A9D29AF5A680FF9AE010_0_0_0_var), NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t1104DECC029E3AAF5304A9D29AF5A680FF9AE010_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t1104DECC029E3AAF5304A9D29AF5A680FF9AE010_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17__ctor_mEDA113BF186DC2339F8580F67643AB56E1ECE556(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t1104DECC029E3AAF5304A9D29AF5A680FF9AE010_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_mD06651F2C538A3224C9FDDE42A9012B321C56492(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t1104DECC029E3AAF5304A9D29AF5A680FF9AE010_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m06B0E0AA273012C9CCD56BB02407E66467557687(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t1104DECC029E3AAF5304A9D29AF5A680FF9AE010_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m2CB85D39D8F8BE0BE1EF9576A51AD73338551C25(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t1104DECC029E3AAF5304A9D29AF5A680FF9AE010_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m60DBC26F568D55337E46ED8A3513605501E94744(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BossHealth_ske_t267F54366240ACE4BBBAD079FCB1B0CE6640B627_CustomAttributesCacheGenerator_BossHealth_ske_EnemyActiveOff_m5F04512A0BE51B250EBDC07BF4E5A8184A1E40D6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEnemyActiveOffU3Ed__17_t43D619270E85C4030AD2A6F1C1D98AC33720EB34_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CEnemyActiveOffU3Ed__17_t43D619270E85C4030AD2A6F1C1D98AC33720EB34_0_0_0_var), NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t43D619270E85C4030AD2A6F1C1D98AC33720EB34_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t43D619270E85C4030AD2A6F1C1D98AC33720EB34_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17__ctor_mD56FDF58CF76DF6DCF6A0C4CDE06B265665DCCF8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t43D619270E85C4030AD2A6F1C1D98AC33720EB34_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_mBBBA8BA4C90EFD0254A9D54C860BE67A49C0EDA7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t43D619270E85C4030AD2A6F1C1D98AC33720EB34_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE5B73C806B8B5F2F2A7CF1822106AD4136D7783(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t43D619270E85C4030AD2A6F1C1D98AC33720EB34_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m765DD1E9FEB89FDD28649CC19B8F00F4C20CD5E9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyActiveOffU3Ed__17_t43D619270E85C4030AD2A6F1C1D98AC33720EB34_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m0F1D88A2309A0F825E691AE1BF307DE9D2EED219(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GameOverScreen_t66B7C93D73D16C603457FEA3A5424F7DDB6B68A7_CustomAttributesCacheGenerator_timeText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameOverScreen_t66B7C93D73D16C603457FEA3A5424F7DDB6B68A7_CustomAttributesCacheGenerator_duration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameOverScreen_t66B7C93D73D16C603457FEA3A5424F7DDB6B68A7_CustomAttributesCacheGenerator_currentTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameOverScreen_t66B7C93D73D16C603457FEA3A5424F7DDB6B68A7_CustomAttributesCacheGenerator_GameOverScreen_TimeIE_mB42A38029BD1334D965780125E80522703D0FA43(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTimeIEU3Ed__8_t2248B562C3607F1D7051857BA7F8109E6F353FA0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTimeIEU3Ed__8_t2248B562C3607F1D7051857BA7F8109E6F353FA0_0_0_0_var), NULL);
	}
}
static void GameOverScreen_t66B7C93D73D16C603457FEA3A5424F7DDB6B68A7_CustomAttributesCacheGenerator_GameOverScreen_LoadAsynchronously_m2197BDDACA1C511BBDAF27424927DEFE650E54B1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadAsynchronouslyU3Ed__10_t96EF2F6BC38FD48D0853B90F403D5486EFFB3743_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadAsynchronouslyU3Ed__10_t96EF2F6BC38FD48D0853B90F403D5486EFFB3743_0_0_0_var), NULL);
	}
}
static void U3CTimeIEU3Ed__8_t2248B562C3607F1D7051857BA7F8109E6F353FA0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__8_t2248B562C3607F1D7051857BA7F8109E6F353FA0_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__8__ctor_mAA3177335E8BFA01622B55BE8DCB2CEF5C18261E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__8_t2248B562C3607F1D7051857BA7F8109E6F353FA0_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__8_System_IDisposable_Dispose_m090F29683C88433E5BA4F5FAE98DA1997CA167E5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__8_t2248B562C3607F1D7051857BA7F8109E6F353FA0_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99EC495C6BCDAC6E1162941E07936462B1BD4E2D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__8_t2248B562C3607F1D7051857BA7F8109E6F353FA0_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__8_System_Collections_IEnumerator_Reset_m9BE542B45B26F5C2D6EB34B44370BCCAB9D5D3E2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeIEU3Ed__8_t2248B562C3607F1D7051857BA7F8109E6F353FA0_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__8_System_Collections_IEnumerator_get_Current_m0B2BFFB0D772E8E233ECDF8F858655AE9A4D03C1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAsynchronouslyU3Ed__10_t96EF2F6BC38FD48D0853B90F403D5486EFFB3743_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadAsynchronouslyU3Ed__10_t96EF2F6BC38FD48D0853B90F403D5486EFFB3743_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__10__ctor_mF01B26F4D4B06F59BA1A098B17A25DCFE6E42DE9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAsynchronouslyU3Ed__10_t96EF2F6BC38FD48D0853B90F403D5486EFFB3743_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__10_System_IDisposable_Dispose_m88D1C07D91A4DA9A053C84DE67353199F14E6047(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAsynchronouslyU3Ed__10_t96EF2F6BC38FD48D0853B90F403D5486EFFB3743_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF15B16A4766E1121E2355C87404FB6A1EF43AF3A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAsynchronouslyU3Ed__10_t96EF2F6BC38FD48D0853B90F403D5486EFFB3743_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__10_System_Collections_IEnumerator_Reset_m2EB3DC3D189F6976EFDB9B847CE615472B0F9BFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadAsynchronouslyU3Ed__10_t96EF2F6BC38FD48D0853B90F403D5486EFFB3743_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__10_System_Collections_IEnumerator_get_Current_mA40DA494789036083420705DAC23DAE8C5D80B08(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SaveManager_tFCE7930FC081EEE88E12994B53512095F258CBCE_CustomAttributesCacheGenerator_SaveManager_SavingTime_m609D3CFA5870774AF4E7B15546B731113A8A19A8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSavingTimeU3Ed__15_tB688C282B8B0A9C6363A6F10C310BAF1BC084883_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSavingTimeU3Ed__15_tB688C282B8B0A9C6363A6F10C310BAF1BC084883_0_0_0_var), NULL);
	}
}
static void U3CSavingTimeU3Ed__15_tB688C282B8B0A9C6363A6F10C310BAF1BC084883_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSavingTimeU3Ed__15_tB688C282B8B0A9C6363A6F10C310BAF1BC084883_CustomAttributesCacheGenerator_U3CSavingTimeU3Ed__15__ctor_m2757447DB09BEDAA4D2FFF6BEB20DAAB8C5A9446(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSavingTimeU3Ed__15_tB688C282B8B0A9C6363A6F10C310BAF1BC084883_CustomAttributesCacheGenerator_U3CSavingTimeU3Ed__15_System_IDisposable_Dispose_mF18CE4056508CE8E40BCFD44E8E2B6700C4E14BD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSavingTimeU3Ed__15_tB688C282B8B0A9C6363A6F10C310BAF1BC084883_CustomAttributesCacheGenerator_U3CSavingTimeU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3B119E99743AFF41A4AB2C0507A755DE9F1E08FD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSavingTimeU3Ed__15_tB688C282B8B0A9C6363A6F10C310BAF1BC084883_CustomAttributesCacheGenerator_U3CSavingTimeU3Ed__15_System_Collections_IEnumerator_Reset_m7C522780FA216172DBA8E07C9890043D4F7C692C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSavingTimeU3Ed__15_tB688C282B8B0A9C6363A6F10C310BAF1BC084883_CustomAttributesCacheGenerator_U3CSavingTimeU3Ed__15_System_Collections_IEnumerator_get_Current_mF59823DEC376A77E23F5CECB85123B68B1E32572(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Sound_tF983595F9C621A86B56E05F9778810369E90A0FE_CustomAttributesCacheGenerator_volumeSlider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterController_t05F5FF47FAD01F995B6C7F57524F2DEC9E282FC6_CustomAttributesCacheGenerator_CharacterController_Reload_m603D8E5810464FFAD8C80C20FA6C4E8ED61C4E38(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CReloadU3Ed__38_t4DF7DFFCF5D53852FEB6334B1D83CDCB9136AA1E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CReloadU3Ed__38_t4DF7DFFCF5D53852FEB6334B1D83CDCB9136AA1E_0_0_0_var), NULL);
	}
}
static void CharacterController_t05F5FF47FAD01F995B6C7F57524F2DEC9E282FC6_CustomAttributesCacheGenerator_CharacterController_WaitforDashCD_mD1C0B5349BC9E07C31AFEF439AE3FEDA192C1625(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitforDashCDU3Ed__39_t14E13865EE683795695090D3455320127C937F80_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitforDashCDU3Ed__39_t14E13865EE683795695090D3455320127C937F80_0_0_0_var), NULL);
	}
}
static void CharacterController_t05F5FF47FAD01F995B6C7F57524F2DEC9E282FC6_CustomAttributesCacheGenerator_CharacterController_WaitforAttackCD_mB07CC382323E16A70F7EA4E06A2A61A9EA44B01D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitforAttackCDU3Ed__40_tF43E5B4B83DEE1A35B02B69DEBA359A94D551D49_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitforAttackCDU3Ed__40_tF43E5B4B83DEE1A35B02B69DEBA359A94D551D49_0_0_0_var), NULL);
	}
}
static void CharacterController_t05F5FF47FAD01F995B6C7F57524F2DEC9E282FC6_CustomAttributesCacheGenerator_CharacterController_WaitforSlow_m4306A08C07FBF80952E7999EA8FA6AADEDA79850(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitforSlowU3Ed__41_t601851BD5C43BACFE807E44C1FB1DA9CBC028E6F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitforSlowU3Ed__41_t601851BD5C43BACFE807E44C1FB1DA9CBC028E6F_0_0_0_var), NULL);
	}
}
static void CharacterController_t05F5FF47FAD01F995B6C7F57524F2DEC9E282FC6_CustomAttributesCacheGenerator_CharacterController_WaitforControl_m99E2CC6C84046A3D3153772CF58AB8080851B1E1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitforControlU3Ed__42_t4BC1189F93B40CC735F2DF62FD76971BF7CAC346_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitforControlU3Ed__42_t4BC1189F93B40CC735F2DF62FD76971BF7CAC346_0_0_0_var), NULL);
	}
}
static void CharacterController_t05F5FF47FAD01F995B6C7F57524F2DEC9E282FC6_CustomAttributesCacheGenerator_CharacterController_TimeBuff_m92A4C732E993B314804D911463AD777E99D0783C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTimeBuffU3Ed__45_t0BE322FFFF97614A730EBA98616AD9AD1615617D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTimeBuffU3Ed__45_t0BE322FFFF97614A730EBA98616AD9AD1615617D_0_0_0_var), NULL);
	}
}
static void U3CReloadU3Ed__38_t4DF7DFFCF5D53852FEB6334B1D83CDCB9136AA1E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CReloadU3Ed__38_t4DF7DFFCF5D53852FEB6334B1D83CDCB9136AA1E_CustomAttributesCacheGenerator_U3CReloadU3Ed__38__ctor_mA2475D6EA0EECC4ED1C1DEDDF436609500668E43(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CReloadU3Ed__38_t4DF7DFFCF5D53852FEB6334B1D83CDCB9136AA1E_CustomAttributesCacheGenerator_U3CReloadU3Ed__38_System_IDisposable_Dispose_m189DF1F4181A8E7C4A6442EA9A8BFE394CC0EA1A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CReloadU3Ed__38_t4DF7DFFCF5D53852FEB6334B1D83CDCB9136AA1E_CustomAttributesCacheGenerator_U3CReloadU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m817CD53DA8749F94A166EBDA976731072045001E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CReloadU3Ed__38_t4DF7DFFCF5D53852FEB6334B1D83CDCB9136AA1E_CustomAttributesCacheGenerator_U3CReloadU3Ed__38_System_Collections_IEnumerator_Reset_m473199BCE668261B4718B1703F368D13D6D718D8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CReloadU3Ed__38_t4DF7DFFCF5D53852FEB6334B1D83CDCB9136AA1E_CustomAttributesCacheGenerator_U3CReloadU3Ed__38_System_Collections_IEnumerator_get_Current_mB53EB3667351EDD280A4DC0377073DF5889818AC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforDashCDU3Ed__39_t14E13865EE683795695090D3455320127C937F80_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitforDashCDU3Ed__39_t14E13865EE683795695090D3455320127C937F80_CustomAttributesCacheGenerator_U3CWaitforDashCDU3Ed__39__ctor_m373960E38BF24499999824042B6B18CC330FCDFB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforDashCDU3Ed__39_t14E13865EE683795695090D3455320127C937F80_CustomAttributesCacheGenerator_U3CWaitforDashCDU3Ed__39_System_IDisposable_Dispose_mEAA348CAEDF9CFACC0549AEB129346432A5E5E57(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforDashCDU3Ed__39_t14E13865EE683795695090D3455320127C937F80_CustomAttributesCacheGenerator_U3CWaitforDashCDU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54214D324F2636F1476FD91F8DEF57A419313AC6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforDashCDU3Ed__39_t14E13865EE683795695090D3455320127C937F80_CustomAttributesCacheGenerator_U3CWaitforDashCDU3Ed__39_System_Collections_IEnumerator_Reset_m0AA6D3E47AD5870D0DC147AAD6E30D775DB22655(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforDashCDU3Ed__39_t14E13865EE683795695090D3455320127C937F80_CustomAttributesCacheGenerator_U3CWaitforDashCDU3Ed__39_System_Collections_IEnumerator_get_Current_mE97F155EF767F943F9AEB8CA7FD0A3A83DE8F5FA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforAttackCDU3Ed__40_tF43E5B4B83DEE1A35B02B69DEBA359A94D551D49_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitforAttackCDU3Ed__40_tF43E5B4B83DEE1A35B02B69DEBA359A94D551D49_CustomAttributesCacheGenerator_U3CWaitforAttackCDU3Ed__40__ctor_mDCD5FFEE4BA5AF5F59FDF9872D00947AEA244881(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforAttackCDU3Ed__40_tF43E5B4B83DEE1A35B02B69DEBA359A94D551D49_CustomAttributesCacheGenerator_U3CWaitforAttackCDU3Ed__40_System_IDisposable_Dispose_m92D63B81EA8760D3EFDF4923517EB60B4668C27C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforAttackCDU3Ed__40_tF43E5B4B83DEE1A35B02B69DEBA359A94D551D49_CustomAttributesCacheGenerator_U3CWaitforAttackCDU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D221862ED79DE7E719959FA79DF0F7C3FADBE37(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforAttackCDU3Ed__40_tF43E5B4B83DEE1A35B02B69DEBA359A94D551D49_CustomAttributesCacheGenerator_U3CWaitforAttackCDU3Ed__40_System_Collections_IEnumerator_Reset_mC58183DFB9DC872EE95A3067AA0481D4B5704012(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforAttackCDU3Ed__40_tF43E5B4B83DEE1A35B02B69DEBA359A94D551D49_CustomAttributesCacheGenerator_U3CWaitforAttackCDU3Ed__40_System_Collections_IEnumerator_get_Current_m047CBA26B024B9420C0A608271EE253FEF0379BF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforSlowU3Ed__41_t601851BD5C43BACFE807E44C1FB1DA9CBC028E6F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitforSlowU3Ed__41_t601851BD5C43BACFE807E44C1FB1DA9CBC028E6F_CustomAttributesCacheGenerator_U3CWaitforSlowU3Ed__41__ctor_m3A69FF99D4BA5414697B5DC9F6800C9F29BD0058(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforSlowU3Ed__41_t601851BD5C43BACFE807E44C1FB1DA9CBC028E6F_CustomAttributesCacheGenerator_U3CWaitforSlowU3Ed__41_System_IDisposable_Dispose_mC215FD97FA1DCA7AF535F08F89A76462D2666C0E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforSlowU3Ed__41_t601851BD5C43BACFE807E44C1FB1DA9CBC028E6F_CustomAttributesCacheGenerator_U3CWaitforSlowU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m48935FB12DC00EC4301931A97F3BD21303DFF5EB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforSlowU3Ed__41_t601851BD5C43BACFE807E44C1FB1DA9CBC028E6F_CustomAttributesCacheGenerator_U3CWaitforSlowU3Ed__41_System_Collections_IEnumerator_Reset_mD63E6D9F2D797CCAB2BF6ABFB4F7F16AF9A80476(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforSlowU3Ed__41_t601851BD5C43BACFE807E44C1FB1DA9CBC028E6F_CustomAttributesCacheGenerator_U3CWaitforSlowU3Ed__41_System_Collections_IEnumerator_get_Current_mE796B7B00AAF4F9E4193550E90652205D3386C6A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforControlU3Ed__42_t4BC1189F93B40CC735F2DF62FD76971BF7CAC346_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitforControlU3Ed__42_t4BC1189F93B40CC735F2DF62FD76971BF7CAC346_CustomAttributesCacheGenerator_U3CWaitforControlU3Ed__42__ctor_mC555FCAC674AF43AA06D04B7A60FDE434485CC40(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforControlU3Ed__42_t4BC1189F93B40CC735F2DF62FD76971BF7CAC346_CustomAttributesCacheGenerator_U3CWaitforControlU3Ed__42_System_IDisposable_Dispose_m339BD26FF42F0EFA072A57648700421AE65D59FE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforControlU3Ed__42_t4BC1189F93B40CC735F2DF62FD76971BF7CAC346_CustomAttributesCacheGenerator_U3CWaitforControlU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55BE11D576B1F7839E3386CAB7EA471C71468E39(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforControlU3Ed__42_t4BC1189F93B40CC735F2DF62FD76971BF7CAC346_CustomAttributesCacheGenerator_U3CWaitforControlU3Ed__42_System_Collections_IEnumerator_Reset_mB02A4E799DAE3A7464F8CC2965C7B4C8DD022AEC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforControlU3Ed__42_t4BC1189F93B40CC735F2DF62FD76971BF7CAC346_CustomAttributesCacheGenerator_U3CWaitforControlU3Ed__42_System_Collections_IEnumerator_get_Current_m45AD58E82837EA91D4BE2D8BCCBFAE55F9D980A4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeBuffU3Ed__45_t0BE322FFFF97614A730EBA98616AD9AD1615617D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTimeBuffU3Ed__45_t0BE322FFFF97614A730EBA98616AD9AD1615617D_CustomAttributesCacheGenerator_U3CTimeBuffU3Ed__45__ctor_mD2E814F622252304651A2143045D1EDFF9C71040(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeBuffU3Ed__45_t0BE322FFFF97614A730EBA98616AD9AD1615617D_CustomAttributesCacheGenerator_U3CTimeBuffU3Ed__45_System_IDisposable_Dispose_mB60A2FE267BE49001CDDABA8D2C115FCB66578E2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeBuffU3Ed__45_t0BE322FFFF97614A730EBA98616AD9AD1615617D_CustomAttributesCacheGenerator_U3CTimeBuffU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5A7519E0C87FC70F54936B9B2A057C6A5E8D0B79(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeBuffU3Ed__45_t0BE322FFFF97614A730EBA98616AD9AD1615617D_CustomAttributesCacheGenerator_U3CTimeBuffU3Ed__45_System_Collections_IEnumerator_Reset_mCD2C34CDD9B9C71AFE577EC290324C4B1B24CA93(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTimeBuffU3Ed__45_t0BE322FFFF97614A730EBA98616AD9AD1615617D_CustomAttributesCacheGenerator_U3CTimeBuffU3Ed__45_System_Collections_IEnumerator_get_Current_mE8B7421BCD2F31F4EB76A037245B241D6AAD16FD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void HealthPlayer_t8E5151459604EFF1BAF84506752D4C0EC7A2CD0B_CustomAttributesCacheGenerator_HealthPlayer_PlayerActiveOff_m3C8156E1BA703EECEBC135A24986704A788C0481(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPlayerActiveOffU3Ed__17_tB4A573BE9F9DDBD451F4A9F4E97C1AF3413958AA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPlayerActiveOffU3Ed__17_tB4A573BE9F9DDBD451F4A9F4E97C1AF3413958AA_0_0_0_var), NULL);
	}
}
static void HealthPlayer_t8E5151459604EFF1BAF84506752D4C0EC7A2CD0B_CustomAttributesCacheGenerator_HealthPlayer_WaitforBurningTwiceTimes_m9B8D329D3F3ED2A06387ED52212B7329CCA72ED9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitforBurningTwiceTimesU3Ed__23_t6BEEC8618296DA43B378F2842EA8448939DB4F74_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitforBurningTwiceTimesU3Ed__23_t6BEEC8618296DA43B378F2842EA8448939DB4F74_0_0_0_var), NULL);
	}
}
static void HealthPlayer_t8E5151459604EFF1BAF84506752D4C0EC7A2CD0B_CustomAttributesCacheGenerator_HealthPlayer_WaitforBurningManyTimes_m21FD33E08F5D2F6F6CA1F1A321B405E71DC855FC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitforBurningManyTimesU3Ed__24_t95FAAD6C2B8E8CDC9951F60AC1E8325FB3F04ABE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitforBurningManyTimesU3Ed__24_t95FAAD6C2B8E8CDC9951F60AC1E8325FB3F04ABE_0_0_0_var), NULL);
	}
}
static void U3CPlayerActiveOffU3Ed__17_tB4A573BE9F9DDBD451F4A9F4E97C1AF3413958AA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPlayerActiveOffU3Ed__17_tB4A573BE9F9DDBD451F4A9F4E97C1AF3413958AA_CustomAttributesCacheGenerator_U3CPlayerActiveOffU3Ed__17__ctor_m9A9DB2B04BFA578AC10BB1E9B790FE0BE9B8693F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerActiveOffU3Ed__17_tB4A573BE9F9DDBD451F4A9F4E97C1AF3413958AA_CustomAttributesCacheGenerator_U3CPlayerActiveOffU3Ed__17_System_IDisposable_Dispose_mCB84FD0D750104FB2CF907E87DD05F7AE8AB9C63(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerActiveOffU3Ed__17_tB4A573BE9F9DDBD451F4A9F4E97C1AF3413958AA_CustomAttributesCacheGenerator_U3CPlayerActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m317F910FAB3ED1F3E76932F9603EB1798D79D3C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerActiveOffU3Ed__17_tB4A573BE9F9DDBD451F4A9F4E97C1AF3413958AA_CustomAttributesCacheGenerator_U3CPlayerActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_mC1EAC213C3F0424F2DE7AF7D4F973287EE125DC6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerActiveOffU3Ed__17_tB4A573BE9F9DDBD451F4A9F4E97C1AF3413958AA_CustomAttributesCacheGenerator_U3CPlayerActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_mB0F263F4102A3C2C994B02E3F15FAA5F083BF625(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforBurningTwiceTimesU3Ed__23_t6BEEC8618296DA43B378F2842EA8448939DB4F74_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitforBurningTwiceTimesU3Ed__23_t6BEEC8618296DA43B378F2842EA8448939DB4F74_CustomAttributesCacheGenerator_U3CWaitforBurningTwiceTimesU3Ed__23__ctor_mF7EA250EF31784D08B73C5490C7851D93532DBC8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforBurningTwiceTimesU3Ed__23_t6BEEC8618296DA43B378F2842EA8448939DB4F74_CustomAttributesCacheGenerator_U3CWaitforBurningTwiceTimesU3Ed__23_System_IDisposable_Dispose_m1409B5C7D84CC7569BAE724C971E338953FB0426(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforBurningTwiceTimesU3Ed__23_t6BEEC8618296DA43B378F2842EA8448939DB4F74_CustomAttributesCacheGenerator_U3CWaitforBurningTwiceTimesU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF9557D405C2B28E0125F2748C6B3C41F5023B0E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforBurningTwiceTimesU3Ed__23_t6BEEC8618296DA43B378F2842EA8448939DB4F74_CustomAttributesCacheGenerator_U3CWaitforBurningTwiceTimesU3Ed__23_System_Collections_IEnumerator_Reset_m2050273B00A7E0222167D456BA00C5168F33162E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforBurningTwiceTimesU3Ed__23_t6BEEC8618296DA43B378F2842EA8448939DB4F74_CustomAttributesCacheGenerator_U3CWaitforBurningTwiceTimesU3Ed__23_System_Collections_IEnumerator_get_Current_m9815C990320876FBB5FDB0D542B3982C070CD026(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforBurningManyTimesU3Ed__24_t95FAAD6C2B8E8CDC9951F60AC1E8325FB3F04ABE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitforBurningManyTimesU3Ed__24_t95FAAD6C2B8E8CDC9951F60AC1E8325FB3F04ABE_CustomAttributesCacheGenerator_U3CWaitforBurningManyTimesU3Ed__24__ctor_mBFAA5317CEA1FBA4BB4652CE2599F754749F50C1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforBurningManyTimesU3Ed__24_t95FAAD6C2B8E8CDC9951F60AC1E8325FB3F04ABE_CustomAttributesCacheGenerator_U3CWaitforBurningManyTimesU3Ed__24_System_IDisposable_Dispose_m0389C895AF95C024112A1A53241A12DDD87CCFDA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforBurningManyTimesU3Ed__24_t95FAAD6C2B8E8CDC9951F60AC1E8325FB3F04ABE_CustomAttributesCacheGenerator_U3CWaitforBurningManyTimesU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09F3C5A63D58ADDAB507EAD2BA87F7F7CA197A9C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforBurningManyTimesU3Ed__24_t95FAAD6C2B8E8CDC9951F60AC1E8325FB3F04ABE_CustomAttributesCacheGenerator_U3CWaitforBurningManyTimesU3Ed__24_System_Collections_IEnumerator_Reset_m5E755DF2A24BE189979B619CD9A68E99006AD458(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitforBurningManyTimesU3Ed__24_t95FAAD6C2B8E8CDC9951F60AC1E8325FB3F04ABE_CustomAttributesCacheGenerator_U3CWaitforBurningManyTimesU3Ed__24_System_Collections_IEnumerator_get_Current_m56DAFC8768C4E1A1B229004A178F9DD5552DF281(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4_CustomAttributesCacheGenerator_Knockback_KnockCo_m895B2CCA6560515B4ECBD06F62DE1FD5DA034A25(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CKnockCoU3Ed__3_t2484562B1B4BC1C411DE1DC6A9A737CAAB9754F3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CKnockCoU3Ed__3_t2484562B1B4BC1C411DE1DC6A9A737CAAB9754F3_0_0_0_var), NULL);
	}
}
static void U3CKnockCoU3Ed__3_t2484562B1B4BC1C411DE1DC6A9A737CAAB9754F3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CKnockCoU3Ed__3_t2484562B1B4BC1C411DE1DC6A9A737CAAB9754F3_CustomAttributesCacheGenerator_U3CKnockCoU3Ed__3__ctor_m13966A956093B208EA961EB6AF631B6749075141(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKnockCoU3Ed__3_t2484562B1B4BC1C411DE1DC6A9A737CAAB9754F3_CustomAttributesCacheGenerator_U3CKnockCoU3Ed__3_System_IDisposable_Dispose_mD43C8AE1C391E9406E80789A9543D352B5825BC0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKnockCoU3Ed__3_t2484562B1B4BC1C411DE1DC6A9A737CAAB9754F3_CustomAttributesCacheGenerator_U3CKnockCoU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D198094D719D238ED5864BEDAB86C06F7144BEA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKnockCoU3Ed__3_t2484562B1B4BC1C411DE1DC6A9A737CAAB9754F3_CustomAttributesCacheGenerator_U3CKnockCoU3Ed__3_System_Collections_IEnumerator_Reset_mCCB2797D5B3C57E35C3864E861573DF29FC19101(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKnockCoU3Ed__3_t2484562B1B4BC1C411DE1DC6A9A737CAAB9754F3_CustomAttributesCacheGenerator_U3CKnockCoU3Ed__3_System_Collections_IEnumerator_get_Current_mFCE1024C9949576CB9507452F41AFFC219D78739(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DropdownSample_t925F66C7435D75374A0E26DE15B4F1D95697F0DF_CustomAttributesCacheGenerator_text(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DropdownSample_t925F66C7435D75374A0E26DE15B4F1D95697F0DF_CustomAttributesCacheGenerator_dropdownWithoutPlaceholder(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DropdownSample_t925F66C7435D75374A0E26DE15B4F1D95697F0DF_CustomAttributesCacheGenerator_dropdownWithPlaceholder(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EnvMapAnimator_t7C30A094A3786710A896AD9A6FA46B13EFEB72FD_CustomAttributesCacheGenerator_EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnCharacterSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnSpriteSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnWordSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnLineSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnLinkSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934_CustomAttributesCacheGenerator_Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B_CustomAttributesCacheGenerator_Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8_CustomAttributesCacheGenerator_ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_0_0_0_var), NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07_CustomAttributesCacheGenerator_SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_0_0_0_var), NULL);
	}
}
static void U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TeleType_t92B5ED4FE818F912A562F085CE035F6B42834934_CustomAttributesCacheGenerator_TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289_CustomAttributesCacheGenerator_TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_0_0_0_var), NULL);
	}
}
static void TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289_CustomAttributesCacheGenerator_TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_0_0_0_var), NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200_CustomAttributesCacheGenerator_TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_0_0_0_var), NULL);
	}
}
static void TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200_CustomAttributesCacheGenerator_TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_0_0_0_var), NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374_CustomAttributesCacheGenerator_VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_0_0_0_var), NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A_CustomAttributesCacheGenerator_VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_0_0_0_var), NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A_CustomAttributesCacheGenerator_VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_0_0_0_var), NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9_CustomAttributesCacheGenerator_VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_0_0_0_var), NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163_CustomAttributesCacheGenerator_VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96_CustomAttributesCacheGenerator_WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_0_0_0_var), NULL);
	}
}
static void U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[420] = 
{
	U3CTimeIEU3Ed__10_tA7DB19C142169A8DCDE9BF17DE95304A5093961C_CustomAttributesCacheGenerator,
	U3CTypeLineU3Ed__13_t24E48C8240490AAE9B028BBA260B00934F9F101A_CustomAttributesCacheGenerator,
	U3CTimeIEU3Ed__10_t79F8D5D08CA76FE8A3892AC9C2EE6A4313A3CEFF_CustomAttributesCacheGenerator,
	U3CTypeLineU3Ed__13_t0195F7FADE21AC251086C7AFE1386AD855C19A95_CustomAttributesCacheGenerator,
	U3CTimeIEU3Ed__10_t28D64D34D87FF08928C4AC197A7A8B5840E2D870_CustomAttributesCacheGenerator,
	U3CTypeLineU3Ed__13_t5F92CF8B8EBB2BD80BDBA9A1A59408257269824B_CustomAttributesCacheGenerator,
	U3CTimeIEU3Ed__10_tD09479C2D9173EFB8015F2C800021947CB904643_CustomAttributesCacheGenerator,
	U3CTypeLineU3Ed__13_t23C6FD7AAE846FE48337853DE0EF332B5E9F53D0_CustomAttributesCacheGenerator,
	U3CTimeIEU3Ed__12_t58B0454AD5F54B56627D9C4271098ECBA023F776_CustomAttributesCacheGenerator,
	U3CTypeLineU3Ed__15_t5C0B45D84D61265E157C216B6535D3DE51337A23_CustomAttributesCacheGenerator,
	U3CLoadAsynchronouslyU3Ed__18_tCBD107D92FB476B8A2BFB64E705C198FE6332D8A_CustomAttributesCacheGenerator,
	U3CTextScrollU3Ed__14_t08B7AB540E24FF97A9F2DC6C8EBF5D09F1CD2832_CustomAttributesCacheGenerator,
	U3CTextScrollU3Ed__15_t58A1A5158A72226876545889B4F26A608D8006A4_CustomAttributesCacheGenerator,
	U3CTextScrollU3Ed__14_t5F82A72CC11220BD411C30F21EC1CEF4CA9EAA96_CustomAttributesCacheGenerator,
	U3CTextScrollU3Ed__14_tE6751550689136A9E9A020C0C5749385DCE0E76B_CustomAttributesCacheGenerator,
	U3CTextScrollU3Ed__15_t9E52613D815AE881174B02ED341EE752693BE4DD_CustomAttributesCacheGenerator,
	U3CTextScrollU3Ed__14_t218CCB237B5D22D65D963056F8D59F9A65CC157D_CustomAttributesCacheGenerator,
	U3CTextScrollU3Ed__14_t45ECEB7344B984B488ED5003C4C7C4E07F13650A_CustomAttributesCacheGenerator,
	U3CTextScrollU3Ed__14_tB27BDF2A99BD3A5DFCFAEE9F4F70FE5B4BAF1F07_CustomAttributesCacheGenerator,
	U3CTextScrollU3Ed__14_tAD044D8A38DC6A68C21C650C35EA1B97827C6F51_CustomAttributesCacheGenerator,
	U3CTextScrollU3Ed__14_tA8FFAA4799C58B02C4E24F5B9ED15F20312E32C3_CustomAttributesCacheGenerator,
	U3CTextScrollU3Ed__14_tD25DEB4F943540119F24078DA82B738A977553A6_CustomAttributesCacheGenerator,
	U3CEnemyActiveOffU3Ed__17_t7AB5E724E27EC4168E7270B7CDBFD943849D21CB_CustomAttributesCacheGenerator,
	U3CEnemyActiveOffU3Ed__17_t95404BB5A4134F5B62C67289E8DD94C5C9E69FD3_CustomAttributesCacheGenerator,
	U3CEnemyActiveOffU3Ed__17_t989DFCAF68BD051B69CBA1C02D74A7C43368C8D7_CustomAttributesCacheGenerator,
	U3CEnemyActiveOffU3Ed__17_t090E1E848E32CB9838DEE9486F4AD27607A95648_CustomAttributesCacheGenerator,
	U3CEnemyActiveOffU3Ed__16_t4AC26D9F732F008DD8073C8CE52FE9581ED94A82_CustomAttributesCacheGenerator,
	U3CEnemyActiveOffU3Ed__17_t1104DECC029E3AAF5304A9D29AF5A680FF9AE010_CustomAttributesCacheGenerator,
	U3CEnemyActiveOffU3Ed__17_t43D619270E85C4030AD2A6F1C1D98AC33720EB34_CustomAttributesCacheGenerator,
	U3CTimeIEU3Ed__8_t2248B562C3607F1D7051857BA7F8109E6F353FA0_CustomAttributesCacheGenerator,
	U3CLoadAsynchronouslyU3Ed__10_t96EF2F6BC38FD48D0853B90F403D5486EFFB3743_CustomAttributesCacheGenerator,
	U3CSavingTimeU3Ed__15_tB688C282B8B0A9C6363A6F10C310BAF1BC084883_CustomAttributesCacheGenerator,
	U3CReloadU3Ed__38_t4DF7DFFCF5D53852FEB6334B1D83CDCB9136AA1E_CustomAttributesCacheGenerator,
	U3CWaitforDashCDU3Ed__39_t14E13865EE683795695090D3455320127C937F80_CustomAttributesCacheGenerator,
	U3CWaitforAttackCDU3Ed__40_tF43E5B4B83DEE1A35B02B69DEBA359A94D551D49_CustomAttributesCacheGenerator,
	U3CWaitforSlowU3Ed__41_t601851BD5C43BACFE807E44C1FB1DA9CBC028E6F_CustomAttributesCacheGenerator,
	U3CWaitforControlU3Ed__42_t4BC1189F93B40CC735F2DF62FD76971BF7CAC346_CustomAttributesCacheGenerator,
	U3CTimeBuffU3Ed__45_t0BE322FFFF97614A730EBA98616AD9AD1615617D_CustomAttributesCacheGenerator,
	U3CPlayerActiveOffU3Ed__17_tB4A573BE9F9DDBD451F4A9F4E97C1AF3413958AA_CustomAttributesCacheGenerator,
	U3CWaitforBurningTwiceTimesU3Ed__23_t6BEEC8618296DA43B378F2842EA8448939DB4F74_CustomAttributesCacheGenerator,
	U3CWaitforBurningManyTimesU3Ed__24_t95FAAD6C2B8E8CDC9951F60AC1E8325FB3F04ABE_CustomAttributesCacheGenerator,
	U3CKnockCoU3Ed__3_t2484562B1B4BC1C411DE1DC6A9A737CAAB9754F3_CustomAttributesCacheGenerator,
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator,
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator,
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator,
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator,
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator,
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator,
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator,
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator,
	U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator,
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator,
	GameOverScreen_t66B7C93D73D16C603457FEA3A5424F7DDB6B68A7_CustomAttributesCacheGenerator_timeText,
	GameOverScreen_t66B7C93D73D16C603457FEA3A5424F7DDB6B68A7_CustomAttributesCacheGenerator_duration,
	GameOverScreen_t66B7C93D73D16C603457FEA3A5424F7DDB6B68A7_CustomAttributesCacheGenerator_currentTime,
	Sound_tF983595F9C621A86B56E05F9778810369E90A0FE_CustomAttributesCacheGenerator_volumeSlider,
	DropdownSample_t925F66C7435D75374A0E26DE15B4F1D95697F0DF_CustomAttributesCacheGenerator_text,
	DropdownSample_t925F66C7435D75374A0E26DE15B4F1D95697F0DF_CustomAttributesCacheGenerator_dropdownWithoutPlaceholder,
	DropdownSample_t925F66C7435D75374A0E26DE15B4F1D95697F0DF_CustomAttributesCacheGenerator_dropdownWithPlaceholder,
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnCharacterSelection,
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnSpriteSelection,
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnWordSelection,
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnLineSelection,
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnLinkSelection,
	IntroManager_t0830037B4178478802A163C0B16656A3D4EE7B54_CustomAttributesCacheGenerator_IntroManager_TimeIE_m8671693B4F354CA40F239A02EB160E5EE906CB40,
	IntroManager_t0830037B4178478802A163C0B16656A3D4EE7B54_CustomAttributesCacheGenerator_IntroManager_TypeLine_m2D1AB9015F67B6094D56BCD80BB4198A1615C4A6,
	U3CTimeIEU3Ed__10_tA7DB19C142169A8DCDE9BF17DE95304A5093961C_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10__ctor_m11C8A6394695414B03FF139147D221BE9CC7F656,
	U3CTimeIEU3Ed__10_tA7DB19C142169A8DCDE9BF17DE95304A5093961C_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_IDisposable_Dispose_mC05D66A28083727BFEE3EF10506098A204A03409,
	U3CTimeIEU3Ed__10_tA7DB19C142169A8DCDE9BF17DE95304A5093961C_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m25917A42988161EEBA1271CF3B23AEB337543CA9,
	U3CTimeIEU3Ed__10_tA7DB19C142169A8DCDE9BF17DE95304A5093961C_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_IEnumerator_Reset_m65DAD9114DAB93BE8D5BB1C625F1FB2CF0F0D472,
	U3CTimeIEU3Ed__10_tA7DB19C142169A8DCDE9BF17DE95304A5093961C_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_IEnumerator_get_Current_m2A25C3B0223067B36A377B4C3FDAE686F28D539E,
	U3CTypeLineU3Ed__13_t24E48C8240490AAE9B028BBA260B00934F9F101A_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13__ctor_mB4362558C8CDE1FCF2DB18EE13D09F9A77800DA3,
	U3CTypeLineU3Ed__13_t24E48C8240490AAE9B028BBA260B00934F9F101A_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_IDisposable_Dispose_mBF2CA07B9929F52BB9D6757B4ABBEBB156D51C92,
	U3CTypeLineU3Ed__13_t24E48C8240490AAE9B028BBA260B00934F9F101A_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6016236D9EEC958D1B15C210D6D80897805FEE75,
	U3CTypeLineU3Ed__13_t24E48C8240490AAE9B028BBA260B00934F9F101A_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_IEnumerator_Reset_m20BF6AA37542A6DE14CE8606807FBA0E4EFD2815,
	U3CTypeLineU3Ed__13_t24E48C8240490AAE9B028BBA260B00934F9F101A_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_IEnumerator_get_Current_m623B9FE74115FEF5D16925CEE1E5397C12B38BCB,
	Story1Manager_t5159C7F9EABC50A48ADF896CA08F3FE3B110DEFE_CustomAttributesCacheGenerator_Story1Manager_TimeIE_m9F889DA26E3CF321B6DDD407B42D4EEED1CAEDCD,
	Story1Manager_t5159C7F9EABC50A48ADF896CA08F3FE3B110DEFE_CustomAttributesCacheGenerator_Story1Manager_TypeLine_m597776B3A41CAA93A0671A6DD7F50DEA37F5E4FC,
	U3CTimeIEU3Ed__10_t79F8D5D08CA76FE8A3892AC9C2EE6A4313A3CEFF_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10__ctor_m2458A1336253AF66749EA49CDCF74B1810C923B6,
	U3CTimeIEU3Ed__10_t79F8D5D08CA76FE8A3892AC9C2EE6A4313A3CEFF_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_IDisposable_Dispose_m126FFCFD9B1CD0A1C81AD3798874A380A891518F,
	U3CTimeIEU3Ed__10_t79F8D5D08CA76FE8A3892AC9C2EE6A4313A3CEFF_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA6B0872963BDC898023AD817C434FE51C7854FF9,
	U3CTimeIEU3Ed__10_t79F8D5D08CA76FE8A3892AC9C2EE6A4313A3CEFF_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_IEnumerator_Reset_mA408B839522C0E46A97723C45EF4DCF246215799,
	U3CTimeIEU3Ed__10_t79F8D5D08CA76FE8A3892AC9C2EE6A4313A3CEFF_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_IEnumerator_get_Current_mB2BCCF780BDA64276290CB3080C262B0AF33773A,
	U3CTypeLineU3Ed__13_t0195F7FADE21AC251086C7AFE1386AD855C19A95_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13__ctor_mB8227D04CFD9803A9E3D112D82A1C2CA0320C3AF,
	U3CTypeLineU3Ed__13_t0195F7FADE21AC251086C7AFE1386AD855C19A95_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_IDisposable_Dispose_m5DE8B5998D1316BFCFE3D69C8D65AA90C8A4A614,
	U3CTypeLineU3Ed__13_t0195F7FADE21AC251086C7AFE1386AD855C19A95_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4785A5F20FCC7D15936A974801F1ABF86969FBA4,
	U3CTypeLineU3Ed__13_t0195F7FADE21AC251086C7AFE1386AD855C19A95_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_IEnumerator_Reset_m26715E5D4DA17CBB171BE6A64BBB6AFB56E647A4,
	U3CTypeLineU3Ed__13_t0195F7FADE21AC251086C7AFE1386AD855C19A95_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_IEnumerator_get_Current_mFD03E403D694A40D19E16EDC51A696B7E6DB7FFA,
	Story2Manager_t754E10D4B87F080C1F6D63E55620DB9900A7ACCD_CustomAttributesCacheGenerator_Story2Manager_TimeIE_m6A9B40A187E64B7BEBE3F30EF100815FBE55FCC2,
	Story2Manager_t754E10D4B87F080C1F6D63E55620DB9900A7ACCD_CustomAttributesCacheGenerator_Story2Manager_TypeLine_m9F81497C07EB9F4A08C1A6761B747F414B723215,
	U3CTimeIEU3Ed__10_t28D64D34D87FF08928C4AC197A7A8B5840E2D870_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10__ctor_mC61D5359D18BC71478D2B26D364DA9E5215CA853,
	U3CTimeIEU3Ed__10_t28D64D34D87FF08928C4AC197A7A8B5840E2D870_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_IDisposable_Dispose_m28569B7D30B0704ACF7F0C2FD6E9A83606F8AA55,
	U3CTimeIEU3Ed__10_t28D64D34D87FF08928C4AC197A7A8B5840E2D870_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0B75C09C6CCDEBC775E1A4FE0151E6BFE4F41DC7,
	U3CTimeIEU3Ed__10_t28D64D34D87FF08928C4AC197A7A8B5840E2D870_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_IEnumerator_Reset_m289A9D9686AC91F03213A3EF84EB9C62091B7B1C,
	U3CTimeIEU3Ed__10_t28D64D34D87FF08928C4AC197A7A8B5840E2D870_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_IEnumerator_get_Current_m938DBF30F82FDDE5FA86D026FC89B0FAE95EB447,
	U3CTypeLineU3Ed__13_t5F92CF8B8EBB2BD80BDBA9A1A59408257269824B_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13__ctor_m3381142803F5490CA2C6EA44676B001A924C87B0,
	U3CTypeLineU3Ed__13_t5F92CF8B8EBB2BD80BDBA9A1A59408257269824B_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_IDisposable_Dispose_mA2E73E049CF5E008F407436C5885C01CD1BE1CFD,
	U3CTypeLineU3Ed__13_t5F92CF8B8EBB2BD80BDBA9A1A59408257269824B_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB3E18BB92F474F438C262387EF09784344930706,
	U3CTypeLineU3Ed__13_t5F92CF8B8EBB2BD80BDBA9A1A59408257269824B_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_IEnumerator_Reset_m18F6D13CF39437E0C7D6FCDFD2A9F7431F5096FE,
	U3CTypeLineU3Ed__13_t5F92CF8B8EBB2BD80BDBA9A1A59408257269824B_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_IEnumerator_get_Current_m32405CE40FDE12E1870029E0EC5B92E58F8582E4,
	Story3Manager_t907700194AF7612DCD53CC0D381D7DA8354B3780_CustomAttributesCacheGenerator_Story3Manager_TimeIE_m3BFB48C73E1A83E8E21720324DE7CC270D8DE122,
	Story3Manager_t907700194AF7612DCD53CC0D381D7DA8354B3780_CustomAttributesCacheGenerator_Story3Manager_TypeLine_m5EC34C6FF1711EC998F1D8D6EE88EAFC52A61675,
	U3CTimeIEU3Ed__10_tD09479C2D9173EFB8015F2C800021947CB904643_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10__ctor_m13DC5856052DFE59F5E14120CEA19C0193D0BEAA,
	U3CTimeIEU3Ed__10_tD09479C2D9173EFB8015F2C800021947CB904643_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_IDisposable_Dispose_mBFA4A57358A033314A1AF41AAB8F2AF92F24C646,
	U3CTimeIEU3Ed__10_tD09479C2D9173EFB8015F2C800021947CB904643_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52B342497384166C38981CA667740BD580076450,
	U3CTimeIEU3Ed__10_tD09479C2D9173EFB8015F2C800021947CB904643_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_IEnumerator_Reset_mBC68911DBF658B3477EA295D7BF2FFD8B0FC484B,
	U3CTimeIEU3Ed__10_tD09479C2D9173EFB8015F2C800021947CB904643_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__10_System_Collections_IEnumerator_get_Current_m75BB82C5CAE4C881AC8B70EECF6E4989EC8470DC,
	U3CTypeLineU3Ed__13_t23C6FD7AAE846FE48337853DE0EF332B5E9F53D0_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13__ctor_m8268E1A248C3515BFE52D666819313D5E77E1197,
	U3CTypeLineU3Ed__13_t23C6FD7AAE846FE48337853DE0EF332B5E9F53D0_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_IDisposable_Dispose_m34607E4719E82B3C90FCAA602BA9158A8AEA2B5B,
	U3CTypeLineU3Ed__13_t23C6FD7AAE846FE48337853DE0EF332B5E9F53D0_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2A5EA8BF16919F3AEC394B997A12533E9AE0DAC0,
	U3CTypeLineU3Ed__13_t23C6FD7AAE846FE48337853DE0EF332B5E9F53D0_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_IEnumerator_Reset_m18E7B4304AC09386ECD997307FA234B521E21439,
	U3CTypeLineU3Ed__13_t23C6FD7AAE846FE48337853DE0EF332B5E9F53D0_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__13_System_Collections_IEnumerator_get_Current_mD86346C7738D5B2E93D38B4CEFFA74C98305D845,
	Story4Manager_t88DB1EB3693D1302B55DCBD3A8DF19F6F713C5C4_CustomAttributesCacheGenerator_Story4Manager_TimeIE_mF20ABF95DD1A9D0D8BDBBA7D7C3E0529133F2EE2,
	Story4Manager_t88DB1EB3693D1302B55DCBD3A8DF19F6F713C5C4_CustomAttributesCacheGenerator_Story4Manager_TypeLine_mB11C4356720EACBB75ED9E29CA3370751FA43C33,
	Story4Manager_t88DB1EB3693D1302B55DCBD3A8DF19F6F713C5C4_CustomAttributesCacheGenerator_Story4Manager_LoadAsynchronously_mD438C42C9179A37EB643566B09675E3FA842CFE6,
	U3CTimeIEU3Ed__12_t58B0454AD5F54B56627D9C4271098ECBA023F776_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__12__ctor_mBB9F8F85D5DAE9BD94AE09041320E26BA12DADA0,
	U3CTimeIEU3Ed__12_t58B0454AD5F54B56627D9C4271098ECBA023F776_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__12_System_IDisposable_Dispose_m4B0734E90CB756B4B8E4B4D73A4C53D967759F1D,
	U3CTimeIEU3Ed__12_t58B0454AD5F54B56627D9C4271098ECBA023F776_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C73D565CC03D11BC1C842373611D2810BB5B3A4,
	U3CTimeIEU3Ed__12_t58B0454AD5F54B56627D9C4271098ECBA023F776_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__12_System_Collections_IEnumerator_Reset_m7EFD622AE07588EFBC6530F070A4DA48F45B5B6B,
	U3CTimeIEU3Ed__12_t58B0454AD5F54B56627D9C4271098ECBA023F776_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__12_System_Collections_IEnumerator_get_Current_m79A22DBAF3530D9FF60B2AA9B5A268FACD7E0C1B,
	U3CTypeLineU3Ed__15_t5C0B45D84D61265E157C216B6535D3DE51337A23_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__15__ctor_m5E924108C2849A14B99387420583E4FF4191ACDC,
	U3CTypeLineU3Ed__15_t5C0B45D84D61265E157C216B6535D3DE51337A23_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__15_System_IDisposable_Dispose_m1751D8F1A560C0BCD88C8384796F0FE92D9EC701,
	U3CTypeLineU3Ed__15_t5C0B45D84D61265E157C216B6535D3DE51337A23_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m76FC58D00BCEA3EE34B60EEA166E53E8D68D2ACD,
	U3CTypeLineU3Ed__15_t5C0B45D84D61265E157C216B6535D3DE51337A23_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__15_System_Collections_IEnumerator_Reset_mBDC2E72B1994109877CBA9AF3F9F2906A14AF27C,
	U3CTypeLineU3Ed__15_t5C0B45D84D61265E157C216B6535D3DE51337A23_CustomAttributesCacheGenerator_U3CTypeLineU3Ed__15_System_Collections_IEnumerator_get_Current_m9670A64D4B8B6142E389CD03268C21FF7123EF88,
	U3CLoadAsynchronouslyU3Ed__18_tCBD107D92FB476B8A2BFB64E705C198FE6332D8A_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__18__ctor_m9193454C25CF53C50BE2952AD275D59D148125B8,
	U3CLoadAsynchronouslyU3Ed__18_tCBD107D92FB476B8A2BFB64E705C198FE6332D8A_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__18_System_IDisposable_Dispose_m81EAE1BAA920EEAAB99C7292C8063FE9A18003D6,
	U3CLoadAsynchronouslyU3Ed__18_tCBD107D92FB476B8A2BFB64E705C198FE6332D8A_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1CD8EE6EC9E99257A29EBE8EE8ACDE0A21FBE909,
	U3CLoadAsynchronouslyU3Ed__18_tCBD107D92FB476B8A2BFB64E705C198FE6332D8A_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__18_System_Collections_IEnumerator_Reset_m533E97DA9C85B050583E198153B03F0C6442557B,
	U3CLoadAsynchronouslyU3Ed__18_tCBD107D92FB476B8A2BFB64E705C198FE6332D8A_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__18_System_Collections_IEnumerator_get_Current_m13A26DE6C2E4018242E9DC5FB764FC8685006B48,
	DialogueManager_t609C696D1E27CEA7A0895EDADECF76865D33931D_CustomAttributesCacheGenerator_DialogueManager_TextScroll_m5D3194B542493389CE2040A3ABF204E5CDA2A029,
	U3CTextScrollU3Ed__14_t08B7AB540E24FF97A9F2DC6C8EBF5D09F1CD2832_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14__ctor_m0FF343CAE7D09A873B318ACAD7760727CBBBBD44,
	U3CTextScrollU3Ed__14_t08B7AB540E24FF97A9F2DC6C8EBF5D09F1CD2832_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m8DD31CCCB316C58800E444EE15133E4E8BC15ECD,
	U3CTextScrollU3Ed__14_t08B7AB540E24FF97A9F2DC6C8EBF5D09F1CD2832_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4FDF89D7CC6D5A16A2361C1113345C725CBCC522,
	U3CTextScrollU3Ed__14_t08B7AB540E24FF97A9F2DC6C8EBF5D09F1CD2832_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m11694E97359876379F5AB7702B0E37F2A0E8C0F9,
	U3CTextScrollU3Ed__14_t08B7AB540E24FF97A9F2DC6C8EBF5D09F1CD2832_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mABE1658974027AC5CF135A1980F2E95028C2C7C4,
	DialogueManager111_tF83F90FA20C3590DAF69C6E5A1E3D5D38A2FE52B_CustomAttributesCacheGenerator_DialogueManager111_TextScroll_mBF4DF710DB2696EFEB78F5E9EB3B19A45EC6CD95,
	U3CTextScrollU3Ed__15_t58A1A5158A72226876545889B4F26A608D8006A4_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15__ctor_mFB72C4F6C3A9277F985C226F0C5AE5C882610675,
	U3CTextScrollU3Ed__15_t58A1A5158A72226876545889B4F26A608D8006A4_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15_System_IDisposable_Dispose_m8682A2B86D921428757604CC5147E8BFB5DE8D80,
	U3CTextScrollU3Ed__15_t58A1A5158A72226876545889B4F26A608D8006A4_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF07A8CC8414E93E791847D1301E2CDF2629D324,
	U3CTextScrollU3Ed__15_t58A1A5158A72226876545889B4F26A608D8006A4_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15_System_Collections_IEnumerator_Reset_m18CBF4829F14DFF81C74F4E30461372D237C879F,
	U3CTextScrollU3Ed__15_t58A1A5158A72226876545889B4F26A608D8006A4_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15_System_Collections_IEnumerator_get_Current_m2E43D012A1C14989FE33597AD52AA78013DCF877,
	DialogueManager10_t3412F239D84E470F38ECCEF84946B8D5A550987B_CustomAttributesCacheGenerator_DialogueManager10_TextScroll_mEFC8587C6A8F527F06560AA0FF26CE9351CBA49C,
	U3CTextScrollU3Ed__14_t5F82A72CC11220BD411C30F21EC1CEF4CA9EAA96_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14__ctor_m25372485B50A52BDD8128CC237EE2E22C46EF534,
	U3CTextScrollU3Ed__14_t5F82A72CC11220BD411C30F21EC1CEF4CA9EAA96_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_IDisposable_Dispose_mBD7F7A2F7E8A76C7B522049B83C0F99302195930,
	U3CTextScrollU3Ed__14_t5F82A72CC11220BD411C30F21EC1CEF4CA9EAA96_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE424541DA8B50682073EF643295B76FA41785D78,
	U3CTextScrollU3Ed__14_t5F82A72CC11220BD411C30F21EC1CEF4CA9EAA96_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m30934F0B954200D61D8A62774DF6D0D2885265F8,
	U3CTextScrollU3Ed__14_t5F82A72CC11220BD411C30F21EC1CEF4CA9EAA96_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_m09ED21E076FD07A2D85934D5EDF75577CDA2EC18,
	DialogueManager1_tDEC1CAE4C52F9AC218021B0C3F31C930A3F6C189_CustomAttributesCacheGenerator_DialogueManager1_TextScroll_m449B179ECE4CEC9C9F207D5AFAE30142BA1719C8,
	U3CTextScrollU3Ed__14_tE6751550689136A9E9A020C0C5749385DCE0E76B_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14__ctor_m46153879164D1408755D717F0CFF3804B9D0DA21,
	U3CTextScrollU3Ed__14_tE6751550689136A9E9A020C0C5749385DCE0E76B_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_IDisposable_Dispose_mD54DB86896E9D8FDCF8BC296B9FEF71554B67D6C,
	U3CTextScrollU3Ed__14_tE6751550689136A9E9A020C0C5749385DCE0E76B_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB172AE4343A8D82D557AF6D79E3A12EB643BB220,
	U3CTextScrollU3Ed__14_tE6751550689136A9E9A020C0C5749385DCE0E76B_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m8C8F933102AF5469324D53568C59C3211A0721BD,
	U3CTextScrollU3Ed__14_tE6751550689136A9E9A020C0C5749385DCE0E76B_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_m08ECAF3014121CD6964575229A159602CFAD3E58,
	DialogueManager3_t4AC1E2D7E7E9F4698B40D55081677F1E8F022244_CustomAttributesCacheGenerator_DialogueManager3_TextScroll_m1B5D37D563F9AAE195726DB2DE62F7877BEE2C4A,
	U3CTextScrollU3Ed__15_t9E52613D815AE881174B02ED341EE752693BE4DD_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15__ctor_m4D514A28DAFB725FCCC3F86305EF9047E11B835F,
	U3CTextScrollU3Ed__15_t9E52613D815AE881174B02ED341EE752693BE4DD_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15_System_IDisposable_Dispose_m5B3A2BA0FDE76B3D910F9F543622E3B7E0104CA5,
	U3CTextScrollU3Ed__15_t9E52613D815AE881174B02ED341EE752693BE4DD_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44B1FEC4291372B6B51003ABC8F76DF83193D7B2,
	U3CTextScrollU3Ed__15_t9E52613D815AE881174B02ED341EE752693BE4DD_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15_System_Collections_IEnumerator_Reset_m98B30F5DEA0D66BE62F371A7A6C3DCB9B95FF6C4,
	U3CTextScrollU3Ed__15_t9E52613D815AE881174B02ED341EE752693BE4DD_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__15_System_Collections_IEnumerator_get_Current_m0F1D7EA630D47E82DA6928695681B07E7F6F3785,
	DialogueManager4_tD71D375A3EE0F27D3B35A57B54FC3D0E89C60B5F_CustomAttributesCacheGenerator_DialogueManager4_TextScroll_m0C54E3C56347658AD1502A8612C40A2804A86F1B,
	U3CTextScrollU3Ed__14_t218CCB237B5D22D65D963056F8D59F9A65CC157D_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14__ctor_m287E95932EF65B84A11568F314D1A15E320B6536,
	U3CTextScrollU3Ed__14_t218CCB237B5D22D65D963056F8D59F9A65CC157D_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m66D5900B028A65713A5BC970753B95A980024A9E,
	U3CTextScrollU3Ed__14_t218CCB237B5D22D65D963056F8D59F9A65CC157D_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m67E6C4920DA41BB4F2897A59F1B82A46C76EDB90,
	U3CTextScrollU3Ed__14_t218CCB237B5D22D65D963056F8D59F9A65CC157D_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m2BD1E12D338D61FAA0A2A9B8C4778CE45A264B89,
	U3CTextScrollU3Ed__14_t218CCB237B5D22D65D963056F8D59F9A65CC157D_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_m9349BFBAB9DF9427D539E5EA95DA106B8B7D9158,
	DialogueManager5_tEA5E19E2260F2CD379D85CC269891BE7B9E86D16_CustomAttributesCacheGenerator_DialogueManager5_TextScroll_mA868CFAC74505D370CE5C4BA8E8469957AD8F086,
	U3CTextScrollU3Ed__14_t45ECEB7344B984B488ED5003C4C7C4E07F13650A_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14__ctor_m5CD63C4796AA3EFFCDDB1B1695B299FA5F129190,
	U3CTextScrollU3Ed__14_t45ECEB7344B984B488ED5003C4C7C4E07F13650A_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m1131A72D8DAA306883DC73A644E10DF939E3DDC8,
	U3CTextScrollU3Ed__14_t45ECEB7344B984B488ED5003C4C7C4E07F13650A_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m586FF4D51CD4F659D098B08A3EF8B37230C59BA6,
	U3CTextScrollU3Ed__14_t45ECEB7344B984B488ED5003C4C7C4E07F13650A_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_mB92B68E8BE2177A8B7EA040B8CC0328708947DB2,
	U3CTextScrollU3Ed__14_t45ECEB7344B984B488ED5003C4C7C4E07F13650A_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_m83A16F0C427A38D04171F21D0FB92541B0319C76,
	DialogueManager6_t0AFC57B6176046A403A50D84778DB5ECCE5BD029_CustomAttributesCacheGenerator_DialogueManager6_TextScroll_m5A10285269F39472F209FE26602A0D0D1D2AE92F,
	U3CTextScrollU3Ed__14_tB27BDF2A99BD3A5DFCFAEE9F4F70FE5B4BAF1F07_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14__ctor_m29BA7873E6EF4DFB87C961BB0BF78BF2976D3128,
	U3CTextScrollU3Ed__14_tB27BDF2A99BD3A5DFCFAEE9F4F70FE5B4BAF1F07_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m82BB053A1C51D75CF09DDB5F7DD3CB29FC73CE9E,
	U3CTextScrollU3Ed__14_tB27BDF2A99BD3A5DFCFAEE9F4F70FE5B4BAF1F07_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB001D87EE04371590F576A76F96AC0CDA78BAFEE,
	U3CTextScrollU3Ed__14_tB27BDF2A99BD3A5DFCFAEE9F4F70FE5B4BAF1F07_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_mCD2873EF7E990CC57029E50B039B3B93F55CFF86,
	U3CTextScrollU3Ed__14_tB27BDF2A99BD3A5DFCFAEE9F4F70FE5B4BAF1F07_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mE5FB965E79F06B9E364142856FF3C3CAA11040D7,
	DialogueManager7_tA43C1EF0EC6E1266C0249B81607A8FC1625C3032_CustomAttributesCacheGenerator_DialogueManager7_TextScroll_mBE32AC7FF53D80990FD20B69E4538EDAAA79D380,
	U3CTextScrollU3Ed__14_tAD044D8A38DC6A68C21C650C35EA1B97827C6F51_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14__ctor_mCCF5C5A9B5C4FDC2A5361E59F0E6D5B63497E721,
	U3CTextScrollU3Ed__14_tAD044D8A38DC6A68C21C650C35EA1B97827C6F51_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m86803CD35DE20355B45C10DFB1BBEFB11FACC611,
	U3CTextScrollU3Ed__14_tAD044D8A38DC6A68C21C650C35EA1B97827C6F51_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF0D25459F989654F0F9931A907E0BE2D7C8C8957,
	U3CTextScrollU3Ed__14_tAD044D8A38DC6A68C21C650C35EA1B97827C6F51_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m612A77A93459DCF470D8F30C2D062034F01B8E2E,
	U3CTextScrollU3Ed__14_tAD044D8A38DC6A68C21C650C35EA1B97827C6F51_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mBA75C9E3BB40C9F9CAD7791953A001FAE2B3A531,
	DialogueManager8_t497B60BABDD18643C84EFD35DF218070380F3642_CustomAttributesCacheGenerator_DialogueManager8_TextScroll_m530F4F98A324FEE74AC4BCB4499A04AE0A4BA482,
	U3CTextScrollU3Ed__14_tA8FFAA4799C58B02C4E24F5B9ED15F20312E32C3_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14__ctor_mD21839154365B3E4559D29CC0531B9C507BFBBBE,
	U3CTextScrollU3Ed__14_tA8FFAA4799C58B02C4E24F5B9ED15F20312E32C3_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_IDisposable_Dispose_mB081476BC8CD3EB1AB9DD3CE91393410457FA640,
	U3CTextScrollU3Ed__14_tA8FFAA4799C58B02C4E24F5B9ED15F20312E32C3_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3FEFEE3C8052FA546CF052470E36DCFD51824CC,
	U3CTextScrollU3Ed__14_tA8FFAA4799C58B02C4E24F5B9ED15F20312E32C3_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m10F6AFF907AA15D3F12357D6D000715B2FD102B9,
	U3CTextScrollU3Ed__14_tA8FFAA4799C58B02C4E24F5B9ED15F20312E32C3_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mF851251A598615B608406828EC8BB974DB7616B3,
	DialogueManager9_t267494FE0F8A0076B623B6C66903F8152735B734_CustomAttributesCacheGenerator_DialogueManager9_TextScroll_m6A595793A9FCDE637C136F78B85D75D9455ECF19,
	U3CTextScrollU3Ed__14_tD25DEB4F943540119F24078DA82B738A977553A6_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14__ctor_mF3E5A9448661287179E4EA31143CF930B9359943,
	U3CTextScrollU3Ed__14_tD25DEB4F943540119F24078DA82B738A977553A6_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m3112D9AA996F620897BE1A44084071E371DCF98F,
	U3CTextScrollU3Ed__14_tD25DEB4F943540119F24078DA82B738A977553A6_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C55D1856B48DA623AC540EA0215DD4D997EC254,
	U3CTextScrollU3Ed__14_tD25DEB4F943540119F24078DA82B738A977553A6_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_mC7D4E6F771B2947EB0A45186D5F769EEFD992FDC,
	U3CTextScrollU3Ed__14_tD25DEB4F943540119F24078DA82B738A977553A6_CustomAttributesCacheGenerator_U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mF4C9073C4148A84F5564C1CD781597BE870B28A7,
	BossHealth_bat_t3A01B528FB0D0F8B5CA18A0CD45F479D02530194_CustomAttributesCacheGenerator_BossHealth_bat_EnemyActiveOff_m0E6061CF40413FD702604226B2C8CB6CCD6AC732,
	U3CEnemyActiveOffU3Ed__17_t7AB5E724E27EC4168E7270B7CDBFD943849D21CB_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17__ctor_mA577EC9EA8948E1D60DA759F2D296C5044F54096,
	U3CEnemyActiveOffU3Ed__17_t7AB5E724E27EC4168E7270B7CDBFD943849D21CB_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_mE52F244C415FAAB8E042303C9A8BE828C153F6E4,
	U3CEnemyActiveOffU3Ed__17_t7AB5E724E27EC4168E7270B7CDBFD943849D21CB_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m794E31662324F3791521289BCBE1BB477C71F130,
	U3CEnemyActiveOffU3Ed__17_t7AB5E724E27EC4168E7270B7CDBFD943849D21CB_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_mC3BF406E3FF14C5382C3C0C5642F7078C23CAFFF,
	U3CEnemyActiveOffU3Ed__17_t7AB5E724E27EC4168E7270B7CDBFD943849D21CB_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m3A95CFD2F8CA16B187FBEF68F07D730F36A098F7,
	BossHealth_bear_t1C8229963E4A243DA7ABFBD7F8582BBFBEAF2918_CustomAttributesCacheGenerator_BossHealth_bear_EnemyActiveOff_m2F45DF2FA4E4D971EA4042766EA11222FD30CC8A,
	U3CEnemyActiveOffU3Ed__17_t95404BB5A4134F5B62C67289E8DD94C5C9E69FD3_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17__ctor_m464AE6200A1E122F7A40E31654B6CCD9E7776279,
	U3CEnemyActiveOffU3Ed__17_t95404BB5A4134F5B62C67289E8DD94C5C9E69FD3_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_m253B9F6D14FFB4368754C68A0B731BDE59F82626,
	U3CEnemyActiveOffU3Ed__17_t95404BB5A4134F5B62C67289E8DD94C5C9E69FD3_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD776DC29AC55AD6852773C217E3FCC42429CA3B8,
	U3CEnemyActiveOffU3Ed__17_t95404BB5A4134F5B62C67289E8DD94C5C9E69FD3_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m5D62DE7F1C1F40D685BEC2EBFEA3BDA71D8B5E8D,
	U3CEnemyActiveOffU3Ed__17_t95404BB5A4134F5B62C67289E8DD94C5C9E69FD3_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m8CAD4C264644BCD9C4EAE254FB2D21F63C5FCF0F,
	BossHealth_bee_t5F68827FC9DDC575F069A010DF9AB0EE7123DD63_CustomAttributesCacheGenerator_BossHealth_bee_EnemyActiveOff_m2A0A8EC50551CC68290E2D4D4569AC31159ACA7D,
	U3CEnemyActiveOffU3Ed__17_t989DFCAF68BD051B69CBA1C02D74A7C43368C8D7_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17__ctor_mC83BEBA335A88147D5893E6528372BF833DC7D2C,
	U3CEnemyActiveOffU3Ed__17_t989DFCAF68BD051B69CBA1C02D74A7C43368C8D7_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_m0A8398FF581CE322BA9D7D845297507334D68646,
	U3CEnemyActiveOffU3Ed__17_t989DFCAF68BD051B69CBA1C02D74A7C43368C8D7_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46706DBCDBF6B07645423CCC5A194AAE73928688,
	U3CEnemyActiveOffU3Ed__17_t989DFCAF68BD051B69CBA1C02D74A7C43368C8D7_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m2AEA766F9011936F0D23BF9592ABFF38B737EAED,
	U3CEnemyActiveOffU3Ed__17_t989DFCAF68BD051B69CBA1C02D74A7C43368C8D7_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m98824C77418948E11B692A5274B598916C00113D,
	BossHealth_guide_t8A1226CD7EA2DE00D8FC3C3925FBF16FAC7413BD_CustomAttributesCacheGenerator_BossHealth_guide_EnemyActiveOff_m57EA0B9D50AC33B4E0ABDB50F9483C29D89ACF91,
	U3CEnemyActiveOffU3Ed__17_t090E1E848E32CB9838DEE9486F4AD27607A95648_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17__ctor_m09EFDED0A4E6C509759C171FC612A7A4190C362A,
	U3CEnemyActiveOffU3Ed__17_t090E1E848E32CB9838DEE9486F4AD27607A95648_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_m1F58E811E2CDB854E5C6DB1FC351391AD8881B1D,
	U3CEnemyActiveOffU3Ed__17_t090E1E848E32CB9838DEE9486F4AD27607A95648_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4E97E597A05A689D1F935145EF371638BB8972C8,
	U3CEnemyActiveOffU3Ed__17_t090E1E848E32CB9838DEE9486F4AD27607A95648_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m213302082DF8640A57F8F8D259FEDCB319F1564A,
	U3CEnemyActiveOffU3Ed__17_t090E1E848E32CB9838DEE9486F4AD27607A95648_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_mF3B08ECAA4BDC5A885CF6D75480B2D30BBBD2365,
	BossHealth_octopus_t1BB7720A9CB7293BF024FCBB3DEA14970DEB669D_CustomAttributesCacheGenerator_BossHealth_octopus_EnemyActiveOff_mF20C14DC9DE90D938AE47F959C8C3181FE9F3DE6,
	U3CEnemyActiveOffU3Ed__16_t4AC26D9F732F008DD8073C8CE52FE9581ED94A82_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__16__ctor_m656F1D29A891EF8964C433F09D84AFD32EF87E91,
	U3CEnemyActiveOffU3Ed__16_t4AC26D9F732F008DD8073C8CE52FE9581ED94A82_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__16_System_IDisposable_Dispose_m40B57F8B7DC59586C6D0BD37F92FFF21F0612FF5,
	U3CEnemyActiveOffU3Ed__16_t4AC26D9F732F008DD8073C8CE52FE9581ED94A82_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m014CC50FCD57228D47D667F2A4D3D5B3CE9A13F0,
	U3CEnemyActiveOffU3Ed__16_t4AC26D9F732F008DD8073C8CE52FE9581ED94A82_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__16_System_Collections_IEnumerator_Reset_m08A275A350536FF546323FFCAB9EB2BE90D166A8,
	U3CEnemyActiveOffU3Ed__16_t4AC26D9F732F008DD8073C8CE52FE9581ED94A82_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__16_System_Collections_IEnumerator_get_Current_m69BAF2DE3C9991F838E0FAEB774C31E5F75958EB,
	BossHealth_tFA863FC454D56EBCFF129D076CA4AD68724B1112_CustomAttributesCacheGenerator_BossHealth_EnemyActiveOff_mF760A81E37497AF5E86198E6A3FAA7BB5391D9A4,
	U3CEnemyActiveOffU3Ed__17_t1104DECC029E3AAF5304A9D29AF5A680FF9AE010_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17__ctor_mEDA113BF186DC2339F8580F67643AB56E1ECE556,
	U3CEnemyActiveOffU3Ed__17_t1104DECC029E3AAF5304A9D29AF5A680FF9AE010_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_mD06651F2C538A3224C9FDDE42A9012B321C56492,
	U3CEnemyActiveOffU3Ed__17_t1104DECC029E3AAF5304A9D29AF5A680FF9AE010_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m06B0E0AA273012C9CCD56BB02407E66467557687,
	U3CEnemyActiveOffU3Ed__17_t1104DECC029E3AAF5304A9D29AF5A680FF9AE010_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m2CB85D39D8F8BE0BE1EF9576A51AD73338551C25,
	U3CEnemyActiveOffU3Ed__17_t1104DECC029E3AAF5304A9D29AF5A680FF9AE010_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m60DBC26F568D55337E46ED8A3513605501E94744,
	BossHealth_ske_t267F54366240ACE4BBBAD079FCB1B0CE6640B627_CustomAttributesCacheGenerator_BossHealth_ske_EnemyActiveOff_m5F04512A0BE51B250EBDC07BF4E5A8184A1E40D6,
	U3CEnemyActiveOffU3Ed__17_t43D619270E85C4030AD2A6F1C1D98AC33720EB34_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17__ctor_mD56FDF58CF76DF6DCF6A0C4CDE06B265665DCCF8,
	U3CEnemyActiveOffU3Ed__17_t43D619270E85C4030AD2A6F1C1D98AC33720EB34_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_mBBBA8BA4C90EFD0254A9D54C860BE67A49C0EDA7,
	U3CEnemyActiveOffU3Ed__17_t43D619270E85C4030AD2A6F1C1D98AC33720EB34_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE5B73C806B8B5F2F2A7CF1822106AD4136D7783,
	U3CEnemyActiveOffU3Ed__17_t43D619270E85C4030AD2A6F1C1D98AC33720EB34_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m765DD1E9FEB89FDD28649CC19B8F00F4C20CD5E9,
	U3CEnemyActiveOffU3Ed__17_t43D619270E85C4030AD2A6F1C1D98AC33720EB34_CustomAttributesCacheGenerator_U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m0F1D88A2309A0F825E691AE1BF307DE9D2EED219,
	GameOverScreen_t66B7C93D73D16C603457FEA3A5424F7DDB6B68A7_CustomAttributesCacheGenerator_GameOverScreen_TimeIE_mB42A38029BD1334D965780125E80522703D0FA43,
	GameOverScreen_t66B7C93D73D16C603457FEA3A5424F7DDB6B68A7_CustomAttributesCacheGenerator_GameOverScreen_LoadAsynchronously_m2197BDDACA1C511BBDAF27424927DEFE650E54B1,
	U3CTimeIEU3Ed__8_t2248B562C3607F1D7051857BA7F8109E6F353FA0_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__8__ctor_mAA3177335E8BFA01622B55BE8DCB2CEF5C18261E,
	U3CTimeIEU3Ed__8_t2248B562C3607F1D7051857BA7F8109E6F353FA0_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__8_System_IDisposable_Dispose_m090F29683C88433E5BA4F5FAE98DA1997CA167E5,
	U3CTimeIEU3Ed__8_t2248B562C3607F1D7051857BA7F8109E6F353FA0_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99EC495C6BCDAC6E1162941E07936462B1BD4E2D,
	U3CTimeIEU3Ed__8_t2248B562C3607F1D7051857BA7F8109E6F353FA0_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__8_System_Collections_IEnumerator_Reset_m9BE542B45B26F5C2D6EB34B44370BCCAB9D5D3E2,
	U3CTimeIEU3Ed__8_t2248B562C3607F1D7051857BA7F8109E6F353FA0_CustomAttributesCacheGenerator_U3CTimeIEU3Ed__8_System_Collections_IEnumerator_get_Current_m0B2BFFB0D772E8E233ECDF8F858655AE9A4D03C1,
	U3CLoadAsynchronouslyU3Ed__10_t96EF2F6BC38FD48D0853B90F403D5486EFFB3743_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__10__ctor_mF01B26F4D4B06F59BA1A098B17A25DCFE6E42DE9,
	U3CLoadAsynchronouslyU3Ed__10_t96EF2F6BC38FD48D0853B90F403D5486EFFB3743_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__10_System_IDisposable_Dispose_m88D1C07D91A4DA9A053C84DE67353199F14E6047,
	U3CLoadAsynchronouslyU3Ed__10_t96EF2F6BC38FD48D0853B90F403D5486EFFB3743_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF15B16A4766E1121E2355C87404FB6A1EF43AF3A,
	U3CLoadAsynchronouslyU3Ed__10_t96EF2F6BC38FD48D0853B90F403D5486EFFB3743_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__10_System_Collections_IEnumerator_Reset_m2EB3DC3D189F6976EFDB9B847CE615472B0F9BFA,
	U3CLoadAsynchronouslyU3Ed__10_t96EF2F6BC38FD48D0853B90F403D5486EFFB3743_CustomAttributesCacheGenerator_U3CLoadAsynchronouslyU3Ed__10_System_Collections_IEnumerator_get_Current_mA40DA494789036083420705DAC23DAE8C5D80B08,
	SaveManager_tFCE7930FC081EEE88E12994B53512095F258CBCE_CustomAttributesCacheGenerator_SaveManager_SavingTime_m609D3CFA5870774AF4E7B15546B731113A8A19A8,
	U3CSavingTimeU3Ed__15_tB688C282B8B0A9C6363A6F10C310BAF1BC084883_CustomAttributesCacheGenerator_U3CSavingTimeU3Ed__15__ctor_m2757447DB09BEDAA4D2FFF6BEB20DAAB8C5A9446,
	U3CSavingTimeU3Ed__15_tB688C282B8B0A9C6363A6F10C310BAF1BC084883_CustomAttributesCacheGenerator_U3CSavingTimeU3Ed__15_System_IDisposable_Dispose_mF18CE4056508CE8E40BCFD44E8E2B6700C4E14BD,
	U3CSavingTimeU3Ed__15_tB688C282B8B0A9C6363A6F10C310BAF1BC084883_CustomAttributesCacheGenerator_U3CSavingTimeU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3B119E99743AFF41A4AB2C0507A755DE9F1E08FD,
	U3CSavingTimeU3Ed__15_tB688C282B8B0A9C6363A6F10C310BAF1BC084883_CustomAttributesCacheGenerator_U3CSavingTimeU3Ed__15_System_Collections_IEnumerator_Reset_m7C522780FA216172DBA8E07C9890043D4F7C692C,
	U3CSavingTimeU3Ed__15_tB688C282B8B0A9C6363A6F10C310BAF1BC084883_CustomAttributesCacheGenerator_U3CSavingTimeU3Ed__15_System_Collections_IEnumerator_get_Current_mF59823DEC376A77E23F5CECB85123B68B1E32572,
	CharacterController_t05F5FF47FAD01F995B6C7F57524F2DEC9E282FC6_CustomAttributesCacheGenerator_CharacterController_Reload_m603D8E5810464FFAD8C80C20FA6C4E8ED61C4E38,
	CharacterController_t05F5FF47FAD01F995B6C7F57524F2DEC9E282FC6_CustomAttributesCacheGenerator_CharacterController_WaitforDashCD_mD1C0B5349BC9E07C31AFEF439AE3FEDA192C1625,
	CharacterController_t05F5FF47FAD01F995B6C7F57524F2DEC9E282FC6_CustomAttributesCacheGenerator_CharacterController_WaitforAttackCD_mB07CC382323E16A70F7EA4E06A2A61A9EA44B01D,
	CharacterController_t05F5FF47FAD01F995B6C7F57524F2DEC9E282FC6_CustomAttributesCacheGenerator_CharacterController_WaitforSlow_m4306A08C07FBF80952E7999EA8FA6AADEDA79850,
	CharacterController_t05F5FF47FAD01F995B6C7F57524F2DEC9E282FC6_CustomAttributesCacheGenerator_CharacterController_WaitforControl_m99E2CC6C84046A3D3153772CF58AB8080851B1E1,
	CharacterController_t05F5FF47FAD01F995B6C7F57524F2DEC9E282FC6_CustomAttributesCacheGenerator_CharacterController_TimeBuff_m92A4C732E993B314804D911463AD777E99D0783C,
	U3CReloadU3Ed__38_t4DF7DFFCF5D53852FEB6334B1D83CDCB9136AA1E_CustomAttributesCacheGenerator_U3CReloadU3Ed__38__ctor_mA2475D6EA0EECC4ED1C1DEDDF436609500668E43,
	U3CReloadU3Ed__38_t4DF7DFFCF5D53852FEB6334B1D83CDCB9136AA1E_CustomAttributesCacheGenerator_U3CReloadU3Ed__38_System_IDisposable_Dispose_m189DF1F4181A8E7C4A6442EA9A8BFE394CC0EA1A,
	U3CReloadU3Ed__38_t4DF7DFFCF5D53852FEB6334B1D83CDCB9136AA1E_CustomAttributesCacheGenerator_U3CReloadU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m817CD53DA8749F94A166EBDA976731072045001E,
	U3CReloadU3Ed__38_t4DF7DFFCF5D53852FEB6334B1D83CDCB9136AA1E_CustomAttributesCacheGenerator_U3CReloadU3Ed__38_System_Collections_IEnumerator_Reset_m473199BCE668261B4718B1703F368D13D6D718D8,
	U3CReloadU3Ed__38_t4DF7DFFCF5D53852FEB6334B1D83CDCB9136AA1E_CustomAttributesCacheGenerator_U3CReloadU3Ed__38_System_Collections_IEnumerator_get_Current_mB53EB3667351EDD280A4DC0377073DF5889818AC,
	U3CWaitforDashCDU3Ed__39_t14E13865EE683795695090D3455320127C937F80_CustomAttributesCacheGenerator_U3CWaitforDashCDU3Ed__39__ctor_m373960E38BF24499999824042B6B18CC330FCDFB,
	U3CWaitforDashCDU3Ed__39_t14E13865EE683795695090D3455320127C937F80_CustomAttributesCacheGenerator_U3CWaitforDashCDU3Ed__39_System_IDisposable_Dispose_mEAA348CAEDF9CFACC0549AEB129346432A5E5E57,
	U3CWaitforDashCDU3Ed__39_t14E13865EE683795695090D3455320127C937F80_CustomAttributesCacheGenerator_U3CWaitforDashCDU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54214D324F2636F1476FD91F8DEF57A419313AC6,
	U3CWaitforDashCDU3Ed__39_t14E13865EE683795695090D3455320127C937F80_CustomAttributesCacheGenerator_U3CWaitforDashCDU3Ed__39_System_Collections_IEnumerator_Reset_m0AA6D3E47AD5870D0DC147AAD6E30D775DB22655,
	U3CWaitforDashCDU3Ed__39_t14E13865EE683795695090D3455320127C937F80_CustomAttributesCacheGenerator_U3CWaitforDashCDU3Ed__39_System_Collections_IEnumerator_get_Current_mE97F155EF767F943F9AEB8CA7FD0A3A83DE8F5FA,
	U3CWaitforAttackCDU3Ed__40_tF43E5B4B83DEE1A35B02B69DEBA359A94D551D49_CustomAttributesCacheGenerator_U3CWaitforAttackCDU3Ed__40__ctor_mDCD5FFEE4BA5AF5F59FDF9872D00947AEA244881,
	U3CWaitforAttackCDU3Ed__40_tF43E5B4B83DEE1A35B02B69DEBA359A94D551D49_CustomAttributesCacheGenerator_U3CWaitforAttackCDU3Ed__40_System_IDisposable_Dispose_m92D63B81EA8760D3EFDF4923517EB60B4668C27C,
	U3CWaitforAttackCDU3Ed__40_tF43E5B4B83DEE1A35B02B69DEBA359A94D551D49_CustomAttributesCacheGenerator_U3CWaitforAttackCDU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D221862ED79DE7E719959FA79DF0F7C3FADBE37,
	U3CWaitforAttackCDU3Ed__40_tF43E5B4B83DEE1A35B02B69DEBA359A94D551D49_CustomAttributesCacheGenerator_U3CWaitforAttackCDU3Ed__40_System_Collections_IEnumerator_Reset_mC58183DFB9DC872EE95A3067AA0481D4B5704012,
	U3CWaitforAttackCDU3Ed__40_tF43E5B4B83DEE1A35B02B69DEBA359A94D551D49_CustomAttributesCacheGenerator_U3CWaitforAttackCDU3Ed__40_System_Collections_IEnumerator_get_Current_m047CBA26B024B9420C0A608271EE253FEF0379BF,
	U3CWaitforSlowU3Ed__41_t601851BD5C43BACFE807E44C1FB1DA9CBC028E6F_CustomAttributesCacheGenerator_U3CWaitforSlowU3Ed__41__ctor_m3A69FF99D4BA5414697B5DC9F6800C9F29BD0058,
	U3CWaitforSlowU3Ed__41_t601851BD5C43BACFE807E44C1FB1DA9CBC028E6F_CustomAttributesCacheGenerator_U3CWaitforSlowU3Ed__41_System_IDisposable_Dispose_mC215FD97FA1DCA7AF535F08F89A76462D2666C0E,
	U3CWaitforSlowU3Ed__41_t601851BD5C43BACFE807E44C1FB1DA9CBC028E6F_CustomAttributesCacheGenerator_U3CWaitforSlowU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m48935FB12DC00EC4301931A97F3BD21303DFF5EB,
	U3CWaitforSlowU3Ed__41_t601851BD5C43BACFE807E44C1FB1DA9CBC028E6F_CustomAttributesCacheGenerator_U3CWaitforSlowU3Ed__41_System_Collections_IEnumerator_Reset_mD63E6D9F2D797CCAB2BF6ABFB4F7F16AF9A80476,
	U3CWaitforSlowU3Ed__41_t601851BD5C43BACFE807E44C1FB1DA9CBC028E6F_CustomAttributesCacheGenerator_U3CWaitforSlowU3Ed__41_System_Collections_IEnumerator_get_Current_mE796B7B00AAF4F9E4193550E90652205D3386C6A,
	U3CWaitforControlU3Ed__42_t4BC1189F93B40CC735F2DF62FD76971BF7CAC346_CustomAttributesCacheGenerator_U3CWaitforControlU3Ed__42__ctor_mC555FCAC674AF43AA06D04B7A60FDE434485CC40,
	U3CWaitforControlU3Ed__42_t4BC1189F93B40CC735F2DF62FD76971BF7CAC346_CustomAttributesCacheGenerator_U3CWaitforControlU3Ed__42_System_IDisposable_Dispose_m339BD26FF42F0EFA072A57648700421AE65D59FE,
	U3CWaitforControlU3Ed__42_t4BC1189F93B40CC735F2DF62FD76971BF7CAC346_CustomAttributesCacheGenerator_U3CWaitforControlU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55BE11D576B1F7839E3386CAB7EA471C71468E39,
	U3CWaitforControlU3Ed__42_t4BC1189F93B40CC735F2DF62FD76971BF7CAC346_CustomAttributesCacheGenerator_U3CWaitforControlU3Ed__42_System_Collections_IEnumerator_Reset_mB02A4E799DAE3A7464F8CC2965C7B4C8DD022AEC,
	U3CWaitforControlU3Ed__42_t4BC1189F93B40CC735F2DF62FD76971BF7CAC346_CustomAttributesCacheGenerator_U3CWaitforControlU3Ed__42_System_Collections_IEnumerator_get_Current_m45AD58E82837EA91D4BE2D8BCCBFAE55F9D980A4,
	U3CTimeBuffU3Ed__45_t0BE322FFFF97614A730EBA98616AD9AD1615617D_CustomAttributesCacheGenerator_U3CTimeBuffU3Ed__45__ctor_mD2E814F622252304651A2143045D1EDFF9C71040,
	U3CTimeBuffU3Ed__45_t0BE322FFFF97614A730EBA98616AD9AD1615617D_CustomAttributesCacheGenerator_U3CTimeBuffU3Ed__45_System_IDisposable_Dispose_mB60A2FE267BE49001CDDABA8D2C115FCB66578E2,
	U3CTimeBuffU3Ed__45_t0BE322FFFF97614A730EBA98616AD9AD1615617D_CustomAttributesCacheGenerator_U3CTimeBuffU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5A7519E0C87FC70F54936B9B2A057C6A5E8D0B79,
	U3CTimeBuffU3Ed__45_t0BE322FFFF97614A730EBA98616AD9AD1615617D_CustomAttributesCacheGenerator_U3CTimeBuffU3Ed__45_System_Collections_IEnumerator_Reset_mCD2C34CDD9B9C71AFE577EC290324C4B1B24CA93,
	U3CTimeBuffU3Ed__45_t0BE322FFFF97614A730EBA98616AD9AD1615617D_CustomAttributesCacheGenerator_U3CTimeBuffU3Ed__45_System_Collections_IEnumerator_get_Current_mE8B7421BCD2F31F4EB76A037245B241D6AAD16FD,
	HealthPlayer_t8E5151459604EFF1BAF84506752D4C0EC7A2CD0B_CustomAttributesCacheGenerator_HealthPlayer_PlayerActiveOff_m3C8156E1BA703EECEBC135A24986704A788C0481,
	HealthPlayer_t8E5151459604EFF1BAF84506752D4C0EC7A2CD0B_CustomAttributesCacheGenerator_HealthPlayer_WaitforBurningTwiceTimes_m9B8D329D3F3ED2A06387ED52212B7329CCA72ED9,
	HealthPlayer_t8E5151459604EFF1BAF84506752D4C0EC7A2CD0B_CustomAttributesCacheGenerator_HealthPlayer_WaitforBurningManyTimes_m21FD33E08F5D2F6F6CA1F1A321B405E71DC855FC,
	U3CPlayerActiveOffU3Ed__17_tB4A573BE9F9DDBD451F4A9F4E97C1AF3413958AA_CustomAttributesCacheGenerator_U3CPlayerActiveOffU3Ed__17__ctor_m9A9DB2B04BFA578AC10BB1E9B790FE0BE9B8693F,
	U3CPlayerActiveOffU3Ed__17_tB4A573BE9F9DDBD451F4A9F4E97C1AF3413958AA_CustomAttributesCacheGenerator_U3CPlayerActiveOffU3Ed__17_System_IDisposable_Dispose_mCB84FD0D750104FB2CF907E87DD05F7AE8AB9C63,
	U3CPlayerActiveOffU3Ed__17_tB4A573BE9F9DDBD451F4A9F4E97C1AF3413958AA_CustomAttributesCacheGenerator_U3CPlayerActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m317F910FAB3ED1F3E76932F9603EB1798D79D3C8,
	U3CPlayerActiveOffU3Ed__17_tB4A573BE9F9DDBD451F4A9F4E97C1AF3413958AA_CustomAttributesCacheGenerator_U3CPlayerActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_mC1EAC213C3F0424F2DE7AF7D4F973287EE125DC6,
	U3CPlayerActiveOffU3Ed__17_tB4A573BE9F9DDBD451F4A9F4E97C1AF3413958AA_CustomAttributesCacheGenerator_U3CPlayerActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_mB0F263F4102A3C2C994B02E3F15FAA5F083BF625,
	U3CWaitforBurningTwiceTimesU3Ed__23_t6BEEC8618296DA43B378F2842EA8448939DB4F74_CustomAttributesCacheGenerator_U3CWaitforBurningTwiceTimesU3Ed__23__ctor_mF7EA250EF31784D08B73C5490C7851D93532DBC8,
	U3CWaitforBurningTwiceTimesU3Ed__23_t6BEEC8618296DA43B378F2842EA8448939DB4F74_CustomAttributesCacheGenerator_U3CWaitforBurningTwiceTimesU3Ed__23_System_IDisposable_Dispose_m1409B5C7D84CC7569BAE724C971E338953FB0426,
	U3CWaitforBurningTwiceTimesU3Ed__23_t6BEEC8618296DA43B378F2842EA8448939DB4F74_CustomAttributesCacheGenerator_U3CWaitforBurningTwiceTimesU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF9557D405C2B28E0125F2748C6B3C41F5023B0E,
	U3CWaitforBurningTwiceTimesU3Ed__23_t6BEEC8618296DA43B378F2842EA8448939DB4F74_CustomAttributesCacheGenerator_U3CWaitforBurningTwiceTimesU3Ed__23_System_Collections_IEnumerator_Reset_m2050273B00A7E0222167D456BA00C5168F33162E,
	U3CWaitforBurningTwiceTimesU3Ed__23_t6BEEC8618296DA43B378F2842EA8448939DB4F74_CustomAttributesCacheGenerator_U3CWaitforBurningTwiceTimesU3Ed__23_System_Collections_IEnumerator_get_Current_m9815C990320876FBB5FDB0D542B3982C070CD026,
	U3CWaitforBurningManyTimesU3Ed__24_t95FAAD6C2B8E8CDC9951F60AC1E8325FB3F04ABE_CustomAttributesCacheGenerator_U3CWaitforBurningManyTimesU3Ed__24__ctor_mBFAA5317CEA1FBA4BB4652CE2599F754749F50C1,
	U3CWaitforBurningManyTimesU3Ed__24_t95FAAD6C2B8E8CDC9951F60AC1E8325FB3F04ABE_CustomAttributesCacheGenerator_U3CWaitforBurningManyTimesU3Ed__24_System_IDisposable_Dispose_m0389C895AF95C024112A1A53241A12DDD87CCFDA,
	U3CWaitforBurningManyTimesU3Ed__24_t95FAAD6C2B8E8CDC9951F60AC1E8325FB3F04ABE_CustomAttributesCacheGenerator_U3CWaitforBurningManyTimesU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09F3C5A63D58ADDAB507EAD2BA87F7F7CA197A9C,
	U3CWaitforBurningManyTimesU3Ed__24_t95FAAD6C2B8E8CDC9951F60AC1E8325FB3F04ABE_CustomAttributesCacheGenerator_U3CWaitforBurningManyTimesU3Ed__24_System_Collections_IEnumerator_Reset_m5E755DF2A24BE189979B619CD9A68E99006AD458,
	U3CWaitforBurningManyTimesU3Ed__24_t95FAAD6C2B8E8CDC9951F60AC1E8325FB3F04ABE_CustomAttributesCacheGenerator_U3CWaitforBurningManyTimesU3Ed__24_System_Collections_IEnumerator_get_Current_m56DAFC8768C4E1A1B229004A178F9DD5552DF281,
	Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4_CustomAttributesCacheGenerator_Knockback_KnockCo_m895B2CCA6560515B4ECBD06F62DE1FD5DA034A25,
	U3CKnockCoU3Ed__3_t2484562B1B4BC1C411DE1DC6A9A737CAAB9754F3_CustomAttributesCacheGenerator_U3CKnockCoU3Ed__3__ctor_m13966A956093B208EA961EB6AF631B6749075141,
	U3CKnockCoU3Ed__3_t2484562B1B4BC1C411DE1DC6A9A737CAAB9754F3_CustomAttributesCacheGenerator_U3CKnockCoU3Ed__3_System_IDisposable_Dispose_mD43C8AE1C391E9406E80789A9543D352B5825BC0,
	U3CKnockCoU3Ed__3_t2484562B1B4BC1C411DE1DC6A9A737CAAB9754F3_CustomAttributesCacheGenerator_U3CKnockCoU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D198094D719D238ED5864BEDAB86C06F7144BEA,
	U3CKnockCoU3Ed__3_t2484562B1B4BC1C411DE1DC6A9A737CAAB9754F3_CustomAttributesCacheGenerator_U3CKnockCoU3Ed__3_System_Collections_IEnumerator_Reset_mCCB2797D5B3C57E35C3864E861573DF29FC19101,
	U3CKnockCoU3Ed__3_t2484562B1B4BC1C411DE1DC6A9A737CAAB9754F3_CustomAttributesCacheGenerator_U3CKnockCoU3Ed__3_System_Collections_IEnumerator_get_Current_mFCE1024C9949576CB9507452F41AFFC219D78739,
	EnvMapAnimator_t7C30A094A3786710A896AD9A6FA46B13EFEB72FD_CustomAttributesCacheGenerator_EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE,
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23,
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B,
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E,
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB,
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF,
	Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934_CustomAttributesCacheGenerator_Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C,
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB,
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73,
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591,
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B,
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E,
	Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B_CustomAttributesCacheGenerator_Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4,
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651,
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561,
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD,
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA,
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2,
	ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8_CustomAttributesCacheGenerator_ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469,
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A,
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B,
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390,
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B,
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362,
	SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07_CustomAttributesCacheGenerator_SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3,
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F,
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF,
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1,
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F,
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A,
	TeleType_t92B5ED4FE818F912A562F085CE035F6B42834934_CustomAttributesCacheGenerator_TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7,
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629,
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9,
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B,
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8,
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261,
	TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289_CustomAttributesCacheGenerator_TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534,
	TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289_CustomAttributesCacheGenerator_TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4,
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D,
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C,
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83,
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691,
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F,
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0,
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8,
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB,
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F,
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27,
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200_CustomAttributesCacheGenerator_TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7,
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200_CustomAttributesCacheGenerator_TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B,
	U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20,
	U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3,
	U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598,
	U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5,
	U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6,
	VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374_CustomAttributesCacheGenerator_VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42,
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F,
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007,
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51,
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932,
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52,
	VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A_CustomAttributesCacheGenerator_VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16,
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8,
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099,
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479,
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D,
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF,
	VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A_CustomAttributesCacheGenerator_VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599,
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2,
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81,
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE,
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA,
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E,
	VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9_CustomAttributesCacheGenerator_VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87,
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760,
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC,
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D,
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB,
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD,
	VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163_CustomAttributesCacheGenerator_VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED,
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2,
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F,
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9,
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C,
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F,
	WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96_CustomAttributesCacheGenerator_WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2,
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49,
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60,
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1,
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535,
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
