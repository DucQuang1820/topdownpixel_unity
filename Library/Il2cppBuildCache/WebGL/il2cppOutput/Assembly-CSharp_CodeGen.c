﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void IntroManager::Start()
extern void IntroManager_Start_mEE7D131DA47D9DB46EF36B199B4921C558D0B2DA (void);
// 0x00000002 System.Collections.IEnumerator IntroManager::TimeIE()
extern void IntroManager_TimeIE_m8671693B4F354CA40F239A02EB160E5EE906CB40 (void);
// 0x00000003 System.Void IntroManager::Update()
extern void IntroManager_Update_m0521B2C4306FD5EBB11AB46F21C6973BC2698DFE (void);
// 0x00000004 System.Void IntroManager::StartIntro()
extern void IntroManager_StartIntro_mE124E82AEFDC00993ACCA86D756CDA2D08D66D6B (void);
// 0x00000005 System.Collections.IEnumerator IntroManager::TypeLine()
extern void IntroManager_TypeLine_m2D1AB9015F67B6094D56BCD80BB4198A1615C4A6 (void);
// 0x00000006 System.Void IntroManager::NextLine()
extern void IntroManager_NextLine_m0DD36CA3163BDEFCFF8E40265DB46C98AC7E4217 (void);
// 0x00000007 System.Void IntroManager::.ctor()
extern void IntroManager__ctor_m4AA6A59302DF7976FD91092B24897D630BA33860 (void);
// 0x00000008 System.Void IntroManager/<TimeIE>d__10::.ctor(System.Int32)
extern void U3CTimeIEU3Ed__10__ctor_m11C8A6394695414B03FF139147D221BE9CC7F656 (void);
// 0x00000009 System.Void IntroManager/<TimeIE>d__10::System.IDisposable.Dispose()
extern void U3CTimeIEU3Ed__10_System_IDisposable_Dispose_mC05D66A28083727BFEE3EF10506098A204A03409 (void);
// 0x0000000A System.Boolean IntroManager/<TimeIE>d__10::MoveNext()
extern void U3CTimeIEU3Ed__10_MoveNext_m2FB9DAEE831452143F60B4F1EAB2F47AD23F223E (void);
// 0x0000000B System.Object IntroManager/<TimeIE>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTimeIEU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m25917A42988161EEBA1271CF3B23AEB337543CA9 (void);
// 0x0000000C System.Void IntroManager/<TimeIE>d__10::System.Collections.IEnumerator.Reset()
extern void U3CTimeIEU3Ed__10_System_Collections_IEnumerator_Reset_m65DAD9114DAB93BE8D5BB1C625F1FB2CF0F0D472 (void);
// 0x0000000D System.Object IntroManager/<TimeIE>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CTimeIEU3Ed__10_System_Collections_IEnumerator_get_Current_m2A25C3B0223067B36A377B4C3FDAE686F28D539E (void);
// 0x0000000E System.Void IntroManager/<TypeLine>d__13::.ctor(System.Int32)
extern void U3CTypeLineU3Ed__13__ctor_mB4362558C8CDE1FCF2DB18EE13D09F9A77800DA3 (void);
// 0x0000000F System.Void IntroManager/<TypeLine>d__13::System.IDisposable.Dispose()
extern void U3CTypeLineU3Ed__13_System_IDisposable_Dispose_mBF2CA07B9929F52BB9D6757B4ABBEBB156D51C92 (void);
// 0x00000010 System.Boolean IntroManager/<TypeLine>d__13::MoveNext()
extern void U3CTypeLineU3Ed__13_MoveNext_m618F7BB14CED5AC9DE24B89DCC376AE11F2610DD (void);
// 0x00000011 System.Object IntroManager/<TypeLine>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTypeLineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6016236D9EEC958D1B15C210D6D80897805FEE75 (void);
// 0x00000012 System.Void IntroManager/<TypeLine>d__13::System.Collections.IEnumerator.Reset()
extern void U3CTypeLineU3Ed__13_System_Collections_IEnumerator_Reset_m20BF6AA37542A6DE14CE8606807FBA0E4EFD2815 (void);
// 0x00000013 System.Object IntroManager/<TypeLine>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CTypeLineU3Ed__13_System_Collections_IEnumerator_get_Current_m623B9FE74115FEF5D16925CEE1E5397C12B38BCB (void);
// 0x00000014 System.Void SoundManager::Awake()
extern void SoundManager_Awake_m78F953F39CFB3F539240E1226D04270B793B1A76 (void);
// 0x00000015 System.Void SoundManager::PlayClip(UnityEngine.AudioClip)
extern void SoundManager_PlayClip_mF074B01B915517909B27888DEBE54893372B7D07 (void);
// 0x00000016 System.Void SoundManager::StopClip(UnityEngine.AudioClip)
extern void SoundManager_StopClip_m2FD9C367D9D5A660C1FE9C96B595F935C6FB8EC2 (void);
// 0x00000017 System.Void SoundManager::.ctor()
extern void SoundManager__ctor_m30D18BC25871BD3FF7FB195A1CAE50A2EE48FCAE (void);
// 0x00000018 System.Void Story1Manager::Start()
extern void Story1Manager_Start_mFC6658980A0E7698830AEEA540104552CD24351C (void);
// 0x00000019 System.Collections.IEnumerator Story1Manager::TimeIE()
extern void Story1Manager_TimeIE_m9F889DA26E3CF321B6DDD407B42D4EEED1CAEDCD (void);
// 0x0000001A System.Void Story1Manager::Update()
extern void Story1Manager_Update_m5D9A3313BEE6F846D3D5920D9BDB7A939DD64FB5 (void);
// 0x0000001B System.Void Story1Manager::StartStory1()
extern void Story1Manager_StartStory1_mD997E78F37A6B8DC42130A574149F673D424A7E3 (void);
// 0x0000001C System.Collections.IEnumerator Story1Manager::TypeLine()
extern void Story1Manager_TypeLine_m597776B3A41CAA93A0671A6DD7F50DEA37F5E4FC (void);
// 0x0000001D System.Void Story1Manager::NextLine()
extern void Story1Manager_NextLine_mB799D1F17702E3A723BE21CF4D1A8AAB39A68345 (void);
// 0x0000001E System.Void Story1Manager::.ctor()
extern void Story1Manager__ctor_mF3D4F1214C343FC08CFAF4A7FC4B243DBB7F4E18 (void);
// 0x0000001F System.Void Story1Manager/<TimeIE>d__10::.ctor(System.Int32)
extern void U3CTimeIEU3Ed__10__ctor_m2458A1336253AF66749EA49CDCF74B1810C923B6 (void);
// 0x00000020 System.Void Story1Manager/<TimeIE>d__10::System.IDisposable.Dispose()
extern void U3CTimeIEU3Ed__10_System_IDisposable_Dispose_m126FFCFD9B1CD0A1C81AD3798874A380A891518F (void);
// 0x00000021 System.Boolean Story1Manager/<TimeIE>d__10::MoveNext()
extern void U3CTimeIEU3Ed__10_MoveNext_m48BCC750C93C67047AEAFBF04E6A26CFE39C86AA (void);
// 0x00000022 System.Object Story1Manager/<TimeIE>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTimeIEU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA6B0872963BDC898023AD817C434FE51C7854FF9 (void);
// 0x00000023 System.Void Story1Manager/<TimeIE>d__10::System.Collections.IEnumerator.Reset()
extern void U3CTimeIEU3Ed__10_System_Collections_IEnumerator_Reset_mA408B839522C0E46A97723C45EF4DCF246215799 (void);
// 0x00000024 System.Object Story1Manager/<TimeIE>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CTimeIEU3Ed__10_System_Collections_IEnumerator_get_Current_mB2BCCF780BDA64276290CB3080C262B0AF33773A (void);
// 0x00000025 System.Void Story1Manager/<TypeLine>d__13::.ctor(System.Int32)
extern void U3CTypeLineU3Ed__13__ctor_mB8227D04CFD9803A9E3D112D82A1C2CA0320C3AF (void);
// 0x00000026 System.Void Story1Manager/<TypeLine>d__13::System.IDisposable.Dispose()
extern void U3CTypeLineU3Ed__13_System_IDisposable_Dispose_m5DE8B5998D1316BFCFE3D69C8D65AA90C8A4A614 (void);
// 0x00000027 System.Boolean Story1Manager/<TypeLine>d__13::MoveNext()
extern void U3CTypeLineU3Ed__13_MoveNext_m6762DB458F963CC1DB0F8BB4BD414DDA4BFFDC04 (void);
// 0x00000028 System.Object Story1Manager/<TypeLine>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTypeLineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4785A5F20FCC7D15936A974801F1ABF86969FBA4 (void);
// 0x00000029 System.Void Story1Manager/<TypeLine>d__13::System.Collections.IEnumerator.Reset()
extern void U3CTypeLineU3Ed__13_System_Collections_IEnumerator_Reset_m26715E5D4DA17CBB171BE6A64BBB6AFB56E647A4 (void);
// 0x0000002A System.Object Story1Manager/<TypeLine>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CTypeLineU3Ed__13_System_Collections_IEnumerator_get_Current_mFD03E403D694A40D19E16EDC51A696B7E6DB7FFA (void);
// 0x0000002B System.Void Story2Manager::Start()
extern void Story2Manager_Start_m36F27D8A77A69B1A9A7E7763EDC3A36C1E9A0633 (void);
// 0x0000002C System.Collections.IEnumerator Story2Manager::TimeIE()
extern void Story2Manager_TimeIE_m6A9B40A187E64B7BEBE3F30EF100815FBE55FCC2 (void);
// 0x0000002D System.Void Story2Manager::Update()
extern void Story2Manager_Update_m2673C3044A2631E09F87351DC1FAF079E51726D3 (void);
// 0x0000002E System.Void Story2Manager::StartStory2()
extern void Story2Manager_StartStory2_mC762E3ABD120371A21A83228440CF053B6401CE3 (void);
// 0x0000002F System.Collections.IEnumerator Story2Manager::TypeLine()
extern void Story2Manager_TypeLine_m9F81497C07EB9F4A08C1A6761B747F414B723215 (void);
// 0x00000030 System.Void Story2Manager::NextLine()
extern void Story2Manager_NextLine_m4F69952FB61A2F262B1DC2D10CB42509E87D9F72 (void);
// 0x00000031 System.Void Story2Manager::.ctor()
extern void Story2Manager__ctor_m00BE02703CABAE879A227B7A054955B57E0289AD (void);
// 0x00000032 System.Void Story2Manager/<TimeIE>d__10::.ctor(System.Int32)
extern void U3CTimeIEU3Ed__10__ctor_mC61D5359D18BC71478D2B26D364DA9E5215CA853 (void);
// 0x00000033 System.Void Story2Manager/<TimeIE>d__10::System.IDisposable.Dispose()
extern void U3CTimeIEU3Ed__10_System_IDisposable_Dispose_m28569B7D30B0704ACF7F0C2FD6E9A83606F8AA55 (void);
// 0x00000034 System.Boolean Story2Manager/<TimeIE>d__10::MoveNext()
extern void U3CTimeIEU3Ed__10_MoveNext_mD0F7B4A0C45F0D8EAB4A8F0181BE1FE0BE826153 (void);
// 0x00000035 System.Object Story2Manager/<TimeIE>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTimeIEU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0B75C09C6CCDEBC775E1A4FE0151E6BFE4F41DC7 (void);
// 0x00000036 System.Void Story2Manager/<TimeIE>d__10::System.Collections.IEnumerator.Reset()
extern void U3CTimeIEU3Ed__10_System_Collections_IEnumerator_Reset_m289A9D9686AC91F03213A3EF84EB9C62091B7B1C (void);
// 0x00000037 System.Object Story2Manager/<TimeIE>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CTimeIEU3Ed__10_System_Collections_IEnumerator_get_Current_m938DBF30F82FDDE5FA86D026FC89B0FAE95EB447 (void);
// 0x00000038 System.Void Story2Manager/<TypeLine>d__13::.ctor(System.Int32)
extern void U3CTypeLineU3Ed__13__ctor_m3381142803F5490CA2C6EA44676B001A924C87B0 (void);
// 0x00000039 System.Void Story2Manager/<TypeLine>d__13::System.IDisposable.Dispose()
extern void U3CTypeLineU3Ed__13_System_IDisposable_Dispose_mA2E73E049CF5E008F407436C5885C01CD1BE1CFD (void);
// 0x0000003A System.Boolean Story2Manager/<TypeLine>d__13::MoveNext()
extern void U3CTypeLineU3Ed__13_MoveNext_m53782D6AE82F566E61AF6C736E1E52E8D766BCE1 (void);
// 0x0000003B System.Object Story2Manager/<TypeLine>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTypeLineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB3E18BB92F474F438C262387EF09784344930706 (void);
// 0x0000003C System.Void Story2Manager/<TypeLine>d__13::System.Collections.IEnumerator.Reset()
extern void U3CTypeLineU3Ed__13_System_Collections_IEnumerator_Reset_m18F6D13CF39437E0C7D6FCDFD2A9F7431F5096FE (void);
// 0x0000003D System.Object Story2Manager/<TypeLine>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CTypeLineU3Ed__13_System_Collections_IEnumerator_get_Current_m32405CE40FDE12E1870029E0EC5B92E58F8582E4 (void);
// 0x0000003E System.Void Story3Manager::Start()
extern void Story3Manager_Start_mFC3E434723CB821E85A0D0F5C25AEE31B1EA28A6 (void);
// 0x0000003F System.Collections.IEnumerator Story3Manager::TimeIE()
extern void Story3Manager_TimeIE_m3BFB48C73E1A83E8E21720324DE7CC270D8DE122 (void);
// 0x00000040 System.Void Story3Manager::Update()
extern void Story3Manager_Update_mADE639B88A82CE6B120F084F8BC106DD66699B1C (void);
// 0x00000041 System.Void Story3Manager::StartStory3()
extern void Story3Manager_StartStory3_m90E4C2DBEEAD4D6B05F0C99D4265E173DFBAE648 (void);
// 0x00000042 System.Collections.IEnumerator Story3Manager::TypeLine()
extern void Story3Manager_TypeLine_m5EC34C6FF1711EC998F1D8D6EE88EAFC52A61675 (void);
// 0x00000043 System.Void Story3Manager::NextLine()
extern void Story3Manager_NextLine_m80D30146AE128CE008B282BEB41899BB9C5F490A (void);
// 0x00000044 System.Void Story3Manager::.ctor()
extern void Story3Manager__ctor_m33F414A739B399302A1A7DF2004B4818430C2BF7 (void);
// 0x00000045 System.Void Story3Manager/<TimeIE>d__10::.ctor(System.Int32)
extern void U3CTimeIEU3Ed__10__ctor_m13DC5856052DFE59F5E14120CEA19C0193D0BEAA (void);
// 0x00000046 System.Void Story3Manager/<TimeIE>d__10::System.IDisposable.Dispose()
extern void U3CTimeIEU3Ed__10_System_IDisposable_Dispose_mBFA4A57358A033314A1AF41AAB8F2AF92F24C646 (void);
// 0x00000047 System.Boolean Story3Manager/<TimeIE>d__10::MoveNext()
extern void U3CTimeIEU3Ed__10_MoveNext_mA01D4D5A3E1283F9A3A4A5D2E76C3C95AD1EB692 (void);
// 0x00000048 System.Object Story3Manager/<TimeIE>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTimeIEU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52B342497384166C38981CA667740BD580076450 (void);
// 0x00000049 System.Void Story3Manager/<TimeIE>d__10::System.Collections.IEnumerator.Reset()
extern void U3CTimeIEU3Ed__10_System_Collections_IEnumerator_Reset_mBC68911DBF658B3477EA295D7BF2FFD8B0FC484B (void);
// 0x0000004A System.Object Story3Manager/<TimeIE>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CTimeIEU3Ed__10_System_Collections_IEnumerator_get_Current_m75BB82C5CAE4C881AC8B70EECF6E4989EC8470DC (void);
// 0x0000004B System.Void Story3Manager/<TypeLine>d__13::.ctor(System.Int32)
extern void U3CTypeLineU3Ed__13__ctor_m8268E1A248C3515BFE52D666819313D5E77E1197 (void);
// 0x0000004C System.Void Story3Manager/<TypeLine>d__13::System.IDisposable.Dispose()
extern void U3CTypeLineU3Ed__13_System_IDisposable_Dispose_m34607E4719E82B3C90FCAA602BA9158A8AEA2B5B (void);
// 0x0000004D System.Boolean Story3Manager/<TypeLine>d__13::MoveNext()
extern void U3CTypeLineU3Ed__13_MoveNext_m6579F3AC96919C91AF1649CD36D4C1A17F65BC85 (void);
// 0x0000004E System.Object Story3Manager/<TypeLine>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTypeLineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2A5EA8BF16919F3AEC394B997A12533E9AE0DAC0 (void);
// 0x0000004F System.Void Story3Manager/<TypeLine>d__13::System.Collections.IEnumerator.Reset()
extern void U3CTypeLineU3Ed__13_System_Collections_IEnumerator_Reset_m18E7B4304AC09386ECD997307FA234B521E21439 (void);
// 0x00000050 System.Object Story3Manager/<TypeLine>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CTypeLineU3Ed__13_System_Collections_IEnumerator_get_Current_mD86346C7738D5B2E93D38B4CEFFA74C98305D845 (void);
// 0x00000051 System.Void Story4Manager::Start()
extern void Story4Manager_Start_m313497C27E87C01DEABDE0D8D5BFC93179DA56F7 (void);
// 0x00000052 System.Collections.IEnumerator Story4Manager::TimeIE()
extern void Story4Manager_TimeIE_mF20ABF95DD1A9D0D8BDBBA7D7C3E0529133F2EE2 (void);
// 0x00000053 System.Void Story4Manager::Update()
extern void Story4Manager_Update_m7BB35AA42249C552F1C4E43F88739A7B40627179 (void);
// 0x00000054 System.Void Story4Manager::StartStory4()
extern void Story4Manager_StartStory4_m881FE2012318129CF12D4B7ECBD21CCDDF8E9166 (void);
// 0x00000055 System.Collections.IEnumerator Story4Manager::TypeLine()
extern void Story4Manager_TypeLine_mB11C4356720EACBB75ED9E29CA3370751FA43C33 (void);
// 0x00000056 System.Void Story4Manager::NextLine()
extern void Story4Manager_NextLine_mCC54520B758572B0944D2DE61A6AFFD33CF30316 (void);
// 0x00000057 System.Void Story4Manager::LoadLevel(System.Int32)
extern void Story4Manager_LoadLevel_mF293EB8FC4D88B4989C279119EDA71EB5318B66B (void);
// 0x00000058 System.Collections.IEnumerator Story4Manager::LoadAsynchronously(System.Int32)
extern void Story4Manager_LoadAsynchronously_mD438C42C9179A37EB643566B09675E3FA842CFE6 (void);
// 0x00000059 System.Void Story4Manager::.ctor()
extern void Story4Manager__ctor_m0656D961514C745B74EA8862AD25365AC60D1D53 (void);
// 0x0000005A System.Void Story4Manager/<TimeIE>d__12::.ctor(System.Int32)
extern void U3CTimeIEU3Ed__12__ctor_mBB9F8F85D5DAE9BD94AE09041320E26BA12DADA0 (void);
// 0x0000005B System.Void Story4Manager/<TimeIE>d__12::System.IDisposable.Dispose()
extern void U3CTimeIEU3Ed__12_System_IDisposable_Dispose_m4B0734E90CB756B4B8E4B4D73A4C53D967759F1D (void);
// 0x0000005C System.Boolean Story4Manager/<TimeIE>d__12::MoveNext()
extern void U3CTimeIEU3Ed__12_MoveNext_mBB39C6A33BA9612716E7063379D29ADF2A01CE42 (void);
// 0x0000005D System.Object Story4Manager/<TimeIE>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTimeIEU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C73D565CC03D11BC1C842373611D2810BB5B3A4 (void);
// 0x0000005E System.Void Story4Manager/<TimeIE>d__12::System.Collections.IEnumerator.Reset()
extern void U3CTimeIEU3Ed__12_System_Collections_IEnumerator_Reset_m7EFD622AE07588EFBC6530F070A4DA48F45B5B6B (void);
// 0x0000005F System.Object Story4Manager/<TimeIE>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CTimeIEU3Ed__12_System_Collections_IEnumerator_get_Current_m79A22DBAF3530D9FF60B2AA9B5A268FACD7E0C1B (void);
// 0x00000060 System.Void Story4Manager/<TypeLine>d__15::.ctor(System.Int32)
extern void U3CTypeLineU3Ed__15__ctor_m5E924108C2849A14B99387420583E4FF4191ACDC (void);
// 0x00000061 System.Void Story4Manager/<TypeLine>d__15::System.IDisposable.Dispose()
extern void U3CTypeLineU3Ed__15_System_IDisposable_Dispose_m1751D8F1A560C0BCD88C8384796F0FE92D9EC701 (void);
// 0x00000062 System.Boolean Story4Manager/<TypeLine>d__15::MoveNext()
extern void U3CTypeLineU3Ed__15_MoveNext_mD43823B66425FC8993F5D1D9E4DAD82F23AD7420 (void);
// 0x00000063 System.Object Story4Manager/<TypeLine>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTypeLineU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m76FC58D00BCEA3EE34B60EEA166E53E8D68D2ACD (void);
// 0x00000064 System.Void Story4Manager/<TypeLine>d__15::System.Collections.IEnumerator.Reset()
extern void U3CTypeLineU3Ed__15_System_Collections_IEnumerator_Reset_mBDC2E72B1994109877CBA9AF3F9F2906A14AF27C (void);
// 0x00000065 System.Object Story4Manager/<TypeLine>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CTypeLineU3Ed__15_System_Collections_IEnumerator_get_Current_m9670A64D4B8B6142E389CD03268C21FF7123EF88 (void);
// 0x00000066 System.Void Story4Manager/<LoadAsynchronously>d__18::.ctor(System.Int32)
extern void U3CLoadAsynchronouslyU3Ed__18__ctor_m9193454C25CF53C50BE2952AD275D59D148125B8 (void);
// 0x00000067 System.Void Story4Manager/<LoadAsynchronously>d__18::System.IDisposable.Dispose()
extern void U3CLoadAsynchronouslyU3Ed__18_System_IDisposable_Dispose_m81EAE1BAA920EEAAB99C7292C8063FE9A18003D6 (void);
// 0x00000068 System.Boolean Story4Manager/<LoadAsynchronously>d__18::MoveNext()
extern void U3CLoadAsynchronouslyU3Ed__18_MoveNext_mD9C3B1A0E974F19B39CC42EBCB12FE7DE33F3A4D (void);
// 0x00000069 System.Object Story4Manager/<LoadAsynchronously>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadAsynchronouslyU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1CD8EE6EC9E99257A29EBE8EE8ACDE0A21FBE909 (void);
// 0x0000006A System.Void Story4Manager/<LoadAsynchronously>d__18::System.Collections.IEnumerator.Reset()
extern void U3CLoadAsynchronouslyU3Ed__18_System_Collections_IEnumerator_Reset_m533E97DA9C85B050583E198153B03F0C6442557B (void);
// 0x0000006B System.Object Story4Manager/<LoadAsynchronously>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CLoadAsynchronouslyU3Ed__18_System_Collections_IEnumerator_get_Current_m13A26DE6C2E4018242E9DC5FB764FC8685006B48 (void);
// 0x0000006C System.Void ActivateTextAtLine::Start()
extern void ActivateTextAtLine_Start_m5132C54E58627A87D622E1019353E01718DE1A3E (void);
// 0x0000006D System.Void ActivateTextAtLine::Update()
extern void ActivateTextAtLine_Update_m5DA13B7F35F66358607FFF0A8678781234592E63 (void);
// 0x0000006E System.Void ActivateTextAtLine::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine_OnTriggerEnter2D_mDE2A215FC0F3ED09B490A0C272C15024AED39A0D (void);
// 0x0000006F System.Void ActivateTextAtLine::OnTriggerExit2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine_OnTriggerExit2D_mE153F58EB60626D6467C7AB637D7DF0914CC13D1 (void);
// 0x00000070 System.Void ActivateTextAtLine::.ctor()
extern void ActivateTextAtLine__ctor_m55901F4FCCF4ACB4B3A35D79179C8A8EC0B908E9 (void);
// 0x00000071 System.Void DialogueManager::Start()
extern void DialogueManager_Start_m612B88A606E0F326778C59A3207CBF0548F5C6A5 (void);
// 0x00000072 System.Void DialogueManager::Update()
extern void DialogueManager_Update_m2A67EDB2B64453FC1243F80AE76CCD7136C21138 (void);
// 0x00000073 System.Collections.IEnumerator DialogueManager::TextScroll(System.String)
extern void DialogueManager_TextScroll_m5D3194B542493389CE2040A3ABF204E5CDA2A029 (void);
// 0x00000074 System.Void DialogueManager::EnableDialogueBox()
extern void DialogueManager_EnableDialogueBox_mA85924DDD60ABA39C42E39BAE1AA1D26E99F6360 (void);
// 0x00000075 System.Void DialogueManager::DisableDialogueBox()
extern void DialogueManager_DisableDialogueBox_mC83B803E1DCE5F4E37B8D74DA19A4C79A1F405BC (void);
// 0x00000076 System.Void DialogueManager::ReloadScript(UnityEngine.TextAsset)
extern void DialogueManager_ReloadScript_m0451D3127B65D5262CB72E4CC870F61FDBB1B168 (void);
// 0x00000077 System.Void DialogueManager::.ctor()
extern void DialogueManager__ctor_mAA8FF2D1E586DBF3472DF58D3B1BF8F4FA03EE1E (void);
// 0x00000078 System.Void DialogueManager/<TextScroll>d__14::.ctor(System.Int32)
extern void U3CTextScrollU3Ed__14__ctor_m0FF343CAE7D09A873B318ACAD7760727CBBBBD44 (void);
// 0x00000079 System.Void DialogueManager/<TextScroll>d__14::System.IDisposable.Dispose()
extern void U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m8DD31CCCB316C58800E444EE15133E4E8BC15ECD (void);
// 0x0000007A System.Boolean DialogueManager/<TextScroll>d__14::MoveNext()
extern void U3CTextScrollU3Ed__14_MoveNext_m921F33248DC4DCA23A61BE819498BB15BA75C0C2 (void);
// 0x0000007B System.Object DialogueManager/<TextScroll>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4FDF89D7CC6D5A16A2361C1113345C725CBCC522 (void);
// 0x0000007C System.Void DialogueManager/<TextScroll>d__14::System.Collections.IEnumerator.Reset()
extern void U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m11694E97359876379F5AB7702B0E37F2A0E8C0F9 (void);
// 0x0000007D System.Object DialogueManager/<TextScroll>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mABE1658974027AC5CF135A1980F2E95028C2C7C4 (void);
// 0x0000007E System.Void DialogueManager111::Start()
extern void DialogueManager111_Start_mC1A9AA1DA185ACD4A2731AB3907FB69A56A0102F (void);
// 0x0000007F System.Void DialogueManager111::Update()
extern void DialogueManager111_Update_m9B06DFE6C6C5327DAC36AB804A3BD2619C914FCC (void);
// 0x00000080 System.Collections.IEnumerator DialogueManager111::TextScroll(System.String)
extern void DialogueManager111_TextScroll_mBF4DF710DB2696EFEB78F5E9EB3B19A45EC6CD95 (void);
// 0x00000081 System.Void DialogueManager111::EnableDialogueBox()
extern void DialogueManager111_EnableDialogueBox_m0BF16E19CD7EF4832BE64110D931F5AAADE5858D (void);
// 0x00000082 System.Void DialogueManager111::DisableDialogueBox()
extern void DialogueManager111_DisableDialogueBox_mE7AF69D498BB2C5D140E2960CE6AECDB6E932DA3 (void);
// 0x00000083 System.Void DialogueManager111::ReloadScript(UnityEngine.TextAsset)
extern void DialogueManager111_ReloadScript_mC52C5C38D6A38026FF031749ED8CB16101FD08C1 (void);
// 0x00000084 System.Void DialogueManager111::.ctor()
extern void DialogueManager111__ctor_m19ACE0E6C3C316890B8A2F847BC42799E46948CA (void);
// 0x00000085 System.Void DialogueManager111/<TextScroll>d__15::.ctor(System.Int32)
extern void U3CTextScrollU3Ed__15__ctor_mFB72C4F6C3A9277F985C226F0C5AE5C882610675 (void);
// 0x00000086 System.Void DialogueManager111/<TextScroll>d__15::System.IDisposable.Dispose()
extern void U3CTextScrollU3Ed__15_System_IDisposable_Dispose_m8682A2B86D921428757604CC5147E8BFB5DE8D80 (void);
// 0x00000087 System.Boolean DialogueManager111/<TextScroll>d__15::MoveNext()
extern void U3CTextScrollU3Ed__15_MoveNext_mACEA5DFB507C8AFF836B205F132397EC1D311FD8 (void);
// 0x00000088 System.Object DialogueManager111/<TextScroll>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTextScrollU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF07A8CC8414E93E791847D1301E2CDF2629D324 (void);
// 0x00000089 System.Void DialogueManager111/<TextScroll>d__15::System.Collections.IEnumerator.Reset()
extern void U3CTextScrollU3Ed__15_System_Collections_IEnumerator_Reset_m18CBF4829F14DFF81C74F4E30461372D237C879F (void);
// 0x0000008A System.Object DialogueManager111/<TextScroll>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CTextScrollU3Ed__15_System_Collections_IEnumerator_get_Current_m2E43D012A1C14989FE33597AD52AA78013DCF877 (void);
// 0x0000008B System.Void ActivateTextAtLine10::Start()
extern void ActivateTextAtLine10_Start_mEAC91AE6BEFF67ADC1626000972AC4EC1A064E8E (void);
// 0x0000008C System.Void ActivateTextAtLine10::Update()
extern void ActivateTextAtLine10_Update_m231E8AEC0FC0949729CCA520E30507E11E173095 (void);
// 0x0000008D System.Void ActivateTextAtLine10::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine10_OnTriggerEnter2D_m19FF4B879A25C02DFD4057C3A95CA8EAD8E01D10 (void);
// 0x0000008E System.Void ActivateTextAtLine10::OnTriggerExit2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine10_OnTriggerExit2D_m2D5E3AB0EB4358FEFA1E503A7D5B4F4558908BD3 (void);
// 0x0000008F System.Void ActivateTextAtLine10::.ctor()
extern void ActivateTextAtLine10__ctor_m09611DFB6A55C714E058283BC7D39170AE3AF1F2 (void);
// 0x00000090 System.Void DialogueManager10::Start()
extern void DialogueManager10_Start_mBF3B4C4D7DC0104FE6E863D227D25170708F0C11 (void);
// 0x00000091 System.Void DialogueManager10::Update()
extern void DialogueManager10_Update_m02DB215761F9243F520240056E210921B1B389B6 (void);
// 0x00000092 System.Collections.IEnumerator DialogueManager10::TextScroll(System.String)
extern void DialogueManager10_TextScroll_mEFC8587C6A8F527F06560AA0FF26CE9351CBA49C (void);
// 0x00000093 System.Void DialogueManager10::EnableDialogueBox()
extern void DialogueManager10_EnableDialogueBox_mD1018DB160CA21DFB2ACF7425C8EFF01E5564621 (void);
// 0x00000094 System.Void DialogueManager10::DisableDialogueBox()
extern void DialogueManager10_DisableDialogueBox_m8B41DFE70F80747F97714C4CBB8AA8C1FCA37EA7 (void);
// 0x00000095 System.Void DialogueManager10::ReloadScript(UnityEngine.TextAsset)
extern void DialogueManager10_ReloadScript_mBA569F3417A566A97A8005E2024A6F0F9D99B16A (void);
// 0x00000096 System.Void DialogueManager10::.ctor()
extern void DialogueManager10__ctor_m4744BA0B6764A588573274AC93047691D2C56707 (void);
// 0x00000097 System.Void DialogueManager10/<TextScroll>d__14::.ctor(System.Int32)
extern void U3CTextScrollU3Ed__14__ctor_m25372485B50A52BDD8128CC237EE2E22C46EF534 (void);
// 0x00000098 System.Void DialogueManager10/<TextScroll>d__14::System.IDisposable.Dispose()
extern void U3CTextScrollU3Ed__14_System_IDisposable_Dispose_mBD7F7A2F7E8A76C7B522049B83C0F99302195930 (void);
// 0x00000099 System.Boolean DialogueManager10/<TextScroll>d__14::MoveNext()
extern void U3CTextScrollU3Ed__14_MoveNext_m37BD56B4B63585B254DE5F229D3A849A29288458 (void);
// 0x0000009A System.Object DialogueManager10/<TextScroll>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE424541DA8B50682073EF643295B76FA41785D78 (void);
// 0x0000009B System.Void DialogueManager10/<TextScroll>d__14::System.Collections.IEnumerator.Reset()
extern void U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m30934F0B954200D61D8A62774DF6D0D2885265F8 (void);
// 0x0000009C System.Object DialogueManager10/<TextScroll>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_m09ED21E076FD07A2D85934D5EDF75577CDA2EC18 (void);
// 0x0000009D System.Void ActivateTextAtLine1::Start()
extern void ActivateTextAtLine1_Start_mF12557E4B0888167F4F748DFF5CFD3904BC4968D (void);
// 0x0000009E System.Void ActivateTextAtLine1::Update()
extern void ActivateTextAtLine1_Update_m80E599B4C33597819B9F12DF39B40FEDB478DEC5 (void);
// 0x0000009F System.Void ActivateTextAtLine1::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine1_OnTriggerEnter2D_m693AD794AC6ED1F14CE0249686322F52FB5C84C6 (void);
// 0x000000A0 System.Void ActivateTextAtLine1::OnTriggerExit2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine1_OnTriggerExit2D_mD0CF8A89A18B1B3B78F56D91104003A8B1A9108C (void);
// 0x000000A1 System.Void ActivateTextAtLine1::.ctor()
extern void ActivateTextAtLine1__ctor_m57D79A4003FD7AB2F31D6D602A2A54F8EF212D99 (void);
// 0x000000A2 System.Void DialogueManager1::Start()
extern void DialogueManager1_Start_m4170E9E118F1F7CE2ABAC1DD9AF1781A7AAA3E6B (void);
// 0x000000A3 System.Void DialogueManager1::Update()
extern void DialogueManager1_Update_mBEAADA526B49A24509D1926A7167D0490236274B (void);
// 0x000000A4 System.Collections.IEnumerator DialogueManager1::TextScroll(System.String)
extern void DialogueManager1_TextScroll_m449B179ECE4CEC9C9F207D5AFAE30142BA1719C8 (void);
// 0x000000A5 System.Void DialogueManager1::EnableDialogueBox()
extern void DialogueManager1_EnableDialogueBox_m682A1E2E8A78299179BBC7749C071CCF5583445F (void);
// 0x000000A6 System.Void DialogueManager1::DisableDialogueBox()
extern void DialogueManager1_DisableDialogueBox_m87ED3DD610D99E7786D7EC8BC77AC2E82C1F0A14 (void);
// 0x000000A7 System.Void DialogueManager1::ReloadScript(UnityEngine.TextAsset)
extern void DialogueManager1_ReloadScript_m1737B1B0C5442C637D89D9B1B0E13351B241E0D2 (void);
// 0x000000A8 System.Void DialogueManager1::.ctor()
extern void DialogueManager1__ctor_mBE4B50DD4B2FB609BB30E393CC2E075759EA23D6 (void);
// 0x000000A9 System.Void DialogueManager1/<TextScroll>d__14::.ctor(System.Int32)
extern void U3CTextScrollU3Ed__14__ctor_m46153879164D1408755D717F0CFF3804B9D0DA21 (void);
// 0x000000AA System.Void DialogueManager1/<TextScroll>d__14::System.IDisposable.Dispose()
extern void U3CTextScrollU3Ed__14_System_IDisposable_Dispose_mD54DB86896E9D8FDCF8BC296B9FEF71554B67D6C (void);
// 0x000000AB System.Boolean DialogueManager1/<TextScroll>d__14::MoveNext()
extern void U3CTextScrollU3Ed__14_MoveNext_m8F9D94FCA36681D7D7BA0635292F5D5E38AB1C67 (void);
// 0x000000AC System.Object DialogueManager1/<TextScroll>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB172AE4343A8D82D557AF6D79E3A12EB643BB220 (void);
// 0x000000AD System.Void DialogueManager1/<TextScroll>d__14::System.Collections.IEnumerator.Reset()
extern void U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m8C8F933102AF5469324D53568C59C3211A0721BD (void);
// 0x000000AE System.Object DialogueManager1/<TextScroll>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_m08ECAF3014121CD6964575229A159602CFAD3E58 (void);
// 0x000000AF System.Void ActivateTextAtLine3::Start()
extern void ActivateTextAtLine3_Start_m5DAD72C786521298B8039978FBCCF91362455F8E (void);
// 0x000000B0 System.Void ActivateTextAtLine3::Update()
extern void ActivateTextAtLine3_Update_mECD8F42319EA84F0B846BCA97E3DD9BC0E3F2597 (void);
// 0x000000B1 System.Void ActivateTextAtLine3::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine3_OnTriggerEnter2D_m0BC2D76D7B84B012E053B5B67AEECC3429559AFB (void);
// 0x000000B2 System.Void ActivateTextAtLine3::OnTriggerExit2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine3_OnTriggerExit2D_mC42AFB1C7A91F6F6F7A620AD641AF17DE593790A (void);
// 0x000000B3 System.Void ActivateTextAtLine3::.ctor()
extern void ActivateTextAtLine3__ctor_m9815F1455BEB4DCC8E00E4C5C6858AE598E06D90 (void);
// 0x000000B4 System.Void DialogueManager3::Start()
extern void DialogueManager3_Start_m94B2515ACE4E011C5E7DF03F3D34DCA5706B3472 (void);
// 0x000000B5 System.Void DialogueManager3::Update()
extern void DialogueManager3_Update_m82FCAE14D26B1F348F5C68AE5725C07635513F47 (void);
// 0x000000B6 System.Collections.IEnumerator DialogueManager3::TextScroll(System.String)
extern void DialogueManager3_TextScroll_m1B5D37D563F9AAE195726DB2DE62F7877BEE2C4A (void);
// 0x000000B7 System.Void DialogueManager3::EnableDialogueBox()
extern void DialogueManager3_EnableDialogueBox_m6ECC3BB779BD33010906CD27D2871C51B16DF7A5 (void);
// 0x000000B8 System.Void DialogueManager3::DisableDialogueBox()
extern void DialogueManager3_DisableDialogueBox_m46B3FBED60B53CBA49B5EB96F6ADE4883E2BB6EC (void);
// 0x000000B9 System.Void DialogueManager3::ReloadScript(UnityEngine.TextAsset)
extern void DialogueManager3_ReloadScript_m910AB8560CC598B39C0C1561E3765E7467B7F8DB (void);
// 0x000000BA System.Void DialogueManager3::.ctor()
extern void DialogueManager3__ctor_m1B706AC70FFDDC4A27AA2E1528D159619ADA5B74 (void);
// 0x000000BB System.Void DialogueManager3/<TextScroll>d__15::.ctor(System.Int32)
extern void U3CTextScrollU3Ed__15__ctor_m4D514A28DAFB725FCCC3F86305EF9047E11B835F (void);
// 0x000000BC System.Void DialogueManager3/<TextScroll>d__15::System.IDisposable.Dispose()
extern void U3CTextScrollU3Ed__15_System_IDisposable_Dispose_m5B3A2BA0FDE76B3D910F9F543622E3B7E0104CA5 (void);
// 0x000000BD System.Boolean DialogueManager3/<TextScroll>d__15::MoveNext()
extern void U3CTextScrollU3Ed__15_MoveNext_mB90356F0A00008F987EF6E8C88325D907DE7067C (void);
// 0x000000BE System.Object DialogueManager3/<TextScroll>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTextScrollU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44B1FEC4291372B6B51003ABC8F76DF83193D7B2 (void);
// 0x000000BF System.Void DialogueManager3/<TextScroll>d__15::System.Collections.IEnumerator.Reset()
extern void U3CTextScrollU3Ed__15_System_Collections_IEnumerator_Reset_m98B30F5DEA0D66BE62F371A7A6C3DCB9B95FF6C4 (void);
// 0x000000C0 System.Object DialogueManager3/<TextScroll>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CTextScrollU3Ed__15_System_Collections_IEnumerator_get_Current_m0F1D7EA630D47E82DA6928695681B07E7F6F3785 (void);
// 0x000000C1 System.Void ActivateTextAtLine4::Start()
extern void ActivateTextAtLine4_Start_mC9C5D1632FDAD920E38EB31BAC4ECB16232F5048 (void);
// 0x000000C2 System.Void ActivateTextAtLine4::Update()
extern void ActivateTextAtLine4_Update_m0A737B58A1B92F024BFE0C80C84DFE6C1FDB174B (void);
// 0x000000C3 System.Void ActivateTextAtLine4::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine4_OnTriggerEnter2D_m5CFAD7616F9D1B4E0BC731B46875CB8D2EA341E7 (void);
// 0x000000C4 System.Void ActivateTextAtLine4::OnTriggerExit2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine4_OnTriggerExit2D_m34230CFE0390423C0FD607B4F5B72A62C7E36D3F (void);
// 0x000000C5 System.Void ActivateTextAtLine4::.ctor()
extern void ActivateTextAtLine4__ctor_m9124FDC7BEDFB1461CAC3CAA95DDF4CC02C6C4AC (void);
// 0x000000C6 System.Void DialogueManager4::Start()
extern void DialogueManager4_Start_mD33C46E902C609C35E49E565D0958DDFD7458A59 (void);
// 0x000000C7 System.Void DialogueManager4::Update()
extern void DialogueManager4_Update_mA28A9FD0318A53E91D1CB3F1AE791048C2522037 (void);
// 0x000000C8 System.Collections.IEnumerator DialogueManager4::TextScroll(System.String)
extern void DialogueManager4_TextScroll_m0C54E3C56347658AD1502A8612C40A2804A86F1B (void);
// 0x000000C9 System.Void DialogueManager4::EnableDialogueBox()
extern void DialogueManager4_EnableDialogueBox_m19DCB02DA55E5712BA1AA2DF01C0FE1F948BE363 (void);
// 0x000000CA System.Void DialogueManager4::DisableDialogueBox()
extern void DialogueManager4_DisableDialogueBox_m50B836470C88EF2E31EBD18AB7783B1515D089EA (void);
// 0x000000CB System.Void DialogueManager4::ReloadScript(UnityEngine.TextAsset)
extern void DialogueManager4_ReloadScript_mB2D717AF585AD258D2EB52718D060690BC4BA4EA (void);
// 0x000000CC System.Void DialogueManager4::.ctor()
extern void DialogueManager4__ctor_mC4E1CC9F7DBC22DC06956B13E3628FE668DAD691 (void);
// 0x000000CD System.Void DialogueManager4/<TextScroll>d__14::.ctor(System.Int32)
extern void U3CTextScrollU3Ed__14__ctor_m287E95932EF65B84A11568F314D1A15E320B6536 (void);
// 0x000000CE System.Void DialogueManager4/<TextScroll>d__14::System.IDisposable.Dispose()
extern void U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m66D5900B028A65713A5BC970753B95A980024A9E (void);
// 0x000000CF System.Boolean DialogueManager4/<TextScroll>d__14::MoveNext()
extern void U3CTextScrollU3Ed__14_MoveNext_m27C49752D7B15DD32AFA06ACC2C82066EC3CADE4 (void);
// 0x000000D0 System.Object DialogueManager4/<TextScroll>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m67E6C4920DA41BB4F2897A59F1B82A46C76EDB90 (void);
// 0x000000D1 System.Void DialogueManager4/<TextScroll>d__14::System.Collections.IEnumerator.Reset()
extern void U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m2BD1E12D338D61FAA0A2A9B8C4778CE45A264B89 (void);
// 0x000000D2 System.Object DialogueManager4/<TextScroll>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_m9349BFBAB9DF9427D539E5EA95DA106B8B7D9158 (void);
// 0x000000D3 System.Void ActivateTextAtLine5::Start()
extern void ActivateTextAtLine5_Start_mF97481AA0D6C649C18E8DF8131037B15BD34331C (void);
// 0x000000D4 System.Void ActivateTextAtLine5::Update()
extern void ActivateTextAtLine5_Update_m3EAE545421EEF09D347ECB1130FC4AFC420719DC (void);
// 0x000000D5 System.Void ActivateTextAtLine5::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine5_OnTriggerEnter2D_mDD616AA06B7F5D46D7A36A6DF5CE1CC78A5E8EEE (void);
// 0x000000D6 System.Void ActivateTextAtLine5::OnTriggerExit2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine5_OnTriggerExit2D_m18CC67629720BA8FE9CBE76CA0F78D8F79F3A644 (void);
// 0x000000D7 System.Void ActivateTextAtLine5::.ctor()
extern void ActivateTextAtLine5__ctor_mDEA43E87500460268803BAE92BA0C39992E421AC (void);
// 0x000000D8 System.Void DialogueManager5::Start()
extern void DialogueManager5_Start_m8FC5531C7D23203D917C69CA71F49868E30358A2 (void);
// 0x000000D9 System.Void DialogueManager5::Update()
extern void DialogueManager5_Update_m454E8BFB2F6558E11322CA8F6335A586EFE3597F (void);
// 0x000000DA System.Collections.IEnumerator DialogueManager5::TextScroll(System.String)
extern void DialogueManager5_TextScroll_mA868CFAC74505D370CE5C4BA8E8469957AD8F086 (void);
// 0x000000DB System.Void DialogueManager5::EnableDialogueBox()
extern void DialogueManager5_EnableDialogueBox_m726A49588D335BC0704D18E598DDDF1F0523811C (void);
// 0x000000DC System.Void DialogueManager5::DisableDialogueBox()
extern void DialogueManager5_DisableDialogueBox_m61E96CFE72D25853490C83415FBD303A464B50C3 (void);
// 0x000000DD System.Void DialogueManager5::ReloadScript(UnityEngine.TextAsset)
extern void DialogueManager5_ReloadScript_m405A2BC3CFBBCCF0970F057379BE5E21B1F10BC9 (void);
// 0x000000DE System.Void DialogueManager5::.ctor()
extern void DialogueManager5__ctor_m319A7400B23794C994E435F0811569BB191DB590 (void);
// 0x000000DF System.Void DialogueManager5/<TextScroll>d__14::.ctor(System.Int32)
extern void U3CTextScrollU3Ed__14__ctor_m5CD63C4796AA3EFFCDDB1B1695B299FA5F129190 (void);
// 0x000000E0 System.Void DialogueManager5/<TextScroll>d__14::System.IDisposable.Dispose()
extern void U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m1131A72D8DAA306883DC73A644E10DF939E3DDC8 (void);
// 0x000000E1 System.Boolean DialogueManager5/<TextScroll>d__14::MoveNext()
extern void U3CTextScrollU3Ed__14_MoveNext_m758DCCF499E9189750CBF4889B2A92641A1C4A37 (void);
// 0x000000E2 System.Object DialogueManager5/<TextScroll>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m586FF4D51CD4F659D098B08A3EF8B37230C59BA6 (void);
// 0x000000E3 System.Void DialogueManager5/<TextScroll>d__14::System.Collections.IEnumerator.Reset()
extern void U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_mB92B68E8BE2177A8B7EA040B8CC0328708947DB2 (void);
// 0x000000E4 System.Object DialogueManager5/<TextScroll>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_m83A16F0C427A38D04171F21D0FB92541B0319C76 (void);
// 0x000000E5 System.Void ActivateTextAtLine6::Start()
extern void ActivateTextAtLine6_Start_mCFAF3C50CD6CC74725FF4AE3CFEB9463918BF0A9 (void);
// 0x000000E6 System.Void ActivateTextAtLine6::Update()
extern void ActivateTextAtLine6_Update_m743A5109226D2AB5953EEBABF043BF6588391485 (void);
// 0x000000E7 System.Void ActivateTextAtLine6::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine6_OnTriggerEnter2D_m1F51ED4D4D4FE1F14DF24C9E143A1F48CDC7F4B7 (void);
// 0x000000E8 System.Void ActivateTextAtLine6::OnTriggerExit2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine6_OnTriggerExit2D_mFC4FC50B3D008FEE7ED846758CB417BD4FD724FB (void);
// 0x000000E9 System.Void ActivateTextAtLine6::.ctor()
extern void ActivateTextAtLine6__ctor_m02F1FE204A897C3F2DA793F7D260BED47C713B8E (void);
// 0x000000EA System.Void DialogueManager6::Start()
extern void DialogueManager6_Start_mCE7747BECF9035DBEF34C072045D71AAFBBC34EB (void);
// 0x000000EB System.Void DialogueManager6::Update()
extern void DialogueManager6_Update_m82F42AE25D3D61B94A780723A1A3A407407DF924 (void);
// 0x000000EC System.Collections.IEnumerator DialogueManager6::TextScroll(System.String)
extern void DialogueManager6_TextScroll_m5A10285269F39472F209FE26602A0D0D1D2AE92F (void);
// 0x000000ED System.Void DialogueManager6::EnableDialogueBox()
extern void DialogueManager6_EnableDialogueBox_mA0009C6BE3F39795CAC41D701CE5A082477CCA6C (void);
// 0x000000EE System.Void DialogueManager6::DisableDialogueBox()
extern void DialogueManager6_DisableDialogueBox_m9AEC9A5F2BF80BCB58263744B7CDBEF0C698132D (void);
// 0x000000EF System.Void DialogueManager6::ReloadScript(UnityEngine.TextAsset)
extern void DialogueManager6_ReloadScript_mB1B016B1BFA2F5DC3E7C13535082701BB90CD09C (void);
// 0x000000F0 System.Void DialogueManager6::.ctor()
extern void DialogueManager6__ctor_mDFD604CDC53BEB5A2F5DA2A50EBB863CFC505439 (void);
// 0x000000F1 System.Void DialogueManager6/<TextScroll>d__14::.ctor(System.Int32)
extern void U3CTextScrollU3Ed__14__ctor_m29BA7873E6EF4DFB87C961BB0BF78BF2976D3128 (void);
// 0x000000F2 System.Void DialogueManager6/<TextScroll>d__14::System.IDisposable.Dispose()
extern void U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m82BB053A1C51D75CF09DDB5F7DD3CB29FC73CE9E (void);
// 0x000000F3 System.Boolean DialogueManager6/<TextScroll>d__14::MoveNext()
extern void U3CTextScrollU3Ed__14_MoveNext_mB8282D2D1C7318DF25D0252B1D43955146D40FA9 (void);
// 0x000000F4 System.Object DialogueManager6/<TextScroll>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB001D87EE04371590F576A76F96AC0CDA78BAFEE (void);
// 0x000000F5 System.Void DialogueManager6/<TextScroll>d__14::System.Collections.IEnumerator.Reset()
extern void U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_mCD2873EF7E990CC57029E50B039B3B93F55CFF86 (void);
// 0x000000F6 System.Object DialogueManager6/<TextScroll>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mE5FB965E79F06B9E364142856FF3C3CAA11040D7 (void);
// 0x000000F7 System.Void ActivateTextAtLine7::Start()
extern void ActivateTextAtLine7_Start_mC3DAF66946F51DED9760C435A229016BCFF59B43 (void);
// 0x000000F8 System.Void ActivateTextAtLine7::Update()
extern void ActivateTextAtLine7_Update_m819CFC6BFBC6E2F59144D61DBB6DAE3FF6CEDD62 (void);
// 0x000000F9 System.Void ActivateTextAtLine7::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine7_OnTriggerEnter2D_mEB71C535E7C077CA93B15111E64E59C74677ACC1 (void);
// 0x000000FA System.Void ActivateTextAtLine7::OnTriggerExit2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine7_OnTriggerExit2D_m5C77DACA6F927B705CE91CBAA7C48D9EC6C527BB (void);
// 0x000000FB System.Void ActivateTextAtLine7::.ctor()
extern void ActivateTextAtLine7__ctor_m730E85D3DB58F829B9848F25FBA17405C130BBDE (void);
// 0x000000FC System.Void DialogueManager7::Start()
extern void DialogueManager7_Start_m1A26434C9850F12D254D1214B95A8079D5B5BC6C (void);
// 0x000000FD System.Void DialogueManager7::Update()
extern void DialogueManager7_Update_m283318D688C4A403256582284F8DA5825809280F (void);
// 0x000000FE System.Collections.IEnumerator DialogueManager7::TextScroll(System.String)
extern void DialogueManager7_TextScroll_mBE32AC7FF53D80990FD20B69E4538EDAAA79D380 (void);
// 0x000000FF System.Void DialogueManager7::EnableDialogueBox()
extern void DialogueManager7_EnableDialogueBox_m99305BDD68064E089D7374B0C203151EA269C8E1 (void);
// 0x00000100 System.Void DialogueManager7::DisableDialogueBox()
extern void DialogueManager7_DisableDialogueBox_mBD94480402BFCE491DD4D7B0DBE31B9E79199615 (void);
// 0x00000101 System.Void DialogueManager7::ReloadScript(UnityEngine.TextAsset)
extern void DialogueManager7_ReloadScript_m223EC77726F0753CD434DC0AD4766D7864655E7D (void);
// 0x00000102 System.Void DialogueManager7::.ctor()
extern void DialogueManager7__ctor_mBC0C6098CAFDB87D286B92FD234CE5372030BB80 (void);
// 0x00000103 System.Void DialogueManager7/<TextScroll>d__14::.ctor(System.Int32)
extern void U3CTextScrollU3Ed__14__ctor_mCCF5C5A9B5C4FDC2A5361E59F0E6D5B63497E721 (void);
// 0x00000104 System.Void DialogueManager7/<TextScroll>d__14::System.IDisposable.Dispose()
extern void U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m86803CD35DE20355B45C10DFB1BBEFB11FACC611 (void);
// 0x00000105 System.Boolean DialogueManager7/<TextScroll>d__14::MoveNext()
extern void U3CTextScrollU3Ed__14_MoveNext_mEC51731C840B350595965938F38643EF3E19AF12 (void);
// 0x00000106 System.Object DialogueManager7/<TextScroll>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF0D25459F989654F0F9931A907E0BE2D7C8C8957 (void);
// 0x00000107 System.Void DialogueManager7/<TextScroll>d__14::System.Collections.IEnumerator.Reset()
extern void U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m612A77A93459DCF470D8F30C2D062034F01B8E2E (void);
// 0x00000108 System.Object DialogueManager7/<TextScroll>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mBA75C9E3BB40C9F9CAD7791953A001FAE2B3A531 (void);
// 0x00000109 System.Void ActivateTextAtLine8::Start()
extern void ActivateTextAtLine8_Start_m0654FE0EB73CEB574A157921F3C9A08AFE905A38 (void);
// 0x0000010A System.Void ActivateTextAtLine8::Update()
extern void ActivateTextAtLine8_Update_m1F52C58A07E8EA73842FC35EC85B71FDCFA84AEE (void);
// 0x0000010B System.Void ActivateTextAtLine8::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine8_OnTriggerEnter2D_m276A9D4D43D9B0DB0FDBCB2DEDB278BABBB78320 (void);
// 0x0000010C System.Void ActivateTextAtLine8::OnTriggerExit2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine8_OnTriggerExit2D_mA7834EF80EA0640FDC6BACD1FB47A9F3CB9E8AF8 (void);
// 0x0000010D System.Void ActivateTextAtLine8::.ctor()
extern void ActivateTextAtLine8__ctor_m57A59510E3180BADD971057EA0CF68CF1A8A5073 (void);
// 0x0000010E System.Void DialogueManager8::Start()
extern void DialogueManager8_Start_m0D2A92B9734A7639EEFD702DC7619C52557FF778 (void);
// 0x0000010F System.Void DialogueManager8::Update()
extern void DialogueManager8_Update_mFBA760983CE60BCA4B5B299962CF71D1BA015D34 (void);
// 0x00000110 System.Collections.IEnumerator DialogueManager8::TextScroll(System.String)
extern void DialogueManager8_TextScroll_m530F4F98A324FEE74AC4BCB4499A04AE0A4BA482 (void);
// 0x00000111 System.Void DialogueManager8::EnableDialogueBox()
extern void DialogueManager8_EnableDialogueBox_m7589EB91B47331C31D0E3CBFE44277440529E4A5 (void);
// 0x00000112 System.Void DialogueManager8::DisableDialogueBox()
extern void DialogueManager8_DisableDialogueBox_m9D4652257014CC2D973C263C6439655195087F2F (void);
// 0x00000113 System.Void DialogueManager8::ReloadScript(UnityEngine.TextAsset)
extern void DialogueManager8_ReloadScript_m73451D8936FFA40F65790EA1D34BF3C0824AAA82 (void);
// 0x00000114 System.Void DialogueManager8::.ctor()
extern void DialogueManager8__ctor_m7F3EA0C0A4F4D56A83D45F8AF17F0659949B6876 (void);
// 0x00000115 System.Void DialogueManager8/<TextScroll>d__14::.ctor(System.Int32)
extern void U3CTextScrollU3Ed__14__ctor_mD21839154365B3E4559D29CC0531B9C507BFBBBE (void);
// 0x00000116 System.Void DialogueManager8/<TextScroll>d__14::System.IDisposable.Dispose()
extern void U3CTextScrollU3Ed__14_System_IDisposable_Dispose_mB081476BC8CD3EB1AB9DD3CE91393410457FA640 (void);
// 0x00000117 System.Boolean DialogueManager8/<TextScroll>d__14::MoveNext()
extern void U3CTextScrollU3Ed__14_MoveNext_m4DDC886C45FAF4B23561F786DAEC54DDA1AFE8E0 (void);
// 0x00000118 System.Object DialogueManager8/<TextScroll>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3FEFEE3C8052FA546CF052470E36DCFD51824CC (void);
// 0x00000119 System.Void DialogueManager8/<TextScroll>d__14::System.Collections.IEnumerator.Reset()
extern void U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m10F6AFF907AA15D3F12357D6D000715B2FD102B9 (void);
// 0x0000011A System.Object DialogueManager8/<TextScroll>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mF851251A598615B608406828EC8BB974DB7616B3 (void);
// 0x0000011B System.Void ActivateTextAtLine9::Start()
extern void ActivateTextAtLine9_Start_m9F678AA992CCCBDFE461384308486A579EBB5727 (void);
// 0x0000011C System.Void ActivateTextAtLine9::Update()
extern void ActivateTextAtLine9_Update_mA949020B249DB943B6C726BBAF8ED5B71901E231 (void);
// 0x0000011D System.Void ActivateTextAtLine9::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine9_OnTriggerEnter2D_m9CB1F2AA262922160F443FD0E4BECDCB0316CF1A (void);
// 0x0000011E System.Void ActivateTextAtLine9::OnTriggerExit2D(UnityEngine.Collider2D)
extern void ActivateTextAtLine9_OnTriggerExit2D_m503E39415483FCAC00F7273A5F35C8B4F3CA9C0D (void);
// 0x0000011F System.Void ActivateTextAtLine9::.ctor()
extern void ActivateTextAtLine9__ctor_m9BB3878FD27FD9DF820BB8A7B67EF6D7B50BC05F (void);
// 0x00000120 System.Void DialogueManager9::Start()
extern void DialogueManager9_Start_m45DFAC5FBBE23828C45592C49DAE111FAB4E6E4B (void);
// 0x00000121 System.Void DialogueManager9::Update()
extern void DialogueManager9_Update_mCB393634E9EAABA048293F593397E49EFBEBA7BF (void);
// 0x00000122 System.Collections.IEnumerator DialogueManager9::TextScroll(System.String)
extern void DialogueManager9_TextScroll_m6A595793A9FCDE637C136F78B85D75D9455ECF19 (void);
// 0x00000123 System.Void DialogueManager9::EnableDialogueBox()
extern void DialogueManager9_EnableDialogueBox_m0754388F3AC7A47D89B751961A73EFAA7D32CBD9 (void);
// 0x00000124 System.Void DialogueManager9::DisableDialogueBox()
extern void DialogueManager9_DisableDialogueBox_m2828162D1DE9118741ADCDE31CAD5C60E594BBEF (void);
// 0x00000125 System.Void DialogueManager9::ReloadScript(UnityEngine.TextAsset)
extern void DialogueManager9_ReloadScript_mBEED7CCF67B58D00336296BFD548B4D02E121751 (void);
// 0x00000126 System.Void DialogueManager9::.ctor()
extern void DialogueManager9__ctor_mFAEB62129805A6FAE8393F8665726B90E54F6491 (void);
// 0x00000127 System.Void DialogueManager9/<TextScroll>d__14::.ctor(System.Int32)
extern void U3CTextScrollU3Ed__14__ctor_mF3E5A9448661287179E4EA31143CF930B9359943 (void);
// 0x00000128 System.Void DialogueManager9/<TextScroll>d__14::System.IDisposable.Dispose()
extern void U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m3112D9AA996F620897BE1A44084071E371DCF98F (void);
// 0x00000129 System.Boolean DialogueManager9/<TextScroll>d__14::MoveNext()
extern void U3CTextScrollU3Ed__14_MoveNext_m8711AB9EBABC00EC514B169BAE2FCBFA0A439739 (void);
// 0x0000012A System.Object DialogueManager9/<TextScroll>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C55D1856B48DA623AC540EA0215DD4D997EC254 (void);
// 0x0000012B System.Void DialogueManager9/<TextScroll>d__14::System.Collections.IEnumerator.Reset()
extern void U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_mC7D4E6F771B2947EB0A45186D5F769EEFD992FDC (void);
// 0x0000012C System.Object DialogueManager9/<TextScroll>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mF4C9073C4148A84F5564C1CD781597BE870B28A7 (void);
// 0x0000012D System.Void BossHealth_bat::Start()
extern void BossHealth_bat_Start_mA7EEB91D849C5344E55FD8B5068F734D38FEA853 (void);
// 0x0000012E System.Void BossHealth_bat::Update()
extern void BossHealth_bat_Update_m59764091BB4DA65826518F69D1762881DD87F97B (void);
// 0x0000012F System.Void BossHealth_bat::HurtBoss(System.Int32)
extern void BossHealth_bat_HurtBoss_m369A57F5D0369B16D19376CD11B60F650F24523D (void);
// 0x00000130 System.Collections.IEnumerator BossHealth_bat::EnemyActiveOff()
extern void BossHealth_bat_EnemyActiveOff_m0E6061CF40413FD702604226B2C8CB6CCD6AC732 (void);
// 0x00000131 System.Void BossHealth_bat::.ctor()
extern void BossHealth_bat__ctor_mDEF7DCB883EE0C9D26924E3214DFB908552F22E1 (void);
// 0x00000132 System.Void BossHealth_bat/<EnemyActiveOff>d__17::.ctor(System.Int32)
extern void U3CEnemyActiveOffU3Ed__17__ctor_mA577EC9EA8948E1D60DA759F2D296C5044F54096 (void);
// 0x00000133 System.Void BossHealth_bat/<EnemyActiveOff>d__17::System.IDisposable.Dispose()
extern void U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_mE52F244C415FAAB8E042303C9A8BE828C153F6E4 (void);
// 0x00000134 System.Boolean BossHealth_bat/<EnemyActiveOff>d__17::MoveNext()
extern void U3CEnemyActiveOffU3Ed__17_MoveNext_m78B769C834BC2D07539DA23F2A5E6AD2894B6A80 (void);
// 0x00000135 System.Object BossHealth_bat/<EnemyActiveOff>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m794E31662324F3791521289BCBE1BB477C71F130 (void);
// 0x00000136 System.Void BossHealth_bat/<EnemyActiveOff>d__17::System.Collections.IEnumerator.Reset()
extern void U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_mC3BF406E3FF14C5382C3C0C5642F7078C23CAFFF (void);
// 0x00000137 System.Object BossHealth_bat/<EnemyActiveOff>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m3A95CFD2F8CA16B187FBEF68F07D730F36A098F7 (void);
// 0x00000138 System.Void BossHealth_bear::Start()
extern void BossHealth_bear_Start_m8671081FD8DB03071099FE050F52681FC140B9B5 (void);
// 0x00000139 System.Void BossHealth_bear::Update()
extern void BossHealth_bear_Update_mCD80C542B86E871D2EAD77B3A40E5A24C38BC0FE (void);
// 0x0000013A System.Void BossHealth_bear::HurtBoss(System.Int32)
extern void BossHealth_bear_HurtBoss_mBC45A5992F9548DEF344D912F1F3B167C134121C (void);
// 0x0000013B System.Collections.IEnumerator BossHealth_bear::EnemyActiveOff()
extern void BossHealth_bear_EnemyActiveOff_m2F45DF2FA4E4D971EA4042766EA11222FD30CC8A (void);
// 0x0000013C System.Void BossHealth_bear::.ctor()
extern void BossHealth_bear__ctor_m8778AD21BF0C9255BE40CFDEB3A1E795FAD3A91F (void);
// 0x0000013D System.Void BossHealth_bear/<EnemyActiveOff>d__17::.ctor(System.Int32)
extern void U3CEnemyActiveOffU3Ed__17__ctor_m464AE6200A1E122F7A40E31654B6CCD9E7776279 (void);
// 0x0000013E System.Void BossHealth_bear/<EnemyActiveOff>d__17::System.IDisposable.Dispose()
extern void U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_m253B9F6D14FFB4368754C68A0B731BDE59F82626 (void);
// 0x0000013F System.Boolean BossHealth_bear/<EnemyActiveOff>d__17::MoveNext()
extern void U3CEnemyActiveOffU3Ed__17_MoveNext_m6CC96FE447CA184FA65AEEE3B42C9D478180300F (void);
// 0x00000140 System.Object BossHealth_bear/<EnemyActiveOff>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD776DC29AC55AD6852773C217E3FCC42429CA3B8 (void);
// 0x00000141 System.Void BossHealth_bear/<EnemyActiveOff>d__17::System.Collections.IEnumerator.Reset()
extern void U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m5D62DE7F1C1F40D685BEC2EBFEA3BDA71D8B5E8D (void);
// 0x00000142 System.Object BossHealth_bear/<EnemyActiveOff>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m8CAD4C264644BCD9C4EAE254FB2D21F63C5FCF0F (void);
// 0x00000143 System.Void BossHealth_bee::Start()
extern void BossHealth_bee_Start_m1AC404F4C413BB84B7001DAB385F1EEEC1493573 (void);
// 0x00000144 System.Void BossHealth_bee::Update()
extern void BossHealth_bee_Update_m5571634F2145EDF4DDFFAC7BAEB0831127C4825E (void);
// 0x00000145 System.Void BossHealth_bee::HurtBoss(System.Int32)
extern void BossHealth_bee_HurtBoss_m12EEE43DEA01B2A5AFC2E9D5EE58B9EA03EF2A32 (void);
// 0x00000146 System.Collections.IEnumerator BossHealth_bee::EnemyActiveOff()
extern void BossHealth_bee_EnemyActiveOff_m2A0A8EC50551CC68290E2D4D4569AC31159ACA7D (void);
// 0x00000147 System.Void BossHealth_bee::.ctor()
extern void BossHealth_bee__ctor_mE3DE533807D18CEE0631E8BD353A3D389DBDE022 (void);
// 0x00000148 System.Void BossHealth_bee/<EnemyActiveOff>d__17::.ctor(System.Int32)
extern void U3CEnemyActiveOffU3Ed__17__ctor_mC83BEBA335A88147D5893E6528372BF833DC7D2C (void);
// 0x00000149 System.Void BossHealth_bee/<EnemyActiveOff>d__17::System.IDisposable.Dispose()
extern void U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_m0A8398FF581CE322BA9D7D845297507334D68646 (void);
// 0x0000014A System.Boolean BossHealth_bee/<EnemyActiveOff>d__17::MoveNext()
extern void U3CEnemyActiveOffU3Ed__17_MoveNext_mB71EFDEC061A3850F6E294F13A6BD6BBC0E32D33 (void);
// 0x0000014B System.Object BossHealth_bee/<EnemyActiveOff>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46706DBCDBF6B07645423CCC5A194AAE73928688 (void);
// 0x0000014C System.Void BossHealth_bee/<EnemyActiveOff>d__17::System.Collections.IEnumerator.Reset()
extern void U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m2AEA766F9011936F0D23BF9592ABFF38B737EAED (void);
// 0x0000014D System.Object BossHealth_bee/<EnemyActiveOff>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m98824C77418948E11B692A5274B598916C00113D (void);
// 0x0000014E System.Void BossHealth_guide::Start()
extern void BossHealth_guide_Start_mB2773F3F37E492C1AA079A6842E683F345B58758 (void);
// 0x0000014F System.Void BossHealth_guide::Update()
extern void BossHealth_guide_Update_mFD88677A8263EFED8DB8DA90048F345CB2B2BF7A (void);
// 0x00000150 System.Void BossHealth_guide::HurtBoss(System.Int32)
extern void BossHealth_guide_HurtBoss_m6F9EF1BA88D568131169E27ED8B1A867014BFD23 (void);
// 0x00000151 System.Collections.IEnumerator BossHealth_guide::EnemyActiveOff()
extern void BossHealth_guide_EnemyActiveOff_m57EA0B9D50AC33B4E0ABDB50F9483C29D89ACF91 (void);
// 0x00000152 System.Void BossHealth_guide::.ctor()
extern void BossHealth_guide__ctor_m8D37A3BE254F43D9F56140DE44FEB755FF8FF3C6 (void);
// 0x00000153 System.Void BossHealth_guide/<EnemyActiveOff>d__17::.ctor(System.Int32)
extern void U3CEnemyActiveOffU3Ed__17__ctor_m09EFDED0A4E6C509759C171FC612A7A4190C362A (void);
// 0x00000154 System.Void BossHealth_guide/<EnemyActiveOff>d__17::System.IDisposable.Dispose()
extern void U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_m1F58E811E2CDB854E5C6DB1FC351391AD8881B1D (void);
// 0x00000155 System.Boolean BossHealth_guide/<EnemyActiveOff>d__17::MoveNext()
extern void U3CEnemyActiveOffU3Ed__17_MoveNext_m150826C406392D16C49FC978BC7D27BE622E5F0B (void);
// 0x00000156 System.Object BossHealth_guide/<EnemyActiveOff>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4E97E597A05A689D1F935145EF371638BB8972C8 (void);
// 0x00000157 System.Void BossHealth_guide/<EnemyActiveOff>d__17::System.Collections.IEnumerator.Reset()
extern void U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m213302082DF8640A57F8F8D259FEDCB319F1564A (void);
// 0x00000158 System.Object BossHealth_guide/<EnemyActiveOff>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_mF3B08ECAA4BDC5A885CF6D75480B2D30BBBD2365 (void);
// 0x00000159 System.Void BossHealth_octopus::Start()
extern void BossHealth_octopus_Start_m983069329E0E5B22487BD2C47E146827E8CB7DB2 (void);
// 0x0000015A System.Void BossHealth_octopus::Update()
extern void BossHealth_octopus_Update_m90624A9B2E5E943AF08BA82644931E7CFE9652F9 (void);
// 0x0000015B System.Void BossHealth_octopus::HurtBoss(System.Int32)
extern void BossHealth_octopus_HurtBoss_m6D44677AF340CF97F61FD5CD78692B43C29D12DF (void);
// 0x0000015C System.Collections.IEnumerator BossHealth_octopus::EnemyActiveOff()
extern void BossHealth_octopus_EnemyActiveOff_mF20C14DC9DE90D938AE47F959C8C3181FE9F3DE6 (void);
// 0x0000015D System.Void BossHealth_octopus::.ctor()
extern void BossHealth_octopus__ctor_mD60621DC3CFF2516D3841EFBC00A8AA3AA9E85D1 (void);
// 0x0000015E System.Void BossHealth_octopus/<EnemyActiveOff>d__16::.ctor(System.Int32)
extern void U3CEnemyActiveOffU3Ed__16__ctor_m656F1D29A891EF8964C433F09D84AFD32EF87E91 (void);
// 0x0000015F System.Void BossHealth_octopus/<EnemyActiveOff>d__16::System.IDisposable.Dispose()
extern void U3CEnemyActiveOffU3Ed__16_System_IDisposable_Dispose_m40B57F8B7DC59586C6D0BD37F92FFF21F0612FF5 (void);
// 0x00000160 System.Boolean BossHealth_octopus/<EnemyActiveOff>d__16::MoveNext()
extern void U3CEnemyActiveOffU3Ed__16_MoveNext_m02E6554E43C3BA5C28A8C7267E00206B96F582B9 (void);
// 0x00000161 System.Object BossHealth_octopus/<EnemyActiveOff>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEnemyActiveOffU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m014CC50FCD57228D47D667F2A4D3D5B3CE9A13F0 (void);
// 0x00000162 System.Void BossHealth_octopus/<EnemyActiveOff>d__16::System.Collections.IEnumerator.Reset()
extern void U3CEnemyActiveOffU3Ed__16_System_Collections_IEnumerator_Reset_m08A275A350536FF546323FFCAB9EB2BE90D166A8 (void);
// 0x00000163 System.Object BossHealth_octopus/<EnemyActiveOff>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CEnemyActiveOffU3Ed__16_System_Collections_IEnumerator_get_Current_m69BAF2DE3C9991F838E0FAEB774C31E5F75958EB (void);
// 0x00000164 System.Void Boss_BatUI::Start()
extern void Boss_BatUI_Start_mA541EB479AB19D6D526B9C8B44DE2D2B8C2E6B48 (void);
// 0x00000165 System.Void Boss_BatUI::Update()
extern void Boss_BatUI_Update_m411A6933469E15A37DA8E5BF4DE036733FCC536B (void);
// 0x00000166 System.Void Boss_BatUI::.ctor()
extern void Boss_BatUI__ctor_m8997480007C0B5550F62C4DE916D773E80BE36F9 (void);
// 0x00000167 System.Void Boss_OctoUI::Start()
extern void Boss_OctoUI_Start_mB5242F6455F7FDB963B6ED367489D6A214171BB1 (void);
// 0x00000168 System.Void Boss_OctoUI::Update()
extern void Boss_OctoUI_Update_m23F95529D2BA133174480529F015C72A25885845 (void);
// 0x00000169 System.Void Boss_OctoUI::.ctor()
extern void Boss_OctoUI__ctor_m39D3F87E3AE8D1813A0B09346571E72DFF41B540 (void);
// 0x0000016A System.Void invulneralbe_intro_bear::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void invulneralbe_intro_bear_OnStateEnter_m07021CFCB6E2B67C4A4733F1EEC30E81131B723D (void);
// 0x0000016B System.Void invulneralbe_intro_bear::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void invulneralbe_intro_bear_OnStateUpdate_m120A65A66444BDA2DAB50E9F8D7168F9D6E61507 (void);
// 0x0000016C System.Void invulneralbe_intro_bear::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void invulneralbe_intro_bear_OnStateExit_mEEEC42A3936207465FCE927664447CF32181E84D (void);
// 0x0000016D System.Void invulneralbe_intro_bear::.ctor()
extern void invulneralbe_intro_bear__ctor_mB186B356261B7FD7A941722700E8C015F3B8DA60 (void);
// 0x0000016E System.Void invulneralbe_intro_bee::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void invulneralbe_intro_bee_OnStateEnter_m12567438DF7CD460E1CD8914C018E4E0FF98F462 (void);
// 0x0000016F System.Void invulneralbe_intro_bee::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void invulneralbe_intro_bee_OnStateUpdate_m54E0A822E045B37E427C0BBBDEDF45A71F863DC3 (void);
// 0x00000170 System.Void invulneralbe_intro_bee::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void invulneralbe_intro_bee_OnStateExit_m0C4C0AAC02BDD8ADB1F5618A5EF56D65857EF13B (void);
// 0x00000171 System.Void invulneralbe_intro_bee::.ctor()
extern void invulneralbe_intro_bee__ctor_m173B306BD4DDFA928F99FD7F60A5895AD77296FB (void);
// 0x00000172 System.Void invulneralbe_intro_guide::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void invulneralbe_intro_guide_OnStateEnter_m41FCC1D9A049E6A80E7CC17DCC1FB16455F36137 (void);
// 0x00000173 System.Void invulneralbe_intro_guide::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void invulneralbe_intro_guide_OnStateUpdate_m7DA52540DA62BB9051EC3BDA80DD14A08326CD7F (void);
// 0x00000174 System.Void invulneralbe_intro_guide::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void invulneralbe_intro_guide_OnStateExit_mFA977A4C2356DFB2401FDF8982179D5E332CEC05 (void);
// 0x00000175 System.Void invulneralbe_intro_guide::.ctor()
extern void invulneralbe_intro_guide__ctor_m240DE89D0C50C106529049A8C1C5CC15AEB311E5 (void);
// 0x00000176 System.Void invulneralbe_intro_octopus::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void invulneralbe_intro_octopus_OnStateEnter_m98AA4B63D700C1DFC664BD5E174B9A8173D2AF30 (void);
// 0x00000177 System.Void invulneralbe_intro_octopus::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void invulneralbe_intro_octopus_OnStateUpdate_m5AC211A1B83D23972436F1F08187DEFC690CEA49 (void);
// 0x00000178 System.Void invulneralbe_intro_octopus::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void invulneralbe_intro_octopus_OnStateExit_m3F21A91ABA8ED9E25CD445FA375B6DD8B2E32A50 (void);
// 0x00000179 System.Void invulneralbe_intro_octopus::.ctor()
extern void invulneralbe_intro_octopus__ctor_m315BA9BB3336200920AED45A7DAD6DC5C7EA9ADD (void);
// 0x0000017A System.Void invulnerable_intro::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void invulnerable_intro_OnStateEnter_m728B6A70E01DE55BCBDEAC1EC7E5BF6C91D649DF (void);
// 0x0000017B System.Void invulnerable_intro::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void invulnerable_intro_OnStateUpdate_m8D4741C6E8C9FEF12626CAD3ECC6A7DF8B539A74 (void);
// 0x0000017C System.Void invulnerable_intro::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void invulnerable_intro_OnStateExit_m484CBA364202346B2D1BED8F4BA3C08C38D8A345 (void);
// 0x0000017D System.Void invulnerable_intro::.ctor()
extern void invulnerable_intro__ctor_m830E223B6908912F955584C5FCF8BA854A755B76 (void);
// 0x0000017E System.Void invulneralbe_intro_ske::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void invulneralbe_intro_ske_OnStateEnter_m1BAF6E7F467E66F1C05F36C5EC51ACA61A812109 (void);
// 0x0000017F System.Void invulneralbe_intro_ske::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void invulneralbe_intro_ske_OnStateUpdate_m763CB3F48D184A03CF3352205909E81FC2B5CFA2 (void);
// 0x00000180 System.Void invulneralbe_intro_ske::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void invulneralbe_intro_ske_OnStateExit_m9A2FF8015BF728A669EA22F255E18BA2251984C5 (void);
// 0x00000181 System.Void invulneralbe_intro_ske::.ctor()
extern void invulneralbe_intro_ske__ctor_m8FCB7B550224E8AAC918A82FF92FBD90439C6572 (void);
// 0x00000182 System.Void BossHealth::Start()
extern void BossHealth_Start_mA3AE29D6097C29A2A0013ACA206294A53CA42BB2 (void);
// 0x00000183 System.Void BossHealth::Update()
extern void BossHealth_Update_mDE473A175B91C0BAA6AF8D7FB4E3B69CB430AD24 (void);
// 0x00000184 System.Void BossHealth::HurtBoss(System.Int32)
extern void BossHealth_HurtBoss_m16E3A925B039D8788F62F5BF29DB617A47596442 (void);
// 0x00000185 System.Collections.IEnumerator BossHealth::EnemyActiveOff()
extern void BossHealth_EnemyActiveOff_mF760A81E37497AF5E86198E6A3FAA7BB5391D9A4 (void);
// 0x00000186 System.Void BossHealth::.ctor()
extern void BossHealth__ctor_mFB5239321838F62176E3743CA846266FED02047B (void);
// 0x00000187 System.Void BossHealth/<EnemyActiveOff>d__17::.ctor(System.Int32)
extern void U3CEnemyActiveOffU3Ed__17__ctor_mEDA113BF186DC2339F8580F67643AB56E1ECE556 (void);
// 0x00000188 System.Void BossHealth/<EnemyActiveOff>d__17::System.IDisposable.Dispose()
extern void U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_mD06651F2C538A3224C9FDDE42A9012B321C56492 (void);
// 0x00000189 System.Boolean BossHealth/<EnemyActiveOff>d__17::MoveNext()
extern void U3CEnemyActiveOffU3Ed__17_MoveNext_mA7FA02FA5A9FF76FF884D3D5ED4ADA49D2BBE83B (void);
// 0x0000018A System.Object BossHealth/<EnemyActiveOff>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m06B0E0AA273012C9CCD56BB02407E66467557687 (void);
// 0x0000018B System.Void BossHealth/<EnemyActiveOff>d__17::System.Collections.IEnumerator.Reset()
extern void U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m2CB85D39D8F8BE0BE1EF9576A51AD73338551C25 (void);
// 0x0000018C System.Object BossHealth/<EnemyActiveOff>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m60DBC26F568D55337E46ED8A3513605501E94744 (void);
// 0x0000018D System.Void BossHealth_ske::Start()
extern void BossHealth_ske_Start_mB0222EF4733E48D0B9C6F4E367F99AE05818B587 (void);
// 0x0000018E System.Void BossHealth_ske::Update()
extern void BossHealth_ske_Update_mA027FA79FDE02A33B025B04CA68570CA8C02F815 (void);
// 0x0000018F System.Void BossHealth_ske::HurtBoss(System.Int32)
extern void BossHealth_ske_HurtBoss_m0875F1319D81D8827F33C4966AFFB6D850FEC3E0 (void);
// 0x00000190 System.Collections.IEnumerator BossHealth_ske::EnemyActiveOff()
extern void BossHealth_ske_EnemyActiveOff_m5F04512A0BE51B250EBDC07BF4E5A8184A1E40D6 (void);
// 0x00000191 System.Void BossHealth_ske::.ctor()
extern void BossHealth_ske__ctor_mE27014B525B9E102E86343ABAE8AE811EBC44108 (void);
// 0x00000192 System.Void BossHealth_ske/<EnemyActiveOff>d__17::.ctor(System.Int32)
extern void U3CEnemyActiveOffU3Ed__17__ctor_mD56FDF58CF76DF6DCF6A0C4CDE06B265665DCCF8 (void);
// 0x00000193 System.Void BossHealth_ske/<EnemyActiveOff>d__17::System.IDisposable.Dispose()
extern void U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_mBBBA8BA4C90EFD0254A9D54C860BE67A49C0EDA7 (void);
// 0x00000194 System.Boolean BossHealth_ske/<EnemyActiveOff>d__17::MoveNext()
extern void U3CEnemyActiveOffU3Ed__17_MoveNext_mDDDE67D382E3FFC813A55AF5D545C36B8FB8FA58 (void);
// 0x00000195 System.Object BossHealth_ske/<EnemyActiveOff>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE5B73C806B8B5F2F2A7CF1822106AD4136D7783 (void);
// 0x00000196 System.Void BossHealth_ske/<EnemyActiveOff>d__17::System.Collections.IEnumerator.Reset()
extern void U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m765DD1E9FEB89FDD28649CC19B8F00F4C20CD5E9 (void);
// 0x00000197 System.Object BossHealth_ske/<EnemyActiveOff>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m0F1D88A2309A0F825E691AE1BF307DE9D2EED219 (void);
// 0x00000198 System.Void BulletController::Start()
extern void BulletController_Start_m077702EC93C8AD8BDCD7C4F994454B0BD2D2C950 (void);
// 0x00000199 System.Void BulletController::.ctor()
extern void BulletController__ctor_m3FA7C9783D93CB95E0C47EB5ECD80A97D25F2CFE (void);
// 0x0000019A System.Void DestroyOverTime::Start()
extern void DestroyOverTime_Start_m662FD2D94D17FF6008521F6C3EE2B2A05960841B (void);
// 0x0000019B System.Void DestroyOverTime::Update()
extern void DestroyOverTime_Update_m9BD3239A91EEC7E53820274885C47DEFFE663495 (void);
// 0x0000019C System.Void DestroyOverTime::.ctor()
extern void DestroyOverTime__ctor_mB8D8C27A30DE70D4609B2EF8B28E7C8C99EF7C14 (void);
// 0x0000019D System.Void BossAI_range_manyBullet::Start()
extern void BossAI_range_manyBullet_Start_m453E9DCD5B618EE606E6EF2922EC8D599678373E (void);
// 0x0000019E System.Void BossAI_range_manyBullet::Update()
extern void BossAI_range_manyBullet_Update_m1EE084FDFCA75A84E38514043AEE02DD0B149682 (void);
// 0x0000019F System.Void BossAI_range_manyBullet::OnDrawGizmosSelected()
extern void BossAI_range_manyBullet_OnDrawGizmosSelected_m46C4F40916465E05DE45F910E21AA688061AC853 (void);
// 0x000001A0 System.Void BossAI_range_manyBullet::.ctor()
extern void BossAI_range_manyBullet__ctor_mCA3B4D2B8A41E78B4CBD0CBC7A9C0F09E7D9A629 (void);
// 0x000001A1 System.Void BulletControler_point::Start()
extern void BulletControler_point_Start_m8C00CF70C68D30F22792F5ED5E7181280E8D1F5B (void);
// 0x000001A2 System.Void BulletControler_point::.ctor()
extern void BulletControler_point__ctor_m1AE31FFE56106B16E60B36CEEB0F238D7D7E2F11 (void);
// 0x000001A3 System.Void BulletController_Bottom::Start()
extern void BulletController_Bottom_Start_mE0028E4BA47A4BDB71CAFD79831361840D00BEBE (void);
// 0x000001A4 System.Void BulletController_Bottom::.ctor()
extern void BulletController_Bottom__ctor_m4AB93D5650E30312C874F373D00C287A2EAAF315 (void);
// 0x000001A5 System.Void BulletController_Top::Start()
extern void BulletController_Top_Start_mBF11C12A40F192076459F0167642AE0707023DF4 (void);
// 0x000001A6 System.Void BulletController_Top::.ctor()
extern void BulletController_Top__ctor_mD1C529ACE9D70F60996F96666D442ACA4E982FAC (void);
// 0x000001A7 System.Void BurningPlayer_range::Start()
extern void BurningPlayer_range_Start_m7CF1BA6A4CA4BBE50B640AE07D51046B14B1FED0 (void);
// 0x000001A8 System.Void BurningPlayer_range::Update()
extern void BurningPlayer_range_Update_m8C3EC9B6A06C615F47B9CEB8DFE97902D9ABAC74 (void);
// 0x000001A9 System.Void BurningPlayer_range::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void BurningPlayer_range_OnCollisionEnter2D_m062E86E7317746A9F7FC8C2689341A86F4055561 (void);
// 0x000001AA System.Void BurningPlayer_range::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void BurningPlayer_range_OnTriggerEnter2D_mE81EB11F454E35CAD15B1268F24CE37732394462 (void);
// 0x000001AB System.Void BurningPlayer_range::OnCollisionStay2D(UnityEngine.Collision2D)
extern void BurningPlayer_range_OnCollisionStay2D_m36BE92C549A83700967693E992748D019EA854E4 (void);
// 0x000001AC System.Void BurningPlayer_range::OnCollisionExit2D(UnityEngine.Collision2D)
extern void BurningPlayer_range_OnCollisionExit2D_m940A14B854A471C54DFF7B9A6D667A9FBE470EE4 (void);
// 0x000001AD System.Void BurningPlayer_range::.ctor()
extern void BurningPlayer_range__ctor_mA6111BB1887DE045FABC6431D2161A7D8726800D (void);
// 0x000001AE System.Void EnemyBoom::Start()
extern void EnemyBoom_Start_m7C85105A17405340ED8AD329A48C05FA2560AF97 (void);
// 0x000001AF System.Void EnemyBoom::Update()
extern void EnemyBoom_Update_mC3F27E6CA3AC5E6FBEC4CF77E270276A533550FD (void);
// 0x000001B0 System.Void EnemyBoom::OnDestroy()
extern void EnemyBoom_OnDestroy_m5C3945187042FA4CAA547E8C101B6034FE6E8E85 (void);
// 0x000001B1 System.Void EnemyBoom::.ctor()
extern void EnemyBoom__ctor_m48EEAC67443C01F6AA7902D7F5CE871A6DFED8CC (void);
// 0x000001B2 System.Void EnemyHidden::Start()
extern void EnemyHidden_Start_mB994E8226BE485862BD4D1F6F001DFAE48F55645 (void);
// 0x000001B3 System.Void EnemyHidden::Update()
extern void EnemyHidden_Update_mEE46B0F0608CC7C3D4988243DDB94127BE456D32 (void);
// 0x000001B4 System.Void EnemyHidden::OnDrawGizmosSelected()
extern void EnemyHidden_OnDrawGizmosSelected_m35050894366F24024622FFEDF5B2F6E566835A94 (void);
// 0x000001B5 System.Void EnemyHidden::.ctor()
extern void EnemyHidden__ctor_m5C2034770CA45E56861481014F523E8F677B5997 (void);
// 0x000001B6 System.Void EnemyMaxSpeed::Start()
extern void EnemyMaxSpeed_Start_mD8AB2A1457C3528F413A96721F4C8E6FAEBB20C4 (void);
// 0x000001B7 System.Void EnemyMaxSpeed::Update()
extern void EnemyMaxSpeed_Update_mF8B8FC186A4B3ACD35476D7C49C1DB6CCD90C6ED (void);
// 0x000001B8 System.Void EnemyMaxSpeed::OnDrawGizmosSelected()
extern void EnemyMaxSpeed_OnDrawGizmosSelected_m3C89F33DEF6AD47414AAFDF94C483E1122962F6C (void);
// 0x000001B9 System.Void EnemyMaxSpeed::.ctor()
extern void EnemyMaxSpeed__ctor_m819BC3E4D593E0CB237B48B89AFA68D200806691 (void);
// 0x000001BA System.Void EnemySpecailAI_mele::Start()
extern void EnemySpecailAI_mele_Start_m49CEA83550E20D807786B4CAAA336B749E29D7D2 (void);
// 0x000001BB System.Void EnemySpecailAI_mele::Update()
extern void EnemySpecailAI_mele_Update_mB6790E1BEC1ED87AE175C421C9CFB3B1A8BAFE4D (void);
// 0x000001BC System.Void EnemySpecailAI_mele::OnDrawGizmosSelected()
extern void EnemySpecailAI_mele_OnDrawGizmosSelected_m0C9157AC88F3333046CFC52397622754CC653C61 (void);
// 0x000001BD System.Void EnemySpecailAI_mele::.ctor()
extern void EnemySpecailAI_mele__ctor_m8FA88C57F7186F64D8B3384AE2DA3BB24ED80FB9 (void);
// 0x000001BE System.Void EnemyTwoBulletAI_range::Start()
extern void EnemyTwoBulletAI_range_Start_mED94AF945EB3CCA9345CDC51878DC75EEB343092 (void);
// 0x000001BF System.Void EnemyTwoBulletAI_range::Update()
extern void EnemyTwoBulletAI_range_Update_mA8397753A93C3CEFA4A548AAC454CD62BFD38130 (void);
// 0x000001C0 System.Void EnemyTwoBulletAI_range::OnDrawGizmosSelected()
extern void EnemyTwoBulletAI_range_OnDrawGizmosSelected_m2E7668AFB401215059920BA368BE4F2D37230D3A (void);
// 0x000001C1 System.Void EnemyTwoBulletAI_range::.ctor()
extern void EnemyTwoBulletAI_range__ctor_m3E186310307EE4FC7172ED107F14E280771BCACA (void);
// 0x000001C2 System.Void HomingBullet::Start()
extern void HomingBullet_Start_m33BF1CF4FE0168633CAA95DFCC6639E7ECE22E0D (void);
// 0x000001C3 System.Void HomingBullet::Update()
extern void HomingBullet_Update_m33D482A89899A7A03357F2BB7872D2E593130AAB (void);
// 0x000001C4 System.Void HomingBullet::.ctor()
extern void HomingBullet__ctor_mFD1067B09D96CD656ECA0375F35AC3428F0DCCDA (void);
// 0x000001C5 System.Void LoseControlPlayer_range::Start()
extern void LoseControlPlayer_range_Start_m2999B9A2F9D57664003EA553DFB672FEDB28822B (void);
// 0x000001C6 System.Void LoseControlPlayer_range::Update()
extern void LoseControlPlayer_range_Update_m273A8E766F233E2394E5C8B2FD8427F45378A173 (void);
// 0x000001C7 System.Void LoseControlPlayer_range::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void LoseControlPlayer_range_OnCollisionEnter2D_mF6C51617DA8079CFC3D4922CB1AA8C7FC67532E8 (void);
// 0x000001C8 System.Void LoseControlPlayer_range::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void LoseControlPlayer_range_OnTriggerEnter2D_m643C8335950592E9E1D4212D31356C647E75F56F (void);
// 0x000001C9 System.Void LoseControlPlayer_range::OnCollisionStay2D(UnityEngine.Collision2D)
extern void LoseControlPlayer_range_OnCollisionStay2D_m2ACA69DB7B33473FF6D4BD7BDFD9BC2AA6180953 (void);
// 0x000001CA System.Void LoseControlPlayer_range::OnCollisionExit2D(UnityEngine.Collision2D)
extern void LoseControlPlayer_range_OnCollisionExit2D_m272ADB69681748347DCC146257022DA2313CC195 (void);
// 0x000001CB System.Void LoseControlPlayer_range::.ctor()
extern void LoseControlPlayer_range__ctor_m8082D7DC4FE43207E8F4E431AB9A9ED3B806F63F (void);
// 0x000001CC System.Void SlowPlayer_range::Start()
extern void SlowPlayer_range_Start_m8AF6A780DD54263C01900ABD47900D6627F2A8E8 (void);
// 0x000001CD System.Void SlowPlayer_range::Update()
extern void SlowPlayer_range_Update_mE35BED872010FE158FEA61E35E33145CF71526F4 (void);
// 0x000001CE System.Void SlowPlayer_range::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void SlowPlayer_range_OnCollisionEnter2D_mF8C1489067AE9859107A4B104AF765077D74CD56 (void);
// 0x000001CF System.Void SlowPlayer_range::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void SlowPlayer_range_OnTriggerEnter2D_mD2CD24FFD115D8ABED401D7FD4B8BCA9F286361F (void);
// 0x000001D0 System.Void SlowPlayer_range::OnCollisionStay2D(UnityEngine.Collision2D)
extern void SlowPlayer_range_OnCollisionStay2D_mD363D5DCD4F04608DB77FFB4468B040FD4AB9802 (void);
// 0x000001D1 System.Void SlowPlayer_range::OnCollisionExit2D(UnityEngine.Collision2D)
extern void SlowPlayer_range_OnCollisionExit2D_m3DA04D01094CB4A212C5FBB9EAE6DFBF650334B8 (void);
// 0x000001D2 System.Void SlowPlayer_range::.ctor()
extern void SlowPlayer_range__ctor_mF26CD857FAC008A28CFE6314FCDE9F8BC21C4B8F (void);
// 0x000001D3 System.Void SoundWave::Start()
extern void SoundWave_Start_m51D8AA75403FE9527EDBFCABE17A5223A368AF93 (void);
// 0x000001D4 System.Void SoundWave::OnDestroy()
extern void SoundWave_OnDestroy_mB418A919064A5AD57DEACA13D697F52605A26ED4 (void);
// 0x000001D5 System.Void SoundWave::.ctor()
extern void SoundWave__ctor_mB9885D46E39108F6DF53945D5586D6689E86FBB9 (void);
// 0x000001D6 System.Void bossAI_range_hidden::Start()
extern void bossAI_range_hidden_Start_m2861FEC7C2D0EAFB8024C01FC47586794ECCEB38 (void);
// 0x000001D7 System.Void bossAI_range_hidden::Update()
extern void bossAI_range_hidden_Update_m6A3CB9FDE704708ECED8CAE131AE02CC25C47FD9 (void);
// 0x000001D8 System.Void bossAI_range_hidden::OnDrawGizmosSelected()
extern void bossAI_range_hidden_OnDrawGizmosSelected_m85037F12BC4FB4C1D033A563558ED7B70A1DD382 (void);
// 0x000001D9 System.Void bossAI_range_hidden::.ctor()
extern void bossAI_range_hidden__ctor_mDA4A5F3885EDCDBBAA0201F7D72356B0E8F7ADAA (void);
// 0x000001DA System.Void bossAI_range_twoButlet::Start()
extern void bossAI_range_twoButlet_Start_m9482F999E756CB4E8A5ADBCA98B766ABD9FED00C (void);
// 0x000001DB System.Void bossAI_range_twoButlet::Update()
extern void bossAI_range_twoButlet_Update_m3C927564371364AA732AC7A8A7CC96265822518C (void);
// 0x000001DC System.Void bossAI_range_twoButlet::OnDrawGizmosSelected()
extern void bossAI_range_twoButlet_OnDrawGizmosSelected_m1B50F7598D827BC1B389294545B85F7AD995FC46 (void);
// 0x000001DD System.Void bossAI_range_twoButlet::.ctor()
extern void bossAI_range_twoButlet__ctor_m001B5A763A616107B93550D1791712D34E6DAE70 (void);
// 0x000001DE System.Void HealthEnemy::Start()
extern void HealthEnemy_Start_mC56EE0321F2612C61B3409959D05E3E1B16F437C (void);
// 0x000001DF System.Void HealthEnemy::Update()
extern void HealthEnemy_Update_mD1E2E578AAE274D60119B1ADDB6BAFA521F58E8F (void);
// 0x000001E0 System.Void HealthEnemy::HurtEnemy(System.Int32)
extern void HealthEnemy_HurtEnemy_m0F465E21702E5A442EA72404AE5E1735A2F04293 (void);
// 0x000001E1 System.Void HealthEnemy::.ctor()
extern void HealthEnemy__ctor_mE6B37AE46A34634ECC166910FAE5C57876AB437F (void);
// 0x000001E2 System.Void HurtPlayer::Start()
extern void HurtPlayer_Start_m3E536B2A7022E93348AA1D6BD86391A39A1756C1 (void);
// 0x000001E3 System.Void HurtPlayer::Update()
extern void HurtPlayer_Update_m588D6701F4DEF4EF859909EEC305E89980D636A1 (void);
// 0x000001E4 System.Void HurtPlayer::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void HurtPlayer_OnCollisionEnter2D_m515D5ABD142BF8F60E335E7E1CEE115A1C5F9F0D (void);
// 0x000001E5 System.Void HurtPlayer::OnCollisionStay2D(UnityEngine.Collision2D)
extern void HurtPlayer_OnCollisionStay2D_mF8B52F0923493B4D9B605CEE24ABC0D7BC93ED30 (void);
// 0x000001E6 System.Void HurtPlayer::OnCollisionExit2D(UnityEngine.Collision2D)
extern void HurtPlayer_OnCollisionExit2D_mA47B649D48E6CED91DD3225D75CA194380E9FFDD (void);
// 0x000001E7 System.Void HurtPlayer::.ctor()
extern void HurtPlayer__ctor_m80C2BE1434C282EDD0B8DD1DB8BD15C006C03DF3 (void);
// 0x000001E8 System.Void HurtPlayer_Range::Start()
extern void HurtPlayer_Range_Start_m6790348704E3203EB834F40EC629B9B247961328 (void);
// 0x000001E9 System.Void HurtPlayer_Range::Update()
extern void HurtPlayer_Range_Update_mFD7CAD7471BD4994060781005CD6723CF33C5C29 (void);
// 0x000001EA System.Void HurtPlayer_Range::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void HurtPlayer_Range_OnCollisionEnter2D_mE0E5E8EE6863A18B4F5FCC4A626F8FBA36FD1D50 (void);
// 0x000001EB System.Void HurtPlayer_Range::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void HurtPlayer_Range_OnTriggerEnter2D_mB7678D9CD6839DC522B85F521B5FC04A5BBD98FC (void);
// 0x000001EC System.Void HurtPlayer_Range::OnCollisionStay2D(UnityEngine.Collision2D)
extern void HurtPlayer_Range_OnCollisionStay2D_m34912BD32548E59537EAA9A85B8964E03E939634 (void);
// 0x000001ED System.Void HurtPlayer_Range::OnCollisionExit2D(UnityEngine.Collision2D)
extern void HurtPlayer_Range_OnCollisionExit2D_mCC4604D7DAEE54F5CC3F6EE59C809901EEFC5769 (void);
// 0x000001EE System.Void HurtPlayer_Range::.ctor()
extern void HurtPlayer_Range__ctor_mDE23AFC6E81DEFAD77A20054055B85EC146F79C8 (void);
// 0x000001EF System.Void ShootingDummy::Start()
extern void ShootingDummy_Start_m24DD5A03887561682FF485401D6D1DED65992D08 (void);
// 0x000001F0 System.Void ShootingDummy::Update()
extern void ShootingDummy_Update_mDD427DE79F8FAA1EB8BBD18B453D4DE422F2B310 (void);
// 0x000001F1 System.Void ShootingDummy::OnDrawGizmosSelected()
extern void ShootingDummy_OnDrawGizmosSelected_m954989824FC5D31EC8FFBA4A32BB885939ACB6F9 (void);
// 0x000001F2 System.Void ShootingDummy::.ctor()
extern void ShootingDummy__ctor_m62E7D85ED11CF13CAFCEBFDA247ABC55A201E6B5 (void);
// 0x000001F3 System.Void bossAI::Start()
extern void bossAI_Start_m3454A1102162B0048F1A88D0E915FE88C1B2274F (void);
// 0x000001F4 System.Void bossAI::Update()
extern void bossAI_Update_mBE8FFB69B945D69745C2744680FC512F8D7AB695 (void);
// 0x000001F5 System.Void bossAI::OnDrawGizmosSelected()
extern void bossAI_OnDrawGizmosSelected_m6D2BEE5C1AD67CEFADE8A5B95EF4EC0F885C9393 (void);
// 0x000001F6 System.Void bossAI::.ctor()
extern void bossAI__ctor_m5EEDC084090F7A11DBCB9349D8C7947F2E712D90 (void);
// 0x000001F7 System.Void bossAI_range::Start()
extern void bossAI_range_Start_m2469BAED2598B4A5A820ADCB448F4E3BF4802AFB (void);
// 0x000001F8 System.Void bossAI_range::Update()
extern void bossAI_range_Update_m0D33FEB3D3BD03CC237DC1498FBAF854AE2AFFD4 (void);
// 0x000001F9 System.Void bossAI_range::OnDrawGizmosSelected()
extern void bossAI_range_OnDrawGizmosSelected_mFD6D96F800BDBB89696FAEFEB19E7C3615DE38D9 (void);
// 0x000001FA System.Void bossAI_range::.ctor()
extern void bossAI_range__ctor_mE6ECA6DB99ED4A5437AC96DF181C94FE7832D845 (void);
// 0x000001FB System.Void enemyAI_melee::Start()
extern void enemyAI_melee_Start_m1C5500CA548367191904FF649AEC669B992E270A (void);
// 0x000001FC System.Void enemyAI_melee::Update()
extern void enemyAI_melee_Update_m4B2225D07E7B3C9AF26BF538FA07517609FBC76F (void);
// 0x000001FD System.Void enemyAI_melee::OnDrawGizmosSelected()
extern void enemyAI_melee_OnDrawGizmosSelected_mD7FB6D542568B7A26EAECD13C9D2EFA2F513F669 (void);
// 0x000001FE System.Void enemyAI_melee::.ctor()
extern void enemyAI_melee__ctor_m0AB73B6F4923B50A82BF6FC88993FCD016C9FD4A (void);
// 0x000001FF System.Void enemyAI_range::Start()
extern void enemyAI_range_Start_m07F7770D4883E7D4BDBE0A752ECFBA692C6542B8 (void);
// 0x00000200 System.Void enemyAI_range::Update()
extern void enemyAI_range_Update_m838820B0878B87F55273BA6302443DDE63896EB5 (void);
// 0x00000201 System.Void enemyAI_range::OnDrawGizmosSelected()
extern void enemyAI_range_OnDrawGizmosSelected_m179DE2671200455333EC03A6C06D6BE96ECC518B (void);
// 0x00000202 System.Void enemyAI_range::.ctor()
extern void enemyAI_range__ctor_m1F03487BB326625996FAA02864069D30E55253AC (void);
// 0x00000203 System.Void BossDontActiveForSaving::Start()
extern void BossDontActiveForSaving_Start_m6CF571F25714A305F91949EE26843ED37416BC40 (void);
// 0x00000204 System.Void BossDontActiveForSaving::Update()
extern void BossDontActiveForSaving_Update_mC0934663FEF8AC52C6DDD01A39219A58568893FE (void);
// 0x00000205 System.Void BossDontActiveForSaving::.ctor()
extern void BossDontActiveForSaving__ctor_mF4440D989398DA89DA3813E6978849F86515CC2C (void);
// 0x00000206 System.Void BossUIManager::Start()
extern void BossUIManager_Start_m44E2034D966801C25F4B5990EFE47F398018D3BB (void);
// 0x00000207 System.Void BossUIManager::Update()
extern void BossUIManager_Update_mC6C71B727BC9710A92961A16A6E0BB3DBC05DEE6 (void);
// 0x00000208 System.Void BossUIManager::.ctor()
extern void BossUIManager__ctor_m1035850BE20EA5D0DE2369BC2F612BF794F21FAD (void);
// 0x00000209 System.Void Boss_SKEUI::Start()
extern void Boss_SKEUI_Start_mF87154DBA2352EA1870C2C4FD3A7BFF79CB94B1A (void);
// 0x0000020A System.Void Boss_SKEUI::Update()
extern void Boss_SKEUI_Update_m2FE6C96A3456BDC36F67A676F8A17BAE0D3AF8FB (void);
// 0x0000020B System.Void Boss_SKEUI::.ctor()
extern void Boss_SKEUI__ctor_mA8FDED1B7EF57D350A17E988D0C268CF82013154 (void);
// 0x0000020C System.Void Boss_bear_UI::Start()
extern void Boss_bear_UI_Start_mC653291D86DA0F2CAEEEFA1B1D7B17A20DE90F32 (void);
// 0x0000020D System.Void Boss_bear_UI::Update()
extern void Boss_bear_UI_Update_m7ECA5BE8D8913DC0D771B3A34DD22126D9137020 (void);
// 0x0000020E System.Void Boss_bear_UI::.ctor()
extern void Boss_bear_UI__ctor_m30A0F6ABC72E4DD232823CB4EF8AD2E8E5475F28 (void);
// 0x0000020F System.Void Boss_bee_UI::Start()
extern void Boss_bee_UI_Start_m99288CDEFD4F8CFC253A61C58AEA8BA2B9AE29B4 (void);
// 0x00000210 System.Void Boss_bee_UI::Update()
extern void Boss_bee_UI_Update_m8388990BA4B262DF1F2EDE7E470EB53B1445DE58 (void);
// 0x00000211 System.Void Boss_bee_UI::.ctor()
extern void Boss_bee_UI__ctor_m50EA0D9C9C78E67AAA5FEEE18E7A7454B9D73626 (void);
// 0x00000212 System.Void Boss_guide_UI::Start()
extern void Boss_guide_UI_Start_m946701A5648F4350297E152A7979C94C8211EBB6 (void);
// 0x00000213 System.Void Boss_guide_UI::Update()
extern void Boss_guide_UI_Update_m38C1CDE034F7CA2052E03AF0B17DD3C3053E79FB (void);
// 0x00000214 System.Void Boss_guide_UI::.ctor()
extern void Boss_guide_UI__ctor_m4E0AC32384F4C2E0C53205780456A9FF7D945144 (void);
// 0x00000215 System.Void CameraFollow::Start()
extern void CameraFollow_Start_mC13DAB135378AADB3E72275F75CDEAF53343810D (void);
// 0x00000216 System.Void CameraFollow::Update()
extern void CameraFollow_Update_m9762CC5B7A2D28B7B8116BA8DE2342AFB654AB08 (void);
// 0x00000217 System.Void CameraFollow::.ctor()
extern void CameraFollow__ctor_m29F88CCFD2ED12A7BCC75A9BBA892CEF179C83DE (void);
// 0x00000218 System.Void DamageIndicator::Start()
extern void DamageIndicator_Start_m4B11F0BBDA0E041B7CC3D21229D415AF5C8C4EF4 (void);
// 0x00000219 System.Void DamageIndicator::Update()
extern void DamageIndicator_Update_m464572748BC80B633E7824A9866A9E47BE399184 (void);
// 0x0000021A System.Void DamageIndicator::SetDamageText(System.Int32)
extern void DamageIndicator_SetDamageText_m96D6644016BA136E458B87C0FDB35B1BF77849F8 (void);
// 0x0000021B System.Void DamageIndicator::SetOutOfMoney()
extern void DamageIndicator_SetOutOfMoney_mA4301DCFA7B93C0DDCEA1FA21447732191509BEB (void);
// 0x0000021C System.Void DamageIndicator::SetItemText(System.String)
extern void DamageIndicator_SetItemText_m6EF24042B6EE7A87EBF536E77BA53BFA81C1711E (void);
// 0x0000021D System.Void DamageIndicator::.ctor()
extern void DamageIndicator__ctor_m829527469E0E1B01175A96EDB17FDEB95236C962 (void);
// 0x0000021E System.Void Fullscreen::FullScreen(System.Boolean)
extern void Fullscreen_FullScreen_m0A8637F3844B0564F31FB4DABF216FBBF4784989 (void);
// 0x0000021F System.Void Fullscreen::.ctor()
extern void Fullscreen__ctor_mE9107247101E1586477FFB6CB8B9753BA8B81E2D (void);
// 0x00000220 System.Void GameOverScreen::Start()
extern void GameOverScreen_Start_mFDDF72E824C00B16DC7A6A96B73561A8D22D5C20 (void);
// 0x00000221 System.Collections.IEnumerator GameOverScreen::TimeIE()
extern void GameOverScreen_TimeIE_mB42A38029BD1334D965780125E80522703D0FA43 (void);
// 0x00000222 System.Void GameOverScreen::LoadLevel(System.Int32)
extern void GameOverScreen_LoadLevel_mF10475F6D1A880CCB57A34ADA4C74540F6283C48 (void);
// 0x00000223 System.Collections.IEnumerator GameOverScreen::LoadAsynchronously(System.Int32)
extern void GameOverScreen_LoadAsynchronously_m2197BDDACA1C511BBDAF27424927DEFE650E54B1 (void);
// 0x00000224 System.Void GameOverScreen::.ctor()
extern void GameOverScreen__ctor_mE9F89EF0E4A9BFE845256528F4FDE19F075BCD95 (void);
// 0x00000225 System.Void GameOverScreen/<TimeIE>d__8::.ctor(System.Int32)
extern void U3CTimeIEU3Ed__8__ctor_mAA3177335E8BFA01622B55BE8DCB2CEF5C18261E (void);
// 0x00000226 System.Void GameOverScreen/<TimeIE>d__8::System.IDisposable.Dispose()
extern void U3CTimeIEU3Ed__8_System_IDisposable_Dispose_m090F29683C88433E5BA4F5FAE98DA1997CA167E5 (void);
// 0x00000227 System.Boolean GameOverScreen/<TimeIE>d__8::MoveNext()
extern void U3CTimeIEU3Ed__8_MoveNext_mC92341834398526839EA956BF39A2D4C45C0B2BC (void);
// 0x00000228 System.Object GameOverScreen/<TimeIE>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTimeIEU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99EC495C6BCDAC6E1162941E07936462B1BD4E2D (void);
// 0x00000229 System.Void GameOverScreen/<TimeIE>d__8::System.Collections.IEnumerator.Reset()
extern void U3CTimeIEU3Ed__8_System_Collections_IEnumerator_Reset_m9BE542B45B26F5C2D6EB34B44370BCCAB9D5D3E2 (void);
// 0x0000022A System.Object GameOverScreen/<TimeIE>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CTimeIEU3Ed__8_System_Collections_IEnumerator_get_Current_m0B2BFFB0D772E8E233ECDF8F858655AE9A4D03C1 (void);
// 0x0000022B System.Void GameOverScreen/<LoadAsynchronously>d__10::.ctor(System.Int32)
extern void U3CLoadAsynchronouslyU3Ed__10__ctor_mF01B26F4D4B06F59BA1A098B17A25DCFE6E42DE9 (void);
// 0x0000022C System.Void GameOverScreen/<LoadAsynchronously>d__10::System.IDisposable.Dispose()
extern void U3CLoadAsynchronouslyU3Ed__10_System_IDisposable_Dispose_m88D1C07D91A4DA9A053C84DE67353199F14E6047 (void);
// 0x0000022D System.Boolean GameOverScreen/<LoadAsynchronously>d__10::MoveNext()
extern void U3CLoadAsynchronouslyU3Ed__10_MoveNext_mFC3CFBEC4CB3A0E13AA57F7B69D6BBD015DB21ED (void);
// 0x0000022E System.Object GameOverScreen/<LoadAsynchronously>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadAsynchronouslyU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF15B16A4766E1121E2355C87404FB6A1EF43AF3A (void);
// 0x0000022F System.Void GameOverScreen/<LoadAsynchronously>d__10::System.Collections.IEnumerator.Reset()
extern void U3CLoadAsynchronouslyU3Ed__10_System_Collections_IEnumerator_Reset_m2EB3DC3D189F6976EFDB9B847CE615472B0F9BFA (void);
// 0x00000230 System.Object GameOverScreen/<LoadAsynchronously>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CLoadAsynchronouslyU3Ed__10_System_Collections_IEnumerator_get_Current_mA40DA494789036083420705DAC23DAE8C5D80B08 (void);
// 0x00000231 System.Void GoldManager::Start()
extern void GoldManager_Start_m4692B6B3F21C031390395FB32DF419B121FA811C (void);
// 0x00000232 System.Void GoldManager::AddMoney(System.Int32)
extern void GoldManager_AddMoney_m71A523BAB56F95ADFFF5AF57377E254582B56139 (void);
// 0x00000233 System.Void GoldManager::MinusMoney()
extern void GoldManager_MinusMoney_mD9A178E69C5CBE5309F3036C45DB29D65EA97153 (void);
// 0x00000234 System.Void GoldManager::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void GoldManager_OnTriggerEnter2D_m7ED56676F277AF6CCB2BD37449C2AD8CF2F26FA9 (void);
// 0x00000235 System.Void GoldManager::.ctor()
extern void GoldManager__ctor_m64805587693429C5BEFE4EF2809CA47D6B077B3F (void);
// 0x00000236 System.Void HealthIndicator::Start()
extern void HealthIndicator_Start_m9EAB09AAF53A32BA4A225F473D24DEEC7E6FB606 (void);
// 0x00000237 System.Void HealthIndicator::Update()
extern void HealthIndicator_Update_m24E78A0378EE18E8359E4A58B428455CD4243A12 (void);
// 0x00000238 System.Void HealthIndicator::SetHealText(System.Int32)
extern void HealthIndicator_SetHealText_m3C2B3E3F7FFA7682C32200F899A6E9B0BFC02368 (void);
// 0x00000239 System.Void HealthIndicator::SetBuffText(System.String)
extern void HealthIndicator_SetBuffText_m825EB05118965278D6B5597EB1C959FB80A6BD83 (void);
// 0x0000023A System.Void HealthIndicator::.ctor()
extern void HealthIndicator__ctor_mC88AF59253C4F2E5894D7BE1BFE1BD0010A2CBAE (void);
// 0x0000023B System.Void LayerTrigger::OnTriggerExit2D(UnityEngine.Collider2D)
extern void LayerTrigger_OnTriggerExit2D_mCE8C8632080C77F62252C106B1B2EFAA3E0B0A27 (void);
// 0x0000023C System.Void LayerTrigger::.ctor()
extern void LayerTrigger__ctor_mB75FEF1FD25B454626697F3FADF7B4CDAB9D958A (void);
// 0x0000023D System.Void MenuinGame::PlayNewGame()
extern void MenuinGame_PlayNewGame_mE9ACFDE3313A217536BF12CF315F14E36315FB38 (void);
// 0x0000023E System.Void MenuinGame::LoadGame()
extern void MenuinGame_LoadGame_m8EDADC5B55DC445147A011FA5545124AE0205349 (void);
// 0x0000023F System.Void MenuinGame::QuitGame()
extern void MenuinGame_QuitGame_m19DB364B9615563FE64C838B71B53F632FEB8B7D (void);
// 0x00000240 System.Void MenuinGame::.ctor()
extern void MenuinGame__ctor_m4EDD7D23BD813E543CF032196D7AB8F4D17BD829 (void);
// 0x00000241 System.Void PauseMenuController::Start()
extern void PauseMenuController_Start_mEAA2328A293796EE86216C908A280BEBB87D3F86 (void);
// 0x00000242 System.Void PauseMenuController::Update()
extern void PauseMenuController_Update_m022C5DBBDBED4EACEFA4EED6C85CA3B7F138AE0C (void);
// 0x00000243 System.Void PauseMenuController::Resume()
extern void PauseMenuController_Resume_mE338B9293CC4AC02F54C4A9FA198AA1D1063CA42 (void);
// 0x00000244 System.Void PauseMenuController::Pause()
extern void PauseMenuController_Pause_mE78040C6E68167806E87835C45764DAB28BC6EEA (void);
// 0x00000245 System.Void PauseMenuController::SaveGame()
extern void PauseMenuController_SaveGame_m8D5C6B1A60998F0026F1CF2ED1167B88E4997CF0 (void);
// 0x00000246 System.Void PauseMenuController::QuitGame()
extern void PauseMenuController_QuitGame_m6BDEEF827F4B7E602B2E0C1A8933B89D56E565E4 (void);
// 0x00000247 System.Void PauseMenuController::.ctor()
extern void PauseMenuController__ctor_m7D2060CB9E1BD7B17CF152ADF6D6E7D638959045 (void);
// 0x00000248 System.Void PauseMenuController::.cctor()
extern void PauseMenuController__cctor_mF77CD11EC9CBE6469016D025C324C99FA0C792D4 (void);
// 0x00000249 System.Void PropsAltar::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void PropsAltar_OnTriggerEnter2D_m7A84B17448AB31FEE3762C8C743DD18C5EF5883D (void);
// 0x0000024A System.Void PropsAltar::OnTriggerExit2D(UnityEngine.Collider2D)
extern void PropsAltar_OnTriggerExit2D_m36A8E5C39217D0DB51631F5F7ACC22DACA639E7D (void);
// 0x0000024B System.Void PropsAltar::Update()
extern void PropsAltar_Update_m07C13405E8CF7859974FB0BFEECAC93EA6B4F9BE (void);
// 0x0000024C System.Void PropsAltar::.ctor()
extern void PropsAltar__ctor_m83651FF438237F5D5E605B7B77B62E8D01F54340 (void);
// 0x0000024D System.Void Resolution::SetRes(System.Int32)
extern void Resolution_SetRes_mAEBF43C0177F6818A10C8C025070ECB8AFF825D2 (void);
// 0x0000024E System.Void Resolution::.ctor()
extern void Resolution__ctor_m36AF40279D2099B748847EC1E6C722B2CEAE4227 (void);
// 0x0000024F System.Void SaveManager::Start()
extern void SaveManager_Start_m4A4409B7E949D33DFB06F3CF060ADC408AC6D0FD (void);
// 0x00000250 System.Void SaveManager::SaveGame()
extern void SaveManager_SaveGame_m352C22B0491CCDE655040886EA0A4235B1DC3D32 (void);
// 0x00000251 System.Void SaveManager::LoadGame()
extern void SaveManager_LoadGame_mD87EB9193A13C6D8F8B155A737E1BDE09DD8E6E7 (void);
// 0x00000252 System.Int32 SaveManager::BoolToInt(System.Boolean)
extern void SaveManager_BoolToInt_m0616EA8A189C69B48693DFDDECFFED185ECD8E4D (void);
// 0x00000253 System.Boolean SaveManager::IntToBool(System.Int32)
extern void SaveManager_IntToBool_m537DD6E8CB1E49F633C38FA1B636BB57E01FC108 (void);
// 0x00000254 System.Collections.IEnumerator SaveManager::SavingTime()
extern void SaveManager_SavingTime_m609D3CFA5870774AF4E7B15546B731113A8A19A8 (void);
// 0x00000255 System.Void SaveManager::.ctor()
extern void SaveManager__ctor_mB7AB326D2EA77388E750706DEABFB056A6E1C356 (void);
// 0x00000256 System.Void SaveManager/<SavingTime>d__15::.ctor(System.Int32)
extern void U3CSavingTimeU3Ed__15__ctor_m2757447DB09BEDAA4D2FFF6BEB20DAAB8C5A9446 (void);
// 0x00000257 System.Void SaveManager/<SavingTime>d__15::System.IDisposable.Dispose()
extern void U3CSavingTimeU3Ed__15_System_IDisposable_Dispose_mF18CE4056508CE8E40BCFD44E8E2B6700C4E14BD (void);
// 0x00000258 System.Boolean SaveManager/<SavingTime>d__15::MoveNext()
extern void U3CSavingTimeU3Ed__15_MoveNext_m9FEC73BF2FC100C28C94A15592F101C9B2CEB596 (void);
// 0x00000259 System.Object SaveManager/<SavingTime>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSavingTimeU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3B119E99743AFF41A4AB2C0507A755DE9F1E08FD (void);
// 0x0000025A System.Void SaveManager/<SavingTime>d__15::System.Collections.IEnumerator.Reset()
extern void U3CSavingTimeU3Ed__15_System_Collections_IEnumerator_Reset_m7C522780FA216172DBA8E07C9890043D4F7C692C (void);
// 0x0000025B System.Object SaveManager/<SavingTime>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CSavingTimeU3Ed__15_System_Collections_IEnumerator_get_Current_mF59823DEC376A77E23F5CECB85123B68B1E32572 (void);
// 0x0000025C System.Void Sound::Start()
extern void Sound_Start_m085324B71DE4DD0B740C6F0A9EF90E0AB3FC4FA0 (void);
// 0x0000025D System.Void Sound::SetVolumn()
extern void Sound_SetVolumn_mB786A246699AB6832E101DB8F4BC2648BE023717 (void);
// 0x0000025E System.Void Sound::SaveVolume()
extern void Sound_SaveVolume_m161D650DB72FEC41E618658FAC299AA2568A1654 (void);
// 0x0000025F System.Void Sound::LoadVolume()
extern void Sound_LoadVolume_m1D8E948965D2BEAADB21EF27E6F4927019FD2C87 (void);
// 0x00000260 System.Void Sound::.ctor()
extern void Sound__ctor_mEA0B0D2FBD514F91C21900B0BB8679CD78843FCD (void);
// 0x00000261 System.Void SpriteColorAnimation::Start()
extern void SpriteColorAnimation_Start_m75525D684C9D8BDBD34F20C108203573F58F1DA7 (void);
// 0x00000262 System.Void SpriteColorAnimation::Update()
extern void SpriteColorAnimation_Update_mD89814B67897A94B695A0336EF999A99780AB7C9 (void);
// 0x00000263 System.Void SpriteColorAnimation::.ctor()
extern void SpriteColorAnimation__ctor_m8CD8497F47547F4DA00017BCAD8314F7E4827031 (void);
// 0x00000264 System.Void UIManager::Start()
extern void UIManager_Start_mAA4B371DC406146E84A9D8803B9C861939B2E04E (void);
// 0x00000265 System.Void UIManager::Update()
extern void UIManager_Update_m8A7C5DF1B797CFD6937FD6961AB9CC7B1A90D385 (void);
// 0x00000266 System.Void UIManager::.ctor()
extern void UIManager__ctor_mDADE1D724D40AF63AE78D51FC1CF1FE4784B4D4B (void);
// 0x00000267 System.Void destroybytime::Start()
extern void destroybytime_Start_m88239D9A3B9BD72749AB7B6204DF74A059810695 (void);
// 0x00000268 System.Void destroybytime::Update()
extern void destroybytime_Update_m39762A214869A21A706B0D9B7A406C3F2C92C432 (void);
// 0x00000269 System.Void destroybytime::.ctor()
extern void destroybytime__ctor_m127A12636BB61D9A8F208065D284B6BEF43FF266 (void);
// 0x0000026A System.Void endgame::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void endgame_OnTriggerEnter2D_mCBF62DF2DB4292653EDA663B01771DA29E016CED (void);
// 0x0000026B System.Void endgame::.ctor()
extern void endgame__ctor_m32D57D64522C224DC656BF90539F3A062244B668 (void);
// 0x0000026C System.Void nextlevel::Start()
extern void nextlevel_Start_m2FF51529622A54FFFABEB2AA9F59D34B3276005E (void);
// 0x0000026D System.Void nextlevel::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void nextlevel_OnTriggerEnter2D_m2F959292A779A41942DFE349C3B3BD840EAFCB6A (void);
// 0x0000026E System.Void nextlevel::.ctor()
extern void nextlevel__ctor_m9E4DC7468D850B648A9D664A416C3809DF79FF7E (void);
// 0x0000026F System.Void nextlevel1::Start()
extern void nextlevel1_Start_mD831AED16AF7CE4AB274B34A976108BC83D1E39F (void);
// 0x00000270 System.Void nextlevel1::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void nextlevel1_OnTriggerEnter2D_m38562E21AB00C94892054E4C7C4BEC19D235A54E (void);
// 0x00000271 System.Void nextlevel1::.ctor()
extern void nextlevel1__ctor_mF806F2543C6E1BAB616AE7DDBEA73B1527C31095 (void);
// 0x00000272 System.Void nextlevel2::Start()
extern void nextlevel2_Start_m165FAD11C25F90886150127B590E646EBA237CE1 (void);
// 0x00000273 System.Void nextlevel2::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void nextlevel2_OnTriggerEnter2D_m3FD0282374584B3E9FA6B67646DD16A18A4E5AB3 (void);
// 0x00000274 System.Void nextlevel2::.ctor()
extern void nextlevel2__ctor_mD4DBA66D517B9BA814CC86CF3D7D230DE998A4C2 (void);
// 0x00000275 System.Void trapdoor::Start()
extern void trapdoor_Start_m82E86048E787BA6E9BDC1A620CF325C18537D40D (void);
// 0x00000276 System.Void trapdoor::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void trapdoor_OnTriggerEnter2D_m9BEE6AF23252FD0805E952B2DA801E10734EA5C6 (void);
// 0x00000277 System.Void trapdoor::OnTriggerExit2D(UnityEngine.Collider2D)
extern void trapdoor_OnTriggerExit2D_m42179F4FBE8C27CA96A5F63A729791050736DF80 (void);
// 0x00000278 System.Void trapdoor::.ctor()
extern void trapdoor__ctor_m8A256D8933A7C4A59378FC11F35AFA56C2E9E8FF (void);
// 0x00000279 System.Void trapdoor2::Start()
extern void trapdoor2_Start_mAF06127B1A209106F68023BE1F1E4FEEAA075129 (void);
// 0x0000027A System.Void trapdoor2::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void trapdoor2_OnTriggerEnter2D_m45D302C592E1008E077E3DFC76D6F28B1343F3E3 (void);
// 0x0000027B System.Void trapdoor2::OnTriggerExit2D(UnityEngine.Collider2D)
extern void trapdoor2_OnTriggerExit2D_mF0F6E1FDD018F7EF9F87D719F06A2FD4F397F14D (void);
// 0x0000027C System.Void trapdoor2::.ctor()
extern void trapdoor2__ctor_m89272C1675297103C65A80134084C6E9F7851326 (void);
// 0x0000027D System.Void trapdoor3::Start()
extern void trapdoor3_Start_m1C7B29B99425B9F9B05A4FD0ABE5D3E74208943F (void);
// 0x0000027E System.Void trapdoor3::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void trapdoor3_OnTriggerEnter2D_m73D6A9DD3324945E1AE25694A90A28B98AEC3D38 (void);
// 0x0000027F System.Void trapdoor3::OnTriggerExit2D(UnityEngine.Collider2D)
extern void trapdoor3_OnTriggerExit2D_m2A13C021D1EB5F47EB4BAF023801094F6619F629 (void);
// 0x00000280 System.Void trapdoor3::.ctor()
extern void trapdoor3__ctor_m128D16D4ED8A6381002E3B66ABE4A0FF3756AC71 (void);
// 0x00000281 System.Void trapdoor4::Start()
extern void trapdoor4_Start_m91D3391A3275ACEE82B1343CF4F849246A7EA5B0 (void);
// 0x00000282 System.Void trapdoor4::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void trapdoor4_OnTriggerEnter2D_m53033080B9A52DDDF0931A4A142D2C5F39D0B833 (void);
// 0x00000283 System.Void trapdoor4::OnTriggerExit2D(UnityEngine.Collider2D)
extern void trapdoor4_OnTriggerExit2D_mF1F11D2C559C22F9EFF067FD2963A816C737C8D3 (void);
// 0x00000284 System.Void trapdoor4::.ctor()
extern void trapdoor4__ctor_m3A0F898D908708F350DFB515B5CA3D6BA1DB7334 (void);
// 0x00000285 System.Void trapdoor5::Start()
extern void trapdoor5_Start_mA072AC38E5D5B9F5E05722B4A13F52153B939FFC (void);
// 0x00000286 System.Void trapdoor5::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void trapdoor5_OnTriggerEnter2D_m28A53C129A8A5953138FB51173BD99E04220F4F3 (void);
// 0x00000287 System.Void trapdoor5::OnTriggerExit2D(UnityEngine.Collider2D)
extern void trapdoor5_OnTriggerExit2D_m16DFC6AA5AC6C4105386E8729C4F44E4E4A1F47E (void);
// 0x00000288 System.Void trapdoor5::.ctor()
extern void trapdoor5__ctor_mCCD0956C986A7ED6D8C670C8D24D18DE14ED55EC (void);
// 0x00000289 System.Void trapdoor6::Start()
extern void trapdoor6_Start_m0220D22F0574E5214EEEC842EF221FE9A167D213 (void);
// 0x0000028A System.Void trapdoor6::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void trapdoor6_OnTriggerEnter2D_m10E68508156F36AEE0DFE70D0CF967AD2A1E8A05 (void);
// 0x0000028B System.Void trapdoor6::OnTriggerExit2D(UnityEngine.Collider2D)
extern void trapdoor6_OnTriggerExit2D_m416C62AC516455C322F1B26CE1DFA6D6DE944C04 (void);
// 0x0000028C System.Void trapdoor6::.ctor()
extern void trapdoor6__ctor_m02193D396BCF545222DFA17205137CA5BB050B5B (void);
// 0x0000028D System.Void activeHealPotion::start()
extern void activeHealPotion_start_mA47A951ECF2ADC8BFB5FCEC286A5259937649F13 (void);
// 0x0000028E System.Void activeHealPotion::Update()
extern void activeHealPotion_Update_mB03B59D4E62021321551EA129D1D8583C42ED4FB (void);
// 0x0000028F System.Void activeHealPotion::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void activeHealPotion_OnTriggerEnter2D_mAABDA7B1E56B96075BF44A82EFEC9E59826C0C32 (void);
// 0x00000290 System.Void activeHealPotion::.ctor()
extern void activeHealPotion__ctor_m14B7068C8823B6D8F9C1EEAAF54A9361F50F73F2 (void);
// 0x00000291 System.Void checkHeal::Start()
extern void checkHeal_Start_mF5B8F685B044F538A2D4E4F76793B18B55A0C070 (void);
// 0x00000292 System.Void checkHeal::FixedUpdate()
extern void checkHeal_FixedUpdate_m790FB663165D2E4D7982BDB961B15528162F3D72 (void);
// 0x00000293 System.Void checkHeal::Update()
extern void checkHeal_Update_m756A8ACE68AB2A6B49BEEC74A5DEEF92C5CC999C (void);
// 0x00000294 System.Void checkHeal::AddPotion(System.Int32)
extern void checkHeal_AddPotion_mEC089A57F74D7E0264E905F438AB1F03E13ADA26 (void);
// 0x00000295 System.Void checkHeal::deheal()
extern void checkHeal_deheal_mD2AF289909A90F1C2F2A2C55DCE3D6689026BBE8 (void);
// 0x00000296 System.Void checkHeal::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void checkHeal_OnTriggerEnter2D_mD2A7CA75E8D59099C4F20CDD866FE175114F0D68 (void);
// 0x00000297 System.Void checkHeal::.ctor()
extern void checkHeal__ctor_mA4522316AE8E53C053AF7F5B72EB82F6ACDFAA2A (void);
// 0x00000298 System.Void HealthPotion::start()
extern void HealthPotion_start_mD8266AF543BF3DCCA17A30DF0412F0D93FB92C58 (void);
// 0x00000299 System.Void HealthPotion::FixedUpdate()
extern void HealthPotion_FixedUpdate_m1052B52DAD81EB0EAD2A1C8D0F73042507312090 (void);
// 0x0000029A System.Void HealthPotion::Use()
extern void HealthPotion_Use_mAFB753AA93959E749FBFF52CE34FD60A0D79C0DD (void);
// 0x0000029B System.Void HealthPotion::.ctor()
extern void HealthPotion__ctor_m095D5A9FDC4496398C56B388014E257CACC85BE6 (void);
// 0x0000029C System.Void Inventory::.ctor()
extern void Inventory__ctor_mF2ACBF005FF40F23F68AE8E9E416A4870EC4B27C (void);
// 0x0000029D System.Void InventoryUI::Start()
extern void InventoryUI_Start_mBA9265B722C514BB1A663CF5D0726FC41388C979 (void);
// 0x0000029E System.Void InventoryUI::Update()
extern void InventoryUI_Update_m33C2131F4756C1EA0AEE90294EF276D0A657F519 (void);
// 0x0000029F System.Void InventoryUI::.ctor()
extern void InventoryUI__ctor_m5C090BC9C2888F79ECDCDD9669D37282DBE18748 (void);
// 0x000002A0 System.Void Pickup::Start()
extern void Pickup_Start_m09D27020F3577A9A96B57A8873B8248828DC4EB1 (void);
// 0x000002A1 System.Void Pickup::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Pickup_OnTriggerEnter2D_m394E601E8FC403BBAE3DD1C8C3F814C7FBB61180 (void);
// 0x000002A2 System.Void Pickup::.ctor()
extern void Pickup__ctor_m6649ADEB2ABDE841407A83DC4682A210AFD8A93B (void);
// 0x000002A3 System.Void Slot::Start()
extern void Slot_Start_mF57295D0D12CFAA6061C5617D6E2984B04000B19 (void);
// 0x000002A4 System.Void Slot::Update()
extern void Slot_Update_m863CC8F8F3CC50005C36EDF384467567C3CB170A (void);
// 0x000002A5 System.Void Slot::DropItem()
extern void Slot_DropItem_m3C3163AB791E4574BA5405DFA58512B1079BA686 (void);
// 0x000002A6 System.Void Slot::FixedUpdate()
extern void Slot_FixedUpdate_m2BF649D39BEE320B349E2A2A5095AD019625981D (void);
// 0x000002A7 System.Void Slot::.ctor()
extern void Slot__ctor_m2F7B85EC24837E410CD89BEDE6FEB6797EBA801E (void);
// 0x000002A8 System.Void Spawn::Start()
extern void Spawn_Start_m99E70C46F85C6318276009783089BE4FEB2F3DCE (void);
// 0x000002A9 System.Void Spawn::SpawnItem()
extern void Spawn_SpawnItem_mAA811385E5B72F16A7C4D0A0F840E6918236CE7C (void);
// 0x000002AA System.Void Spawn::.ctor()
extern void Spawn__ctor_m29DA62C57DCB06B5990FB843EE2F8533D4FCEC1F (void);
// 0x000002AB System.Void AttackEnemy::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void AttackEnemy_OnTriggerEnter2D_mEA42161BCEAAD80466B2123CEBCCEE06A3F79F53 (void);
// 0x000002AC System.Void AttackEnemy::.ctor()
extern void AttackEnemy__ctor_m530F570E7CF06FF5043AB858B6529E157280AFF6 (void);
// 0x000002AD System.Void AttackEnemy_Range::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void AttackEnemy_Range_OnTriggerEnter2D_mC6AFE28A86CED28912C7E6D1B8C57F3206DF730F (void);
// 0x000002AE System.Void AttackEnemy_Range::.ctor()
extern void AttackEnemy_Range__ctor_m8B9A07653032143BB23649D1AA28695D77C82C20 (void);
// 0x000002AF System.Void BowControl::Update()
extern void BowControl_Update_m5695D0A397B2CC3532E151F370A59846F44C406E (void);
// 0x000002B0 System.Single BowControl::AngleBetweenTwoPoints(UnityEngine.Vector3,UnityEngine.Vector3)
extern void BowControl_AngleBetweenTwoPoints_m51B5B99A65D6D4E4E1920E41BC68740B5A0F7003 (void);
// 0x000002B1 System.Void BowControl::.ctor()
extern void BowControl__ctor_m125746B485F1958307A879432A5FFE17120AD309 (void);
// 0x000002B2 System.Void CharacterController::Awake()
extern void CharacterController_Awake_m64EE31F3FBB55CF083FD5EE8CA7FCF23C313C8D2 (void);
// 0x000002B3 System.Void CharacterController::Start()
extern void CharacterController_Start_m642795422FAB6FB862D6C5E11C6CF516A0C9F094 (void);
// 0x000002B4 System.Void CharacterController::Update()
extern void CharacterController_Update_m273ABBFABD6BDE21F06F2759BA66D9908D5B54EA (void);
// 0x000002B5 System.Void CharacterController::SlowPlayer(System.Int32)
extern void CharacterController_SlowPlayer_mBA4F0E5CBA246E00B05E559FB28FFF676C98EAAC (void);
// 0x000002B6 System.Void CharacterController::LoseControlPlayer()
extern void CharacterController_LoseControlPlayer_m75256A2E022B90101807B65D0EA10E4FE6FE80D9 (void);
// 0x000002B7 System.Void CharacterController::FixedUpdate()
extern void CharacterController_FixedUpdate_m46AB7D2F16E9A249A3A3EDD3B9FBA5B0C4E6DF0E (void);
// 0x000002B8 System.Collections.IEnumerator CharacterController::Reload()
extern void CharacterController_Reload_m603D8E5810464FFAD8C80C20FA6C4E8ED61C4E38 (void);
// 0x000002B9 System.Collections.IEnumerator CharacterController::WaitforDashCD()
extern void CharacterController_WaitforDashCD_mD1C0B5349BC9E07C31AFEF439AE3FEDA192C1625 (void);
// 0x000002BA System.Collections.IEnumerator CharacterController::WaitforAttackCD()
extern void CharacterController_WaitforAttackCD_mB07CC382323E16A70F7EA4E06A2A61A9EA44B01D (void);
// 0x000002BB System.Collections.IEnumerator CharacterController::WaitforSlow(System.Int32)
extern void CharacterController_WaitforSlow_m4306A08C07FBF80952E7999EA8FA6AADEDA79850 (void);
// 0x000002BC System.Collections.IEnumerator CharacterController::WaitforControl()
extern void CharacterController_WaitforControl_m99E2CC6C84046A3D3153772CF58AB8080851B1E1 (void);
// 0x000002BD System.Void CharacterController::BuffArrow()
extern void CharacterController_BuffArrow_mC7FA1EC7C66204556B68C0525087B65B92BAA86A (void);
// 0x000002BE System.Void CharacterController::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void CharacterController_OnTriggerEnter2D_m8F1DA5AE8D70A76393B579573D313279F6061E6A (void);
// 0x000002BF System.Collections.IEnumerator CharacterController::TimeBuff()
extern void CharacterController_TimeBuff_m92A4C732E993B314804D911463AD777E99D0783C (void);
// 0x000002C0 System.Void CharacterController::.ctor()
extern void CharacterController__ctor_m0F793EBA2EFFEA8CBCE9CDF61A3A1BB5EF89C5E5 (void);
// 0x000002C1 System.Void CharacterController/<Reload>d__38::.ctor(System.Int32)
extern void U3CReloadU3Ed__38__ctor_mA2475D6EA0EECC4ED1C1DEDDF436609500668E43 (void);
// 0x000002C2 System.Void CharacterController/<Reload>d__38::System.IDisposable.Dispose()
extern void U3CReloadU3Ed__38_System_IDisposable_Dispose_m189DF1F4181A8E7C4A6442EA9A8BFE394CC0EA1A (void);
// 0x000002C3 System.Boolean CharacterController/<Reload>d__38::MoveNext()
extern void U3CReloadU3Ed__38_MoveNext_m81399917B8EB86B81CB35DF6F0A42CC91D707619 (void);
// 0x000002C4 System.Object CharacterController/<Reload>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReloadU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m817CD53DA8749F94A166EBDA976731072045001E (void);
// 0x000002C5 System.Void CharacterController/<Reload>d__38::System.Collections.IEnumerator.Reset()
extern void U3CReloadU3Ed__38_System_Collections_IEnumerator_Reset_m473199BCE668261B4718B1703F368D13D6D718D8 (void);
// 0x000002C6 System.Object CharacterController/<Reload>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CReloadU3Ed__38_System_Collections_IEnumerator_get_Current_mB53EB3667351EDD280A4DC0377073DF5889818AC (void);
// 0x000002C7 System.Void CharacterController/<WaitforDashCD>d__39::.ctor(System.Int32)
extern void U3CWaitforDashCDU3Ed__39__ctor_m373960E38BF24499999824042B6B18CC330FCDFB (void);
// 0x000002C8 System.Void CharacterController/<WaitforDashCD>d__39::System.IDisposable.Dispose()
extern void U3CWaitforDashCDU3Ed__39_System_IDisposable_Dispose_mEAA348CAEDF9CFACC0549AEB129346432A5E5E57 (void);
// 0x000002C9 System.Boolean CharacterController/<WaitforDashCD>d__39::MoveNext()
extern void U3CWaitforDashCDU3Ed__39_MoveNext_mC8E9D38DAF9BCE7E12ADEC586F61FC868E1850E2 (void);
// 0x000002CA System.Object CharacterController/<WaitforDashCD>d__39::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitforDashCDU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54214D324F2636F1476FD91F8DEF57A419313AC6 (void);
// 0x000002CB System.Void CharacterController/<WaitforDashCD>d__39::System.Collections.IEnumerator.Reset()
extern void U3CWaitforDashCDU3Ed__39_System_Collections_IEnumerator_Reset_m0AA6D3E47AD5870D0DC147AAD6E30D775DB22655 (void);
// 0x000002CC System.Object CharacterController/<WaitforDashCD>d__39::System.Collections.IEnumerator.get_Current()
extern void U3CWaitforDashCDU3Ed__39_System_Collections_IEnumerator_get_Current_mE97F155EF767F943F9AEB8CA7FD0A3A83DE8F5FA (void);
// 0x000002CD System.Void CharacterController/<WaitforAttackCD>d__40::.ctor(System.Int32)
extern void U3CWaitforAttackCDU3Ed__40__ctor_mDCD5FFEE4BA5AF5F59FDF9872D00947AEA244881 (void);
// 0x000002CE System.Void CharacterController/<WaitforAttackCD>d__40::System.IDisposable.Dispose()
extern void U3CWaitforAttackCDU3Ed__40_System_IDisposable_Dispose_m92D63B81EA8760D3EFDF4923517EB60B4668C27C (void);
// 0x000002CF System.Boolean CharacterController/<WaitforAttackCD>d__40::MoveNext()
extern void U3CWaitforAttackCDU3Ed__40_MoveNext_m817885042FD397CBA0AF3AD47E3CC198B6ACEB7E (void);
// 0x000002D0 System.Object CharacterController/<WaitforAttackCD>d__40::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitforAttackCDU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D221862ED79DE7E719959FA79DF0F7C3FADBE37 (void);
// 0x000002D1 System.Void CharacterController/<WaitforAttackCD>d__40::System.Collections.IEnumerator.Reset()
extern void U3CWaitforAttackCDU3Ed__40_System_Collections_IEnumerator_Reset_mC58183DFB9DC872EE95A3067AA0481D4B5704012 (void);
// 0x000002D2 System.Object CharacterController/<WaitforAttackCD>d__40::System.Collections.IEnumerator.get_Current()
extern void U3CWaitforAttackCDU3Ed__40_System_Collections_IEnumerator_get_Current_m047CBA26B024B9420C0A608271EE253FEF0379BF (void);
// 0x000002D3 System.Void CharacterController/<WaitforSlow>d__41::.ctor(System.Int32)
extern void U3CWaitforSlowU3Ed__41__ctor_m3A69FF99D4BA5414697B5DC9F6800C9F29BD0058 (void);
// 0x000002D4 System.Void CharacterController/<WaitforSlow>d__41::System.IDisposable.Dispose()
extern void U3CWaitforSlowU3Ed__41_System_IDisposable_Dispose_mC215FD97FA1DCA7AF535F08F89A76462D2666C0E (void);
// 0x000002D5 System.Boolean CharacterController/<WaitforSlow>d__41::MoveNext()
extern void U3CWaitforSlowU3Ed__41_MoveNext_m55494E31F2342B9407ED8543973F465C3E126F6E (void);
// 0x000002D6 System.Object CharacterController/<WaitforSlow>d__41::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitforSlowU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m48935FB12DC00EC4301931A97F3BD21303DFF5EB (void);
// 0x000002D7 System.Void CharacterController/<WaitforSlow>d__41::System.Collections.IEnumerator.Reset()
extern void U3CWaitforSlowU3Ed__41_System_Collections_IEnumerator_Reset_mD63E6D9F2D797CCAB2BF6ABFB4F7F16AF9A80476 (void);
// 0x000002D8 System.Object CharacterController/<WaitforSlow>d__41::System.Collections.IEnumerator.get_Current()
extern void U3CWaitforSlowU3Ed__41_System_Collections_IEnumerator_get_Current_mE796B7B00AAF4F9E4193550E90652205D3386C6A (void);
// 0x000002D9 System.Void CharacterController/<WaitforControl>d__42::.ctor(System.Int32)
extern void U3CWaitforControlU3Ed__42__ctor_mC555FCAC674AF43AA06D04B7A60FDE434485CC40 (void);
// 0x000002DA System.Void CharacterController/<WaitforControl>d__42::System.IDisposable.Dispose()
extern void U3CWaitforControlU3Ed__42_System_IDisposable_Dispose_m339BD26FF42F0EFA072A57648700421AE65D59FE (void);
// 0x000002DB System.Boolean CharacterController/<WaitforControl>d__42::MoveNext()
extern void U3CWaitforControlU3Ed__42_MoveNext_m82A56DA3A174D7FFD6727DBD6026DC839B8973AE (void);
// 0x000002DC System.Object CharacterController/<WaitforControl>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitforControlU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55BE11D576B1F7839E3386CAB7EA471C71468E39 (void);
// 0x000002DD System.Void CharacterController/<WaitforControl>d__42::System.Collections.IEnumerator.Reset()
extern void U3CWaitforControlU3Ed__42_System_Collections_IEnumerator_Reset_mB02A4E799DAE3A7464F8CC2965C7B4C8DD022AEC (void);
// 0x000002DE System.Object CharacterController/<WaitforControl>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CWaitforControlU3Ed__42_System_Collections_IEnumerator_get_Current_m45AD58E82837EA91D4BE2D8BCCBFAE55F9D980A4 (void);
// 0x000002DF System.Void CharacterController/<TimeBuff>d__45::.ctor(System.Int32)
extern void U3CTimeBuffU3Ed__45__ctor_mD2E814F622252304651A2143045D1EDFF9C71040 (void);
// 0x000002E0 System.Void CharacterController/<TimeBuff>d__45::System.IDisposable.Dispose()
extern void U3CTimeBuffU3Ed__45_System_IDisposable_Dispose_mB60A2FE267BE49001CDDABA8D2C115FCB66578E2 (void);
// 0x000002E1 System.Boolean CharacterController/<TimeBuff>d__45::MoveNext()
extern void U3CTimeBuffU3Ed__45_MoveNext_mF9CBFB1DF4FCC72D7DD650D582293D5601C8FEA2 (void);
// 0x000002E2 System.Object CharacterController/<TimeBuff>d__45::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTimeBuffU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5A7519E0C87FC70F54936B9B2A057C6A5E8D0B79 (void);
// 0x000002E3 System.Void CharacterController/<TimeBuff>d__45::System.Collections.IEnumerator.Reset()
extern void U3CTimeBuffU3Ed__45_System_Collections_IEnumerator_Reset_mCD2C34CDD9B9C71AFE577EC290324C4B1B24CA93 (void);
// 0x000002E4 System.Object CharacterController/<TimeBuff>d__45::System.Collections.IEnumerator.get_Current()
extern void U3CTimeBuffU3Ed__45_System_Collections_IEnumerator_get_Current_mE8B7421BCD2F31F4EB76A037245B241D6AAD16FD (void);
// 0x000002E5 System.Void FloatingNumber::Update()
extern void FloatingNumber_Update_m424398E22F1CAD3BFC399AE657BC8B9BCC1A7FBC (void);
// 0x000002E6 System.Void FloatingNumber::.ctor()
extern void FloatingNumber__ctor_m7CAD8E2D63060D777E329AD976A8F95F26577B8A (void);
// 0x000002E7 System.Void HealthPlayer::Start()
extern void HealthPlayer_Start_m9555D7632FD0BB0312BEF01FA4A633A16E8D3B6D (void);
// 0x000002E8 System.Void HealthPlayer::Update()
extern void HealthPlayer_Update_m79AE8AE7B35892F70BA94760A300CF03F08D8AE9 (void);
// 0x000002E9 System.Void HealthPlayer::HurtPlayer(System.Int32)
extern void HealthPlayer_HurtPlayer_mA18BC4629F852A421683C43E3D588CF8BE2A9217 (void);
// 0x000002EA System.Collections.IEnumerator HealthPlayer::PlayerActiveOff()
extern void HealthPlayer_PlayerActiveOff_m3C8156E1BA703EECEBC135A24986704A788C0481 (void);
// 0x000002EB System.Void HealthPlayer::Heal(System.Int32)
extern void HealthPlayer_Heal_mAF039A2AFBE52F6258E37ED41D73C385B3AEFF63 (void);
// 0x000002EC System.Void HealthPlayer::HealPotion(System.Int32)
extern void HealthPlayer_HealPotion_m3209C904EA069B533139C04328FF904E50F1718A (void);
// 0x000002ED System.Void HealthPlayer::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void HealthPlayer_OnTriggerEnter2D_m2BE253EB2257B56FEAC60ED7E186B1C1C0C88E38 (void);
// 0x000002EE System.Void HealthPlayer::FireBurningPlayer(System.Int32)
extern void HealthPlayer_FireBurningPlayer_m5F131C33AF964DDE8F005A10CD54BF4317FA1D25 (void);
// 0x000002EF System.Void HealthPlayer::ToxicBurningPlayer(System.Int32)
extern void HealthPlayer_ToxicBurningPlayer_m4A7C08A7F34B0B5F909108176B89BAC33E73CA6E (void);
// 0x000002F0 System.Collections.IEnumerator HealthPlayer::WaitforBurningTwiceTimes(System.Int32)
extern void HealthPlayer_WaitforBurningTwiceTimes_m9B8D329D3F3ED2A06387ED52212B7329CCA72ED9 (void);
// 0x000002F1 System.Collections.IEnumerator HealthPlayer::WaitforBurningManyTimes(System.Int32)
extern void HealthPlayer_WaitforBurningManyTimes_m21FD33E08F5D2F6F6CA1F1A321B405E71DC855FC (void);
// 0x000002F2 System.Void HealthPlayer::.ctor()
extern void HealthPlayer__ctor_m64FE6E1F7FBBEF21DF99578AD42F7F8B45804D11 (void);
// 0x000002F3 System.Void HealthPlayer/<PlayerActiveOff>d__17::.ctor(System.Int32)
extern void U3CPlayerActiveOffU3Ed__17__ctor_m9A9DB2B04BFA578AC10BB1E9B790FE0BE9B8693F (void);
// 0x000002F4 System.Void HealthPlayer/<PlayerActiveOff>d__17::System.IDisposable.Dispose()
extern void U3CPlayerActiveOffU3Ed__17_System_IDisposable_Dispose_mCB84FD0D750104FB2CF907E87DD05F7AE8AB9C63 (void);
// 0x000002F5 System.Boolean HealthPlayer/<PlayerActiveOff>d__17::MoveNext()
extern void U3CPlayerActiveOffU3Ed__17_MoveNext_m0F0BC121AE61EB10C7D8CD061DE7B0D001C0417B (void);
// 0x000002F6 System.Object HealthPlayer/<PlayerActiveOff>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayerActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m317F910FAB3ED1F3E76932F9603EB1798D79D3C8 (void);
// 0x000002F7 System.Void HealthPlayer/<PlayerActiveOff>d__17::System.Collections.IEnumerator.Reset()
extern void U3CPlayerActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_mC1EAC213C3F0424F2DE7AF7D4F973287EE125DC6 (void);
// 0x000002F8 System.Object HealthPlayer/<PlayerActiveOff>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CPlayerActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_mB0F263F4102A3C2C994B02E3F15FAA5F083BF625 (void);
// 0x000002F9 System.Void HealthPlayer/<WaitforBurningTwiceTimes>d__23::.ctor(System.Int32)
extern void U3CWaitforBurningTwiceTimesU3Ed__23__ctor_mF7EA250EF31784D08B73C5490C7851D93532DBC8 (void);
// 0x000002FA System.Void HealthPlayer/<WaitforBurningTwiceTimes>d__23::System.IDisposable.Dispose()
extern void U3CWaitforBurningTwiceTimesU3Ed__23_System_IDisposable_Dispose_m1409B5C7D84CC7569BAE724C971E338953FB0426 (void);
// 0x000002FB System.Boolean HealthPlayer/<WaitforBurningTwiceTimes>d__23::MoveNext()
extern void U3CWaitforBurningTwiceTimesU3Ed__23_MoveNext_mE0A11613CC83367C6EF78FE4732362FAD51D3858 (void);
// 0x000002FC System.Object HealthPlayer/<WaitforBurningTwiceTimes>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitforBurningTwiceTimesU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF9557D405C2B28E0125F2748C6B3C41F5023B0E (void);
// 0x000002FD System.Void HealthPlayer/<WaitforBurningTwiceTimes>d__23::System.Collections.IEnumerator.Reset()
extern void U3CWaitforBurningTwiceTimesU3Ed__23_System_Collections_IEnumerator_Reset_m2050273B00A7E0222167D456BA00C5168F33162E (void);
// 0x000002FE System.Object HealthPlayer/<WaitforBurningTwiceTimes>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CWaitforBurningTwiceTimesU3Ed__23_System_Collections_IEnumerator_get_Current_m9815C990320876FBB5FDB0D542B3982C070CD026 (void);
// 0x000002FF System.Void HealthPlayer/<WaitforBurningManyTimes>d__24::.ctor(System.Int32)
extern void U3CWaitforBurningManyTimesU3Ed__24__ctor_mBFAA5317CEA1FBA4BB4652CE2599F754749F50C1 (void);
// 0x00000300 System.Void HealthPlayer/<WaitforBurningManyTimes>d__24::System.IDisposable.Dispose()
extern void U3CWaitforBurningManyTimesU3Ed__24_System_IDisposable_Dispose_m0389C895AF95C024112A1A53241A12DDD87CCFDA (void);
// 0x00000301 System.Boolean HealthPlayer/<WaitforBurningManyTimes>d__24::MoveNext()
extern void U3CWaitforBurningManyTimesU3Ed__24_MoveNext_m374D6280463450C0B6EA5AA199CBC1397B5E1AC2 (void);
// 0x00000302 System.Object HealthPlayer/<WaitforBurningManyTimes>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitforBurningManyTimesU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09F3C5A63D58ADDAB507EAD2BA87F7F7CA197A9C (void);
// 0x00000303 System.Void HealthPlayer/<WaitforBurningManyTimes>d__24::System.Collections.IEnumerator.Reset()
extern void U3CWaitforBurningManyTimesU3Ed__24_System_Collections_IEnumerator_Reset_m5E755DF2A24BE189979B619CD9A68E99006AD458 (void);
// 0x00000304 System.Object HealthPlayer/<WaitforBurningManyTimes>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CWaitforBurningManyTimesU3Ed__24_System_Collections_IEnumerator_get_Current_m56DAFC8768C4E1A1B229004A178F9DD5552DF281 (void);
// 0x00000305 System.Void Knockback::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Knockback_OnTriggerEnter2D_mF928BA361B80E1DE2D3146855D185F9F04B06D5C (void);
// 0x00000306 System.Collections.IEnumerator Knockback::KnockCo(UnityEngine.Rigidbody2D)
extern void Knockback_KnockCo_m895B2CCA6560515B4ECBD06F62DE1FD5DA034A25 (void);
// 0x00000307 System.Void Knockback::.ctor()
extern void Knockback__ctor_m2B8B481F28FC4A33133BD970C8F4E86F809DFD40 (void);
// 0x00000308 System.Void Knockback/<KnockCo>d__3::.ctor(System.Int32)
extern void U3CKnockCoU3Ed__3__ctor_m13966A956093B208EA961EB6AF631B6749075141 (void);
// 0x00000309 System.Void Knockback/<KnockCo>d__3::System.IDisposable.Dispose()
extern void U3CKnockCoU3Ed__3_System_IDisposable_Dispose_mD43C8AE1C391E9406E80789A9543D352B5825BC0 (void);
// 0x0000030A System.Boolean Knockback/<KnockCo>d__3::MoveNext()
extern void U3CKnockCoU3Ed__3_MoveNext_mB0B18000D70D1759D0BEDBF6E5E68AD808A62B7A (void);
// 0x0000030B System.Object Knockback/<KnockCo>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKnockCoU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D198094D719D238ED5864BEDAB86C06F7144BEA (void);
// 0x0000030C System.Void Knockback/<KnockCo>d__3::System.Collections.IEnumerator.Reset()
extern void U3CKnockCoU3Ed__3_System_Collections_IEnumerator_Reset_mCCB2797D5B3C57E35C3864E861573DF29FC19101 (void);
// 0x0000030D System.Object Knockback/<KnockCo>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CKnockCoU3Ed__3_System_Collections_IEnumerator_get_Current_mFCE1024C9949576CB9507452F41AFFC219D78739 (void);
// 0x0000030E System.Void arrowDestroy::Start()
extern void arrowDestroy_Start_mD354873B30E04DB44ACFAF916E1756F285DD325A (void);
// 0x0000030F System.Void arrowDestroy::Update()
extern void arrowDestroy_Update_m28C88035832027F0DAAE40C5DED0B401410C97BF (void);
// 0x00000310 System.Void arrowDestroy::.ctor()
extern void arrowDestroy__ctor_mAADB5F0EF3DE6F666C87EAB09585B9F76CAEFED0 (void);
// 0x00000311 System.Void QuestComplted::Start()
extern void QuestComplted_Start_m27D1DE34B13DB40B73FFAF3FD8642B314073CBA8 (void);
// 0x00000312 System.Void QuestComplted::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void QuestComplted_OnTriggerEnter2D_mAA77ED2069963A8D34BF91DC8C1087A6A17ADE55 (void);
// 0x00000313 System.Void QuestComplted::.ctor()
extern void QuestComplted__ctor_m82B2DA439B88EF4366F64E30C378805275BAA5AA (void);
// 0x00000314 System.Void MainMenu::PlayGame()
extern void MainMenu_PlayGame_m96A3CE2743BCB00B665AA3AC575AE4EBD9ED40B0 (void);
// 0x00000315 System.Void MainMenu::QuitGame()
extern void MainMenu_QuitGame_m9F32E266C6F6CE345067D062258362159D267030 (void);
// 0x00000316 System.Void MainMenu::.ctor()
extern void MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D (void);
// 0x00000317 System.Void MoveBackground::Start()
extern void MoveBackground_Start_m73952C44ECDB8E02A7C84CE140363C3158096CD3 (void);
// 0x00000318 System.Void MoveBackground::Update()
extern void MoveBackground_Update_m233EF7D84EBF4ED003A6EC2F9942DD701BABDC2E (void);
// 0x00000319 System.Void MoveBackground::.ctor()
extern void MoveBackground__ctor_mAF0741CCE7230F5038EA97EDD54679F1D20DF22D (void);
// 0x0000031A System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2 (void);
// 0x0000031B System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B (void);
// 0x0000031C System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371 (void);
// 0x0000031D System.Void ChatController::.ctor()
extern void ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026 (void);
// 0x0000031E System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6 (void);
// 0x0000031F System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9 (void);
// 0x00000320 System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93 (void);
// 0x00000321 System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE (void);
// 0x00000322 System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC (void);
// 0x00000323 System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23 (void);
// 0x00000324 System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B (void);
// 0x00000325 System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE (void);
// 0x00000326 System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E (void);
// 0x00000327 System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB (void);
// 0x00000328 System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF (void);
// 0x00000329 System.Void checkpointController::Start()
extern void checkpointController_Start_mBD9EF8C79AE04CBB39770690E3340C6DB0EBF46F (void);
// 0x0000032A System.Void checkpointController::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void checkpointController_OnTriggerEnter2D_m9294C5151B9546B89E4389D802A64D5D8A2AA336 (void);
// 0x0000032B System.Void checkpointController::.ctor()
extern void checkpointController__ctor_m5D77ED241F33CBAB053C007E75869D67259585FF (void);
// 0x0000032C System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226 (void);
// 0x0000032D System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E (void);
// 0x0000032E System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1 (void);
// 0x0000032F System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3 (void);
// 0x00000330 TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385 (void);
// 0x00000331 System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4 (void);
// 0x00000332 TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B (void);
// 0x00000333 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788 (void);
// 0x00000334 TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A (void);
// 0x00000335 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076 (void);
// 0x00000336 TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D (void);
// 0x00000337 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA (void);
// 0x00000338 TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB (void);
// 0x00000339 System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5 (void);
// 0x0000033A System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70 (void);
// 0x0000033B System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11 (void);
// 0x0000033C System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC (void);
// 0x0000033D System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44 (void);
// 0x0000033E System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC (void);
// 0x0000033F System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53 (void);
// 0x00000340 System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66 (void);
// 0x00000341 System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43 (void);
// 0x00000342 System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77 (void);
// 0x00000343 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8 (void);
// 0x00000344 System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7 (void);
// 0x00000345 System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2 (void);
// 0x00000346 System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C (void);
// 0x00000347 System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018 (void);
// 0x00000348 System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0 (void);
// 0x00000349 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C (void);
// 0x0000034A System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F (void);
// 0x0000034B System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB (void);
// 0x0000034C System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73 (void);
// 0x0000034D System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C (void);
// 0x0000034E System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591 (void);
// 0x0000034F System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B (void);
// 0x00000350 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E (void);
// 0x00000351 System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4 (void);
// 0x00000352 System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376 (void);
// 0x00000353 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651 (void);
// 0x00000354 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561 (void);
// 0x00000355 System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1 (void);
// 0x00000356 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD (void);
// 0x00000357 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA (void);
// 0x00000358 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2 (void);
// 0x00000359 System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046 (void);
// 0x0000035A System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5 (void);
// 0x0000035B System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77 (void);
// 0x0000035C System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516 (void);
// 0x0000035D System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE (void);
// 0x0000035E System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9 (void);
// 0x0000035F System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C (void);
// 0x00000360 System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E (void);
// 0x00000361 System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610 (void);
// 0x00000362 System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373 (void);
// 0x00000363 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3 (void);
// 0x00000364 System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124 (void);
// 0x00000365 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA (void);
// 0x00000366 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3 (void);
// 0x00000367 System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC (void);
// 0x00000368 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8 (void);
// 0x00000369 System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2 (void);
// 0x0000036A System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469 (void);
// 0x0000036B System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64 (void);
// 0x0000036C System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A (void);
// 0x0000036D System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B (void);
// 0x0000036E System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF (void);
// 0x0000036F System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390 (void);
// 0x00000370 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B (void);
// 0x00000371 System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362 (void);
// 0x00000372 System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F (void);
// 0x00000373 System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5 (void);
// 0x00000374 System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697 (void);
// 0x00000375 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB (void);
// 0x00000376 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F (void);
// 0x00000377 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3 (void);
// 0x00000378 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3 (void);
// 0x00000379 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86 (void);
// 0x0000037A System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F (void);
// 0x0000037B System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF (void);
// 0x0000037C System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF (void);
// 0x0000037D System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1 (void);
// 0x0000037E System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F (void);
// 0x0000037F System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A (void);
// 0x00000380 System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4 (void);
// 0x00000381 System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB (void);
// 0x00000382 System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12 (void);
// 0x00000383 System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3 (void);
// 0x00000384 System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720 (void);
// 0x00000385 System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21 (void);
// 0x00000386 System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1 (void);
// 0x00000387 System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C (void);
// 0x00000388 System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF (void);
// 0x00000389 System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A (void);
// 0x0000038A System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D (void);
// 0x0000038B System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0 (void);
// 0x0000038C System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B (void);
// 0x0000038D System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363 (void);
// 0x0000038E System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C (void);
// 0x0000038F System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B (void);
// 0x00000390 System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE (void);
// 0x00000391 System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943 (void);
// 0x00000392 System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD (void);
// 0x00000393 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0 (void);
// 0x00000394 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66 (void);
// 0x00000395 System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF (void);
// 0x00000396 System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A (void);
// 0x00000397 System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB (void);
// 0x00000398 System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781 (void);
// 0x00000399 System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910 (void);
// 0x0000039A System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691 (void);
// 0x0000039B System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D (void);
// 0x0000039C System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F (void);
// 0x0000039D System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F (void);
// 0x0000039E System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC (void);
// 0x0000039F System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2 (void);
// 0x000003A0 System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33 (void);
// 0x000003A1 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E (void);
// 0x000003A2 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944 (void);
// 0x000003A3 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749 (void);
// 0x000003A4 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647 (void);
// 0x000003A5 System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84 (void);
// 0x000003A6 System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC (void);
// 0x000003A7 System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB (void);
// 0x000003A8 System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF (void);
// 0x000003A9 System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE (void);
// 0x000003AA System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7 (void);
// 0x000003AB System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF (void);
// 0x000003AC System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629 (void);
// 0x000003AD System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9 (void);
// 0x000003AE System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715 (void);
// 0x000003AF System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B (void);
// 0x000003B0 System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8 (void);
// 0x000003B1 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261 (void);
// 0x000003B2 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0 (void);
// 0x000003B3 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA (void);
// 0x000003B4 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50 (void);
// 0x000003B5 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712 (void);
// 0x000003B6 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8 (void);
// 0x000003B7 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534 (void);
// 0x000003B8 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4 (void);
// 0x000003B9 System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848 (void);
// 0x000003BA System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D (void);
// 0x000003BB System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C (void);
// 0x000003BC System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3 (void);
// 0x000003BD System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83 (void);
// 0x000003BE System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691 (void);
// 0x000003BF System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F (void);
// 0x000003C0 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0 (void);
// 0x000003C1 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8 (void);
// 0x000003C2 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210 (void);
// 0x000003C3 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB (void);
// 0x000003C4 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F (void);
// 0x000003C5 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27 (void);
// 0x000003C6 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046 (void);
// 0x000003C7 System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1 (void);
// 0x000003C8 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7 (void);
// 0x000003C9 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF (void);
// 0x000003CA System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E (void);
// 0x000003CB System.Void TMPro.Examples.TextMeshProFloatingText::.cctor()
extern void TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8 (void);
// 0x000003CC System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B (void);
// 0x000003CD System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C (void);
// 0x000003CE System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28 (void);
// 0x000003CF System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963 (void);
// 0x000003D0 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8 (void);
// 0x000003D1 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B (void);
// 0x000003D2 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20 (void);
// 0x000003D3 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3 (void);
// 0x000003D4 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A (void);
// 0x000003D5 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598 (void);
// 0x000003D6 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5 (void);
// 0x000003D7 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6 (void);
// 0x000003D8 System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB (void);
// 0x000003D9 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA (void);
// 0x000003DA System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1 (void);
// 0x000003DB System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC (void);
// 0x000003DC System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66 (void);
// 0x000003DD System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42 (void);
// 0x000003DE System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9 (void);
// 0x000003DF System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F (void);
// 0x000003E0 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007 (void);
// 0x000003E1 System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6 (void);
// 0x000003E2 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51 (void);
// 0x000003E3 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932 (void);
// 0x000003E4 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52 (void);
// 0x000003E5 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F (void);
// 0x000003E6 System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825 (void);
// 0x000003E7 System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846 (void);
// 0x000003E8 System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97 (void);
// 0x000003E9 System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549 (void);
// 0x000003EA System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16 (void);
// 0x000003EB System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE (void);
// 0x000003EC System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8 (void);
// 0x000003ED System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099 (void);
// 0x000003EE System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B (void);
// 0x000003EF System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479 (void);
// 0x000003F0 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D (void);
// 0x000003F1 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF (void);
// 0x000003F2 System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2 (void);
// 0x000003F3 System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA (void);
// 0x000003F4 System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2 (void);
// 0x000003F5 System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE (void);
// 0x000003F6 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E (void);
// 0x000003F7 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599 (void);
// 0x000003F8 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62 (void);
// 0x000003F9 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2 (void);
// 0x000003FA System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81 (void);
// 0x000003FB System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F (void);
// 0x000003FC System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE (void);
// 0x000003FD System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA (void);
// 0x000003FE System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E (void);
// 0x000003FF System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5 (void);
// 0x00000400 System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971 (void);
// 0x00000401 System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4 (void);
// 0x00000402 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1 (void);
// 0x00000403 System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5 (void);
// 0x00000404 System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87 (void);
// 0x00000405 System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC (void);
// 0x00000406 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760 (void);
// 0x00000407 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC (void);
// 0x00000408 System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E (void);
// 0x00000409 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D (void);
// 0x0000040A System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB (void);
// 0x0000040B System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD (void);
// 0x0000040C System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C (void);
// 0x0000040D System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC (void);
// 0x0000040E System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884 (void);
// 0x0000040F System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C (void);
// 0x00000410 System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F (void);
// 0x00000411 System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED (void);
// 0x00000412 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2 (void);
// 0x00000413 System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348 (void);
// 0x00000414 System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107 (void);
// 0x00000415 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2 (void);
// 0x00000416 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F (void);
// 0x00000417 System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66 (void);
// 0x00000418 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9 (void);
// 0x00000419 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C (void);
// 0x0000041A System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F (void);
// 0x0000041B System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0 (void);
// 0x0000041C System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4 (void);
// 0x0000041D UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB (void);
// 0x0000041E System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2 (void);
// 0x0000041F System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099 (void);
// 0x00000420 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49 (void);
// 0x00000421 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60 (void);
// 0x00000422 System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84 (void);
// 0x00000423 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1 (void);
// 0x00000424 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535 (void);
// 0x00000425 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25 (void);
static Il2CppMethodPointer s_methodPointers[1061] = 
{
	IntroManager_Start_mEE7D131DA47D9DB46EF36B199B4921C558D0B2DA,
	IntroManager_TimeIE_m8671693B4F354CA40F239A02EB160E5EE906CB40,
	IntroManager_Update_m0521B2C4306FD5EBB11AB46F21C6973BC2698DFE,
	IntroManager_StartIntro_mE124E82AEFDC00993ACCA86D756CDA2D08D66D6B,
	IntroManager_TypeLine_m2D1AB9015F67B6094D56BCD80BB4198A1615C4A6,
	IntroManager_NextLine_m0DD36CA3163BDEFCFF8E40265DB46C98AC7E4217,
	IntroManager__ctor_m4AA6A59302DF7976FD91092B24897D630BA33860,
	U3CTimeIEU3Ed__10__ctor_m11C8A6394695414B03FF139147D221BE9CC7F656,
	U3CTimeIEU3Ed__10_System_IDisposable_Dispose_mC05D66A28083727BFEE3EF10506098A204A03409,
	U3CTimeIEU3Ed__10_MoveNext_m2FB9DAEE831452143F60B4F1EAB2F47AD23F223E,
	U3CTimeIEU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m25917A42988161EEBA1271CF3B23AEB337543CA9,
	U3CTimeIEU3Ed__10_System_Collections_IEnumerator_Reset_m65DAD9114DAB93BE8D5BB1C625F1FB2CF0F0D472,
	U3CTimeIEU3Ed__10_System_Collections_IEnumerator_get_Current_m2A25C3B0223067B36A377B4C3FDAE686F28D539E,
	U3CTypeLineU3Ed__13__ctor_mB4362558C8CDE1FCF2DB18EE13D09F9A77800DA3,
	U3CTypeLineU3Ed__13_System_IDisposable_Dispose_mBF2CA07B9929F52BB9D6757B4ABBEBB156D51C92,
	U3CTypeLineU3Ed__13_MoveNext_m618F7BB14CED5AC9DE24B89DCC376AE11F2610DD,
	U3CTypeLineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6016236D9EEC958D1B15C210D6D80897805FEE75,
	U3CTypeLineU3Ed__13_System_Collections_IEnumerator_Reset_m20BF6AA37542A6DE14CE8606807FBA0E4EFD2815,
	U3CTypeLineU3Ed__13_System_Collections_IEnumerator_get_Current_m623B9FE74115FEF5D16925CEE1E5397C12B38BCB,
	SoundManager_Awake_m78F953F39CFB3F539240E1226D04270B793B1A76,
	SoundManager_PlayClip_mF074B01B915517909B27888DEBE54893372B7D07,
	SoundManager_StopClip_m2FD9C367D9D5A660C1FE9C96B595F935C6FB8EC2,
	SoundManager__ctor_m30D18BC25871BD3FF7FB195A1CAE50A2EE48FCAE,
	Story1Manager_Start_mFC6658980A0E7698830AEEA540104552CD24351C,
	Story1Manager_TimeIE_m9F889DA26E3CF321B6DDD407B42D4EEED1CAEDCD,
	Story1Manager_Update_m5D9A3313BEE6F846D3D5920D9BDB7A939DD64FB5,
	Story1Manager_StartStory1_mD997E78F37A6B8DC42130A574149F673D424A7E3,
	Story1Manager_TypeLine_m597776B3A41CAA93A0671A6DD7F50DEA37F5E4FC,
	Story1Manager_NextLine_mB799D1F17702E3A723BE21CF4D1A8AAB39A68345,
	Story1Manager__ctor_mF3D4F1214C343FC08CFAF4A7FC4B243DBB7F4E18,
	U3CTimeIEU3Ed__10__ctor_m2458A1336253AF66749EA49CDCF74B1810C923B6,
	U3CTimeIEU3Ed__10_System_IDisposable_Dispose_m126FFCFD9B1CD0A1C81AD3798874A380A891518F,
	U3CTimeIEU3Ed__10_MoveNext_m48BCC750C93C67047AEAFBF04E6A26CFE39C86AA,
	U3CTimeIEU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA6B0872963BDC898023AD817C434FE51C7854FF9,
	U3CTimeIEU3Ed__10_System_Collections_IEnumerator_Reset_mA408B839522C0E46A97723C45EF4DCF246215799,
	U3CTimeIEU3Ed__10_System_Collections_IEnumerator_get_Current_mB2BCCF780BDA64276290CB3080C262B0AF33773A,
	U3CTypeLineU3Ed__13__ctor_mB8227D04CFD9803A9E3D112D82A1C2CA0320C3AF,
	U3CTypeLineU3Ed__13_System_IDisposable_Dispose_m5DE8B5998D1316BFCFE3D69C8D65AA90C8A4A614,
	U3CTypeLineU3Ed__13_MoveNext_m6762DB458F963CC1DB0F8BB4BD414DDA4BFFDC04,
	U3CTypeLineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4785A5F20FCC7D15936A974801F1ABF86969FBA4,
	U3CTypeLineU3Ed__13_System_Collections_IEnumerator_Reset_m26715E5D4DA17CBB171BE6A64BBB6AFB56E647A4,
	U3CTypeLineU3Ed__13_System_Collections_IEnumerator_get_Current_mFD03E403D694A40D19E16EDC51A696B7E6DB7FFA,
	Story2Manager_Start_m36F27D8A77A69B1A9A7E7763EDC3A36C1E9A0633,
	Story2Manager_TimeIE_m6A9B40A187E64B7BEBE3F30EF100815FBE55FCC2,
	Story2Manager_Update_m2673C3044A2631E09F87351DC1FAF079E51726D3,
	Story2Manager_StartStory2_mC762E3ABD120371A21A83228440CF053B6401CE3,
	Story2Manager_TypeLine_m9F81497C07EB9F4A08C1A6761B747F414B723215,
	Story2Manager_NextLine_m4F69952FB61A2F262B1DC2D10CB42509E87D9F72,
	Story2Manager__ctor_m00BE02703CABAE879A227B7A054955B57E0289AD,
	U3CTimeIEU3Ed__10__ctor_mC61D5359D18BC71478D2B26D364DA9E5215CA853,
	U3CTimeIEU3Ed__10_System_IDisposable_Dispose_m28569B7D30B0704ACF7F0C2FD6E9A83606F8AA55,
	U3CTimeIEU3Ed__10_MoveNext_mD0F7B4A0C45F0D8EAB4A8F0181BE1FE0BE826153,
	U3CTimeIEU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0B75C09C6CCDEBC775E1A4FE0151E6BFE4F41DC7,
	U3CTimeIEU3Ed__10_System_Collections_IEnumerator_Reset_m289A9D9686AC91F03213A3EF84EB9C62091B7B1C,
	U3CTimeIEU3Ed__10_System_Collections_IEnumerator_get_Current_m938DBF30F82FDDE5FA86D026FC89B0FAE95EB447,
	U3CTypeLineU3Ed__13__ctor_m3381142803F5490CA2C6EA44676B001A924C87B0,
	U3CTypeLineU3Ed__13_System_IDisposable_Dispose_mA2E73E049CF5E008F407436C5885C01CD1BE1CFD,
	U3CTypeLineU3Ed__13_MoveNext_m53782D6AE82F566E61AF6C736E1E52E8D766BCE1,
	U3CTypeLineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB3E18BB92F474F438C262387EF09784344930706,
	U3CTypeLineU3Ed__13_System_Collections_IEnumerator_Reset_m18F6D13CF39437E0C7D6FCDFD2A9F7431F5096FE,
	U3CTypeLineU3Ed__13_System_Collections_IEnumerator_get_Current_m32405CE40FDE12E1870029E0EC5B92E58F8582E4,
	Story3Manager_Start_mFC3E434723CB821E85A0D0F5C25AEE31B1EA28A6,
	Story3Manager_TimeIE_m3BFB48C73E1A83E8E21720324DE7CC270D8DE122,
	Story3Manager_Update_mADE639B88A82CE6B120F084F8BC106DD66699B1C,
	Story3Manager_StartStory3_m90E4C2DBEEAD4D6B05F0C99D4265E173DFBAE648,
	Story3Manager_TypeLine_m5EC34C6FF1711EC998F1D8D6EE88EAFC52A61675,
	Story3Manager_NextLine_m80D30146AE128CE008B282BEB41899BB9C5F490A,
	Story3Manager__ctor_m33F414A739B399302A1A7DF2004B4818430C2BF7,
	U3CTimeIEU3Ed__10__ctor_m13DC5856052DFE59F5E14120CEA19C0193D0BEAA,
	U3CTimeIEU3Ed__10_System_IDisposable_Dispose_mBFA4A57358A033314A1AF41AAB8F2AF92F24C646,
	U3CTimeIEU3Ed__10_MoveNext_mA01D4D5A3E1283F9A3A4A5D2E76C3C95AD1EB692,
	U3CTimeIEU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52B342497384166C38981CA667740BD580076450,
	U3CTimeIEU3Ed__10_System_Collections_IEnumerator_Reset_mBC68911DBF658B3477EA295D7BF2FFD8B0FC484B,
	U3CTimeIEU3Ed__10_System_Collections_IEnumerator_get_Current_m75BB82C5CAE4C881AC8B70EECF6E4989EC8470DC,
	U3CTypeLineU3Ed__13__ctor_m8268E1A248C3515BFE52D666819313D5E77E1197,
	U3CTypeLineU3Ed__13_System_IDisposable_Dispose_m34607E4719E82B3C90FCAA602BA9158A8AEA2B5B,
	U3CTypeLineU3Ed__13_MoveNext_m6579F3AC96919C91AF1649CD36D4C1A17F65BC85,
	U3CTypeLineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2A5EA8BF16919F3AEC394B997A12533E9AE0DAC0,
	U3CTypeLineU3Ed__13_System_Collections_IEnumerator_Reset_m18E7B4304AC09386ECD997307FA234B521E21439,
	U3CTypeLineU3Ed__13_System_Collections_IEnumerator_get_Current_mD86346C7738D5B2E93D38B4CEFFA74C98305D845,
	Story4Manager_Start_m313497C27E87C01DEABDE0D8D5BFC93179DA56F7,
	Story4Manager_TimeIE_mF20ABF95DD1A9D0D8BDBBA7D7C3E0529133F2EE2,
	Story4Manager_Update_m7BB35AA42249C552F1C4E43F88739A7B40627179,
	Story4Manager_StartStory4_m881FE2012318129CF12D4B7ECBD21CCDDF8E9166,
	Story4Manager_TypeLine_mB11C4356720EACBB75ED9E29CA3370751FA43C33,
	Story4Manager_NextLine_mCC54520B758572B0944D2DE61A6AFFD33CF30316,
	Story4Manager_LoadLevel_mF293EB8FC4D88B4989C279119EDA71EB5318B66B,
	Story4Manager_LoadAsynchronously_mD438C42C9179A37EB643566B09675E3FA842CFE6,
	Story4Manager__ctor_m0656D961514C745B74EA8862AD25365AC60D1D53,
	U3CTimeIEU3Ed__12__ctor_mBB9F8F85D5DAE9BD94AE09041320E26BA12DADA0,
	U3CTimeIEU3Ed__12_System_IDisposable_Dispose_m4B0734E90CB756B4B8E4B4D73A4C53D967759F1D,
	U3CTimeIEU3Ed__12_MoveNext_mBB39C6A33BA9612716E7063379D29ADF2A01CE42,
	U3CTimeIEU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C73D565CC03D11BC1C842373611D2810BB5B3A4,
	U3CTimeIEU3Ed__12_System_Collections_IEnumerator_Reset_m7EFD622AE07588EFBC6530F070A4DA48F45B5B6B,
	U3CTimeIEU3Ed__12_System_Collections_IEnumerator_get_Current_m79A22DBAF3530D9FF60B2AA9B5A268FACD7E0C1B,
	U3CTypeLineU3Ed__15__ctor_m5E924108C2849A14B99387420583E4FF4191ACDC,
	U3CTypeLineU3Ed__15_System_IDisposable_Dispose_m1751D8F1A560C0BCD88C8384796F0FE92D9EC701,
	U3CTypeLineU3Ed__15_MoveNext_mD43823B66425FC8993F5D1D9E4DAD82F23AD7420,
	U3CTypeLineU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m76FC58D00BCEA3EE34B60EEA166E53E8D68D2ACD,
	U3CTypeLineU3Ed__15_System_Collections_IEnumerator_Reset_mBDC2E72B1994109877CBA9AF3F9F2906A14AF27C,
	U3CTypeLineU3Ed__15_System_Collections_IEnumerator_get_Current_m9670A64D4B8B6142E389CD03268C21FF7123EF88,
	U3CLoadAsynchronouslyU3Ed__18__ctor_m9193454C25CF53C50BE2952AD275D59D148125B8,
	U3CLoadAsynchronouslyU3Ed__18_System_IDisposable_Dispose_m81EAE1BAA920EEAAB99C7292C8063FE9A18003D6,
	U3CLoadAsynchronouslyU3Ed__18_MoveNext_mD9C3B1A0E974F19B39CC42EBCB12FE7DE33F3A4D,
	U3CLoadAsynchronouslyU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1CD8EE6EC9E99257A29EBE8EE8ACDE0A21FBE909,
	U3CLoadAsynchronouslyU3Ed__18_System_Collections_IEnumerator_Reset_m533E97DA9C85B050583E198153B03F0C6442557B,
	U3CLoadAsynchronouslyU3Ed__18_System_Collections_IEnumerator_get_Current_m13A26DE6C2E4018242E9DC5FB764FC8685006B48,
	ActivateTextAtLine_Start_m5132C54E58627A87D622E1019353E01718DE1A3E,
	ActivateTextAtLine_Update_m5DA13B7F35F66358607FFF0A8678781234592E63,
	ActivateTextAtLine_OnTriggerEnter2D_mDE2A215FC0F3ED09B490A0C272C15024AED39A0D,
	ActivateTextAtLine_OnTriggerExit2D_mE153F58EB60626D6467C7AB637D7DF0914CC13D1,
	ActivateTextAtLine__ctor_m55901F4FCCF4ACB4B3A35D79179C8A8EC0B908E9,
	DialogueManager_Start_m612B88A606E0F326778C59A3207CBF0548F5C6A5,
	DialogueManager_Update_m2A67EDB2B64453FC1243F80AE76CCD7136C21138,
	DialogueManager_TextScroll_m5D3194B542493389CE2040A3ABF204E5CDA2A029,
	DialogueManager_EnableDialogueBox_mA85924DDD60ABA39C42E39BAE1AA1D26E99F6360,
	DialogueManager_DisableDialogueBox_mC83B803E1DCE5F4E37B8D74DA19A4C79A1F405BC,
	DialogueManager_ReloadScript_m0451D3127B65D5262CB72E4CC870F61FDBB1B168,
	DialogueManager__ctor_mAA8FF2D1E586DBF3472DF58D3B1BF8F4FA03EE1E,
	U3CTextScrollU3Ed__14__ctor_m0FF343CAE7D09A873B318ACAD7760727CBBBBD44,
	U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m8DD31CCCB316C58800E444EE15133E4E8BC15ECD,
	U3CTextScrollU3Ed__14_MoveNext_m921F33248DC4DCA23A61BE819498BB15BA75C0C2,
	U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4FDF89D7CC6D5A16A2361C1113345C725CBCC522,
	U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m11694E97359876379F5AB7702B0E37F2A0E8C0F9,
	U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mABE1658974027AC5CF135A1980F2E95028C2C7C4,
	DialogueManager111_Start_mC1A9AA1DA185ACD4A2731AB3907FB69A56A0102F,
	DialogueManager111_Update_m9B06DFE6C6C5327DAC36AB804A3BD2619C914FCC,
	DialogueManager111_TextScroll_mBF4DF710DB2696EFEB78F5E9EB3B19A45EC6CD95,
	DialogueManager111_EnableDialogueBox_m0BF16E19CD7EF4832BE64110D931F5AAADE5858D,
	DialogueManager111_DisableDialogueBox_mE7AF69D498BB2C5D140E2960CE6AECDB6E932DA3,
	DialogueManager111_ReloadScript_mC52C5C38D6A38026FF031749ED8CB16101FD08C1,
	DialogueManager111__ctor_m19ACE0E6C3C316890B8A2F847BC42799E46948CA,
	U3CTextScrollU3Ed__15__ctor_mFB72C4F6C3A9277F985C226F0C5AE5C882610675,
	U3CTextScrollU3Ed__15_System_IDisposable_Dispose_m8682A2B86D921428757604CC5147E8BFB5DE8D80,
	U3CTextScrollU3Ed__15_MoveNext_mACEA5DFB507C8AFF836B205F132397EC1D311FD8,
	U3CTextScrollU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF07A8CC8414E93E791847D1301E2CDF2629D324,
	U3CTextScrollU3Ed__15_System_Collections_IEnumerator_Reset_m18CBF4829F14DFF81C74F4E30461372D237C879F,
	U3CTextScrollU3Ed__15_System_Collections_IEnumerator_get_Current_m2E43D012A1C14989FE33597AD52AA78013DCF877,
	ActivateTextAtLine10_Start_mEAC91AE6BEFF67ADC1626000972AC4EC1A064E8E,
	ActivateTextAtLine10_Update_m231E8AEC0FC0949729CCA520E30507E11E173095,
	ActivateTextAtLine10_OnTriggerEnter2D_m19FF4B879A25C02DFD4057C3A95CA8EAD8E01D10,
	ActivateTextAtLine10_OnTriggerExit2D_m2D5E3AB0EB4358FEFA1E503A7D5B4F4558908BD3,
	ActivateTextAtLine10__ctor_m09611DFB6A55C714E058283BC7D39170AE3AF1F2,
	DialogueManager10_Start_mBF3B4C4D7DC0104FE6E863D227D25170708F0C11,
	DialogueManager10_Update_m02DB215761F9243F520240056E210921B1B389B6,
	DialogueManager10_TextScroll_mEFC8587C6A8F527F06560AA0FF26CE9351CBA49C,
	DialogueManager10_EnableDialogueBox_mD1018DB160CA21DFB2ACF7425C8EFF01E5564621,
	DialogueManager10_DisableDialogueBox_m8B41DFE70F80747F97714C4CBB8AA8C1FCA37EA7,
	DialogueManager10_ReloadScript_mBA569F3417A566A97A8005E2024A6F0F9D99B16A,
	DialogueManager10__ctor_m4744BA0B6764A588573274AC93047691D2C56707,
	U3CTextScrollU3Ed__14__ctor_m25372485B50A52BDD8128CC237EE2E22C46EF534,
	U3CTextScrollU3Ed__14_System_IDisposable_Dispose_mBD7F7A2F7E8A76C7B522049B83C0F99302195930,
	U3CTextScrollU3Ed__14_MoveNext_m37BD56B4B63585B254DE5F229D3A849A29288458,
	U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE424541DA8B50682073EF643295B76FA41785D78,
	U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m30934F0B954200D61D8A62774DF6D0D2885265F8,
	U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_m09ED21E076FD07A2D85934D5EDF75577CDA2EC18,
	ActivateTextAtLine1_Start_mF12557E4B0888167F4F748DFF5CFD3904BC4968D,
	ActivateTextAtLine1_Update_m80E599B4C33597819B9F12DF39B40FEDB478DEC5,
	ActivateTextAtLine1_OnTriggerEnter2D_m693AD794AC6ED1F14CE0249686322F52FB5C84C6,
	ActivateTextAtLine1_OnTriggerExit2D_mD0CF8A89A18B1B3B78F56D91104003A8B1A9108C,
	ActivateTextAtLine1__ctor_m57D79A4003FD7AB2F31D6D602A2A54F8EF212D99,
	DialogueManager1_Start_m4170E9E118F1F7CE2ABAC1DD9AF1781A7AAA3E6B,
	DialogueManager1_Update_mBEAADA526B49A24509D1926A7167D0490236274B,
	DialogueManager1_TextScroll_m449B179ECE4CEC9C9F207D5AFAE30142BA1719C8,
	DialogueManager1_EnableDialogueBox_m682A1E2E8A78299179BBC7749C071CCF5583445F,
	DialogueManager1_DisableDialogueBox_m87ED3DD610D99E7786D7EC8BC77AC2E82C1F0A14,
	DialogueManager1_ReloadScript_m1737B1B0C5442C637D89D9B1B0E13351B241E0D2,
	DialogueManager1__ctor_mBE4B50DD4B2FB609BB30E393CC2E075759EA23D6,
	U3CTextScrollU3Ed__14__ctor_m46153879164D1408755D717F0CFF3804B9D0DA21,
	U3CTextScrollU3Ed__14_System_IDisposable_Dispose_mD54DB86896E9D8FDCF8BC296B9FEF71554B67D6C,
	U3CTextScrollU3Ed__14_MoveNext_m8F9D94FCA36681D7D7BA0635292F5D5E38AB1C67,
	U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB172AE4343A8D82D557AF6D79E3A12EB643BB220,
	U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m8C8F933102AF5469324D53568C59C3211A0721BD,
	U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_m08ECAF3014121CD6964575229A159602CFAD3E58,
	ActivateTextAtLine3_Start_m5DAD72C786521298B8039978FBCCF91362455F8E,
	ActivateTextAtLine3_Update_mECD8F42319EA84F0B846BCA97E3DD9BC0E3F2597,
	ActivateTextAtLine3_OnTriggerEnter2D_m0BC2D76D7B84B012E053B5B67AEECC3429559AFB,
	ActivateTextAtLine3_OnTriggerExit2D_mC42AFB1C7A91F6F6F7A620AD641AF17DE593790A,
	ActivateTextAtLine3__ctor_m9815F1455BEB4DCC8E00E4C5C6858AE598E06D90,
	DialogueManager3_Start_m94B2515ACE4E011C5E7DF03F3D34DCA5706B3472,
	DialogueManager3_Update_m82FCAE14D26B1F348F5C68AE5725C07635513F47,
	DialogueManager3_TextScroll_m1B5D37D563F9AAE195726DB2DE62F7877BEE2C4A,
	DialogueManager3_EnableDialogueBox_m6ECC3BB779BD33010906CD27D2871C51B16DF7A5,
	DialogueManager3_DisableDialogueBox_m46B3FBED60B53CBA49B5EB96F6ADE4883E2BB6EC,
	DialogueManager3_ReloadScript_m910AB8560CC598B39C0C1561E3765E7467B7F8DB,
	DialogueManager3__ctor_m1B706AC70FFDDC4A27AA2E1528D159619ADA5B74,
	U3CTextScrollU3Ed__15__ctor_m4D514A28DAFB725FCCC3F86305EF9047E11B835F,
	U3CTextScrollU3Ed__15_System_IDisposable_Dispose_m5B3A2BA0FDE76B3D910F9F543622E3B7E0104CA5,
	U3CTextScrollU3Ed__15_MoveNext_mB90356F0A00008F987EF6E8C88325D907DE7067C,
	U3CTextScrollU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44B1FEC4291372B6B51003ABC8F76DF83193D7B2,
	U3CTextScrollU3Ed__15_System_Collections_IEnumerator_Reset_m98B30F5DEA0D66BE62F371A7A6C3DCB9B95FF6C4,
	U3CTextScrollU3Ed__15_System_Collections_IEnumerator_get_Current_m0F1D7EA630D47E82DA6928695681B07E7F6F3785,
	ActivateTextAtLine4_Start_mC9C5D1632FDAD920E38EB31BAC4ECB16232F5048,
	ActivateTextAtLine4_Update_m0A737B58A1B92F024BFE0C80C84DFE6C1FDB174B,
	ActivateTextAtLine4_OnTriggerEnter2D_m5CFAD7616F9D1B4E0BC731B46875CB8D2EA341E7,
	ActivateTextAtLine4_OnTriggerExit2D_m34230CFE0390423C0FD607B4F5B72A62C7E36D3F,
	ActivateTextAtLine4__ctor_m9124FDC7BEDFB1461CAC3CAA95DDF4CC02C6C4AC,
	DialogueManager4_Start_mD33C46E902C609C35E49E565D0958DDFD7458A59,
	DialogueManager4_Update_mA28A9FD0318A53E91D1CB3F1AE791048C2522037,
	DialogueManager4_TextScroll_m0C54E3C56347658AD1502A8612C40A2804A86F1B,
	DialogueManager4_EnableDialogueBox_m19DCB02DA55E5712BA1AA2DF01C0FE1F948BE363,
	DialogueManager4_DisableDialogueBox_m50B836470C88EF2E31EBD18AB7783B1515D089EA,
	DialogueManager4_ReloadScript_mB2D717AF585AD258D2EB52718D060690BC4BA4EA,
	DialogueManager4__ctor_mC4E1CC9F7DBC22DC06956B13E3628FE668DAD691,
	U3CTextScrollU3Ed__14__ctor_m287E95932EF65B84A11568F314D1A15E320B6536,
	U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m66D5900B028A65713A5BC970753B95A980024A9E,
	U3CTextScrollU3Ed__14_MoveNext_m27C49752D7B15DD32AFA06ACC2C82066EC3CADE4,
	U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m67E6C4920DA41BB4F2897A59F1B82A46C76EDB90,
	U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m2BD1E12D338D61FAA0A2A9B8C4778CE45A264B89,
	U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_m9349BFBAB9DF9427D539E5EA95DA106B8B7D9158,
	ActivateTextAtLine5_Start_mF97481AA0D6C649C18E8DF8131037B15BD34331C,
	ActivateTextAtLine5_Update_m3EAE545421EEF09D347ECB1130FC4AFC420719DC,
	ActivateTextAtLine5_OnTriggerEnter2D_mDD616AA06B7F5D46D7A36A6DF5CE1CC78A5E8EEE,
	ActivateTextAtLine5_OnTriggerExit2D_m18CC67629720BA8FE9CBE76CA0F78D8F79F3A644,
	ActivateTextAtLine5__ctor_mDEA43E87500460268803BAE92BA0C39992E421AC,
	DialogueManager5_Start_m8FC5531C7D23203D917C69CA71F49868E30358A2,
	DialogueManager5_Update_m454E8BFB2F6558E11322CA8F6335A586EFE3597F,
	DialogueManager5_TextScroll_mA868CFAC74505D370CE5C4BA8E8469957AD8F086,
	DialogueManager5_EnableDialogueBox_m726A49588D335BC0704D18E598DDDF1F0523811C,
	DialogueManager5_DisableDialogueBox_m61E96CFE72D25853490C83415FBD303A464B50C3,
	DialogueManager5_ReloadScript_m405A2BC3CFBBCCF0970F057379BE5E21B1F10BC9,
	DialogueManager5__ctor_m319A7400B23794C994E435F0811569BB191DB590,
	U3CTextScrollU3Ed__14__ctor_m5CD63C4796AA3EFFCDDB1B1695B299FA5F129190,
	U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m1131A72D8DAA306883DC73A644E10DF939E3DDC8,
	U3CTextScrollU3Ed__14_MoveNext_m758DCCF499E9189750CBF4889B2A92641A1C4A37,
	U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m586FF4D51CD4F659D098B08A3EF8B37230C59BA6,
	U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_mB92B68E8BE2177A8B7EA040B8CC0328708947DB2,
	U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_m83A16F0C427A38D04171F21D0FB92541B0319C76,
	ActivateTextAtLine6_Start_mCFAF3C50CD6CC74725FF4AE3CFEB9463918BF0A9,
	ActivateTextAtLine6_Update_m743A5109226D2AB5953EEBABF043BF6588391485,
	ActivateTextAtLine6_OnTriggerEnter2D_m1F51ED4D4D4FE1F14DF24C9E143A1F48CDC7F4B7,
	ActivateTextAtLine6_OnTriggerExit2D_mFC4FC50B3D008FEE7ED846758CB417BD4FD724FB,
	ActivateTextAtLine6__ctor_m02F1FE204A897C3F2DA793F7D260BED47C713B8E,
	DialogueManager6_Start_mCE7747BECF9035DBEF34C072045D71AAFBBC34EB,
	DialogueManager6_Update_m82F42AE25D3D61B94A780723A1A3A407407DF924,
	DialogueManager6_TextScroll_m5A10285269F39472F209FE26602A0D0D1D2AE92F,
	DialogueManager6_EnableDialogueBox_mA0009C6BE3F39795CAC41D701CE5A082477CCA6C,
	DialogueManager6_DisableDialogueBox_m9AEC9A5F2BF80BCB58263744B7CDBEF0C698132D,
	DialogueManager6_ReloadScript_mB1B016B1BFA2F5DC3E7C13535082701BB90CD09C,
	DialogueManager6__ctor_mDFD604CDC53BEB5A2F5DA2A50EBB863CFC505439,
	U3CTextScrollU3Ed__14__ctor_m29BA7873E6EF4DFB87C961BB0BF78BF2976D3128,
	U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m82BB053A1C51D75CF09DDB5F7DD3CB29FC73CE9E,
	U3CTextScrollU3Ed__14_MoveNext_mB8282D2D1C7318DF25D0252B1D43955146D40FA9,
	U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB001D87EE04371590F576A76F96AC0CDA78BAFEE,
	U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_mCD2873EF7E990CC57029E50B039B3B93F55CFF86,
	U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mE5FB965E79F06B9E364142856FF3C3CAA11040D7,
	ActivateTextAtLine7_Start_mC3DAF66946F51DED9760C435A229016BCFF59B43,
	ActivateTextAtLine7_Update_m819CFC6BFBC6E2F59144D61DBB6DAE3FF6CEDD62,
	ActivateTextAtLine7_OnTriggerEnter2D_mEB71C535E7C077CA93B15111E64E59C74677ACC1,
	ActivateTextAtLine7_OnTriggerExit2D_m5C77DACA6F927B705CE91CBAA7C48D9EC6C527BB,
	ActivateTextAtLine7__ctor_m730E85D3DB58F829B9848F25FBA17405C130BBDE,
	DialogueManager7_Start_m1A26434C9850F12D254D1214B95A8079D5B5BC6C,
	DialogueManager7_Update_m283318D688C4A403256582284F8DA5825809280F,
	DialogueManager7_TextScroll_mBE32AC7FF53D80990FD20B69E4538EDAAA79D380,
	DialogueManager7_EnableDialogueBox_m99305BDD68064E089D7374B0C203151EA269C8E1,
	DialogueManager7_DisableDialogueBox_mBD94480402BFCE491DD4D7B0DBE31B9E79199615,
	DialogueManager7_ReloadScript_m223EC77726F0753CD434DC0AD4766D7864655E7D,
	DialogueManager7__ctor_mBC0C6098CAFDB87D286B92FD234CE5372030BB80,
	U3CTextScrollU3Ed__14__ctor_mCCF5C5A9B5C4FDC2A5361E59F0E6D5B63497E721,
	U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m86803CD35DE20355B45C10DFB1BBEFB11FACC611,
	U3CTextScrollU3Ed__14_MoveNext_mEC51731C840B350595965938F38643EF3E19AF12,
	U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF0D25459F989654F0F9931A907E0BE2D7C8C8957,
	U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m612A77A93459DCF470D8F30C2D062034F01B8E2E,
	U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mBA75C9E3BB40C9F9CAD7791953A001FAE2B3A531,
	ActivateTextAtLine8_Start_m0654FE0EB73CEB574A157921F3C9A08AFE905A38,
	ActivateTextAtLine8_Update_m1F52C58A07E8EA73842FC35EC85B71FDCFA84AEE,
	ActivateTextAtLine8_OnTriggerEnter2D_m276A9D4D43D9B0DB0FDBCB2DEDB278BABBB78320,
	ActivateTextAtLine8_OnTriggerExit2D_mA7834EF80EA0640FDC6BACD1FB47A9F3CB9E8AF8,
	ActivateTextAtLine8__ctor_m57A59510E3180BADD971057EA0CF68CF1A8A5073,
	DialogueManager8_Start_m0D2A92B9734A7639EEFD702DC7619C52557FF778,
	DialogueManager8_Update_mFBA760983CE60BCA4B5B299962CF71D1BA015D34,
	DialogueManager8_TextScroll_m530F4F98A324FEE74AC4BCB4499A04AE0A4BA482,
	DialogueManager8_EnableDialogueBox_m7589EB91B47331C31D0E3CBFE44277440529E4A5,
	DialogueManager8_DisableDialogueBox_m9D4652257014CC2D973C263C6439655195087F2F,
	DialogueManager8_ReloadScript_m73451D8936FFA40F65790EA1D34BF3C0824AAA82,
	DialogueManager8__ctor_m7F3EA0C0A4F4D56A83D45F8AF17F0659949B6876,
	U3CTextScrollU3Ed__14__ctor_mD21839154365B3E4559D29CC0531B9C507BFBBBE,
	U3CTextScrollU3Ed__14_System_IDisposable_Dispose_mB081476BC8CD3EB1AB9DD3CE91393410457FA640,
	U3CTextScrollU3Ed__14_MoveNext_m4DDC886C45FAF4B23561F786DAEC54DDA1AFE8E0,
	U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3FEFEE3C8052FA546CF052470E36DCFD51824CC,
	U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_m10F6AFF907AA15D3F12357D6D000715B2FD102B9,
	U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mF851251A598615B608406828EC8BB974DB7616B3,
	ActivateTextAtLine9_Start_m9F678AA992CCCBDFE461384308486A579EBB5727,
	ActivateTextAtLine9_Update_mA949020B249DB943B6C726BBAF8ED5B71901E231,
	ActivateTextAtLine9_OnTriggerEnter2D_m9CB1F2AA262922160F443FD0E4BECDCB0316CF1A,
	ActivateTextAtLine9_OnTriggerExit2D_m503E39415483FCAC00F7273A5F35C8B4F3CA9C0D,
	ActivateTextAtLine9__ctor_m9BB3878FD27FD9DF820BB8A7B67EF6D7B50BC05F,
	DialogueManager9_Start_m45DFAC5FBBE23828C45592C49DAE111FAB4E6E4B,
	DialogueManager9_Update_mCB393634E9EAABA048293F593397E49EFBEBA7BF,
	DialogueManager9_TextScroll_m6A595793A9FCDE637C136F78B85D75D9455ECF19,
	DialogueManager9_EnableDialogueBox_m0754388F3AC7A47D89B751961A73EFAA7D32CBD9,
	DialogueManager9_DisableDialogueBox_m2828162D1DE9118741ADCDE31CAD5C60E594BBEF,
	DialogueManager9_ReloadScript_mBEED7CCF67B58D00336296BFD548B4D02E121751,
	DialogueManager9__ctor_mFAEB62129805A6FAE8393F8665726B90E54F6491,
	U3CTextScrollU3Ed__14__ctor_mF3E5A9448661287179E4EA31143CF930B9359943,
	U3CTextScrollU3Ed__14_System_IDisposable_Dispose_m3112D9AA996F620897BE1A44084071E371DCF98F,
	U3CTextScrollU3Ed__14_MoveNext_m8711AB9EBABC00EC514B169BAE2FCBFA0A439739,
	U3CTextScrollU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C55D1856B48DA623AC540EA0215DD4D997EC254,
	U3CTextScrollU3Ed__14_System_Collections_IEnumerator_Reset_mC7D4E6F771B2947EB0A45186D5F769EEFD992FDC,
	U3CTextScrollU3Ed__14_System_Collections_IEnumerator_get_Current_mF4C9073C4148A84F5564C1CD781597BE870B28A7,
	BossHealth_bat_Start_mA7EEB91D849C5344E55FD8B5068F734D38FEA853,
	BossHealth_bat_Update_m59764091BB4DA65826518F69D1762881DD87F97B,
	BossHealth_bat_HurtBoss_m369A57F5D0369B16D19376CD11B60F650F24523D,
	BossHealth_bat_EnemyActiveOff_m0E6061CF40413FD702604226B2C8CB6CCD6AC732,
	BossHealth_bat__ctor_mDEF7DCB883EE0C9D26924E3214DFB908552F22E1,
	U3CEnemyActiveOffU3Ed__17__ctor_mA577EC9EA8948E1D60DA759F2D296C5044F54096,
	U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_mE52F244C415FAAB8E042303C9A8BE828C153F6E4,
	U3CEnemyActiveOffU3Ed__17_MoveNext_m78B769C834BC2D07539DA23F2A5E6AD2894B6A80,
	U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m794E31662324F3791521289BCBE1BB477C71F130,
	U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_mC3BF406E3FF14C5382C3C0C5642F7078C23CAFFF,
	U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m3A95CFD2F8CA16B187FBEF68F07D730F36A098F7,
	BossHealth_bear_Start_m8671081FD8DB03071099FE050F52681FC140B9B5,
	BossHealth_bear_Update_mCD80C542B86E871D2EAD77B3A40E5A24C38BC0FE,
	BossHealth_bear_HurtBoss_mBC45A5992F9548DEF344D912F1F3B167C134121C,
	BossHealth_bear_EnemyActiveOff_m2F45DF2FA4E4D971EA4042766EA11222FD30CC8A,
	BossHealth_bear__ctor_m8778AD21BF0C9255BE40CFDEB3A1E795FAD3A91F,
	U3CEnemyActiveOffU3Ed__17__ctor_m464AE6200A1E122F7A40E31654B6CCD9E7776279,
	U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_m253B9F6D14FFB4368754C68A0B731BDE59F82626,
	U3CEnemyActiveOffU3Ed__17_MoveNext_m6CC96FE447CA184FA65AEEE3B42C9D478180300F,
	U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD776DC29AC55AD6852773C217E3FCC42429CA3B8,
	U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m5D62DE7F1C1F40D685BEC2EBFEA3BDA71D8B5E8D,
	U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m8CAD4C264644BCD9C4EAE254FB2D21F63C5FCF0F,
	BossHealth_bee_Start_m1AC404F4C413BB84B7001DAB385F1EEEC1493573,
	BossHealth_bee_Update_m5571634F2145EDF4DDFFAC7BAEB0831127C4825E,
	BossHealth_bee_HurtBoss_m12EEE43DEA01B2A5AFC2E9D5EE58B9EA03EF2A32,
	BossHealth_bee_EnemyActiveOff_m2A0A8EC50551CC68290E2D4D4569AC31159ACA7D,
	BossHealth_bee__ctor_mE3DE533807D18CEE0631E8BD353A3D389DBDE022,
	U3CEnemyActiveOffU3Ed__17__ctor_mC83BEBA335A88147D5893E6528372BF833DC7D2C,
	U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_m0A8398FF581CE322BA9D7D845297507334D68646,
	U3CEnemyActiveOffU3Ed__17_MoveNext_mB71EFDEC061A3850F6E294F13A6BD6BBC0E32D33,
	U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46706DBCDBF6B07645423CCC5A194AAE73928688,
	U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m2AEA766F9011936F0D23BF9592ABFF38B737EAED,
	U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m98824C77418948E11B692A5274B598916C00113D,
	BossHealth_guide_Start_mB2773F3F37E492C1AA079A6842E683F345B58758,
	BossHealth_guide_Update_mFD88677A8263EFED8DB8DA90048F345CB2B2BF7A,
	BossHealth_guide_HurtBoss_m6F9EF1BA88D568131169E27ED8B1A867014BFD23,
	BossHealth_guide_EnemyActiveOff_m57EA0B9D50AC33B4E0ABDB50F9483C29D89ACF91,
	BossHealth_guide__ctor_m8D37A3BE254F43D9F56140DE44FEB755FF8FF3C6,
	U3CEnemyActiveOffU3Ed__17__ctor_m09EFDED0A4E6C509759C171FC612A7A4190C362A,
	U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_m1F58E811E2CDB854E5C6DB1FC351391AD8881B1D,
	U3CEnemyActiveOffU3Ed__17_MoveNext_m150826C406392D16C49FC978BC7D27BE622E5F0B,
	U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4E97E597A05A689D1F935145EF371638BB8972C8,
	U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m213302082DF8640A57F8F8D259FEDCB319F1564A,
	U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_mF3B08ECAA4BDC5A885CF6D75480B2D30BBBD2365,
	BossHealth_octopus_Start_m983069329E0E5B22487BD2C47E146827E8CB7DB2,
	BossHealth_octopus_Update_m90624A9B2E5E943AF08BA82644931E7CFE9652F9,
	BossHealth_octopus_HurtBoss_m6D44677AF340CF97F61FD5CD78692B43C29D12DF,
	BossHealth_octopus_EnemyActiveOff_mF20C14DC9DE90D938AE47F959C8C3181FE9F3DE6,
	BossHealth_octopus__ctor_mD60621DC3CFF2516D3841EFBC00A8AA3AA9E85D1,
	U3CEnemyActiveOffU3Ed__16__ctor_m656F1D29A891EF8964C433F09D84AFD32EF87E91,
	U3CEnemyActiveOffU3Ed__16_System_IDisposable_Dispose_m40B57F8B7DC59586C6D0BD37F92FFF21F0612FF5,
	U3CEnemyActiveOffU3Ed__16_MoveNext_m02E6554E43C3BA5C28A8C7267E00206B96F582B9,
	U3CEnemyActiveOffU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m014CC50FCD57228D47D667F2A4D3D5B3CE9A13F0,
	U3CEnemyActiveOffU3Ed__16_System_Collections_IEnumerator_Reset_m08A275A350536FF546323FFCAB9EB2BE90D166A8,
	U3CEnemyActiveOffU3Ed__16_System_Collections_IEnumerator_get_Current_m69BAF2DE3C9991F838E0FAEB774C31E5F75958EB,
	Boss_BatUI_Start_mA541EB479AB19D6D526B9C8B44DE2D2B8C2E6B48,
	Boss_BatUI_Update_m411A6933469E15A37DA8E5BF4DE036733FCC536B,
	Boss_BatUI__ctor_m8997480007C0B5550F62C4DE916D773E80BE36F9,
	Boss_OctoUI_Start_mB5242F6455F7FDB963B6ED367489D6A214171BB1,
	Boss_OctoUI_Update_m23F95529D2BA133174480529F015C72A25885845,
	Boss_OctoUI__ctor_m39D3F87E3AE8D1813A0B09346571E72DFF41B540,
	invulneralbe_intro_bear_OnStateEnter_m07021CFCB6E2B67C4A4733F1EEC30E81131B723D,
	invulneralbe_intro_bear_OnStateUpdate_m120A65A66444BDA2DAB50E9F8D7168F9D6E61507,
	invulneralbe_intro_bear_OnStateExit_mEEEC42A3936207465FCE927664447CF32181E84D,
	invulneralbe_intro_bear__ctor_mB186B356261B7FD7A941722700E8C015F3B8DA60,
	invulneralbe_intro_bee_OnStateEnter_m12567438DF7CD460E1CD8914C018E4E0FF98F462,
	invulneralbe_intro_bee_OnStateUpdate_m54E0A822E045B37E427C0BBBDEDF45A71F863DC3,
	invulneralbe_intro_bee_OnStateExit_m0C4C0AAC02BDD8ADB1F5618A5EF56D65857EF13B,
	invulneralbe_intro_bee__ctor_m173B306BD4DDFA928F99FD7F60A5895AD77296FB,
	invulneralbe_intro_guide_OnStateEnter_m41FCC1D9A049E6A80E7CC17DCC1FB16455F36137,
	invulneralbe_intro_guide_OnStateUpdate_m7DA52540DA62BB9051EC3BDA80DD14A08326CD7F,
	invulneralbe_intro_guide_OnStateExit_mFA977A4C2356DFB2401FDF8982179D5E332CEC05,
	invulneralbe_intro_guide__ctor_m240DE89D0C50C106529049A8C1C5CC15AEB311E5,
	invulneralbe_intro_octopus_OnStateEnter_m98AA4B63D700C1DFC664BD5E174B9A8173D2AF30,
	invulneralbe_intro_octopus_OnStateUpdate_m5AC211A1B83D23972436F1F08187DEFC690CEA49,
	invulneralbe_intro_octopus_OnStateExit_m3F21A91ABA8ED9E25CD445FA375B6DD8B2E32A50,
	invulneralbe_intro_octopus__ctor_m315BA9BB3336200920AED45A7DAD6DC5C7EA9ADD,
	invulnerable_intro_OnStateEnter_m728B6A70E01DE55BCBDEAC1EC7E5BF6C91D649DF,
	invulnerable_intro_OnStateUpdate_m8D4741C6E8C9FEF12626CAD3ECC6A7DF8B539A74,
	invulnerable_intro_OnStateExit_m484CBA364202346B2D1BED8F4BA3C08C38D8A345,
	invulnerable_intro__ctor_m830E223B6908912F955584C5FCF8BA854A755B76,
	invulneralbe_intro_ske_OnStateEnter_m1BAF6E7F467E66F1C05F36C5EC51ACA61A812109,
	invulneralbe_intro_ske_OnStateUpdate_m763CB3F48D184A03CF3352205909E81FC2B5CFA2,
	invulneralbe_intro_ske_OnStateExit_m9A2FF8015BF728A669EA22F255E18BA2251984C5,
	invulneralbe_intro_ske__ctor_m8FCB7B550224E8AAC918A82FF92FBD90439C6572,
	BossHealth_Start_mA3AE29D6097C29A2A0013ACA206294A53CA42BB2,
	BossHealth_Update_mDE473A175B91C0BAA6AF8D7FB4E3B69CB430AD24,
	BossHealth_HurtBoss_m16E3A925B039D8788F62F5BF29DB617A47596442,
	BossHealth_EnemyActiveOff_mF760A81E37497AF5E86198E6A3FAA7BB5391D9A4,
	BossHealth__ctor_mFB5239321838F62176E3743CA846266FED02047B,
	U3CEnemyActiveOffU3Ed__17__ctor_mEDA113BF186DC2339F8580F67643AB56E1ECE556,
	U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_mD06651F2C538A3224C9FDDE42A9012B321C56492,
	U3CEnemyActiveOffU3Ed__17_MoveNext_mA7FA02FA5A9FF76FF884D3D5ED4ADA49D2BBE83B,
	U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m06B0E0AA273012C9CCD56BB02407E66467557687,
	U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m2CB85D39D8F8BE0BE1EF9576A51AD73338551C25,
	U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m60DBC26F568D55337E46ED8A3513605501E94744,
	BossHealth_ske_Start_mB0222EF4733E48D0B9C6F4E367F99AE05818B587,
	BossHealth_ske_Update_mA027FA79FDE02A33B025B04CA68570CA8C02F815,
	BossHealth_ske_HurtBoss_m0875F1319D81D8827F33C4966AFFB6D850FEC3E0,
	BossHealth_ske_EnemyActiveOff_m5F04512A0BE51B250EBDC07BF4E5A8184A1E40D6,
	BossHealth_ske__ctor_mE27014B525B9E102E86343ABAE8AE811EBC44108,
	U3CEnemyActiveOffU3Ed__17__ctor_mD56FDF58CF76DF6DCF6A0C4CDE06B265665DCCF8,
	U3CEnemyActiveOffU3Ed__17_System_IDisposable_Dispose_mBBBA8BA4C90EFD0254A9D54C860BE67A49C0EDA7,
	U3CEnemyActiveOffU3Ed__17_MoveNext_mDDDE67D382E3FFC813A55AF5D545C36B8FB8FA58,
	U3CEnemyActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE5B73C806B8B5F2F2A7CF1822106AD4136D7783,
	U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_m765DD1E9FEB89FDD28649CC19B8F00F4C20CD5E9,
	U3CEnemyActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_m0F1D88A2309A0F825E691AE1BF307DE9D2EED219,
	BulletController_Start_m077702EC93C8AD8BDCD7C4F994454B0BD2D2C950,
	BulletController__ctor_m3FA7C9783D93CB95E0C47EB5ECD80A97D25F2CFE,
	DestroyOverTime_Start_m662FD2D94D17FF6008521F6C3EE2B2A05960841B,
	DestroyOverTime_Update_m9BD3239A91EEC7E53820274885C47DEFFE663495,
	DestroyOverTime__ctor_mB8D8C27A30DE70D4609B2EF8B28E7C8C99EF7C14,
	BossAI_range_manyBullet_Start_m453E9DCD5B618EE606E6EF2922EC8D599678373E,
	BossAI_range_manyBullet_Update_m1EE084FDFCA75A84E38514043AEE02DD0B149682,
	BossAI_range_manyBullet_OnDrawGizmosSelected_m46C4F40916465E05DE45F910E21AA688061AC853,
	BossAI_range_manyBullet__ctor_mCA3B4D2B8A41E78B4CBD0CBC7A9C0F09E7D9A629,
	BulletControler_point_Start_m8C00CF70C68D30F22792F5ED5E7181280E8D1F5B,
	BulletControler_point__ctor_m1AE31FFE56106B16E60B36CEEB0F238D7D7E2F11,
	BulletController_Bottom_Start_mE0028E4BA47A4BDB71CAFD79831361840D00BEBE,
	BulletController_Bottom__ctor_m4AB93D5650E30312C874F373D00C287A2EAAF315,
	BulletController_Top_Start_mBF11C12A40F192076459F0167642AE0707023DF4,
	BulletController_Top__ctor_mD1C529ACE9D70F60996F96666D442ACA4E982FAC,
	BurningPlayer_range_Start_m7CF1BA6A4CA4BBE50B640AE07D51046B14B1FED0,
	BurningPlayer_range_Update_m8C3EC9B6A06C615F47B9CEB8DFE97902D9ABAC74,
	BurningPlayer_range_OnCollisionEnter2D_m062E86E7317746A9F7FC8C2689341A86F4055561,
	BurningPlayer_range_OnTriggerEnter2D_mE81EB11F454E35CAD15B1268F24CE37732394462,
	BurningPlayer_range_OnCollisionStay2D_m36BE92C549A83700967693E992748D019EA854E4,
	BurningPlayer_range_OnCollisionExit2D_m940A14B854A471C54DFF7B9A6D667A9FBE470EE4,
	BurningPlayer_range__ctor_mA6111BB1887DE045FABC6431D2161A7D8726800D,
	EnemyBoom_Start_m7C85105A17405340ED8AD329A48C05FA2560AF97,
	EnemyBoom_Update_mC3F27E6CA3AC5E6FBEC4CF77E270276A533550FD,
	EnemyBoom_OnDestroy_m5C3945187042FA4CAA547E8C101B6034FE6E8E85,
	EnemyBoom__ctor_m48EEAC67443C01F6AA7902D7F5CE871A6DFED8CC,
	EnemyHidden_Start_mB994E8226BE485862BD4D1F6F001DFAE48F55645,
	EnemyHidden_Update_mEE46B0F0608CC7C3D4988243DDB94127BE456D32,
	EnemyHidden_OnDrawGizmosSelected_m35050894366F24024622FFEDF5B2F6E566835A94,
	EnemyHidden__ctor_m5C2034770CA45E56861481014F523E8F677B5997,
	EnemyMaxSpeed_Start_mD8AB2A1457C3528F413A96721F4C8E6FAEBB20C4,
	EnemyMaxSpeed_Update_mF8B8FC186A4B3ACD35476D7C49C1DB6CCD90C6ED,
	EnemyMaxSpeed_OnDrawGizmosSelected_m3C89F33DEF6AD47414AAFDF94C483E1122962F6C,
	EnemyMaxSpeed__ctor_m819BC3E4D593E0CB237B48B89AFA68D200806691,
	EnemySpecailAI_mele_Start_m49CEA83550E20D807786B4CAAA336B749E29D7D2,
	EnemySpecailAI_mele_Update_mB6790E1BEC1ED87AE175C421C9CFB3B1A8BAFE4D,
	EnemySpecailAI_mele_OnDrawGizmosSelected_m0C9157AC88F3333046CFC52397622754CC653C61,
	EnemySpecailAI_mele__ctor_m8FA88C57F7186F64D8B3384AE2DA3BB24ED80FB9,
	EnemyTwoBulletAI_range_Start_mED94AF945EB3CCA9345CDC51878DC75EEB343092,
	EnemyTwoBulletAI_range_Update_mA8397753A93C3CEFA4A548AAC454CD62BFD38130,
	EnemyTwoBulletAI_range_OnDrawGizmosSelected_m2E7668AFB401215059920BA368BE4F2D37230D3A,
	EnemyTwoBulletAI_range__ctor_m3E186310307EE4FC7172ED107F14E280771BCACA,
	HomingBullet_Start_m33BF1CF4FE0168633CAA95DFCC6639E7ECE22E0D,
	HomingBullet_Update_m33D482A89899A7A03357F2BB7872D2E593130AAB,
	HomingBullet__ctor_mFD1067B09D96CD656ECA0375F35AC3428F0DCCDA,
	LoseControlPlayer_range_Start_m2999B9A2F9D57664003EA553DFB672FEDB28822B,
	LoseControlPlayer_range_Update_m273A8E766F233E2394E5C8B2FD8427F45378A173,
	LoseControlPlayer_range_OnCollisionEnter2D_mF6C51617DA8079CFC3D4922CB1AA8C7FC67532E8,
	LoseControlPlayer_range_OnTriggerEnter2D_m643C8335950592E9E1D4212D31356C647E75F56F,
	LoseControlPlayer_range_OnCollisionStay2D_m2ACA69DB7B33473FF6D4BD7BDFD9BC2AA6180953,
	LoseControlPlayer_range_OnCollisionExit2D_m272ADB69681748347DCC146257022DA2313CC195,
	LoseControlPlayer_range__ctor_m8082D7DC4FE43207E8F4E431AB9A9ED3B806F63F,
	SlowPlayer_range_Start_m8AF6A780DD54263C01900ABD47900D6627F2A8E8,
	SlowPlayer_range_Update_mE35BED872010FE158FEA61E35E33145CF71526F4,
	SlowPlayer_range_OnCollisionEnter2D_mF8C1489067AE9859107A4B104AF765077D74CD56,
	SlowPlayer_range_OnTriggerEnter2D_mD2CD24FFD115D8ABED401D7FD4B8BCA9F286361F,
	SlowPlayer_range_OnCollisionStay2D_mD363D5DCD4F04608DB77FFB4468B040FD4AB9802,
	SlowPlayer_range_OnCollisionExit2D_m3DA04D01094CB4A212C5FBB9EAE6DFBF650334B8,
	SlowPlayer_range__ctor_mF26CD857FAC008A28CFE6314FCDE9F8BC21C4B8F,
	SoundWave_Start_m51D8AA75403FE9527EDBFCABE17A5223A368AF93,
	SoundWave_OnDestroy_mB418A919064A5AD57DEACA13D697F52605A26ED4,
	SoundWave__ctor_mB9885D46E39108F6DF53945D5586D6689E86FBB9,
	bossAI_range_hidden_Start_m2861FEC7C2D0EAFB8024C01FC47586794ECCEB38,
	bossAI_range_hidden_Update_m6A3CB9FDE704708ECED8CAE131AE02CC25C47FD9,
	bossAI_range_hidden_OnDrawGizmosSelected_m85037F12BC4FB4C1D033A563558ED7B70A1DD382,
	bossAI_range_hidden__ctor_mDA4A5F3885EDCDBBAA0201F7D72356B0E8F7ADAA,
	bossAI_range_twoButlet_Start_m9482F999E756CB4E8A5ADBCA98B766ABD9FED00C,
	bossAI_range_twoButlet_Update_m3C927564371364AA732AC7A8A7CC96265822518C,
	bossAI_range_twoButlet_OnDrawGizmosSelected_m1B50F7598D827BC1B389294545B85F7AD995FC46,
	bossAI_range_twoButlet__ctor_m001B5A763A616107B93550D1791712D34E6DAE70,
	HealthEnemy_Start_mC56EE0321F2612C61B3409959D05E3E1B16F437C,
	HealthEnemy_Update_mD1E2E578AAE274D60119B1ADDB6BAFA521F58E8F,
	HealthEnemy_HurtEnemy_m0F465E21702E5A442EA72404AE5E1735A2F04293,
	HealthEnemy__ctor_mE6B37AE46A34634ECC166910FAE5C57876AB437F,
	HurtPlayer_Start_m3E536B2A7022E93348AA1D6BD86391A39A1756C1,
	HurtPlayer_Update_m588D6701F4DEF4EF859909EEC305E89980D636A1,
	HurtPlayer_OnCollisionEnter2D_m515D5ABD142BF8F60E335E7E1CEE115A1C5F9F0D,
	HurtPlayer_OnCollisionStay2D_mF8B52F0923493B4D9B605CEE24ABC0D7BC93ED30,
	HurtPlayer_OnCollisionExit2D_mA47B649D48E6CED91DD3225D75CA194380E9FFDD,
	HurtPlayer__ctor_m80C2BE1434C282EDD0B8DD1DB8BD15C006C03DF3,
	HurtPlayer_Range_Start_m6790348704E3203EB834F40EC629B9B247961328,
	HurtPlayer_Range_Update_mFD7CAD7471BD4994060781005CD6723CF33C5C29,
	HurtPlayer_Range_OnCollisionEnter2D_mE0E5E8EE6863A18B4F5FCC4A626F8FBA36FD1D50,
	HurtPlayer_Range_OnTriggerEnter2D_mB7678D9CD6839DC522B85F521B5FC04A5BBD98FC,
	HurtPlayer_Range_OnCollisionStay2D_m34912BD32548E59537EAA9A85B8964E03E939634,
	HurtPlayer_Range_OnCollisionExit2D_mCC4604D7DAEE54F5CC3F6EE59C809901EEFC5769,
	HurtPlayer_Range__ctor_mDE23AFC6E81DEFAD77A20054055B85EC146F79C8,
	ShootingDummy_Start_m24DD5A03887561682FF485401D6D1DED65992D08,
	ShootingDummy_Update_mDD427DE79F8FAA1EB8BBD18B453D4DE422F2B310,
	ShootingDummy_OnDrawGizmosSelected_m954989824FC5D31EC8FFBA4A32BB885939ACB6F9,
	ShootingDummy__ctor_m62E7D85ED11CF13CAFCEBFDA247ABC55A201E6B5,
	bossAI_Start_m3454A1102162B0048F1A88D0E915FE88C1B2274F,
	bossAI_Update_mBE8FFB69B945D69745C2744680FC512F8D7AB695,
	bossAI_OnDrawGizmosSelected_m6D2BEE5C1AD67CEFADE8A5B95EF4EC0F885C9393,
	bossAI__ctor_m5EEDC084090F7A11DBCB9349D8C7947F2E712D90,
	bossAI_range_Start_m2469BAED2598B4A5A820ADCB448F4E3BF4802AFB,
	bossAI_range_Update_m0D33FEB3D3BD03CC237DC1498FBAF854AE2AFFD4,
	bossAI_range_OnDrawGizmosSelected_mFD6D96F800BDBB89696FAEFEB19E7C3615DE38D9,
	bossAI_range__ctor_mE6ECA6DB99ED4A5437AC96DF181C94FE7832D845,
	enemyAI_melee_Start_m1C5500CA548367191904FF649AEC669B992E270A,
	enemyAI_melee_Update_m4B2225D07E7B3C9AF26BF538FA07517609FBC76F,
	enemyAI_melee_OnDrawGizmosSelected_mD7FB6D542568B7A26EAECD13C9D2EFA2F513F669,
	enemyAI_melee__ctor_m0AB73B6F4923B50A82BF6FC88993FCD016C9FD4A,
	enemyAI_range_Start_m07F7770D4883E7D4BDBE0A752ECFBA692C6542B8,
	enemyAI_range_Update_m838820B0878B87F55273BA6302443DDE63896EB5,
	enemyAI_range_OnDrawGizmosSelected_m179DE2671200455333EC03A6C06D6BE96ECC518B,
	enemyAI_range__ctor_m1F03487BB326625996FAA02864069D30E55253AC,
	BossDontActiveForSaving_Start_m6CF571F25714A305F91949EE26843ED37416BC40,
	BossDontActiveForSaving_Update_mC0934663FEF8AC52C6DDD01A39219A58568893FE,
	BossDontActiveForSaving__ctor_mF4440D989398DA89DA3813E6978849F86515CC2C,
	BossUIManager_Start_m44E2034D966801C25F4B5990EFE47F398018D3BB,
	BossUIManager_Update_mC6C71B727BC9710A92961A16A6E0BB3DBC05DEE6,
	BossUIManager__ctor_m1035850BE20EA5D0DE2369BC2F612BF794F21FAD,
	Boss_SKEUI_Start_mF87154DBA2352EA1870C2C4FD3A7BFF79CB94B1A,
	Boss_SKEUI_Update_m2FE6C96A3456BDC36F67A676F8A17BAE0D3AF8FB,
	Boss_SKEUI__ctor_mA8FDED1B7EF57D350A17E988D0C268CF82013154,
	Boss_bear_UI_Start_mC653291D86DA0F2CAEEEFA1B1D7B17A20DE90F32,
	Boss_bear_UI_Update_m7ECA5BE8D8913DC0D771B3A34DD22126D9137020,
	Boss_bear_UI__ctor_m30A0F6ABC72E4DD232823CB4EF8AD2E8E5475F28,
	Boss_bee_UI_Start_m99288CDEFD4F8CFC253A61C58AEA8BA2B9AE29B4,
	Boss_bee_UI_Update_m8388990BA4B262DF1F2EDE7E470EB53B1445DE58,
	Boss_bee_UI__ctor_m50EA0D9C9C78E67AAA5FEEE18E7A7454B9D73626,
	Boss_guide_UI_Start_m946701A5648F4350297E152A7979C94C8211EBB6,
	Boss_guide_UI_Update_m38C1CDE034F7CA2052E03AF0B17DD3C3053E79FB,
	Boss_guide_UI__ctor_m4E0AC32384F4C2E0C53205780456A9FF7D945144,
	CameraFollow_Start_mC13DAB135378AADB3E72275F75CDEAF53343810D,
	CameraFollow_Update_m9762CC5B7A2D28B7B8116BA8DE2342AFB654AB08,
	CameraFollow__ctor_m29F88CCFD2ED12A7BCC75A9BBA892CEF179C83DE,
	DamageIndicator_Start_m4B11F0BBDA0E041B7CC3D21229D415AF5C8C4EF4,
	DamageIndicator_Update_m464572748BC80B633E7824A9866A9E47BE399184,
	DamageIndicator_SetDamageText_m96D6644016BA136E458B87C0FDB35B1BF77849F8,
	DamageIndicator_SetOutOfMoney_mA4301DCFA7B93C0DDCEA1FA21447732191509BEB,
	DamageIndicator_SetItemText_m6EF24042B6EE7A87EBF536E77BA53BFA81C1711E,
	DamageIndicator__ctor_m829527469E0E1B01175A96EDB17FDEB95236C962,
	Fullscreen_FullScreen_m0A8637F3844B0564F31FB4DABF216FBBF4784989,
	Fullscreen__ctor_mE9107247101E1586477FFB6CB8B9753BA8B81E2D,
	GameOverScreen_Start_mFDDF72E824C00B16DC7A6A96B73561A8D22D5C20,
	GameOverScreen_TimeIE_mB42A38029BD1334D965780125E80522703D0FA43,
	GameOverScreen_LoadLevel_mF10475F6D1A880CCB57A34ADA4C74540F6283C48,
	GameOverScreen_LoadAsynchronously_m2197BDDACA1C511BBDAF27424927DEFE650E54B1,
	GameOverScreen__ctor_mE9F89EF0E4A9BFE845256528F4FDE19F075BCD95,
	U3CTimeIEU3Ed__8__ctor_mAA3177335E8BFA01622B55BE8DCB2CEF5C18261E,
	U3CTimeIEU3Ed__8_System_IDisposable_Dispose_m090F29683C88433E5BA4F5FAE98DA1997CA167E5,
	U3CTimeIEU3Ed__8_MoveNext_mC92341834398526839EA956BF39A2D4C45C0B2BC,
	U3CTimeIEU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99EC495C6BCDAC6E1162941E07936462B1BD4E2D,
	U3CTimeIEU3Ed__8_System_Collections_IEnumerator_Reset_m9BE542B45B26F5C2D6EB34B44370BCCAB9D5D3E2,
	U3CTimeIEU3Ed__8_System_Collections_IEnumerator_get_Current_m0B2BFFB0D772E8E233ECDF8F858655AE9A4D03C1,
	U3CLoadAsynchronouslyU3Ed__10__ctor_mF01B26F4D4B06F59BA1A098B17A25DCFE6E42DE9,
	U3CLoadAsynchronouslyU3Ed__10_System_IDisposable_Dispose_m88D1C07D91A4DA9A053C84DE67353199F14E6047,
	U3CLoadAsynchronouslyU3Ed__10_MoveNext_mFC3CFBEC4CB3A0E13AA57F7B69D6BBD015DB21ED,
	U3CLoadAsynchronouslyU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF15B16A4766E1121E2355C87404FB6A1EF43AF3A,
	U3CLoadAsynchronouslyU3Ed__10_System_Collections_IEnumerator_Reset_m2EB3DC3D189F6976EFDB9B847CE615472B0F9BFA,
	U3CLoadAsynchronouslyU3Ed__10_System_Collections_IEnumerator_get_Current_mA40DA494789036083420705DAC23DAE8C5D80B08,
	GoldManager_Start_m4692B6B3F21C031390395FB32DF419B121FA811C,
	GoldManager_AddMoney_m71A523BAB56F95ADFFF5AF57377E254582B56139,
	GoldManager_MinusMoney_mD9A178E69C5CBE5309F3036C45DB29D65EA97153,
	GoldManager_OnTriggerEnter2D_m7ED56676F277AF6CCB2BD37449C2AD8CF2F26FA9,
	GoldManager__ctor_m64805587693429C5BEFE4EF2809CA47D6B077B3F,
	HealthIndicator_Start_m9EAB09AAF53A32BA4A225F473D24DEEC7E6FB606,
	HealthIndicator_Update_m24E78A0378EE18E8359E4A58B428455CD4243A12,
	HealthIndicator_SetHealText_m3C2B3E3F7FFA7682C32200F899A6E9B0BFC02368,
	HealthIndicator_SetBuffText_m825EB05118965278D6B5597EB1C959FB80A6BD83,
	HealthIndicator__ctor_mC88AF59253C4F2E5894D7BE1BFE1BD0010A2CBAE,
	LayerTrigger_OnTriggerExit2D_mCE8C8632080C77F62252C106B1B2EFAA3E0B0A27,
	LayerTrigger__ctor_mB75FEF1FD25B454626697F3FADF7B4CDAB9D958A,
	MenuinGame_PlayNewGame_mE9ACFDE3313A217536BF12CF315F14E36315FB38,
	MenuinGame_LoadGame_m8EDADC5B55DC445147A011FA5545124AE0205349,
	MenuinGame_QuitGame_m19DB364B9615563FE64C838B71B53F632FEB8B7D,
	MenuinGame__ctor_m4EDD7D23BD813E543CF032196D7AB8F4D17BD829,
	PauseMenuController_Start_mEAA2328A293796EE86216C908A280BEBB87D3F86,
	PauseMenuController_Update_m022C5DBBDBED4EACEFA4EED6C85CA3B7F138AE0C,
	PauseMenuController_Resume_mE338B9293CC4AC02F54C4A9FA198AA1D1063CA42,
	PauseMenuController_Pause_mE78040C6E68167806E87835C45764DAB28BC6EEA,
	PauseMenuController_SaveGame_m8D5C6B1A60998F0026F1CF2ED1167B88E4997CF0,
	PauseMenuController_QuitGame_m6BDEEF827F4B7E602B2E0C1A8933B89D56E565E4,
	PauseMenuController__ctor_m7D2060CB9E1BD7B17CF152ADF6D6E7D638959045,
	PauseMenuController__cctor_mF77CD11EC9CBE6469016D025C324C99FA0C792D4,
	PropsAltar_OnTriggerEnter2D_m7A84B17448AB31FEE3762C8C743DD18C5EF5883D,
	PropsAltar_OnTriggerExit2D_m36A8E5C39217D0DB51631F5F7ACC22DACA639E7D,
	PropsAltar_Update_m07C13405E8CF7859974FB0BFEECAC93EA6B4F9BE,
	PropsAltar__ctor_m83651FF438237F5D5E605B7B77B62E8D01F54340,
	Resolution_SetRes_mAEBF43C0177F6818A10C8C025070ECB8AFF825D2,
	Resolution__ctor_m36AF40279D2099B748847EC1E6C722B2CEAE4227,
	SaveManager_Start_m4A4409B7E949D33DFB06F3CF060ADC408AC6D0FD,
	SaveManager_SaveGame_m352C22B0491CCDE655040886EA0A4235B1DC3D32,
	SaveManager_LoadGame_mD87EB9193A13C6D8F8B155A737E1BDE09DD8E6E7,
	SaveManager_BoolToInt_m0616EA8A189C69B48693DFDDECFFED185ECD8E4D,
	SaveManager_IntToBool_m537DD6E8CB1E49F633C38FA1B636BB57E01FC108,
	SaveManager_SavingTime_m609D3CFA5870774AF4E7B15546B731113A8A19A8,
	SaveManager__ctor_mB7AB326D2EA77388E750706DEABFB056A6E1C356,
	U3CSavingTimeU3Ed__15__ctor_m2757447DB09BEDAA4D2FFF6BEB20DAAB8C5A9446,
	U3CSavingTimeU3Ed__15_System_IDisposable_Dispose_mF18CE4056508CE8E40BCFD44E8E2B6700C4E14BD,
	U3CSavingTimeU3Ed__15_MoveNext_m9FEC73BF2FC100C28C94A15592F101C9B2CEB596,
	U3CSavingTimeU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3B119E99743AFF41A4AB2C0507A755DE9F1E08FD,
	U3CSavingTimeU3Ed__15_System_Collections_IEnumerator_Reset_m7C522780FA216172DBA8E07C9890043D4F7C692C,
	U3CSavingTimeU3Ed__15_System_Collections_IEnumerator_get_Current_mF59823DEC376A77E23F5CECB85123B68B1E32572,
	Sound_Start_m085324B71DE4DD0B740C6F0A9EF90E0AB3FC4FA0,
	Sound_SetVolumn_mB786A246699AB6832E101DB8F4BC2648BE023717,
	Sound_SaveVolume_m161D650DB72FEC41E618658FAC299AA2568A1654,
	Sound_LoadVolume_m1D8E948965D2BEAADB21EF27E6F4927019FD2C87,
	Sound__ctor_mEA0B0D2FBD514F91C21900B0BB8679CD78843FCD,
	SpriteColorAnimation_Start_m75525D684C9D8BDBD34F20C108203573F58F1DA7,
	SpriteColorAnimation_Update_mD89814B67897A94B695A0336EF999A99780AB7C9,
	SpriteColorAnimation__ctor_m8CD8497F47547F4DA00017BCAD8314F7E4827031,
	UIManager_Start_mAA4B371DC406146E84A9D8803B9C861939B2E04E,
	UIManager_Update_m8A7C5DF1B797CFD6937FD6961AB9CC7B1A90D385,
	UIManager__ctor_mDADE1D724D40AF63AE78D51FC1CF1FE4784B4D4B,
	destroybytime_Start_m88239D9A3B9BD72749AB7B6204DF74A059810695,
	destroybytime_Update_m39762A214869A21A706B0D9B7A406C3F2C92C432,
	destroybytime__ctor_m127A12636BB61D9A8F208065D284B6BEF43FF266,
	endgame_OnTriggerEnter2D_mCBF62DF2DB4292653EDA663B01771DA29E016CED,
	endgame__ctor_m32D57D64522C224DC656BF90539F3A062244B668,
	nextlevel_Start_m2FF51529622A54FFFABEB2AA9F59D34B3276005E,
	nextlevel_OnTriggerEnter2D_m2F959292A779A41942DFE349C3B3BD840EAFCB6A,
	nextlevel__ctor_m9E4DC7468D850B648A9D664A416C3809DF79FF7E,
	nextlevel1_Start_mD831AED16AF7CE4AB274B34A976108BC83D1E39F,
	nextlevel1_OnTriggerEnter2D_m38562E21AB00C94892054E4C7C4BEC19D235A54E,
	nextlevel1__ctor_mF806F2543C6E1BAB616AE7DDBEA73B1527C31095,
	nextlevel2_Start_m165FAD11C25F90886150127B590E646EBA237CE1,
	nextlevel2_OnTriggerEnter2D_m3FD0282374584B3E9FA6B67646DD16A18A4E5AB3,
	nextlevel2__ctor_mD4DBA66D517B9BA814CC86CF3D7D230DE998A4C2,
	trapdoor_Start_m82E86048E787BA6E9BDC1A620CF325C18537D40D,
	trapdoor_OnTriggerEnter2D_m9BEE6AF23252FD0805E952B2DA801E10734EA5C6,
	trapdoor_OnTriggerExit2D_m42179F4FBE8C27CA96A5F63A729791050736DF80,
	trapdoor__ctor_m8A256D8933A7C4A59378FC11F35AFA56C2E9E8FF,
	trapdoor2_Start_mAF06127B1A209106F68023BE1F1E4FEEAA075129,
	trapdoor2_OnTriggerEnter2D_m45D302C592E1008E077E3DFC76D6F28B1343F3E3,
	trapdoor2_OnTriggerExit2D_mF0F6E1FDD018F7EF9F87D719F06A2FD4F397F14D,
	trapdoor2__ctor_m89272C1675297103C65A80134084C6E9F7851326,
	trapdoor3_Start_m1C7B29B99425B9F9B05A4FD0ABE5D3E74208943F,
	trapdoor3_OnTriggerEnter2D_m73D6A9DD3324945E1AE25694A90A28B98AEC3D38,
	trapdoor3_OnTriggerExit2D_m2A13C021D1EB5F47EB4BAF023801094F6619F629,
	trapdoor3__ctor_m128D16D4ED8A6381002E3B66ABE4A0FF3756AC71,
	trapdoor4_Start_m91D3391A3275ACEE82B1343CF4F849246A7EA5B0,
	trapdoor4_OnTriggerEnter2D_m53033080B9A52DDDF0931A4A142D2C5F39D0B833,
	trapdoor4_OnTriggerExit2D_mF1F11D2C559C22F9EFF067FD2963A816C737C8D3,
	trapdoor4__ctor_m3A0F898D908708F350DFB515B5CA3D6BA1DB7334,
	trapdoor5_Start_mA072AC38E5D5B9F5E05722B4A13F52153B939FFC,
	trapdoor5_OnTriggerEnter2D_m28A53C129A8A5953138FB51173BD99E04220F4F3,
	trapdoor5_OnTriggerExit2D_m16DFC6AA5AC6C4105386E8729C4F44E4E4A1F47E,
	trapdoor5__ctor_mCCD0956C986A7ED6D8C670C8D24D18DE14ED55EC,
	trapdoor6_Start_m0220D22F0574E5214EEEC842EF221FE9A167D213,
	trapdoor6_OnTriggerEnter2D_m10E68508156F36AEE0DFE70D0CF967AD2A1E8A05,
	trapdoor6_OnTriggerExit2D_m416C62AC516455C322F1B26CE1DFA6D6DE944C04,
	trapdoor6__ctor_m02193D396BCF545222DFA17205137CA5BB050B5B,
	activeHealPotion_start_mA47A951ECF2ADC8BFB5FCEC286A5259937649F13,
	activeHealPotion_Update_mB03B59D4E62021321551EA129D1D8583C42ED4FB,
	activeHealPotion_OnTriggerEnter2D_mAABDA7B1E56B96075BF44A82EFEC9E59826C0C32,
	activeHealPotion__ctor_m14B7068C8823B6D8F9C1EEAAF54A9361F50F73F2,
	checkHeal_Start_mF5B8F685B044F538A2D4E4F76793B18B55A0C070,
	checkHeal_FixedUpdate_m790FB663165D2E4D7982BDB961B15528162F3D72,
	checkHeal_Update_m756A8ACE68AB2A6B49BEEC74A5DEEF92C5CC999C,
	checkHeal_AddPotion_mEC089A57F74D7E0264E905F438AB1F03E13ADA26,
	checkHeal_deheal_mD2AF289909A90F1C2F2A2C55DCE3D6689026BBE8,
	checkHeal_OnTriggerEnter2D_mD2A7CA75E8D59099C4F20CDD866FE175114F0D68,
	checkHeal__ctor_mA4522316AE8E53C053AF7F5B72EB82F6ACDFAA2A,
	HealthPotion_start_mD8266AF543BF3DCCA17A30DF0412F0D93FB92C58,
	HealthPotion_FixedUpdate_m1052B52DAD81EB0EAD2A1C8D0F73042507312090,
	HealthPotion_Use_mAFB753AA93959E749FBFF52CE34FD60A0D79C0DD,
	HealthPotion__ctor_m095D5A9FDC4496398C56B388014E257CACC85BE6,
	Inventory__ctor_mF2ACBF005FF40F23F68AE8E9E416A4870EC4B27C,
	InventoryUI_Start_mBA9265B722C514BB1A663CF5D0726FC41388C979,
	InventoryUI_Update_m33C2131F4756C1EA0AEE90294EF276D0A657F519,
	InventoryUI__ctor_m5C090BC9C2888F79ECDCDD9669D37282DBE18748,
	Pickup_Start_m09D27020F3577A9A96B57A8873B8248828DC4EB1,
	Pickup_OnTriggerEnter2D_m394E601E8FC403BBAE3DD1C8C3F814C7FBB61180,
	Pickup__ctor_m6649ADEB2ABDE841407A83DC4682A210AFD8A93B,
	Slot_Start_mF57295D0D12CFAA6061C5617D6E2984B04000B19,
	Slot_Update_m863CC8F8F3CC50005C36EDF384467567C3CB170A,
	Slot_DropItem_m3C3163AB791E4574BA5405DFA58512B1079BA686,
	Slot_FixedUpdate_m2BF649D39BEE320B349E2A2A5095AD019625981D,
	Slot__ctor_m2F7B85EC24837E410CD89BEDE6FEB6797EBA801E,
	Spawn_Start_m99E70C46F85C6318276009783089BE4FEB2F3DCE,
	Spawn_SpawnItem_mAA811385E5B72F16A7C4D0A0F840E6918236CE7C,
	Spawn__ctor_m29DA62C57DCB06B5990FB843EE2F8533D4FCEC1F,
	AttackEnemy_OnTriggerEnter2D_mEA42161BCEAAD80466B2123CEBCCEE06A3F79F53,
	AttackEnemy__ctor_m530F570E7CF06FF5043AB858B6529E157280AFF6,
	AttackEnemy_Range_OnTriggerEnter2D_mC6AFE28A86CED28912C7E6D1B8C57F3206DF730F,
	AttackEnemy_Range__ctor_m8B9A07653032143BB23649D1AA28695D77C82C20,
	BowControl_Update_m5695D0A397B2CC3532E151F370A59846F44C406E,
	BowControl_AngleBetweenTwoPoints_m51B5B99A65D6D4E4E1920E41BC68740B5A0F7003,
	BowControl__ctor_m125746B485F1958307A879432A5FFE17120AD309,
	CharacterController_Awake_m64EE31F3FBB55CF083FD5EE8CA7FCF23C313C8D2,
	CharacterController_Start_m642795422FAB6FB862D6C5E11C6CF516A0C9F094,
	CharacterController_Update_m273ABBFABD6BDE21F06F2759BA66D9908D5B54EA,
	CharacterController_SlowPlayer_mBA4F0E5CBA246E00B05E559FB28FFF676C98EAAC,
	CharacterController_LoseControlPlayer_m75256A2E022B90101807B65D0EA10E4FE6FE80D9,
	CharacterController_FixedUpdate_m46AB7D2F16E9A249A3A3EDD3B9FBA5B0C4E6DF0E,
	CharacterController_Reload_m603D8E5810464FFAD8C80C20FA6C4E8ED61C4E38,
	CharacterController_WaitforDashCD_mD1C0B5349BC9E07C31AFEF439AE3FEDA192C1625,
	CharacterController_WaitforAttackCD_mB07CC382323E16A70F7EA4E06A2A61A9EA44B01D,
	CharacterController_WaitforSlow_m4306A08C07FBF80952E7999EA8FA6AADEDA79850,
	CharacterController_WaitforControl_m99E2CC6C84046A3D3153772CF58AB8080851B1E1,
	CharacterController_BuffArrow_mC7FA1EC7C66204556B68C0525087B65B92BAA86A,
	CharacterController_OnTriggerEnter2D_m8F1DA5AE8D70A76393B579573D313279F6061E6A,
	CharacterController_TimeBuff_m92A4C732E993B314804D911463AD777E99D0783C,
	CharacterController__ctor_m0F793EBA2EFFEA8CBCE9CDF61A3A1BB5EF89C5E5,
	U3CReloadU3Ed__38__ctor_mA2475D6EA0EECC4ED1C1DEDDF436609500668E43,
	U3CReloadU3Ed__38_System_IDisposable_Dispose_m189DF1F4181A8E7C4A6442EA9A8BFE394CC0EA1A,
	U3CReloadU3Ed__38_MoveNext_m81399917B8EB86B81CB35DF6F0A42CC91D707619,
	U3CReloadU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m817CD53DA8749F94A166EBDA976731072045001E,
	U3CReloadU3Ed__38_System_Collections_IEnumerator_Reset_m473199BCE668261B4718B1703F368D13D6D718D8,
	U3CReloadU3Ed__38_System_Collections_IEnumerator_get_Current_mB53EB3667351EDD280A4DC0377073DF5889818AC,
	U3CWaitforDashCDU3Ed__39__ctor_m373960E38BF24499999824042B6B18CC330FCDFB,
	U3CWaitforDashCDU3Ed__39_System_IDisposable_Dispose_mEAA348CAEDF9CFACC0549AEB129346432A5E5E57,
	U3CWaitforDashCDU3Ed__39_MoveNext_mC8E9D38DAF9BCE7E12ADEC586F61FC868E1850E2,
	U3CWaitforDashCDU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54214D324F2636F1476FD91F8DEF57A419313AC6,
	U3CWaitforDashCDU3Ed__39_System_Collections_IEnumerator_Reset_m0AA6D3E47AD5870D0DC147AAD6E30D775DB22655,
	U3CWaitforDashCDU3Ed__39_System_Collections_IEnumerator_get_Current_mE97F155EF767F943F9AEB8CA7FD0A3A83DE8F5FA,
	U3CWaitforAttackCDU3Ed__40__ctor_mDCD5FFEE4BA5AF5F59FDF9872D00947AEA244881,
	U3CWaitforAttackCDU3Ed__40_System_IDisposable_Dispose_m92D63B81EA8760D3EFDF4923517EB60B4668C27C,
	U3CWaitforAttackCDU3Ed__40_MoveNext_m817885042FD397CBA0AF3AD47E3CC198B6ACEB7E,
	U3CWaitforAttackCDU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D221862ED79DE7E719959FA79DF0F7C3FADBE37,
	U3CWaitforAttackCDU3Ed__40_System_Collections_IEnumerator_Reset_mC58183DFB9DC872EE95A3067AA0481D4B5704012,
	U3CWaitforAttackCDU3Ed__40_System_Collections_IEnumerator_get_Current_m047CBA26B024B9420C0A608271EE253FEF0379BF,
	U3CWaitforSlowU3Ed__41__ctor_m3A69FF99D4BA5414697B5DC9F6800C9F29BD0058,
	U3CWaitforSlowU3Ed__41_System_IDisposable_Dispose_mC215FD97FA1DCA7AF535F08F89A76462D2666C0E,
	U3CWaitforSlowU3Ed__41_MoveNext_m55494E31F2342B9407ED8543973F465C3E126F6E,
	U3CWaitforSlowU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m48935FB12DC00EC4301931A97F3BD21303DFF5EB,
	U3CWaitforSlowU3Ed__41_System_Collections_IEnumerator_Reset_mD63E6D9F2D797CCAB2BF6ABFB4F7F16AF9A80476,
	U3CWaitforSlowU3Ed__41_System_Collections_IEnumerator_get_Current_mE796B7B00AAF4F9E4193550E90652205D3386C6A,
	U3CWaitforControlU3Ed__42__ctor_mC555FCAC674AF43AA06D04B7A60FDE434485CC40,
	U3CWaitforControlU3Ed__42_System_IDisposable_Dispose_m339BD26FF42F0EFA072A57648700421AE65D59FE,
	U3CWaitforControlU3Ed__42_MoveNext_m82A56DA3A174D7FFD6727DBD6026DC839B8973AE,
	U3CWaitforControlU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55BE11D576B1F7839E3386CAB7EA471C71468E39,
	U3CWaitforControlU3Ed__42_System_Collections_IEnumerator_Reset_mB02A4E799DAE3A7464F8CC2965C7B4C8DD022AEC,
	U3CWaitforControlU3Ed__42_System_Collections_IEnumerator_get_Current_m45AD58E82837EA91D4BE2D8BCCBFAE55F9D980A4,
	U3CTimeBuffU3Ed__45__ctor_mD2E814F622252304651A2143045D1EDFF9C71040,
	U3CTimeBuffU3Ed__45_System_IDisposable_Dispose_mB60A2FE267BE49001CDDABA8D2C115FCB66578E2,
	U3CTimeBuffU3Ed__45_MoveNext_mF9CBFB1DF4FCC72D7DD650D582293D5601C8FEA2,
	U3CTimeBuffU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5A7519E0C87FC70F54936B9B2A057C6A5E8D0B79,
	U3CTimeBuffU3Ed__45_System_Collections_IEnumerator_Reset_mCD2C34CDD9B9C71AFE577EC290324C4B1B24CA93,
	U3CTimeBuffU3Ed__45_System_Collections_IEnumerator_get_Current_mE8B7421BCD2F31F4EB76A037245B241D6AAD16FD,
	FloatingNumber_Update_m424398E22F1CAD3BFC399AE657BC8B9BCC1A7FBC,
	FloatingNumber__ctor_m7CAD8E2D63060D777E329AD976A8F95F26577B8A,
	HealthPlayer_Start_m9555D7632FD0BB0312BEF01FA4A633A16E8D3B6D,
	HealthPlayer_Update_m79AE8AE7B35892F70BA94760A300CF03F08D8AE9,
	HealthPlayer_HurtPlayer_mA18BC4629F852A421683C43E3D588CF8BE2A9217,
	HealthPlayer_PlayerActiveOff_m3C8156E1BA703EECEBC135A24986704A788C0481,
	HealthPlayer_Heal_mAF039A2AFBE52F6258E37ED41D73C385B3AEFF63,
	HealthPlayer_HealPotion_m3209C904EA069B533139C04328FF904E50F1718A,
	HealthPlayer_OnTriggerEnter2D_m2BE253EB2257B56FEAC60ED7E186B1C1C0C88E38,
	HealthPlayer_FireBurningPlayer_m5F131C33AF964DDE8F005A10CD54BF4317FA1D25,
	HealthPlayer_ToxicBurningPlayer_m4A7C08A7F34B0B5F909108176B89BAC33E73CA6E,
	HealthPlayer_WaitforBurningTwiceTimes_m9B8D329D3F3ED2A06387ED52212B7329CCA72ED9,
	HealthPlayer_WaitforBurningManyTimes_m21FD33E08F5D2F6F6CA1F1A321B405E71DC855FC,
	HealthPlayer__ctor_m64FE6E1F7FBBEF21DF99578AD42F7F8B45804D11,
	U3CPlayerActiveOffU3Ed__17__ctor_m9A9DB2B04BFA578AC10BB1E9B790FE0BE9B8693F,
	U3CPlayerActiveOffU3Ed__17_System_IDisposable_Dispose_mCB84FD0D750104FB2CF907E87DD05F7AE8AB9C63,
	U3CPlayerActiveOffU3Ed__17_MoveNext_m0F0BC121AE61EB10C7D8CD061DE7B0D001C0417B,
	U3CPlayerActiveOffU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m317F910FAB3ED1F3E76932F9603EB1798D79D3C8,
	U3CPlayerActiveOffU3Ed__17_System_Collections_IEnumerator_Reset_mC1EAC213C3F0424F2DE7AF7D4F973287EE125DC6,
	U3CPlayerActiveOffU3Ed__17_System_Collections_IEnumerator_get_Current_mB0F263F4102A3C2C994B02E3F15FAA5F083BF625,
	U3CWaitforBurningTwiceTimesU3Ed__23__ctor_mF7EA250EF31784D08B73C5490C7851D93532DBC8,
	U3CWaitforBurningTwiceTimesU3Ed__23_System_IDisposable_Dispose_m1409B5C7D84CC7569BAE724C971E338953FB0426,
	U3CWaitforBurningTwiceTimesU3Ed__23_MoveNext_mE0A11613CC83367C6EF78FE4732362FAD51D3858,
	U3CWaitforBurningTwiceTimesU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF9557D405C2B28E0125F2748C6B3C41F5023B0E,
	U3CWaitforBurningTwiceTimesU3Ed__23_System_Collections_IEnumerator_Reset_m2050273B00A7E0222167D456BA00C5168F33162E,
	U3CWaitforBurningTwiceTimesU3Ed__23_System_Collections_IEnumerator_get_Current_m9815C990320876FBB5FDB0D542B3982C070CD026,
	U3CWaitforBurningManyTimesU3Ed__24__ctor_mBFAA5317CEA1FBA4BB4652CE2599F754749F50C1,
	U3CWaitforBurningManyTimesU3Ed__24_System_IDisposable_Dispose_m0389C895AF95C024112A1A53241A12DDD87CCFDA,
	U3CWaitforBurningManyTimesU3Ed__24_MoveNext_m374D6280463450C0B6EA5AA199CBC1397B5E1AC2,
	U3CWaitforBurningManyTimesU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09F3C5A63D58ADDAB507EAD2BA87F7F7CA197A9C,
	U3CWaitforBurningManyTimesU3Ed__24_System_Collections_IEnumerator_Reset_m5E755DF2A24BE189979B619CD9A68E99006AD458,
	U3CWaitforBurningManyTimesU3Ed__24_System_Collections_IEnumerator_get_Current_m56DAFC8768C4E1A1B229004A178F9DD5552DF281,
	Knockback_OnTriggerEnter2D_mF928BA361B80E1DE2D3146855D185F9F04B06D5C,
	Knockback_KnockCo_m895B2CCA6560515B4ECBD06F62DE1FD5DA034A25,
	Knockback__ctor_m2B8B481F28FC4A33133BD970C8F4E86F809DFD40,
	U3CKnockCoU3Ed__3__ctor_m13966A956093B208EA961EB6AF631B6749075141,
	U3CKnockCoU3Ed__3_System_IDisposable_Dispose_mD43C8AE1C391E9406E80789A9543D352B5825BC0,
	U3CKnockCoU3Ed__3_MoveNext_mB0B18000D70D1759D0BEDBF6E5E68AD808A62B7A,
	U3CKnockCoU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D198094D719D238ED5864BEDAB86C06F7144BEA,
	U3CKnockCoU3Ed__3_System_Collections_IEnumerator_Reset_mCCB2797D5B3C57E35C3864E861573DF29FC19101,
	U3CKnockCoU3Ed__3_System_Collections_IEnumerator_get_Current_mFCE1024C9949576CB9507452F41AFFC219D78739,
	arrowDestroy_Start_mD354873B30E04DB44ACFAF916E1756F285DD325A,
	arrowDestroy_Update_m28C88035832027F0DAAE40C5DED0B401410C97BF,
	arrowDestroy__ctor_mAADB5F0EF3DE6F666C87EAB09585B9F76CAEFED0,
	QuestComplted_Start_m27D1DE34B13DB40B73FFAF3FD8642B314073CBA8,
	QuestComplted_OnTriggerEnter2D_mAA77ED2069963A8D34BF91DC8C1087A6A17ADE55,
	QuestComplted__ctor_m82B2DA439B88EF4366F64E30C378805275BAA5AA,
	MainMenu_PlayGame_m96A3CE2743BCB00B665AA3AC575AE4EBD9ED40B0,
	MainMenu_QuitGame_m9F32E266C6F6CE345067D062258362159D267030,
	MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D,
	MoveBackground_Start_m73952C44ECDB8E02A7C84CE140363C3158096CD3,
	MoveBackground_Update_m233EF7D84EBF4ED003A6EC2F9942DD701BABDC2E,
	MoveBackground__ctor_mAF0741CCE7230F5038EA97EDD54679F1D20DF22D,
	ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2,
	ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B,
	ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371,
	ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026,
	DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6,
	DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9,
	EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93,
	EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE,
	EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC,
	U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B,
	U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF,
	checkpointController_Start_mBD9EF8C79AE04CBB39770690E3340C6DB0EBF46F,
	checkpointController_OnTriggerEnter2D_m9294C5151B9546B89E4389D802A64D5D8A2AA336,
	checkpointController__ctor_m5D77ED241F33CBAB053C007E75869D67259585FF,
	TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226,
	TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E,
	TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1,
	TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3,
	TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385,
	TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4,
	TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B,
	TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788,
	TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A,
	TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076,
	TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D,
	TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA,
	TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB,
	TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5,
	TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70,
	TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11,
	TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC,
	TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44,
	TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC,
	TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53,
	TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66,
	TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43,
	TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77,
	TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8,
	CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7,
	SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2,
	WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C,
	LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018,
	LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0,
	Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C,
	Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F,
	U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73,
	U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E,
	Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4,
	Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376,
	U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561,
	U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2,
	Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046,
	Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5,
	Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77,
	Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516,
	Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE,
	Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9,
	Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C,
	CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E,
	CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610,
	CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373,
	CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3,
	CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124,
	ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA,
	ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3,
	ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC,
	ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8,
	ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2,
	ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469,
	ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64,
	U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B,
	U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362,
	SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F,
	SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5,
	SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697,
	SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB,
	SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F,
	SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3,
	SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3,
	SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86,
	U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF,
	U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A,
	TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4,
	TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB,
	TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12,
	TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3,
	TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720,
	TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21,
	TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1,
	TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C,
	TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF,
	TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A,
	TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D,
	TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0,
	TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B,
	TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363,
	TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C,
	TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B,
	TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE,
	TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943,
	TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD,
	TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0,
	TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66,
	TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF,
	TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A,
	TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB,
	TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910,
	TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691,
	TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D,
	TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F,
	TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F,
	TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2,
	TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33,
	TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E,
	TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944,
	TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647,
	TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84,
	TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB,
	TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF,
	TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE,
	TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7,
	TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF,
	U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9,
	U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261,
	TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0,
	TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA,
	TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50,
	TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712,
	TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8,
	TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534,
	TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4,
	TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848,
	U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C,
	U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F,
	U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8,
	U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27,
	TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046,
	TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF,
	TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E,
	TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B,
	U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3,
	U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6,
	TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB,
	TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA,
	TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1,
	VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC,
	VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66,
	VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42,
	VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52,
	VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F,
	VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825,
	VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846,
	VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97,
	VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549,
	VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16,
	VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE,
	U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF,
	VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2,
	VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA,
	VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2,
	VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE,
	VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E,
	VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599,
	VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62,
	U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E,
	VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5,
	VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971,
	VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4,
	VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1,
	VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5,
	VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87,
	VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC,
	U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD,
	VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C,
	VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC,
	VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884,
	VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C,
	VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F,
	VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED,
	VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2,
	U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107,
	U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F,
	WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0,
	WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4,
	WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB,
	WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2,
	WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099,
	U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60,
	U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25,
};
static const int32_t s_InvokerIndices[1061] = 
{
	1538,
	1496,
	1538,
	1538,
	1496,
	1538,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1287,
	1287,
	1538,
	1538,
	1496,
	1538,
	1538,
	1496,
	1538,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1496,
	1538,
	1538,
	1496,
	1538,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1496,
	1538,
	1538,
	1496,
	1538,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1496,
	1538,
	1538,
	1496,
	1538,
	1276,
	999,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1287,
	1287,
	1538,
	1538,
	1538,
	1002,
	1538,
	1538,
	1287,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1002,
	1538,
	1538,
	1287,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1287,
	1287,
	1538,
	1538,
	1538,
	1002,
	1538,
	1538,
	1287,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1287,
	1287,
	1538,
	1538,
	1538,
	1002,
	1538,
	1538,
	1287,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1287,
	1287,
	1538,
	1538,
	1538,
	1002,
	1538,
	1538,
	1287,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1287,
	1287,
	1538,
	1538,
	1538,
	1002,
	1538,
	1538,
	1287,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1287,
	1287,
	1538,
	1538,
	1538,
	1002,
	1538,
	1538,
	1287,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1287,
	1287,
	1538,
	1538,
	1538,
	1002,
	1538,
	1538,
	1287,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1287,
	1287,
	1538,
	1538,
	1538,
	1002,
	1538,
	1538,
	1287,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1287,
	1287,
	1538,
	1538,
	1538,
	1002,
	1538,
	1538,
	1287,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1287,
	1287,
	1538,
	1538,
	1538,
	1002,
	1538,
	1538,
	1287,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1276,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1276,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1276,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1276,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1276,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	472,
	472,
	472,
	1538,
	472,
	472,
	472,
	1538,
	472,
	472,
	472,
	1538,
	472,
	472,
	472,
	1538,
	472,
	472,
	472,
	1538,
	472,
	472,
	472,
	1538,
	1538,
	1538,
	1276,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1276,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1287,
	1287,
	1287,
	1287,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1287,
	1287,
	1287,
	1287,
	1538,
	1538,
	1538,
	1287,
	1287,
	1287,
	1287,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1276,
	1538,
	1538,
	1538,
	1287,
	1287,
	1287,
	1538,
	1538,
	1538,
	1287,
	1287,
	1287,
	1287,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1276,
	1538,
	1287,
	1538,
	1304,
	1538,
	1538,
	1496,
	1276,
	999,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1276,
	1538,
	1287,
	1538,
	1538,
	1538,
	1276,
	1287,
	1538,
	1287,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	2425,
	1287,
	1287,
	1538,
	1538,
	1276,
	1538,
	1538,
	1538,
	1538,
	954,
	1100,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1287,
	1538,
	1538,
	1287,
	1538,
	1538,
	1287,
	1538,
	1538,
	1287,
	1538,
	1538,
	1287,
	1287,
	1538,
	1538,
	1287,
	1287,
	1538,
	1538,
	1287,
	1287,
	1538,
	1538,
	1287,
	1287,
	1538,
	1538,
	1287,
	1287,
	1538,
	1538,
	1287,
	1287,
	1538,
	1538,
	1538,
	1287,
	1538,
	1538,
	1538,
	1538,
	1276,
	1538,
	1287,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1287,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1287,
	1538,
	1287,
	1538,
	1538,
	636,
	1538,
	1538,
	1538,
	1538,
	1276,
	1538,
	1538,
	1496,
	1496,
	1496,
	999,
	1496,
	1538,
	1287,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1538,
	1538,
	1276,
	1496,
	1276,
	1276,
	1287,
	1276,
	1276,
	999,
	999,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1287,
	1002,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1538,
	1538,
	1287,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1287,
	1538,
	1538,
	1538,
	1538,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1287,
	1538,
	341,
	1538,
	341,
	1538,
	1496,
	1287,
	1496,
	1287,
	1496,
	1287,
	1496,
	1287,
	1496,
	1287,
	1538,
	1538,
	1287,
	1287,
	656,
	656,
	476,
	476,
	484,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1538,
	1538,
	1538,
	1002,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1538,
	1538,
	1538,
	1538,
	1276,
	1538,
	1538,
	1538,
	656,
	656,
	476,
	476,
	484,
	1538,
	1538,
	1538,
	1538,
	1287,
	1287,
	1538,
	1538,
	1538,
	1538,
	1287,
	1538,
	1287,
	1287,
	1287,
	1287,
	1276,
	1538,
	1538,
	1538,
	1538,
	1276,
	1538,
	1538,
	1276,
	1538,
	1538,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1538,
	1538,
	1287,
	1002,
	1002,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1496,
	1496,
	1538,
	2425,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1538,
	1538,
	1538,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1538,
	1538,
	1287,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1538,
	1538,
	1287,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1538,
	1538,
	1287,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1538,
	1538,
	1287,
	1496,
	1538,
	1538,
	525,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
	1538,
	1538,
	1002,
	1496,
	1538,
	1276,
	1538,
	1516,
	1496,
	1538,
	1496,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1061,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
